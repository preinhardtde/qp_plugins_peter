! -*- F90 -*-
        subroutine write_orb
          implicit none
          integer :: i,ioper,l,m,j

!         write(6,*) 
!         write(6,*) ' in detail: '
!         do i=1,ao_spher_num
!          write(6,9901) i,sym_ao_sphe_aotype(i),name_irreps(sym_ao_sphe_type(i))&
!               ,(sym_ao_sphe(ioper,i),ioper=1,nimages_ao_sphe(i))
9901       format(' symmetry-adapted AO No ',i4,' (',a4,') in IRREP ' &
                 ,a3,' is composed of ',8i6)
!         end do
!         write(6,*) ' that is all, writing the orbitals in '  &
!            ,'spherical harmonics to the file <VECTOR.SYM.sphe> '
!         write(6,*) '              writing the orbitals in '  &
!            ,'cartesian AOs to the file <VECTOR.SYM.cart_5d> '
!         write(6,*)
!         real*8 :: orbital(ao_spher_num)
!         open(unit=37,file='VECTOR.SYM.sphe',status='unknown' &
!           ,form='formatted')
!         open(unit=28,file='VECTOR.SYM.cart_5d',status='unknown' &
!           ,form='formatted')
!         do i=1,ao_spher_num
!          orbital=0.D0
!          do j=1,nimages_ao_sphe(i)
!           orbital(abs(sym_ao_sphe(j,i)))=sign(1.D0,dble(sym_ao_sphe(j,i)))
!          end do
!          write(6,*) ' writing SAO No ',i,' as ',sym_ao_sphe_type(i)
!          write(37,'(4E20.12)') (orbital(j),j=1,ao_spher_num)
!          write(28,'(4E20.12)') (ao_cart_to_sphesym_coef(j,i) &
!              ,j=1,ao_num)
!          do j=1,ao_num
!           if (abs(ao_cart_to_sphesym_coef(j,i)).gt.1.D-8) then
!            write(6,*) j,i,ao_cart_to_sphesym_coef(j,i)
!           end if
!          end do
!          write(37,*)
!          write(28,*)
!         end do

! the same in cartesian AOs
          write(6,*) '              writing the orbitals in '  &
             ,'cartesian AOs to the file <VECTOR.SYM.cart_6d> '
          write(6,*)
          do i=1,ao_num
!          write(6,9901) i,sym_ao_cart_aotype(abs(sym_ao_cart(1,i))),name_irreps(sym_ao_cart_type(i))&
!               ,(sym_ao_cart(ioper,i),ioper=1,nimages_ao_cart(i))
           write(6,9901) i,sym_ao_cart_aotype(i),name_irreps(sym_ao_cart_type(i))&
                ,(sym_ao_cart(ioper,i),ioper=1,nimages_ao_cart(i))
          end do
          open(unit=29,file='VECTOR.SYM.cart_6d',status='unknown' &
            ,form='formatted')
          do i=1,ao_num
           write(6,*) ' writing SAO No ',i,' as ',sym_ao_cart_type(i)
           write(29,'(4E20.12)') (ao_cart_to_sym_coef(j,i),j=1,ao_num)
           do j=1,ao_num
            if (abs(ao_cart_to_sym_coef(j,i)).gt.1.D-8) then
             write(6,*) j,i,ao_cart_to_sym_coef(j,i)
            end if
           end do
           write(29,*)
          end do
          close(37)
          close(28)
          close(29)

        end subroutine write_orb
