! -*- F90 -*-
 BEGIN_PROVIDER [integer, index_oper, (noper)]
&BEGIN_PROVIDER [character*3, name_irreps, (noper)]
      implicit none
      integer :: i
BEGIN_DOC 
! order of the groups
! C1, Ci, Cs, C2, C2h C2v D2 D2h
! order of operators
! E C2z C2y C2x i sigma_xy sigma_xz sigma_yz
!
! ordering after https://www.webqc.org
END_DOC
      index_oper=1
      if (group_index.eq.1) then
! C1
       name_irreps(1)='A  '
      else if (group_index.eq.2) then
! Ci
       index_oper(2)=5
       name_irreps(1)='Ag '
       name_irreps(2)='Au '
      else if (group_index.eq.3) then
! Cs
       index_oper(2)=6
       name_irreps(1)='A'' '
       name_irreps(2)='A'''''
      else if (group_index.eq.4) then
! C2
       index_oper(2)=2
       name_irreps(1)='A  '
       name_irreps(2)='B  '
      else if (group_index.eq.5) then
! C2h
       index_oper(2)=2
       index_oper(3)=5
       index_oper(4)=6
       name_irreps(1)='Ag '
       name_irreps(2)='Au '
       name_irreps(3)='Bg '
       name_irreps(4)='Bu '
      else if (group_index.eq.6) then
! C2v
       index_oper(2)=2
       index_oper(3)=7
       index_oper(4)=8
       name_irreps(1)='A1 '
       name_irreps(2)='A2 '
       name_irreps(3)='B1 '
       name_irreps(4)='B2 '
      else if (group_index.eq.7) then
! D2
       index_oper(2)=2
       index_oper(3)=3
       index_oper(4)=4
       name_irreps(1)='A1 '
       name_irreps(2)='B1 '
       name_irreps(3)='B2 '
       name_irreps(4)='B3 '
      else if (group_index.eq.8) then
! D2h
       do i=1,8
        index_oper(i)=i
       end do
       name_irreps(1)='Ag '
       name_irreps(2)='B1g'
       name_irreps(3)='B2g'
       name_irreps(4)='B3g'
       name_irreps(5)='Au '
       name_irreps(6)='B1u'
       name_irreps(7)='B2u'
       name_irreps(8)='B3u'
      end if
      END_PROVIDER

BEGIN_PROVIDER [integer, character_table, (noper,noper) ]
      implicit none
      character_table=1
      if (noper.eq.2) then
       character_table(2,2)=-1
      else if (noper.eq.4) then
       character_table(2,3)=-1
       character_table(2,4)=-1
       character_table(3,2)=-1
       character_table(3,4)=-1
       character_table(4,2)=-1
       character_table(4,3)=-1
      else if (noper.eq.8) then
       character_table(2,3)=-1
       character_table(2,4)=-1
       character_table(3,2)=-1
       character_table(3,4)=-1
       character_table(4,2)=-1
       character_table(4,3)=-1

       character_table(2,7)=-1
       character_table(2,8)=-1
       character_table(3,6)=-1
       character_table(3,8)=-1
       character_table(4,6)=-1
       character_table(4,7)=-1

       character_table(6,3)=-1
       character_table(6,4)=-1
       character_table(7,2)=-1
       character_table(7,4)=-1
       character_table(8,2)=-1
       character_table(8,3)=-1

       character_table(5,5)=-1
       character_table(5,6)=-1
       character_table(5,7)=-1
       character_table(5,8)=-1
       character_table(6,5)=-1
       character_table(6,6)=-1
       character_table(7,5)=-1
       character_table(7,7)=-1
       character_table(8,5)=-1
       character_table(8,8)=-1
      end if
END_PROVIDER

BEGIN_PROVIDER [integer, group_multiplication_table, (noper,noper)]
      implicit none
      integer :: i,j

      group_multiplication_table=1

      do i=2,noper
       group_multiplication_table(1,i)=i
       group_multiplication_table(i,1)=i
      end do

      if (noper.ge.4) then
       group_multiplication_table(2,3)=4
       group_multiplication_table(2,4)=3
       group_multiplication_table(3,4)=2
       do i=1,4
        do j=i+1,4
         group_multiplication_table(j,i)=group_multiplication_table(i,j)
        end do
       end do
       if (noper.eq.8) then
        do i=1,4
         do j=1,4
          group_multiplication_table(i  ,j+4)=group_multiplication_table(i,j)+4
          group_multiplication_table(i+4,j  )=group_multiplication_table(i,j)+4
          group_multiplication_table(i+4,j+4)=group_multiplication_table(i,j)
         end do
        end do
       end if
      end if
END_PROVIDER

BEGIN_PROVIDER [character*8, name_oper, (8)]
       name_oper(1)='   E    '
       name_oper(2)='  C2z   '
       name_oper(3)='  C2y   '
       name_oper(4)='  C2x   '
       name_oper(5)='   i    '
       name_oper(6)='sigma_xy'
       name_oper(7)='sigma_xz'
       name_oper(8)='sigma_yz'
END_PROVIDER

BEGIN_PROVIDER [character*3, group_name, (8)]
        implicit none
        group_name(1)='C1 '
        group_name(2)='Ci '
        group_name(3)='Cs '
        group_name(4)='C2 '
        group_name(5)='C2h'
        group_name(6)='C2v'
        group_name(7)='D2 '
        group_name(8)='D2h'
END_PROVIDER

BEGIN_PROVIDER [integer, group_index]
        implicit none

!       write(6,*) ' noper, nrot, nsigma = ',noper,nrot,nsigma
        if (noper.eq.1) then
         group_index=1
        else if (noper.eq.2) then
         if (l_oper(5)) then
          group_index=2
         else
         if (nrot.eq.1) then
          group_index=4
         else
          group_index=3
         end if
        end if
        else if (noper.eq.4) then
         if (l_oper(5)) then
          group_index=5
         else
          if (nrot.eq.1) then
           group_index=6
          else
           group_index=7
          end if
         end if
        else if (noper.eq.8) then
          group_index=8
        else
         write(6,*) ' could not determine symmetry '
         stop
        end if

END_PROVIDER

BEGIN_PROVIDER [integer, char_contrib, (0:5,8)]
        char_contrib(0,1)=1
        implicit none
        integer :: l,s

        s=1
        do l=0,5
! E
         char_contrib(l,1)=2*l+1
! C2z, C2y, C2z
         char_contrib(l,2)= s
         char_contrib(l,3)= s
         char_contrib(l,4)= s
! i
        char_contrib(l,5)= s*(2*l+1)
! sigma xy, xz, yz
        char_contrib(l,6)= 1
        char_contrib(l,7)= 1
        char_contrib(l,8)= 1

        s=-s
       end do
END_PROVIDER

BEGIN_PROVIDER [real*8, diag_rotmat, (3,8)]
BEGIN_DOC
! the diagonal of the matrices associated to the operators
END_DOC
        diag_rotmat=1.D0
! C2z
        diag_rotmat(1,2)=-1.D0
        diag_rotmat(2,2)=-1.D0
! C2y
        diag_rotmat(1,3)=-1.D0
        diag_rotmat(3,3)=-1.D0
! C2x
        diag_rotmat(2,4)=-1.D0
        diag_rotmat(3,4)=-1.D0
! i
        diag_rotmat(1,5)=-1.D0
        diag_rotmat(2,5)=-1.D0
        diag_rotmat(3,5)=-1.D0
! sigma_xy
        diag_rotmat(3,6)=-1.D0
! sigma_xz
        diag_rotmat(2,7)=-1.D0
! sigma_yz
        diag_rotmat(1,8)=-1.D0
END_PROVIDER

BEGIN_PROVIDER [integer, operaction_cart, (0:5,0:5,0:5,8)]
BEGIN_DOC
! action of the symmetry operators on cartesian orbitals at the center
END_DOC
     implicit none
     integer :: xpow,ypow,zpow
     operaction_cart=1
     do xpow=0,5
      do ypow=0,5
       do zpow=0,5
        if (mod(xpow+ypow,2).eq.1) operaction_cart(xpow,ypow,zpow,2)=-1
        if (mod(xpow+zpow,2).eq.1) operaction_cart(xpow,ypow,zpow,3)=-1
        if (mod(ypow+zpow,2).eq.1) operaction_cart(xpow,ypow,zpow,4)=-1
        if (mod(xpow+ypow+zpow,2).eq.1) operaction_cart(xpow,ypow,zpow,5)=-1
        if (mod(zpow,2).eq.1) operaction_cart(xpow,ypow,zpow,6)=-1
        if (mod(ypow,2).eq.1) operaction_cart(xpow,ypow,zpow,7)=-1
        if (mod(xpow,2).eq.1) operaction_cart(xpow,ypow,zpow,8)=-1
       end do
      end do
     end do
END_PROVIDER
          
BEGIN_PROVIDER [integer, operaction, (-5:5,0:5,8)]
BEGIN_DOC
! action of the symmetry operators on spherical orbitals at the center
END_DOC
        implicit none
        integer :: ioper,l,m
        operaction=0
        do ioper=1,8
         do l=0,5
          do m=-l,l
           operaction(m,l,ioper)=1
          end do
         end do
        end do
! we have to find only where to put -1
! C2z, x -> -x, y-> -y
        do l=1,5
         do m=1,l,2
          operaction( m,l,2)=-1
          operaction(-m,l,2)=-1
         end do
        end do
! C2y, x -> -x, z-> -z
        do l=1,5
         if (mod(l,2).eq.0) then
          do m=1,l
           operaction(-m,l,3)=-1
          end do
         else
          do m=0,l
           operaction( m,l,3)=-1
          end do
         end if
        end do
! C2x, y -> -y, z-> -z
        do l=1,5
         do m=l-1,0,-2
          operaction( m,l,4)=-1
          operaction(-(m+1),l,4)=-1
         end do
        end do
! i, x -> -x, y -> -y, z-> -z
        do l=1,5,2
         do m=-l,l
          operaction(m,l,5)=-1
         end do
        end do
! sigma_xy, z -> -z        
          do l=1,5,2
           do m=0,l,2
            operaction( m,l,6)=-1
            operaction(-m,l,6)=-1
           end do
          end do
          do l=2,5,2
           do m=1,l,2
            operaction( m,l,6)=-1
            operaction(-m,l,6)=-1
           end do
          end do
! sigma_xz, y -> -y        
        do l=1,5
         do m=1,l
          operaction(-m,l,7)=-1
         end do
        end do
! sigma_yz, x -> -x        
        do l=1,5
         do m=1,l,2
          operaction( m,l,8)=-1
         end do
         do m=2,l,2
          operaction(-m,l,8)=-1
         end do
        end do
END_PROVIDER

BEGIN_PROVIDER [character*5, ao_symbol_cart, (0:5,0:5,0:5)]
        implicit none
        integer :: i,j,k,l,ii,jj,kk,ll,length
        character*1 :: xs,ys,zs,bs
        xs='x'
        ys='y'
        zs='z'
        bs=' '
        ao_symbol_cart=''
        do i=0,5
         do j=0,5-i
          do k=0,5-i-j
           length=0
           do ii=1,i
            ao_symbol_cart(i,j,k)=ao_symbol_cart(i,j,k)(1:length)//xs
            length+=1
           end do
           do jj=1,j
            ao_symbol_cart(i,j,k)=ao_symbol_cart(i,j,k)(1:length)//ys
            length+=1
           end do
           do kk=1,k
            ao_symbol_cart(i,j,k)=ao_symbol_cart(i,j,k)(1:length)//zs
            length+=1
           end do
           do l=1,5-i-j-k
            ao_symbol_cart(i,j,k)=ao_symbol_cart(i,j,k)(1:length)//bs
            length+=1
           end do
          end do
         end do
        end do
        ao_symbol_cart(0,0,0)='s    '
END_PROVIDER
   
BEGIN_PROVIDER [character*4, ao_symbol, (-5:5,0:5)]
          ao_symbol(0,0)='s   '

          ao_symbol(-1,1)='py  '
          ao_symbol( 0,1)='pz  '
          ao_symbol( 1,1)='px  '

          ao_symbol(-2,2)='dxy '
          ao_symbol(-1,2)='dyz '
          ao_symbol( 0,2)='dz2 '
          ao_symbol( 1,2)='dxz '
          ao_symbol( 2,2)='dx2 '
! complement r2 like s
! 1 more function

          ao_symbol(-3,3)='fyyy'
          ao_symbol(-2,3)='fxyz'
          ao_symbol(-1,3)='fyzz'
          ao_symbol( 0,3)='fzzz'
          ao_symbol( 1,3)='fxzz'
          ao_symbol( 2,3)='fxxz'
          ao_symbol( 3,3)='fxxx'
! complements xr2, yr2, zr2 like p
! 3 more functions

          ao_symbol(-4,4)='gxy3'
          ao_symbol(-3,4)='gy3z'
          ao_symbol(-2,4)='gxyZ'
          ao_symbol(-1,4)='gyz3'
          ao_symbol( 0,4)='gz4 '
          ao_symbol( 1,4)='gxz3'
          ao_symbol( 2,4)='gy4 '
          ao_symbol( 3,4)='gx3z'
          ao_symbol( 4,4)='gx4 '
! complements (xy,xz,yz,xx,yy,zz) times r2
! 6 more functions

          ao_symbol(-5,5)='y5  '
          ao_symbol(-4,5)='x3yz'
          ao_symbol(-3,5)='y3z2'
          ao_symbol(-2,5)='xyz3'
          ao_symbol(-1,5)='yz4 '
          ao_symbol( 0,5)='z5  '
          ao_symbol( 1,5)='xz4 '
          ao_symbol( 2,5)='y2z3'
          ao_symbol( 3,5)='xy4 '
          ao_symbol( 4,5)='y4z '
          ao_symbol( 5,5)='x3y2'
! complements (xxx,xxy,xxz,xyy,xyz,xzz,yyy,yyz,yzz,zzzz) times r2
! 10 more functions

          write(6,*)
          write(6,*) ' Shorthand notation:  '
          if (ao_l_max.ge.2) then
           write(6,*) ' d_0 = dz2 = (3z2-r2) '
           write(6,*) ' d_2 = dx2 = (x2-y2) '
           if (ao_l_max.ge.3) then
            write(6,*)
            write(6,*) ' f-3 = fyyy = yyy-3xxy '
            write(6,*) ' f-2 = fxyz '
            write(6,*) ' f-1 = fyzz = 4yzz-yyy-xxy '
            write(6,*) ' f0  = fzzz = 2zzz-3yyz-3xxz '
            write(6,*) ' f1  = fxzz = 4xzz-xyy-xxx '
            write(6,*) ' f1  = fyyz = xxz-yyz '
            write(6,*) ' f3  = fxxx = xxx-3xyy '
            if (ao_l_max.ge.4) then
             write(6,*)
             write(6,*) '  gxy3 = xy(xx-yy)'
             write(6,*) '  gy3z = yz(3xx-yy)'
             write(6,*) '  gxyZ = xy(6zz-xx-yy)'
             write(6,*) '  gyz3 = yz(4zz-3xx-3yy)'
             write(6,*) '  gz4  = zz(zz-3xx-3yy)+0.375(xx+yy)(xx+yy)'
             write(6,*) '  gxz3 = xz(4zz-3yy-3xx)'
             write(6,*) '  gy4  = 6(xx-yy)zz+yyyy-xxxx'
             write(6,*) '  gx3z = xxxz-3xyyz'
             write(6,*) '  gx4  = yyyy+xxxx-6xxyy'
             if (ao_l_max.ge.5) then
              write(6,*)
              write(6,*) '  y5   = y(yyyy-10xxyy+5xxxx)  '
              write(6,*) '  x3yz = xyz(xx-yy)  '
              write(6,*) '  y3z2 = -8yyyzz+yyyyy+24xxyzz-2xxyyy-2xxxxy  '
              write(6,*) '  xyz3 = xyz(2zz-xx-yy)  '
              write(6,*) '  yz4  = 8yzzzz-12yyyzz+yyyyy-12xxyzz+2xxyyy+xxxxy  '
              write(6,*) '  z5   = 8zzzzz+40(xx-yy)zzz+15yyyyz+30xxyyz+15xxxxz'
              write(6,*) '  xz4  = 8xzzzz-12xyyzz+xyyyy-12xxxzz+2xxxyy+xxxxx '
              write(6,*) '  y2z3 = 2(xx-yy)zzz-(xxxx-yyyy)z '
              write(6,*) '  xy4  = -24xyyzz+3xyyyy+8xxxzz+2xxxyy-xxxxx  '
              write(6,*) '  y4z  = yyyyz-6xxyyz+xxxxz '
              write(6,*) '  x3y2 = x(5yyyy-10xxyy+xxxx) '
             end if
            end if
           end if
          end if
          write(6,*)

END_PROVIDER

