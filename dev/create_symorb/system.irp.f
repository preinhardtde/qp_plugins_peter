! -*- F90 -*-
 BEGIN_PROVIDER [ integer, ao_spher_num  ]
&BEGIN_PROVIDER [ integer, nshell_tot ]
&BEGIN_PROVIDER [ integer, nshell, (nucl_num) ]
&BEGIN_PROVIDER [ integer, ao_spher_num_atom, (nucl_num) ]

BEGIN_DOC
! the quantum package does not know about shells, only AOs
! are used
! We reconstruct the information
END_DOC

   implicit none
   integer :: i,l
   
   nshell_tot=0
   ao_spher_num=0
   ao_spher_num_atom=0
   nshell=0
   
   i=1
   do while (i.le.ao_num)
    l=ao_l(i)
    write(6,*) ' i = ',i,' has a l of ',l
    if (l.gt.5) then
     write(6,*) ' not yet operational beyond h functions '
     stop
    end if
    nshell(ao_nucl(i))+=1
    nshell_tot+=1
    ao_spher_num+=(2*l+1)
    ao_spher_num_atom(ao_nucl(i))+=2*l+1
    i+=(l*l+3*l+2)/2
   end do

   if (ao_spher_num.ne.ao_cart_to_sphe_num) then
write(6,*) ' ao_spher_num = ',ao_spher_num
write(6,*) ' ao_cart_to_sphe_num = ',ao_cart_to_sphe_num
      stop 'error in cart-sphe '
     end if

!  write(6,*) ' nshell_tot         = ',nshell_tot
!  write(6,*) ' ao_spher_num       = ',ao_spher_num
!  write(6,*) ' ao_spher_num_atom  = ',ao_spher_num_atom
!  write(6,*) ' nshell             = ',nshell
          
END_PROVIDER

 BEGIN_PROVIDER [ integer, indx_bas_start, (nshell_tot) ]
&BEGIN_PROVIDER [ integer, lvalue, (nshell_tot) ]
&BEGIN_PROVIDER [ integer, lvalue_spher_ao, (ao_spher_num) ]
&BEGIN_PROVIDER [ integer, mvalue_spher_ao, (ao_spher_num) ]
&BEGIN_PROVIDER [ integer, ibas_atom, (ao_spher_num) ]
&BEGIN_PROVIDER [ integer, ibas_cart_atom, (ao_num)]
&BEGIN_PROVIDER [ integer, ibas_start_atom, (nucl_num) ]
&BEGIN_PROVIDER [ integer, ibas_start_atom_cart, (nucl_num) ]
&BEGIN_PROVIDER [ integer, ao_spher_nucl, (ao_spher_num) ]

      implicit none
      integer :: i,l,ishl,ibas,indx_atom,indx_bas_atom,m,ii,indx
      
      indx_bas_start=0
      lvalue=0
      lvalue_spher_ao=0
      mvalue_spher_ao=0
      ibas_atom=0
      ibas_cart_atom=0
      ibas_start_atom=0
      ibas_start_atom_cart=0
! count in cartesian AOs
      ibas_start_atom_cart(1)=1
      indx=0
      indx_atom=1
      do i=1,ao_num
       if (ao_nucl(i).ne.indx_atom) then
        indx_atom=ao_nucl(i)
        ibas_start_atom_cart(indx_atom)=i
        indx=0
       end if
       indx+=1
       ibas_cart_atom(i)=indx
      end do
    
      i=1
      ishl=1
      ibas=1
      indx_atom=ao_nucl(1)
      indx_bas_atom=1
      ibas_start_atom(1)=1
      do while (i.lt.ao_num)
       indx_bas_start(ishl)=ibas
       if (indx_atom.ne.ao_nucl(i)) then
        indx_atom=ao_nucl(i)
        ibas_start_atom(indx_atom)=ibas
        indx_bas_atom=1
       end if

! ordering of the m values for the quantum package

       l=ao_l(i)
       lvalue(ishl)=l
       mvalue_spher_ao(ibas)=0
       lvalue_spher_ao(ibas)=l
       ao_spher_nucl(ibas)=indx_atom
       ibas_atom(ibas)=indx_bas_atom
       ibas+=1
       indx_bas_atom+=1
       do m=1,l
        mvalue_spher_ao(ibas)=m
        lvalue_spher_ao(ibas)=l
        ao_spher_nucl(ibas)=indx_atom
        ibas_atom(ibas)=indx_bas_atom
        ibas+=1
        indx_bas_atom+=1

        mvalue_spher_ao(ibas)=-m
        lvalue_spher_ao(ibas)=l
        ao_spher_nucl(ibas)=indx_atom
        ibas_atom(ibas)=indx_bas_atom
        ibas+=1
        indx_bas_atom+=1

       end do
       ishl+=1
       i+=(l*l+3*l+2)/2
      end do

!     write(6,*) 'indx_bas_start  = ',indx_bas_start 
!     write(6,*) 'lvalue          = ',lvalue         
!     write(6,*) 'lvalue_spher_ao = ',lvalue_spher_ao
!     write(6,*) 'mvalue_spher_ao = ',mvalue_spher_ao
!     write(6,*) 'ibas_atom       = ',ibas_atom      
!     write(6,*) 'ibas_start_atom = ',ibas_start_atom
!     write(6,*) 'ao_spher_nucl   = ',ao_spher_nucl
      
!     write(6,*)
!     write(6,*) '  detailled information on basis functions '
!     write(6,*) ' basis function       l       m  '
!     write(6,*)
!     do ii=1,ao_spher_num
!      l= lvalue_spher_ao(ii)
!      m= mvalue_spher_ao(ii)
!      write(6,9904) ii,ao_symbol(m,l),l,m
!     end do
9904  format(10X,i4,8X,A4,4X,2i3)
     
END_PROVIDER


