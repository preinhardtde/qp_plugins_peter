! -*- F90 -*-
      subroutine solve_lin(mat,b,n,ndim)
        implicit none
        real*8 :: mat(ndim,ndim),b(ndim)
        integer :: n,ndim
BEGIN_DOC
! solves the linear equation b + A.x = 0
! the solution vector comes back in b
END_DOC
        integer :: i,info,j

!        write(6,*) ' dimension of the problem : ',n
!        write(6,*) '    RHS                           Matrix'
!        do i=1,n
!         write(6,9901) b(i),(mat(j,i),j=1,n)
!        end do
9901    format(F15.6,10X,5(F15.5))
!        write(6,*) 

        do i=1,n
         b(i)=-b(i)
        end do
! solve A.x = b
        call dgesv(n,1,mat,ndim,ipiv_peter,b,n,info)
      end subroutine solve_lin

BEGIN_PROVIDER [integer, ipiv_peter, (noper) ]
END_PROVIDER
        
