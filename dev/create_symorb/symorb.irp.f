! -*- F90 -*-
 BEGIN_PROVIDER[integer, norb_sym_spher, (noper)]
BEGIN_DOC
! in Abelian groups we should have as many irreps as we have operators
! we know which atom goes to which other atom und a symmetry operation,
! so we can find out which orbital goes to which other orbital.
! The operations exchange atoms as we only have C2, sigma and i.
! Corresponding orbitals are only +/- combinations of 2 orbitals.
! rotations: s have 1, p have -1, d have 1, f have -1 etc, thus (-1)^l
! sigma: s have 1, p perpendicular -1, p parallel +1 etc.
! i: s +1, p -1, d +1 etc
!
! we may as well construct the (2l+1) dimensional matrices for each operation
! C2x.(px, py, pz)_1 = (px,-py,-pz)_2 etc 
!
! we may set up the global character table:
! under E we have just ao_spher_num
! under C2x we have   s-shell:+1, p-shell:-1, d-shell:+1, guess f:-1
! under i we have     s-shell:+1, p-shell:-3, d-shell:+5, guess f:-7
! under sigma we have s-shell:+1, p-shell:+1, d-shell:+1, guess f:+1
! only un-moved atoms count
!
END_DOC
        implicit none
        integer :: isym,iat,ish,char_total(8),indx_shell,j,i
        real*8 :: solution(noper)

        do i=1,noper
         do j=1,noper
          mat_tmp(i,j)=character_table(j,i)
         end do
        end do
        
! s orbitals
        integer :: l
        do l=0,ao_l_max

         do i=1,noper
          do j=1,noper
           mat_tmp(i,j)=character_table(j,i)
          end do
         end do
        
         write(6,*)
         write(6,*) ' global character of ',trim(l_to_character(l)),' orbitals '
         char_total=0
         do isym=1,noper
          indx_shell=0
          do iat=1,nucl_num
           do ish=1,nshell(iat)
            indx_shell+=1
            if (unmoved_atom(isym,iat).and.lvalue(indx_shell).eq.l) then
             char_total(isym)+=char_contrib(lvalue(indx_shell),index_oper(isym))
            end if
           end do
          end do
         end do

         do i=1,noper
          solution(i)=-dble(char_total(i))
         end do
         call solve_lin(mat_tmp,solution,noper,noper)
         
         write(6,9901) (name_oper(index_oper(isym)),isym=1,noper)
         write(6,9902) (char_total(isym),isym=1,noper)
         write(6,9903) (nint(solution(isym)),name_irreps(isym),isym=1,noper)
         write(6,*)
        end do
        
        do i=1,noper
         do j=1,noper
          mat_tmp(i,j)=character_table(j,i)
         end do
        end do
        
! whole basis set
        char_total(1)=ao_spher_num
        do isym=2,noper
         char_total(isym)=0
         indx_shell=0
         do iat=1,nucl_num
          do ish=1,nshell(iat)
           indx_shell+=1
           if (unmoved_atom(isym,iat)) then
            char_total(isym)+=char_contrib(lvalue(indx_shell),index_oper(isym))
           end if
          end do
         end do
        end do
        do i=1,noper
         solution(i)=-dble(char_total(i))
        end do
        call solve_lin(mat_tmp,solution,noper,noper)
        
        write(6,*)
        write(6,*) ' global character of the complete basis set '
        write(6,9901) (name_oper(index_oper(isym)),isym=1,noper)
9901    format(' operations     ',8(2x,A8))
9902    format(' total character',8(3h   ,i3,4h    ))
9903    format(' decomposition: ',i3,1x,A3,7(' +',i3,1x,A3))
        write(6,9902) (char_total(isym),isym=1,noper)
        write(6,9903) (nint(solution(isym)),name_irreps(isym),isym=1,noper)
        write(6,*)

        do isym=1,noper
         norb_sym_spher(isym)=nint(solution(isym))
        end do
               
END_PROVIDER

 BEGIN_PROVIDER[integer, norb_sym_cart, (noper)]
BEGIN_DOC
! in Abelian groups we should have as many irreps as we have operators
! we know which atom goes to which other atom und a symmetry operation,
! so we can find out which orbital goes to which other orbital.
! The operations exchange atoms as we only have C2, sigma and i.
! Corresponding orbitals are only +/- combinations of 2 orbitals.
! rotations: s have 1, p have -1, d have 1, f have -1 etc, thus (-1)^l
! sigma: s have 1, p perpendicular -1, p parallel +1 etc.
! i: s +1, p -1, d +1 etc
!
! we may as well construct the (2l+1) dimensional matrices for each operation
! C2x.(px, py, pz)_1 = (px,-py,-pz)_2 etc 
!
! we may set up the global character table:
! under E we have just ao_spher_num
! under C2x we have   s-shell:+1, p-shell:-1, d-shell:+1, guess f:-1
! under i we have     s-shell:+1, p-shell:-3, d-shell:+5, guess f:-7
! under sigma we have s-shell:+1, p-shell:+1, d-shell:+1, guess f:+1
! only un-moved atoms count
!
END_DOC
        implicit none
        integer :: isym,iat,ish,char_total(8),indx_shell,j,i
        real*8 :: solution(noper)

        do i=1,noper
         do j=1,noper
          mat_tmp(i,j)=character_table(j,i)
         end do
        end do

! we do not know of l and m anymore in cartesian orbitals
! we only know of AOs
! s orbitals
        do i=1,noper
         do j=1,noper
          mat_tmp(i,j)=character_table(j,i)
         end do
        end do
        
        char_total=0
        do isym=1,noper
         do i=1,ao_num
          iat=ao_nucl(i)
          if (unmoved_atom(isym,iat)) then
           char_total(isym)+=operaction_cart(ao_power(i,1)  &
                ,ao_power(i,2),ao_power(i,3),index_oper(isym))
          end if
         end do
        end do

        do i=1,noper
         solution(i)=-dble(char_total(i))
        end do
        call solve_lin(mat_tmp,solution,noper,noper)
         
        write(6,9901) (name_oper(index_oper(isym)),isym=1,noper)
        write(6,9902) (char_total(isym),isym=1,noper)
        write(6,9903) (nint(solution(isym)),name_irreps(isym),isym=1,noper)
        write(6,*)
        
        write(6,*)
        write(6,*) ' global character of the complete basis set '
        write(6,9901) (name_oper(index_oper(isym)),isym=1,noper)
9901    format(' operations     ',8(2x,A8))
9902    format(' total character',8(3h   ,i3,4h    ))
9903    format(' decomposition: ',i3,1x,A3,7(' +',i3,1x,A3))
        write(6,9902) (char_total(isym),isym=1,noper)
        write(6,9903) (nint(solution(isym)),name_irreps(isym),isym=1,noper)
        write(6,*)

        do isym=1,noper
         norb_sym_cart(isym)=nint(solution(isym))
        end do
               
END_PROVIDER

 BEGIN_PROVIDER [logical, unmoved_atom, (noper,nucl_num)]
&BEGIN_PROVIDER [integer, nimages_atom, (nucl_num) ]
&BEGIN_PROVIDER [integer, image_atom, (noper,nucl_num)]
        implicit none
         integer :: iat, ioper,i,jat
         real*8 :: posrot(3)
         real*8 :: dd

         nimages_atom=0
         image_atom=0
         write(6,*) 
         do ioper=1,noper
          do iat=1,nucl_num
           unmoved_atom(ioper,iat)=.true.
           do i=1,3
            posrot(i)=diag_rotmat(i,index_oper(ioper))*nucl_coord_sym(i,iat)
            if (abs(posrot(i)-nucl_coord_sym(i,iat)).gt.1.D-5)  unmoved_atom(ioper,iat)=.false.
           end do
!          write(6,*) ' rotation ',ioper,iat,(nucl_coord_sym(i,iat),i=1,3),posrot,unmoved_atom(ioper,iat)
           if (unmoved_atom(ioper,iat)) then
            image_atom(ioper,iat)=iat
           else
            do jat=1,nucl_num
             dd=0.D0
             dd+=(posrot(1)-nucl_coord_sym(1,jat))*(posrot(1)-nucl_coord_sym(1,jat))
             dd+=(posrot(2)-nucl_coord_sym(2,jat))*(posrot(2)-nucl_coord_sym(2,jat))
             dd+=(posrot(3)-nucl_coord_sym(3,jat))*(posrot(3)-nucl_coord_sym(3,jat))
             if (sqrt(dd).lt.1.D-5) image_atom(ioper,iat)=jat
            end do
           end if
! did we have this already ?
           logical :: lalready
           lalready=.false.
           do i=1,ioper-1
            if (image_atom(ioper,iat).eq.image_atom(i,iat)) lalready=.true.
           end do
           if (.not.lalready) then
            nimages_atom(iat)+=1
           end if
!           write(6,*) ' image of ',iat,' under ',name_oper(index_oper(ioper)),' is ',image_atom(ioper,iat)
!           write(6,*) ' unmoved ',iat,ioper,unmoved_atom(ioper,iat)
          end do
          write(6,*)
         end do

!        write(6,'(21x,8(a8,2x))') (name_oper(index_oper(ioper)),ioper=1,noper) 
!        do iat=1,nucl_num
!         write(6,'(i4,10h Images of,i3,5h are ,8(i3,7x))') nimages_atom(iat),iat,(image_atom(ioper,iat),ioper=1,noper)
!        end do
         write(6,*)
         write(6,*) ' symmetry reduction: '
         do iat=1,nucl_num
          if (nimages_atom(iat).gt.1) then
           do ioper=2,noper
            if (image_atom(ioper,iat).ne.iat) nimages_atom(image_atom(ioper,iat))=0
           end do
          end if
         end do

         write(6,'(21x,8(a8,2x))') (name_oper(index_oper(ioper)),ioper=1,noper) 
         do iat=1,nucl_num
          if (nimages_atom(iat).ge.1) write(6,'(i4,10h Images of,i3,5h are ,8(i3,7x))') nimages_atom(iat) &
               ,iat,(image_atom(ioper,iat),ioper=1,noper)
         end do
END_PROVIDER

 BEGIN_PROVIDER [integer, nimages_ao_sphe, (ao_spher_num) ]
&BEGIN_PROVIDER [integer, image_ao_sphe, (noper,ao_spher_num)]
&BEGIN_PROVIDER [integer, sym_ao_sphe, (noper,ao_spher_num)]
&BEGIN_PROVIDER [integer, sym_ao_sphe_type, (ao_spher_num)]
&BEGIN_PROVIDER [integer, sym_ao_sphe_indx, (ao_spher_num)]
&BEGIN_PROVIDER [character*4, sym_ao_sphe_aotype, (ao_spher_num)]
         implicit none
         integer :: i,iat,ioper,jat
         integer :: ibas,l,m,joper
         integer :: symorb_i(noper),symorb_j(noper),jsign
         integer :: char_line(noper),irrep
         logical :: lfound
         integer :: isymorb
         write(6,*)
! devise an algorithm:
! determine how many images are present for a given atom.
! a theorem: the + combination is in the same irrep as the orbital on the unmoved atom.
! another one: if moved the linear combination of all images is in the first IRREP
!        write(6,*) ibas_start_atom

!        write(6,*) ' basis functions -> irreps '

         isymorb=0
         
         do i=1,ao_spher_num
          iat=ao_spher_nucl(i)
          ibas=ibas_atom(i)
          m=mvalue_spher_ao(i)
          l=lvalue_spher_ao(i)
!!$          write(6,*) ' AO No ',i,' (l,m) = ',l,m
!!$          write(6,*) (operaction(m,l,ioper),ioper=1,8)
          do ioper=1,noper
           char_line(ioper)=operaction(m,l,index_oper(ioper))
          end do
!          write(6,*) char_line
          lfound=.false.
          irrep=0
          do while (.not.lfound)
           irrep+=1
           if (irrep.eq.noper+1) then
            stop ' ??? '
           end if
!           write(6,*) ' irrep= ',irrep
           lfound=.true.
           do ioper=1,noper
            if (character_table(irrep,ioper).ne.char_line(ioper))  lfound=.false.
           end do
          end do
!          write(6,*) ' irrep= ',irrep
          if (nimages_atom(ao_spher_nucl(i)).eq.1) then
!          write(6,*) ' AO No ',i,' (',ao_symbol(m,l),') on atom No ',ao_spher_nucl(i) &
!               ,' goes to IRREP No ',irrep,' i.e. ',name_irreps(irrep)
           isymorb+=1
           sym_ao_sphe(1,isymorb)=i
           sym_ao_sphe_type(isymorb)=irrep
           sym_ao_sphe_aotype(isymorb)=ao_symbol(m,l)
           nimages_ao_sphe(isymorb)=1
          else if  (nimages_atom(ao_spher_nucl(i)).gt.1) then
!          write(6,*) ' AO No ',i,' (',ao_symbol(m,l),') on atom No ',ao_spher_nucl(i) &
!               ,' contributes to ',nimages_atom(ao_spher_nucl(i)),' IRREPs'
! we have to determine the nimages_atom possible linear combinations
!           write(6,*) ' the combination ',(sstring(2),ibas_start_atom(image_atom(ioper,jat))+jbas,ioper=1,noper) &
!                ,' is in ',name_irreps(irrep)
           integer :: koper,signarray(noper)
           logical :: nogood
           do koper=0,2**(noper-1)-1
            integer :: nsigns
            call extract_number_of_bits1(koper,nsigns,signarray)
            if (nsigns.eq.0.or.nsigns.eq.noper/2) then
!!$             do ioper=noper/2+1,noper
!!$              signarray(ioper)=-1
!!$             end do
! we try to distribute the - signs, we have noper/2
             do ioper=1,noper
!            symorb_i(ioper)=operaction(m,l,ioper)*(ibas_start_atom(image_atom(ioper,iat))+ibas)
              symorb_i(ioper)=signarray(ioper)*(ibas_start_atom(image_atom(ioper,iat))+ibas-1)
             end do
! let us act with an operator on the linear combination
             nogood=.false.
             do joper=1,noper
              do ioper=1,noper
               jat=image_atom(ioper,iat)
               jsign=sign(1,symorb_i(ioper))
               symorb_j(ioper)=jsign*(ibas_start_atom(image_atom(joper,jat))+ibas-1)*operaction(m,l,index_oper(joper))
              end do
              integer :: val
              call comp_symorb(symorb_i,symorb_j,val)
!              write(6,*) ' symorb_j = ',symorb_j,' -> ',val
              char_line(joper)=val
              if (val.eq.0) nogood=.true.
             end do
             if (.not.nogood) then
              lfound=.false.
              irrep=0
              do while (.not.lfound)
               irrep+=1
               lfound=.true.
               do ioper=1,noper
                if (character_table(irrep,ioper).ne.char_line(ioper))  lfound=.false.
               end do
              end do
! eliminate orbitals occurring more than once
              nimages_ao_sphe(i)=noper
              do ioper=1,noper
               if (symorb_i(ioper).ne.0) then
                do joper=ioper+1,noper
                 if (symorb_i(ioper).eq.symorb_i(joper)) then
                  nimages_ao_sphe(i)-=1
                  symorb_i(joper)=0
                 end if
                end do
               end if
              end do
              joper=0
              do ioper=1,noper
               if (symorb_i(ioper).ne.0) then
                joper+=1
                symorb_i(joper)=symorb_i(ioper)
                if (ioper.ne.joper) symorb_i(ioper)=0
               end if
              end do
!             write(6,*) ' the combination ',(symorb_i(ioper),ioper=1,nimages_ao(i)),' is in ',name_irreps(irrep)
              isymorb+=1
              sym_ao_sphe_type(isymorb)=irrep
              sym_ao_sphe_aotype(isymorb)=ao_symbol(m,l)
              nimages_ao_sphe(isymorb)=nimages_ao_sphe(i)
              do ioper=1,nimages_ao_sphe(i)
               sym_ao_sphe(ioper,isymorb)=symorb_i(ioper)
              end do
             end if
            end if
           end do
           
          end if
         end do
!
! sort symorbs wrt IRREPs
!
   integer :: idum,j
   character*4 :: cdum
         do i=1,ao_spher_num-1
          do j=i+1,ao_spher_num
           if (sym_ao_sphe_type(i).gt.sym_ao_sphe_type(j)) then
            idum=sym_ao_sphe_type(i)
            sym_ao_sphe_type(i)=sym_ao_sphe_type(j)
            sym_ao_sphe_type(j)=idum
            cdum=sym_ao_sphe_aotype(i)
            sym_ao_sphe_aotype(i)=sym_ao_sphe_aotype(j)
            sym_ao_sphe_aotype(j)=cdum
            idum=nimages_ao_sphe(i)
            nimages_ao_sphe(i)=nimages_ao_sphe(j)
            nimages_ao_sphe(j)=idum
            do ioper=1,noper
             idum=sym_ao_sphe(ioper,i)
             sym_ao_sphe(ioper,i)=sym_ao_sphe(ioper,j)
             sym_ao_sphe(ioper,j)=idum
            end do
           end if
          end do
         end do

         integer :: indx,iblock
         indx=0
         iblock=0
         do i=1,ao_spher_num
          if (iblock.ne.sym_ao_sphe_type(i)) then
           indx=1
           iblock=sym_ao_sphe_type(i)
          end if
          sym_ao_sphe_indx(i)=indx
          write(6,'(i8,3x,A3,3x,A4)') indx,name_irreps(iblock),sym_ao_sphe_aotype(i)
          indx+=1
         end do         
          
END_PROVIDER

BEGIN_PROVIDER [real*8, ao_cart_to_sphesym_coef, (ao_num,ao_num)]
    implicit none
    integer :: i,j,indx,isig,ii,jj
    real*8 :: orb_norm

    ao_cart_to_sphesym_coef=0.D0
    do i=1,ao_cart_to_sphe_num
     do j=1,nimages_ao_sphe(i)
      indx=abs(sym_ao_sphe(j,i))
      isig=sign(1,sym_ao_sphe(j,i))
!     write(6,*) isig,indx,' -> ',i
      do ii=1,ao_num
       ao_cart_to_sphesym_coef(ii,i)+=isig*ao_cart_to_sphe_coef(ii,indx)
      end do
     end do
     orb_norm=0.D0
     do ii=1,ao_num
      do jj=1,ao_num
       orb_norm+=ao_cart_to_sphesym_coef(ii,i) &
            *ao_cart_to_sphesym_coef(jj,i)*ao_overlap(ii,jj)
      end do
     end do
     do ii=1,ao_num
      ao_cart_to_sphesym_coef(ii,i)*=1.D0/sqrt(orb_norm)
     end do
    end do

END_PROVIDER

 BEGIN_PROVIDER [integer, nimages_ao_cart, (ao_num) ]
&BEGIN_PROVIDER [integer, sym_ao_cart, (noper,ao_num)]
&BEGIN_PROVIDER [integer, sym_ao_cart_inv, (noper,ao_num)]
&BEGIN_PROVIDER [integer, nused_ao_cart, (ao_num)]
&BEGIN_PROVIDER [integer, sym_ao_cart_type, (ao_num)]
&BEGIN_PROVIDER [integer, sym_ao_cart_indx, (ao_num)]
&BEGIN_PROVIDER [integer, block_offset_cart, (noper)]
&BEGIN_PROVIDER [character*5, sym_ao_cart_aotype, (ao_num)]
         implicit none
         integer :: i,iat,ioper,jat
         integer :: ibas,l,m,joper
         integer :: symorb_i(noper),symorb_j(noper),jsign
         integer :: char_line(noper),irrep
         logical :: lfound
         integer :: isymorb
         write(6,*)
BEGIN_DOC
! devise an algorithm:
! determine how many images are present for a given atom.
! a theorem: the + combination is in the same irrep as the orbital on the unmoved atom.
! another one: if moved the linear combination of all images is in the first IRREP
!  nimages_ao_cart - number of images 
!  sym_ao_cart        AOs in which the symmetry-adapted orbital is expanded
!  sym_ao_cart_inv   symmerty-adapted AOS to which an AO contributes
!  nused_ao_cart     number of symmmetry AOS to which an AO contributes
!  sym_ao_cart_type  irrep of a symmetry AO
!  sym_ao_cart_indx  index of the symmetry AO within an IRREP
!  sym_ao_cart_aotype    name of the underlying AO
END_DOC
!        write(6,*) ibas_start_atom

!        write(6,*) ' basis functions -> irreps '

         isymorb=0
         sym_ao_cart=0
         
         do i=1,ao_num
          iat=ao_nucl(i)
          ibas=ibas_cart_atom(i)
          do ioper=1,noper
           char_line(ioper)=operaction_cart(ao_power(i,1)  &
                 ,ao_power(i,2),ao_power(i,3),index_oper(ioper))
          end do
!          write(6,*) ' cart: ',i,iat,ibas,char_line
          lfound=.false.
          irrep=0
          do while (.not.lfound)
           irrep+=1
           if (irrep.eq.noper+1) then
            stop ' ??? '
           end if
           lfound=.true.
           do ioper=1,noper
            if (character_table(irrep,ioper).ne.char_line(ioper))  lfound=.false.
           end do
          end do
          if (nimages_atom(ao_nucl(i)).eq.1) then
           isymorb+=1
           sym_ao_cart(1,isymorb)=i
           sym_ao_cart_type(isymorb)=irrep
           sym_ao_cart_aotype(isymorb)=ao_symbol_cart(ao_power(i,1),ao_power(i,2),ao_power(i,3))
           nimages_ao_cart(isymorb)=1
!           write(6,*) ' i, isymorb,  sym_ao_cart_type(isymorb)= ',i, isymorb,  sym_ao_cart_aotype(isymorb),irrep,name_irreps(irrep)
          else if  (nimages_atom(ao_nucl(i)).gt.1) then
!           write(6,*) ' AO No ',i,' (',ao_symbol_cart(ao_power(i,1),ao_power(i,2),ao_power(i,3)),') on atom No ',ao_nucl(i) &
!                ,' contributes to ',nimages_atom(ao_nucl(i)),' IRREPs'
! we have to determine the nimages_atom possible linear combinations
           integer :: koper,signarray(noper)
           logical :: nogood
           do koper=0,2**(noper-1)-1
            integer :: nsigns
            call extract_number_of_bits1(koper,nsigns,signarray)
            if (nsigns.eq.0.or.nsigns.eq.noper/2) then
! we try to distribute the - signs, we have noper/2
             do ioper=1,noper
              symorb_i(ioper)=signarray(ioper)*(ibas_start_atom_cart(image_atom(ioper,iat))+ibas-1)
             end do
!             write(6,*) ' ibas, ibas_start_atom_cart = ',ibas, ibas_start_atom_cart
! let us act with an operator on the linear combination
             nogood=.false.
             do joper=1,noper
              do ioper=1,noper
               jat=image_atom(ioper,iat)
               jsign=sign(1,symorb_i(ioper))
               symorb_j(ioper)=jsign*(ibas_start_atom_cart(image_atom(joper,jat))+ibas-1)   &
                   *operaction_cart(ao_power(i,1),ao_power(i,2),ao_power(i,3),index_oper(joper))
              end do
              integer :: val
              call comp_symorb(symorb_i,symorb_j,val)
!              if (val.ne.0)  write(6,*) ' symorb_j = ',symorb_j,' -> ',val
              char_line(joper)=val
              if (val.eq.0) nogood=.true.
             end do
             if (.not.nogood) then
              lfound=.false.
              irrep=0
              do while (.not.lfound)
               irrep+=1
               lfound=.true.
               do ioper=1,noper
                if (character_table(irrep,ioper).ne.char_line(ioper))  lfound=.false.
               end do
              end do
! eliminate orbitals occurring more than once
!              write(6,*) ' i, symorb_i = ',i, symorb_i
              nimages_ao_cart(i)=noper
              do ioper=1,noper
               if (symorb_i(ioper).ne.0) then
                do joper=ioper+1,noper
                 if (symorb_i(ioper).eq.symorb_i(joper)) then
                  nimages_ao_cart(i)-=1
                  symorb_i(joper)=0
                 end if
                end do
               end if
              end do
              joper=0
              do ioper=1,noper
               if (symorb_i(ioper).ne.0) then
                joper+=1
                symorb_i(joper)=symorb_i(ioper)
                if (ioper.ne.joper) symorb_i(ioper)=0
               end if
              end do
!              write(6,*) ' i, symorb_i = ',i, symorb_i
!              write(6,*) ' the combination ',(symorb_i(ioper),ioper=1,nimages_ao_cart(i)),' is in ',name_irreps(irrep)
!              write(6,*)
              isymorb+=1
              sym_ao_cart_type(isymorb)=irrep
              sym_ao_cart_aotype(isymorb)=ao_symbol_cart(ao_power(i,1),ao_power(i,2),ao_power(i,3))
              nimages_ao_cart(isymorb)=nimages_ao_cart(i)
              do ioper=1,nimages_ao_cart(i)
               sym_ao_cart(ioper,isymorb)=symorb_i(ioper)
              end do
             end if
            end if
           end do
           
          end if
         end do

         do ioper=1,noper
          indx=0
          do i=1,ao_num
           if (sym_ao_cart_type(i).eq.ioper) then
            indx+=1
           end if
          end do
 !         write(6,*) ' IRREP No ',ioper,indx,norb_sym_cart(ioper)
          if (indx.ne.norb_sym_cart(ioper)) then
           write(6,*) ' Houston, we have a problem '
           stop
          end if
         end do
         
!
! sort symorbs wrt IRREPs
!
   integer :: idum,j
   character*4 :: cdum
         do i=1,ao_num-1
          do j=i+1,ao_num
           if (sym_ao_cart_type(i).gt.sym_ao_cart_type(j)) then
            idum=sym_ao_cart_type(i)
            sym_ao_cart_type(i)=sym_ao_cart_type(j)
            sym_ao_cart_type(j)=idum
            cdum=sym_ao_cart_aotype(i)
            sym_ao_cart_aotype(i)=sym_ao_cart_aotype(j)
            sym_ao_cart_aotype(j)=cdum
            idum=nimages_ao_cart(i)
            nimages_ao_cart(i)=nimages_ao_cart(j)
            nimages_ao_cart(j)=idum
            do ioper=1,noper
             idum=sym_ao_cart(ioper,i)
             sym_ao_cart(ioper,i)=sym_ao_cart(ioper,j)
             sym_ao_cart(ioper,j)=idum
            end do
           end if
          end do
         end do
!
! to which symmetry AO an AO  contributes
!
integer :: ii,iii
         write(6,*)
         sym_ao_cart_inv=0
         nused_ao_cart=0
         do i=1,ao_num
          do ii=1,nimages_ao_cart(i)
           iii=abs(sym_ao_cart(ii,i))
           nused_ao_cart(iii)+=1
           sym_ao_cart_inv(nused_ao_cart(iii),iii)=i
          end do
         end do
         do i=1,ao_num
          write(6,*) ' AO No ',i,' used for symmetry AOs ',(sym_ao_cart_inv(ii,i),ii=1,nused_ao_cart(i))
         end do
         write(6,*)

         integer :: indx,iblock,re
         indx=0
         iblock=0
         do i=1,ao_num
!          write(6,*) ' i,iblock, sym_ao_cart_type(i) = ',i,iblock, sym_ao_cart_type(i)
          if (iblock.ne.sym_ao_cart_type(i)) then
           indx=1
           iblock=sym_ao_cart_type(i)
   integer :: jblock
           do jblock=iblock,noper
            block_offset_cart(jblock)=i-1
           end do
!           write(6,*) iblock, block_offset_cart(iblock)
          end if
          sym_ao_cart_indx(i)=indx
!          write(6,'(2i8,3x,A3,3x,A4)') i,indx,name_irreps(iblock),sym_ao_cart_aotype(i)
          indx+=1
         end do         

END_PROVIDER

BEGIN_PROVIDER [real*8, ao_cart_to_sym_coef, (ao_num,ao_num)]
    implicit none
    integer :: i,j,indx,isig,ii,jj
    real*8 :: orb_norm

    ao_cart_to_sym_coef=0.D0
    do i=1,ao_num
     do j=1,nimages_ao_cart(i)
      indx=abs(sym_ao_cart(j,i))
      isig=sign(1,sym_ao_cart(j,i))
!     write(6,*) isig,indx,' -> ',i
      ao_cart_to_sym_coef(indx,i)+=isig
     end do
     orb_norm=0.D0
     do ii=1,ao_num
      do jj=1,ao_num
       orb_norm+=ao_cart_to_sym_coef(ii,i) &
            *ao_cart_to_sym_coef(jj,i)*ao_overlap(ii,jj)
      end do
     end do
     do ii=1,ao_num
      ao_cart_to_sym_coef(ii,i)*=1.D0/sqrt(orb_norm)
     end do
    end do

integer :: ioper
        open(unit=12,file='SYMORB_cart.packed',form='formatted',status='unknown')
        open(unit=13,file='SYMORB_cart.less_packed',form='formatted',status='unknown')
        do i=1,ao_num
         write(12,'(i4,E20.12,8i7)') nimages_ao_cart(i)     &
          ,ao_cart_to_sym_coef(max(1,abs(sym_ao_cart(1,i))),i)     &
          ,(sym_ao_cart(ioper,i),ioper=1,noper)
         write(13,'(i4,8(i7,E20.11))') nimages_ao_cart(i),(abs(sym_ao_cart(ioper,i))   &
                  ,ao_cart_to_sym_coef(max(1,abs(sym_ao_cart(ioper,i))),i),ioper=1,noper)
        end do
        close(12)
        close(13)
        write(6,*) ' wrote packed symmetry-adapted orbitals to <SYMORB_cart.packed>'
        write(6,*) ' wrote less packed symmetry-adapted orbitals to <SYMORB_cart.less_packed>'

        call ezfio_set_create_symorb_symorb_table_write(sym_ao_cart)
        call ezfio_set_create_symorb_symorb_coef_write(ao_cart_to_sym_coef)
          
END_PROVIDER

subroutine comp_symorb(symorb_i,symorb_j,val)
  implicit none
  integer :: symorb_i(noper),symorb_j(noper),val,i,j
!  write(6,*) '   symorb_i, symorb_j ',symorb_i, symorb_j 
  val=2
  do i=1,noper
   do j=1,noper
    if (abs(symorb_i(i)).eq.abs(symorb_j(j))) then
     if (val.eq.2) then
      val=symorb_i(i)/symorb_j(j)
     else
      if (val.ne.symorb_i(i)/symorb_j(j)) then
       val=0
       return
      end if
     end if
    end if
   end do
  end do
  
end subroutine comp_symorb

 BEGIN_PROVIDER [real*8, mat_tmp, (noper,noper)]
&BEGIN_PROVIDER [real*8, vec_tmp, (noper)]
END_PROVIDER


subroutine extract_number_of_bits1(n,nsigns,signarray)
  implicit none
  integer :: n,nsigns,signarray(noper),i
  nsigns=0
  signarray=1
  do i=0,noper-1
   if (btest(n,i)) then
    signarray(i+1)=-1
    nsigns+=1
   end if
  end do
!  write(6,*) n,nsigns,signarray
  
end subroutine extract_number_of_bits1

