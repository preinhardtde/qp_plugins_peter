! -*- F90 -*-
 BEGIN_PROVIDER [real*8, elec_inertia_tensor, (3,3)]
&BEGIN_PROVIDER [real*8, elec_inertia_eigvec, (3,3)]
&BEGIN_PROVIDER [real*8, elec_inertia_eigval, (3)]
    implicit none
    integer :: iat,i,m,mtot,matz,ierr,j
    real*8 :: t1(3),t2(3)
    elec_inertia_tensor=0.D0
    do iat=1,nucl_num
     m=nint(nucl_charge(iat))
     do i=1,3
      do j=1,3
       if (i.ne.j) then 
        elec_inertia_tensor(i,j)+=m*nucl_coord_trans(i,iat)*nucl_coord_trans(j,iat)
        elec_inertia_tensor(i,i)+=m*nucl_coord_trans(j,iat)*nucl_coord_trans(j,iat)
       end if
      end do
     end do
    end do
    write(6,*) ' elec_inertia tensor '
    write(6,'(3F12.4)') ((elec_inertia_tensor(i,j),j=1,3),i=1,3)
    matz=1
    call lapack_diag(elec_inertia_eigval,elec_inertia_eigvec,elec_inertia_tensor,3,3)
    write(6,*) ' eigenvalues and corresponding eigenvectors '
    write(6,'(F12.4,1h,3F12.4)') (elec_inertia_eigval(i)    &
         ,(elec_inertia_eigvec(j,i),j=1,3),i=1,3)
    if (elec_inertia_eigval(1).eq.0.D0) then
     if (elec_inertia_eigval(2).eq.0.D0) then
      write(6,*) ' point-like molecule - seems an atom '
     else
     write(6,*) ' linear molecule ! '
    end if
   end if
END_PROVIDER

