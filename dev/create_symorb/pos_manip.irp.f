! -*- F90 -*-
  BEGIN_PROVIDER [real*8, rotmat, (3,3)]
  END_PROVIDER
  
 BEGIN_PROVIDER [real*8, elec_center_of_mass, (3)]
&BEGIN_PROVIDER [real*8, l_elec_center_of_mass]
    implicit none
    integer :: iat,i,m,mtot
    elec_center_of_mass=0.D0
    mtot=0.D0
    do iat=1,nucl_num
     m=nint(nucl_charge(iat))
     mtot+=m
     do i=1,3
      elec_center_of_mass(i)+=m*nucl_coord(iat,i)
     end do
    end do
    elec_center_of_mass*=1.D0/dble(mtot)
    write(6,*) ' elec_center_of_mass = ',elec_center_of_mass
    l_elec_center_of_mass=0.D0
    do i=1,3
     l_elec_center_of_mass+=elec_center_of_mass(i)*elec_center_of_mass(i)
    end do
    l_elec_center_of_mass=sqrt(l_elec_center_of_mass)
END_PROVIDER

BEGIN_PROVIDER [real*8, nucl_coord_trans, (3,nucl_num)]
   implicit none
   integer :: i,iat
   write(6,*) ' initial positions '
   write(6,'(3F12.4)') ((nucl_coord(iat,i),i=1,3),iat=1,nucl_num)
   do iat=1,nucl_num
    do i=1,3
     nucl_coord_trans(i,iat)=nucl_coord(iat,i)-elec_center_of_mass(i)
    end do
   end do
   write(6,*) ' translated positions '
   write(6,'(3F12.4)') ((nucl_coord_trans(i,iat),i=1,3),iat=1,nucl_num)
END_PROVIDER

 BEGIN_PROVIDER [real*8, nucl_coord_sym, (3,nucl_num)]
&BEGIN_PROVIDER [logical, l_oper, (8)]
&BEGIN_PROVIDER [logical, l_symorient]
&BEGIN_PROVIDER [integer, nrot]
&BEGIN_PROVIDER [integer, nsigma]
!&BEGIN_PROVIDER [integer, noper]
BEGIN_DOC
! we set up the matrices of the operations and look whether the
! we reproduce all atoms under the transformations
! we first rotate the frame until we have the order Jzz < Jyy < Jxx
! then we look for C2 axes
END_DOC
   implicit none
   logical :: determine_sym
   integer :: i,iat,j,l
   real*8 :: rotax(3),rotang,zvec(3),scalp,norm_rotax

   nucl_coord_sym=nucl_coord_trans

   zvec=0.D0
   zvec(3)=1.D0
   
   call vec_prod(elec_inertia_eigvec(1,1),zvec,rotax)
   norm_rotax=sqrt(scalp(rotax,rotax))
   write(6,*) ' norm_rotax = ',norm_rotax
   if ((norm_rotax.eq.1.D0.or.norm_rotax.lt.1.D-5).and.l_elec_center_of_mass.lt.1.D-5) then
    write(6,*) ' coordinates seem already oriented' 
    l_symorient=.true.
   else
    l_symorient=.false.
    do i=1,3
     rotax(i)*=1.D0/norm_rotax
    end do
    if (norm_rotax.lt.1.D-5) then
     write(6,*) ' z-axis already aligned '
    write(6,*) ' nucl_coord_trans ',nucl_coord_trans
    else
     rotang=acos(scalp(elec_inertia_eigvec(1,1),zvec))
!     write(6,*) ' 1. rotax, rotang = ',rotax,', ',rotang
!     write(6,*) scalp(rotax,rotax)
     call fill_rotmat(rotax,rotang)
     call rotate_frame(nucl_coord_sym,nucl_num)
     call rotate_frame(elec_inertia_eigvec,3)
    
!    write(6,*) ' eigenvalues, rotated eigenvectors '
!    write(6,'(F12.4,1h,3F12.4)') (elec_inertia_eigval(i),(elec_inertia_eigvec(j,i),j=1,3),i=1,3)
     
!    write(6,*) ' symorient orientation '
!    write(6,'(i4,3F12.4)') (iat,(nucl_coord_sym(i,iat),i=1,3),iat=1,nucl_num)
    end if
! rotate around z for having the 2nd eigenvector on y
    zvec   =0.D0
    zvec(2)=1.D0
    rotang=acos(scalp(elec_inertia_eigvec(1,2),zvec))
    rotax=0.D0
    rotax(3)=1.D0
!  write(6,*) ' 2. rotax, rotang = ',rotax,', ',rotang
    call fill_rotmat(rotax,rotang)
    call rotate_frame(nucl_coord_sym,nucl_num)
    call rotate_frame(elec_inertia_eigvec,3)

!  write(6,*) ' eigenvalues, rotated eigenvectors '
!  write(6,'(F12.4,1h,3F12.4)') (elec_inertia_eigval(i),(elec_inertia_eigvec(j,i),j=1,3),i=1,3)

    end if

    write(6,*) ' testing symmetry operations '

    logical :: l_cycle
    integer :: isym 
    l_cycle=.true.

    do while (l_cycle)
    
     noper=0
     nsigma=0
     nrot=0
     rotmat=0.D0
     do isym=1,8
      do i=1,3
       rotmat(i,i)=diag_rotmat(i,isym)
      end do
      l_oper(isym)=determine_sym(nucl_coord_sym)
      if (l_oper(isym)) then
       noper+=1
       if (isym.gt.5) then
        nsigma+=1
       else
        if (isym.ge.2.and.isym.le.4) then
         nrot+=1
        end if
       end if
      end if
     end do

!    if (noper.eq.1) then
!     write(6,*) 
!     write(6,*)  ' no symmetry operations to exploit'
!     write(6,*)  
!     stop
!    end if
!    write(6,*) ' noper, nrot, nsigma ',noper, nrot, nsigma,l_oper
     
     if (nrot.eq.1) then
! if one rotation only, it is C2z
      if (l_oper(4)) then
       rotmat=0.D0
       rotmat(2,2)=1.D0
       rotmat(1,3)=-1.D0
       rotmat(3,1)=1.D0
       call rotate_frame(nucl_coord_sym,nucl_num)
       call rotate_frame(elec_inertia_eigvec,3)
      else if (l_oper(3)) then
       rotmat=0.D0
       rotmat(1,1)=1.D0
       rotmat(2,3)=-1.D0
       rotmat(3,2)=1.D0
       call rotate_frame(nucl_coord_sym,nucl_num)
       call rotate_frame(elec_inertia_eigvec,3)
      else
       l_cycle=.false.
      end if
     else
      if (nsigma.eq.1) then
! if one mirror plane only, it is sigma_h
       if (l_oper(7)) then
           rotmat=0.D0
           rotmat(1,1)= 1.D0
           rotmat(2,3)=-1.D0
           rotmat(3,2)= 1.D0
           call rotate_frame(nucl_coord_sym,nucl_num)
           call rotate_frame(elec_inertia_eigvec,3)
          else if (l_oper(8)) then
           rotmat=0.D0
           rotmat(2,2)= 1.D0
           rotmat(1,3)=-1.D0
           rotmat(3,1)= 1.D0
           call rotate_frame(nucl_coord_sym,nucl_num)
           call rotate_frame(elec_inertia_eigvec,3)
          else
           l_cycle=.false.
          end if
         else
          l_cycle=.false.
         end if
        end if
       end do

       write(6,*) ' symorient orientation '
       write(6,'(i4,3F14.8)') (iat,(nucl_coord_sym(i,iat),i=1,3),iat=1,nucl_num)

       if (nosym) then
        write(6,*) ' turning symmetry off '
        noper=1
        nsigma=0
        nrot=0
       end if

END_PROVIDER

      logical function determine_sym(pos)
        implicit none
        integer :: iat,i,j,jat
        real*8 :: vec(3),dd,pos(3,nucl_num)
        logical :: lfound
!        write(6,*)
 !       write(6,*) ' testing symmetry '
!        write(6,'(3F12.4)') ((rotmat(i,j),j=1,3),i=1,3)
        determine_sym=.true.
        do iat=1,nucl_num
!         write(6,*) ' initial position : ',nint(nucl_charge(iat)),(pos(j,iat),j=1,3)
         if (determine_sym) then
          vec=0.D0
          do i=1,3
           do j=1,3
            vec(i)+=rotmat(i,j)*pos(j,iat)
           end do
          end do
          lfound=.false.
          do jat=1,nucl_num
           if (.not.lfound) then
            dd= (vec(1)-pos(1,jat))*(vec(1)-pos(1,jat))
            dd+=(vec(2)-pos(2,jat))*(vec(2)-pos(2,jat))
            dd+=(vec(3)-pos(3,jat))*(vec(3)-pos(3,jat))
            dd=sqrt(dd)
            if (dd.lt.1.D-5.and.nucl_charge(iat).eq.nucl_charge(jat)) lfound=.true.
           end if
          end do
          if (.not.lfound) determine_sym=.false.
         end if
        end do
      end function determine_sym
     
   subroutine rotate_frame(pos,n)
     implicit none
     integer :: i,j,iat,n
     real*8 :: zvec(3),pos(3,n)

     do iat=1,n
      zvec=0.D0
      do i=1,3
       do j=1,3
        zvec(i)+=rotmat(i,j)*pos(j,iat)
       end do
      end do
      do i=1,3
       pos(i,iat)=zvec(i)
      end do
     end do
  end subroutine rotate_frame

subroutine fill_rotmat(vec,angle)
BEGIN_DOC
! arbitrary rotation matrix
END_DOC

    implicit none
    real*8 :: vec(3),angle,u(3),ux,uy,uz,l,c,s,cm

    c=cos(angle)
    s=sin(angle)
    cm=1.D0-c

    l=sqrt(vec(1)*vec(1)+vec(2)*vec(2)+vec(3)*vec(3))
    u=vec/l
    ux=u(1)
    uy=u(2)
    uz=u(3)
    rotmat=0.D0
    rotmat(1,1)=c+ux*ux*cm
    rotmat(1,2)=ux*uy*c-uz*s
    rotmat(1,3)=ux*uz*cm+uy*s
    rotmat(2,1)=uy*ux*cm+uz*s
    rotmat(2,2)=c+uy*uy*cm
    rotmat(2,3)=uy*uz*cm-ux*s
    rotmat(3,1)=uz*ux*cm-uy*s
    rotmat(3,2)=uz*uy*cm+ux*s
    rotmat(3,3)=c+uz*uz*cm
    integer :: i,j
!   write(6,*) '   ---------------------------- '
!   write(6,*) ' the rotation matrix '
!   write(6,'(3F12.4)') ((rotmat(i,j),j=1,3),i=1,3)
!   write(6,*) '   ---------------------------- '
!   write(6,*)
end subroutine fill_rotmat

subroutine vec_prod(a,b,c)
    implicit none
    real*8 :: a(3),b(3),c(3)
    c(1)=a(2)*b(3)-a(3)*b(2)
    c(2)=a(3)*b(1)-a(1)*b(3)
    c(3)=a(1)*b(2)-a(2)*b(1)
end subroutine vec_prod

real*8 function scalp(a,b)
  implicit none
  real*8 :: a(3),b(3)
  scalp=a(1)*b(1)+a(2)*b(2)+a(3)*b(3)
end function scalp
