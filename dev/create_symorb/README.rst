=============
create_symorb
=============

Generating symmetry-adapted basis functions for abelian groups :
The program determines the symmetry group of the atomic framework
(ignoring isotope effects), prints relevant information on the 
grooup found, and writes the symmetry-adapted linear combinations 
of the basis functions (in spherical harmonics) to a file.
Only the ezfio is needed as input.

