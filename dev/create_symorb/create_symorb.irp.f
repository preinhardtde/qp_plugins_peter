! -*- F90 -*-
program main
 call header
 call setup
 call driver
end program main

subroutine header
    write(6,*)
    write(6,*) ' simple symmetry analysis of a molecule '
    write(6,*) ' determine Abelian group and symmetry-adapted AOs '
    write(6,*) ' P Reinhardt, Paris, 9/2021 '
    write(6,*)
end subroutine header

subroutine setup
     implicit none
     nosym=.false.
end subroutine setup

subroutine driver
     implicit none
     integer :: ioper
     write(6,*)
     do i=1,8
      write(6,*)  name_oper(i),': ', l_oper(i)
     end do
     write(6,*)
real*8 :: cpu0,wall0
         call cpu_time(cpu0)
         call wall_time(wall0)

integer :: i,j
     write(6,*) 
     write(6,*) ' positions of the atoms: '
     write(6,'(A4,1h,3F12.6)') (element_symbol(nint(nucl_charge(i))),(nucl_coord_sym(j,i),j=1,3),i=1,nucl_num)
     write(6,*) 
     write(6,*)

!    call write_sym
     write(6,*) (norb_sym_cart(ioper),name_irreps(ioper),ioper=1,noper)

     if (l_symorient) then
      call write_orb
      call cpustamp(cpu0,wall0,cpu0,wall0,'ORB ')
      call write_integrals
     else
      if (noper.gt.1) then
       write(6,*) 
       write(6,*) ' nuclear coordinates changed, please create a new EZFIO '
       write(6,*) ' before going on with symmetry-adapted basis functions '
       write(6,*) 
       write(6,*) ' corresponding xyz file: '
       write(6,*) nucl_num
       write(6,*) ' symmetry group: ',group_name(group_index)
       write(6,'(A2,1h,3F13.8)') (element_symbol(nint(nucl_charge(i))),(nucl_coord_sym(j,i),j=1,3),i=1,nucl_num)
       write(6,*) 
      else
       write(6,*) 
       write(6,*) ' the barycenter is off the origin, but your system has no symmetry to exploit'
       write(6,*) ' no need for a new ezfio '
       write(6,*) 
       call write_orb
       call cpustamp(cpu0,wall0,cpu0,wall0,'ORB ')
       call write_integrals
      end if
     end if

   end subroutine driver
