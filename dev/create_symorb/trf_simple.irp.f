! -*- F90 -*-
    subroutine trf_simple
BEGIN_DOC
! routine for writing integrals to a file
! in principle we need only the place for the final bielectronic integrals
! and a square matrix
END_DOC
      implicit none
      real*8, allocatable :: motwo_tr(:,:,:,:),half_tr(:,:,:,:),aotwo(:,:,:,:)
      real*8, allocatable :: one_index_buffer(:),two_index_buffer(:,:)
      allocate (one_index_buffer(ao_num))

      integer :: i,j,k,l,a,b,c,d,aa,bb,cc,dd
      real*8 :: ao_two_e_integral,cia,cjb,ckc,cld,hhh
      real*8 :: cai,cbi,caj,cbj
      real*8 :: ao_two_e_integral_schwartz_accel
      write(6,*) ' block_offset_cart =',block_offset_cart
      integer :: ii,jj,kk,ll,ioper,joper,lstart,addr1D
      real*8 :: prefactor,term

      real*8 :: cpu0,wall0
      call cpu_time(cpu0)
      call wall_time(wall0)

      allocate (aotwo(ao_num,ao_num,ao_num,ao_num))
      aotwo=0.D0

      do i=1,ao_num
       write(6,*) ' i = ',i
       do j=i,ao_num
        do k=i,ao_num
        call get_ao_two_e_integrals(i,k,j,ao_num,one_index_buffer)
! $OMP PARALLEL DO &
! $OMP DEFAULT(NONE)                                            &
! $OMP PRIVATE(l)       &
! $OMP SHARED (ao_num,one_index_buffer,aotwo,i,j,k)
           do l=1,ao_num
            aotwo(i,j,k,l)=one_index_buffer(l)
           end do
! $OMP END PARALLEL DO
        end do
       end do
      end do
      call cpustamp(cpu0,wall0,cpu0,wall0,'ints')

      do i=1,ao_num
       do j=i,ao_num
        do k=i,ao_num
         do l=1,ao_num
          hhh=aotwo(i,j,k,l)
          aotwo(j,i,k,l)=hhh
          aotwo(j,i,l,k)=hhh
          aotwo(i,j,l,k)=hhh
          aotwo(k,l,i,j)=hhh
          aotwo(k,l,j,i)=hhh
          aotwo(l,k,j,i)=hhh
          aotwo(l,k,i,j)=hhh
         end do
        end do
       end do
      end do

      call cpustamp(cpu0,wall0,cpu0,wall0,'4sym')

!     open(unit=12,file='AOTWOints',status='unknown',form='formatted')
!     do i=1,ao_num
!      do j=i,ao_num
!       do k=i,ao_num
!        if (k.eq.i) then
!         lstart=j
!        else
!         lstart=k
!        end if
!        do l=lstart,ao_num
!         hhh=aotwo(i,j,k,l)
!         if (abs(hhh).gt.1.D-12) write(12,'(4I4,E20.12)') i,j,k,l,hhh
!        end do
!       end do
!      end do
!     end do
!     close(12)
!     call cpustamp(cpu0,wall0,cpu0,wall0,'wrao')

!!$      do i=1,ao_num
!!$       write(6,*) ' orbital ',i,1.D0/sqrt(ao_overlap(i,i))
!!$       do j=1,ao_num
!!$        if (abs(ao_cart_to_sym_coef(j,i)).gt.1.D-12) write(6,*) j,ao_cart_to_sym_coef(j,i)
!!$       end do
!!$      end do


! write(6,9921) i,j,k,l,hhh,aa,bb,k,l,term,cia,cib,cja,cjb
9921  format(' integral (',2i4,'|',2i4,') = ',F12.6,' goes to htrf (',2i4,'|',2i4,')',5F12.6)

      allocate (half_tr(ao_num,ao_num,ao_num,ao_num))
      half_tr=0.D0
      
       real*8 :: cib,cja,clc,ckd
! this is the N^6 algorithm
! transform i -> a
        do k=1,ao_num
         do l=k,ao_num
      do i=1,ao_num
       do j=1,ao_num
        ii=min(i,j)
        jj=max(i,j)
          hhh=aotwo(ii,jj,k,l)
! hhh=two_index_buffer(iii,jjj)
          if (hhh.ne.0.D0) then
           do a=1,nused_ao_cart(i)
            aa=sym_ao_cart_inv(a,i)
            cia=ao_cart_to_sym_coef(i,aa)
            cja=ao_cart_to_sym_coef(j,aa)
            do b=1,nused_ao_cart(j)
             bb=sym_ao_cart_inv(b,j)
             if (bb.ge.aa) then
              cib=ao_cart_to_sym_coef(i,bb)
              cjb=ao_cart_to_sym_coef(j,bb)
              term=cib*cja+cia*cjb
              if (aa.eq.bb) then
               term*=0.5D0
              else
               if (i.eq.j.and.nused_ao_cart(i).ne.1) then
                term*=0.5D0
               end if
              end if
              half_tr(aa,bb,k,l)+=hhh*term
              if (aa.eq.1.and.bb.eq.2.and.k.eq.1.and.L.eq.1) write(6,*) aa,bb,k,l,hhh,term,hhh*term,half_tr(aa,bb,k,l),cia,cib,cja,cjb,aa,bb,i,j
             end if
            end do
           end do
          end if    
         end do
        end do
       end do
      end do
      call cpustamp(cpu0,wall0,cpu0,wall0,'hftr')
      
      deallocate (aotwo)
      allocate (motwo_tr(ao_num,ao_num,ao_num,ao_num))
      motwo_tr=0.D0
      open(unit=12,file='HFTWOints',status='unknown',form='formatted')
      do i=1,ao_num
       do j=i,ao_num
        do k=1,ao_num
         do l=k,ao_num
          hhh=half_tr(i,j,k,l)
          if (abs(hhh).gt.1.D-12) write(12,'(4I4,E20.12)') i,j,k,l,hhh
         end do
        end do
       end do
      end do
      close(12)
      call cpustamp(cpu0,wall0,cpu0,wall0,'whlf')
      
      do a=1,ao_num
       do b=a,ao_num
        do k=1,ao_num
         do l=1,ao_num
          kk=min(k,l)
          ll=max(k,l)
          hhh=half_tr(a,b,kk,ll)
          if (hhh.ne.0.D0) then
           do c=1,nused_ao_cart(k)
            cc=sym_ao_cart_inv(c,k)
            ckc=ao_cart_to_sym_coef(k,cc)
            clc=ao_cart_to_sym_coef(l,cc)
            do d=1,nused_ao_cart(l)
             dd=sym_ao_cart_inv(d,l)
             if (cc.le.dd) then
              ckd=ao_cart_to_sym_coef(k,dd)
              cld=ao_cart_to_sym_coef(l,dd)
              term=(ckc*cld+ckd*clc)
              if (cc.eq.dd) then
               term*=0.5D0
              else
               if (k.eq.l.and.nused_ao_cart(k).ne.1) then
                term*=0.5D0
               end if
              end if
              motwo_tr(a,b,cc,dd)+=hhh*term
!!$               write(6,*) a,b,k,l,cc,dd,ckc,clc,ckd,cld,term
             end if
            end do
           end do
          end if
         end do
        end do
       end do
      end do
      call cpustamp(cpu0,wall0,cpu0,wall0,'ftrf')


      deallocate(half_tr)
      
      do a=1,ao_num
       do b=a,ao_num
        do c=a,ao_num
         integer :: dstart
         if (c.eq.a) then
          dstart=b
         else
          dstart=c
         end if
         do d=dstart,ao_num
          hhh=motwo_tr(a,b,c,d)
          motwo_tr(b,a,c,d)=hhh
          motwo_tr(b,a,d,c)=hhh
          motwo_tr(a,b,d,c)=hhh
          
          motwo_tr(c,d,a,b)=hhh
          motwo_tr(c,d,b,a)=hhh
          motwo_tr(d,c,b,a)=hhh
          motwo_tr(d,c,a,b)=hhh
          
         end do
        end do
       end do
      end do
      call cpustamp(cpu0,wall0,cpu0,wall0,'fsym')
      
!     open(unit=12,file='SYMTWOints',status='unknown',form='formatted')
!     do ioper=1,noper
!      write(12,'(A8,A4,3H - ,A4)') '! block ',name_irreps(ioper),name_irreps(ioper)
!      do i=1,norb_sym_cart(ioper)
!       ii=block_offset_cart(ioper)+i
!       do j=i,norb_sym_cart(ioper)
!        jj=block_offset_cart(ioper)+j
!        do k=i,norb_sym_cart(ioper)
!         kk=block_offset_cart(ioper)+k
!         if (i.eq.k) then
!          lstart=j
!         else
!          lstart=k
!         end if
!         do l=lstart,norb_sym_cart(ioper)
!          ll=block_offset_cart(ioper)+l
!          if (abs(motwo_tr(ii,jj,kk,ll)).gt.1.D-12) write(12,'(4I4,E20.12)') i,j,k,l,motwo_tr(ii,jj,kk,ll)
!         end do
!        end do
!       end do
!      end do
!      write(12,*) ' -1 -1 -1 -1 0.'
       
!      do joper=ioper+1,noper
!       write(12,'(A8,A4,3H - ,A4)') '! block ',name_irreps(ioper),name_irreps(joper)
!       do i=1,norb_sym_cart(ioper)
!        ii=block_offset_cart(ioper)+i
!        do j=i,norb_sym_cart(ioper)
!         jj=block_offset_cart(ioper)+j
!         do k=i,norb_sym_cart(joper)
!          kk=block_offset_cart(joper)+k
!          do l=k,norb_sym_cart(joper)
!           ll=block_offset_cart(joper)+l
!           if (abs(motwo_tr(ii,jj,kk,ll)).gt.1.D-12) write(12,'(4I4,E20.12)') i,j,k,l,motwo_tr(ii,jj,kk,ll)
!          end do
!         end do
!        end do
!       end do
!       write(12,*) ' -1 -1 -1 -1 0.'
!      end do
!     end do
      
      call cpustamp(cpu0,wall0,cpu0,wall0,'calc')

! we store 4(ij|kl)-(ik|jl)-(il|kj)

      open(unit=12,file='TWOEL.BLK_6d_simple',status='unknown',form='formatted')
      do ioper=1,noper
       do joper=ioper,noper
       write(12,'(A9,A4,A4)') '! Blocks ',name_irreps(ioper),name_irreps(joper)
       do i=1,norb_sym_cart(ioper)
        ii=block_offset_cart(ioper)+i
        do j=i,norb_sym_cart(ioper)
         jj=block_offset_cart(ioper)+j
         do k=1,norb_sym_cart(joper)
          kk=block_offset_cart(joper)+k
          if (kk.eq.ii) then
           lstart=j
          else
           lstart=k
          end if
          do l=lstart,norb_sym_cart(joper)
           ll=block_offset_cart(joper)+l
           hhh=4.D0*motwo_tr(ii,jj,kk,ll)-motwo_tr(ii,kk,ll,jj)-motwo_tr(ii,ll,kk,jj)
           if (abs(hhh).gt.1.D-12)  write(12,'(4I8,E20.12)') i,j,k,l,hhh
          end do
         end do
        end do
       end do
       write(12,*) ' -1 -1 -1 -1 0.E0 '
       end do
       end do

     close(12)
       
     call cpustamp(cpu0,wall0,cpu0,wall0,'dist')
     deallocate (motwo_tr)
     
   end subroutine trf_simple
    
    subroutine trf_simple2
      implicit none
      real*8, allocatable :: motwo_tr(:)
      real*8, allocatable :: one_index_buffer(:),two_index_buffer(:,:)

      integer :: i,j,k,l,a,b,c,d,aa,bb,cc,dd
      real*8 :: cia,cjb,ckc,cld,hhh
      real*8 :: cai,cbi,caj,cbj
      integer :: ii,jj,kk,ll,ioper,joper,lstart,addr1D
      real*8 :: prefactor,term

      real*8 :: cpu0,wall0
      call cpu_time(cpu0)
      call wall_time(wall0)

      allocate (motwo_tr(blocked_twoint_dim))
      allocate (two_index_buffer(ao_num,ao_num),one_index_buffer(ao_num))
      motwo_tr=0.D0

real*8 :: cib,cja,clc,ckd
      write(6,*) ' block_offset_cart =',block_offset_cart
      open(unit=12,file='HFTWOints2',status='unknown',form='formatted')

      do ii=1,ao_num
       write(6,*) ' ii = ',ii
! get the IRREP
       ioper=sym_ao_cart_type(ii)
       i=sym_ao_cart_indx(ii)
       do jj=ii,ao_num
! get the IRREP
        joper=sym_ao_cart_type(jj)
        j=sym_ao_cart_indx(jj)
! a block of bielectronic integrals (ij|kl) for all kl and one ij.
        two_index_buffer=0.D0
        do kk=1,ao_num
         one_index_buffer=0.D0
         call get_ao_two_e_integrals(ii,kk,jj,ao_num,one_index_buffer)
         do ll=kk,ao_num
          two_index_buffer(kk,ll)=one_index_buffer(ll)
         end do
        end do
! transform kl -> cd
! we'll need only the upper triangle, and can store the 
! half-transformed integrals on the lower triangle
! the diagonal will hold the diagonal of the half-transformed block
!
! this is the N^6 algorithm
        one_index_buffer=0.D0
        do kk=1,ao_num
         do ll=1,ao_num
          if (kk.gt.ll) then
           hhh=two_index_buffer(ll,kk)
          else
           hhh=two_index_buffer(kk,ll)
          end if

          if (hhh.ne.0.D0) then
           do a=1,nused_ao_cart(kk)
            aa=sym_ao_cart_inv(a,kk)
            cia=ao_cart_to_sym_coef(kk,aa)
            cja=ao_cart_to_sym_coef(ll,aa)
            do b=1,nused_ao_cart(ll)
             bb=sym_ao_cart_inv(b,ll)
             if (bb.ge.aa) then
              cib=ao_cart_to_sym_coef(kk,bb)
              cjb=ao_cart_to_sym_coef(ll,bb)
              term=cib*cja+cia*cjb
              if (aa.eq.bb) then
               term*=0.5D0
               one_index_buffer(aa)+=hhh*term
              else
               if (kk.eq.ll.and.nused_ao_cart(kk).ne.1) then
                term*=0.5D0
               end if
               two_index_buffer(bb,aa)+=hhh*term
               if (aa.eq.1.and.bb.eq.2.and.ii.eq.1.and.jj.eq.1) write(6,*) ii,jj,aa,bb,hhh,term,hhh*term,two_index_buffer(bb,aa),cia,cib,cja,cjb,aa,bb,kk,ll
              end if
             end if
            end do
           end do
          end if    
         end do
        end do
!
! complete the block
!
      do kk=1,ao_num
       hhh=one_index_buffer(kk)
       two_index_buffer(kk,kk)=hhh
       if (abs(hhh).gt.1.D-12) write(12,'(4I4,E20.12)') kk,kk,ii,jj,hhh
       do ll=kk+1,ao_num
        hhh=two_index_buffer(ll,kk)
        two_index_buffer(kk,ll)=hhh
        if (abs(hhh).gt.1.D-12) write(12,'(4I4,E20.12)') kk,ll,ii,jj,hhh
       end do
      end do
!
! distribute this over motwo_tr
!
integer :: loper,koper,blocked_twoint_1Daddress
      if (ioper.eq.joper) then
       do kk=ii,ao_num
        koper=sym_ao_cart_type(kk)
        k=sym_ao_cart_indx(kk)
        if (ii.eq.kk) then
         lstart=jj
        else
         lstart=kk
        end if
        do ll=lstart,ao_num
         loper=sym_ao_cart_type(ll)
         if (loper.eq.koper) then
          l=sym_ao_cart_indx(ll)

          do a=1,nused_ao_cart(ii)
           aa=sym_ao_cart_inv(a,ii)
           cia=ao_cart_to_sym_coef(ii,aa)
           cja=ao_cart_to_sym_coef(jj,aa)
           do b=1,nused_ao_cart(jj)
            bb=sym_ao_cart_inv(b,jj)
            if (bb.ge.aa) then
             cib=ao_cart_to_sym_coef(ii,bb)
             cjb=ao_cart_to_sym_coef(jj,bb)
             term=cib*cja+cia*cjb
             if (aa.eq.bb) then
              term*=0.5D0
              one_index_buffer(aa)+=hhh*term
             else
              if (ii.eq.jj.and.nused_ao_cart(ii).ne.1) then
               term*=0.5D0
              end if
              addr1D=blocked_twoint_1Daddress(i,j,ioper,k,l,koper)
! we add to 4(ij|kl)
              motwo_tr(addr1D)+=4.D0*hhh*term
             end if
            end if
           end do
          end do
         end if
        end do
       end do
      end if
! find (ik|lj) and (il|kj) to be subtracted from 4(ij|kl)
! we have in memory the halftransformed integrals (ij|ab)
! for i <= j, and all a b
!     do kk=1,ao_num
!      koper=sym_ao_cart_type(kk)
!      if (koper.eq.ioper.or.koper.eq.joper) then
!      do ll=1,ao_num
!       loper=sym_ao_cart_type(ll)


       end do
      end do

     deallocate (motwo_tr)
     
   end subroutine trf_simple2
    
