\documentclass[aps,epsf,rotating]{revtex4}
\input epsf
\input colordvi
\epsfverbosetrue
%
\def\bild#1#2#3{$$\vbox{\hfil\hskip -1 true cm\epsfysize=#2 true cm
\epsffile{#1.eps}\bildunterschrift{#3}\hfill}$$}
\def\vierbild#1#2#3#4#5#6{$$\vbox{\hfil\hskip -1 true cm\epsfysize=#5 true cm
\epsffile{#1.ps}\epsffile{#2.ps}\epsffile{#3.ps}\epsffile{#4.ps}\bildunterschrift{#6}\hfill}$$}

\def\Det#1#2{\Phi_{\rm #1}^{\rm #2}}
\def\DDet#1#2#3#4{\Phi_{\rm #1#2}^{\rm #3#4}}
\def\bint#1#2#3#4{(\,\rm #1\, #2\,|\,#3\,#4\,)}


\def\dbtilde#1{\overset{\approx}{#1}}

\def\E#1#2{\hat{E}_{#1#2}}

\def\bild#1#2#3{
\begin{figure}
$$\vbox{\hfil\hskip -1 true cm\epsfysize=#2 true cm \epsffile{#1.eps}} $$
\caption{\vtop{\hsize = 10 true cm #3}}
\label{#1}
\end{figure}
}


\def\obaseskip{\baselineskip = 10 pt}
\def\bb#1{\setbox1=\vtop{\hsize = 15 true cm \obaseskip\noindent #1}\box1}
\def\reference#1#2#3#4#5{\sl #1\rm , #2, \bf #3 \rm (#4) #5}
\newcount\footnoteindex
\footnoteindex=1
\def\footnotenumber{$^{\number\footnoteindex}$}
\def\footnotenew#1{\footnote{\bb{\obaseskip #1}}}

\def\footref#1#2#3#4#5{\footnotenew{\reference{\obaseskip #1}{\obaseskip #2}{\obaseskip #3}{\obaseskip #4}{\obaseskip #5}}}

\def\va{\vec{\rm a}}
\def\vr{\vec{\rm r}}
\def\vR{\vec{\rm R}}
\def\vb{\vec{\rm b}}
\def\vc{\vec{\rm c}}
\def\vd{\vec{\rm d}}
\def\vg{\vec{\rm g}}
\def\vgp{\vec{\rm g'}}
\def\vm{\vec{\rm m}}
\def\vn{\vec{\rm n}}
\def\vh{\vec{\rm h}}
\def\vs{\vec{\rm s}}
\def\vi{\vec{\rm i}}
\def\vj{\vec{\rm j}}
\def\vk{\vec{\rm k}}
\def\vl{\vec{\rm l}}
\def\vmu{\vec{\mu}}
\def\vo{\vec{0}}

\def\ket#1{|#1\rangle}
\def\bra#1{\langle #1}

\def\matel#1#2#3{\left\langle\, #1\, \left| \, 
#2\,\right|\,#3\,\right\rangle}
\def\reference#1#2#3#4#5{\sl #1\rm , #2, \bf #3 \rm (#4) #5}
%
\begin{document}
\title{Symmetry in the Quantum Package} 
\author{P.\ Reinhardt}
\affiliation{Laboratoire de Chimie Th\'eorique, Facult\'e des Sciences et de 
l'Ing\'enierie, Sorbonne Universit\'e,
  \\ 4, place Jussieu, F -- 75252 Paris CEDEX 05, France}

\begin{abstract}
How symmetry is  introduced in the Quantum package.
  \bigskip
  \noindent(TeXed on \today)
\end{abstract}

\maketitle


\section{Introduction}
Making use of symmetry may reduce significantly the computational effort of a 
calculation due to the reduced size of matrices to be diagonalized. Less during 
their construction. Another aspect is a possible definition of precise states 
by assigning electrons to symmetry-adapted orbitals -- a diagonalization of a 
Fock matrix with subsequent ordering with respect to orbital energies may 
change the symmetry of the optimized state or lead to oscillations between 
occupation patterns and orbital coefficients. 

Another strategy may be to correct starting orbitals defining an initial 
state via a Singles-CI procedure without diagonalizing a Fock matrix 
and energy-ordering of the eigenvectors. If well-defined, final orbitals will 
remain in the same IRREP as starting orbitals. Again symmetry is a desired 
feature in a quantum-chemistry code.

Most quantum chemistry programs allow for Abelian groups with one-dimensional 
IRERPs -- this is being implemented here in the quantum package. Of course, 
this is not sufficient for separating for instance $\Delta$ and $\Sigma$ states 
in homo-nuclear diatomics. 

\section{Elements of group theory}
We have 8 abelian groups, C$_1$, $C_s$, $C_i$, C$_2$, $C_{\rm 2h}$, C$_{\rm 
2v}$, D$_2$ and D$_{\rm 2h}$ with the 8 symmetry operators $E$, $C_2(z)$, 
$C_2(y)$, $C_2(x)$, $i$, $\sigma_{\rm xy}$, $\sigma_{\rm xz}$, $\sigma_{\rm 
yz}$. The $3\times 3$ matrices associated to the coordinate transformations are 
all diagonal, with 1 or $-1$ on the diagonal, and may be coded with 3 bits from 
0 ($=E$) to 7 ($=i$). 

\begin{table}[h!]
 \begin{tabular}{c|cc|cccccccc}
 & & & C$_1$ & C$_s$ & C$_i$ & C$_2$ & C$_{\rm 2h}$ & C$_{\rm 2v}$ & D$_2$ & 
D$_{\rm 2h}$ \\
\hline
\#\  of ops. & & & 1 & 2 & 2 & 2 & 4 & 4 & 4 & 8 \\
\hline
 0 & 000 & $E$                 & x & x & x & x & x & x & x & x \\
 1 & 001 & $\sigma_{\rm xy}$   &   & x &   &   & x & x &   & x \\
 2 & 010 & $\sigma_{\rm xz}$   &   &   &   &   &   & x &   & x \\
 3 & 011 & $C_2(x)$            &   &   &   &   &   &   & x & x \\
 4 & 100 & $\sigma_{\rm yz}$   &   &   &   &   &   &   &   & x \\
 5 & 101 & $C_2(y)$            &   &   &   &   &   &   & x & x \\
 6 & 110 & $C_2(z)$            &   &   &   & x & x & x & x & x \\
 7 & 111 & $i$                 &   &   & x &   & x &   &   & x \\
 \end{tabular}
\caption{Summary of the 8 Abelian groups. Numeration of the symmetry operators 
corresponds to a 3-bit coding.}
\end{table}

As all irreducible representations are one-dimensional, symmetry-adapted atomic 
orbitals can ony be constructed by AOs of the same kind (i.e.\ same $\ell$ and 
$m$ values).

By the way, the same holds for cartesian AOs - they as well transform very 
simply under the symmetry operations: the sum of the exponents which go to $-1$
 of a polnonmial $x^iy^jz^k$ has to be odd for that the polynomial changes 
sign under a symmetry operation. 

Cartesian symmetry-adapted AOs hav ethe advantage that there are at most 8 
original cartesian AOs are to be combined. They can be stored very efficiently.

\section{The construction}
First of all the nuclei are oriented for having the electronic barycenter 
(nuclear masses set to the nuclear charges) on the origin. 
Isotope effects are supposed to be unimportant for the symmetry 
of the electronic wavefunction. Diagonalization of the (electronic) inertia 
tensor and orientation on the main axes allows for determining the presence of 
symmetry invariances.

If only one rotation axis is ppresent, the molecule is oriented along the $z$ 
axis, and if a single plane is encountered, it should be the $\sigma_{\rm xy}$ 
one. This leaves some ambiguity of the orientation for $C_{\rm 2v}$, D$_2$ and 
D$_{\rm 2h}$.

A table is set up for the images of the nuclear center under the various 
symmetry operations, and nuclei are reduced to symmetry-unique ones. 

Each atomic orbital on the symmetry-unique atoms is combined with the 
corresponding ones on the equivalent atoms, multiplied with a sign. This makes 
at most 128 possibilities, the sign on the symmetry-unique atom being 
positive. The linear combinations are exposed to the valid symmetry operations 
and, if reproduced modulo a sign, the results compared to the possible lines 
in the character table of the determined symmetry group.

\section{Observations}
Two little theorems: (i) the linear combinations with a ``+'' sign of all 
images belongs to the IRREP of the generating AO as if this one were placed on 
the barycenter, and (ii) the combination of all (signed) images of the 
generating AO is in the first IRREP (A, A$_1$, A$_g$ etc.) From this is deduced 
that there is always a linear combination of the generating orbitals which lies 
in the first IRREP -- if the images are not on the same place as the generating 
AO.

\section{Using symmetry-adapted AOs}
Once we have the symmetry-adapted AOs, we transform the AO integrals on this 
basis. For one-electron integrals (overlap, kinetic energy, nuclear 
attraction, pseudo-potentials) the integral matrix decomposes into symmetry 
blocks $\langle A | {\rm operator} | A \rangle$. Bielectronic integrals 
decompose into two kinds of blocks, Coulomb and Exchange-like as $\langle 
AA|1/r_{12}|BB\rangle$ and $\langle AB|1/r_{12}|AB\rangle$ where $A$ and $B$ 
may belong to different IRREPs. The density matrix, and as well the Fock matrix 
are blocked, too. 
 
\begin{eqnarray}
 F_{\alpha\beta} & = &   h_{\alpha\beta} + \sum_{\gamma\delta} P_{\gamma\delta} 
\left[2(\alpha\beta|\gamma\delta) - (\alpha\delta|\beta\gamma)  \right] 
\label{eq:first} \\  & = & 
  h_{\alpha\beta} + \underbrace{\sum_{\gamma} P_{\gamma\gamma} 
\left[2(\alpha\beta|\gamma\gamma) - (\alpha\gamma|\beta\gamma)  \right] +
\sum_{\gamma<\delta} P_{\gamma\delta} 
\left[4(\alpha\beta|\gamma\delta) - (\alpha\delta|\gamma\beta) - 
(\beta\delta|\gamma\alpha) \right]}_{=\Xi_{\alpha\beta}} \label{eq:second} 
\end{eqnarray}
where $\alpha$ and $\beta$ belong to one IRREP and $\gamma$ and $\delta$ to 
another one. The integrals in eq.\ (\ref{eq:first}) are not any more symmetric 
in their indexes, whereas in eq.\ (\ref{eq:second}) a 3-index quantity occurs, 
and the 3-integral contractions have the same permutation symmetry 
as simple integrals.   

The contracted integrals do not allow any more for canonical ordering, we have 
to store the three classes of integrals $(AA|AA)$, $(AA|BB)$ and $(AB|AB)$ 
separately. Canonical ordering is maintained in the first class. In the second 
class $A<B$. In the third class we have $(ab|AB)$ where $a\leq A$, but nothing 
is said about $b$ and $B$. If $n_A$ and $n_B$ are the number of orbitals in 
IRREPS A and B, we have $\displaystyle{\frac{1}{2}\left(\frac{n_A(n_A+1)}{2} 
\right) \times \left(\frac{n_A(n_A+1)}{2}+1 \right)} $ integrals $(AA|AA)$, 
$\displaystyle{\left(\frac{n_A(n_A+1)}{2}\right) \times 
\left(\frac{n_B(n_B+1)}{2} \right)} $ integrals $(AA|BB)$ and 
$\displaystyle{\left(\frac{n_A(n_A+1)}{2} 
\right) \times n_B^2 } $ integrals $(AB|AB)$.

How to store these matrices in memory? Using the {\tt irpf90} we may provide a 
separate block for each IRREP, which makes writing repetitive code allocating 
block1, block2, block3 etc, all of different dimensions. Simpler seems to store 
upper (or lower) triangles of the matrices in 1D arrays together with address 
functions. 

One-electron quantities can be stored as a whole, bidimensional matrix, without 
an index function, and matrix multiplication carried out as before. 

If these address functions are simple for one-electron integrals and 
bi-electronic integrals for different IRREPS, the counter for 4 indexes 
$(AA|AA)$ in canonical ordering (integrals $(ij|kl)$ with $i\leq j$, $k\leq l$, 
and $j\leq l$ for $i=k$, for $n$ basis functions) is a quartic polynomial in $i$

\begin{eqnarray}
f(i,j,k,l;n) & = & \frac{1}{8} \left(-i^4+i^3 (4 n+2)+i^2 (4
   j-2 n (3 n+5)-3)+2 i (2 n+1) (-2 j+n
   (n+3)+1) \right. \nonumber \\
   & & \left. -4 \left(j^2-j (n (n+3)+1)+n
   (n (n+3)-2 k)+(k-1) k-2 l+4
   n
   \right)\right) \\
f(i,j,k,l;n) & = & (k-1)(n-1)-\frac{(k-1)(k-2)}{2}+l\nonumber \\
    & & 
+\frac{n(n+1)}{2}(j-1)-\frac{j(j-1)}{2} \nonumber \\ & & +(i-1)\left(n \left( 
\frac{n(n-1)} {2}+1\right)-j(n-1)-1\right)\nonumber \\
        & & +\frac{(i-1)(i-2)}{2}\left(j-\frac{n(3n-7)}{2}-4\right)\nonumber \\
        & & + \frac{(i-1)(i-2)(i-3)}{2}(n-2) - 
\frac{(i-1)(i-2)(i-3)(i-4)}{8}
\end{eqnarray}
This address function being quite complicated, we may code a hybrid scheme: 
blocks $(AA|AA)$ are treated integral-driven in a single loop, whereas 
integrals of different IRREPS are assembled with an address function, as
\begin{eqnarray}
 F^A_{\alpha\beta} & = &   h^A_{\alpha\beta} + \sum_{\gamma\delta\in A} 
 P^A_{\gamma\delta} \left[2(\alpha\beta|\gamma\delta) - 
(\alpha\delta|\beta\gamma)  \right] \nonumber \\  & & 
+ \sum_{\gamma\in B \neq A} P^B_{\gamma\gamma} 
\left[2(\alpha\beta|\gamma\gamma) - (\alpha\gamma|\beta\gamma)  \right] +
\sum_{\gamma<\delta\in B \neq A } P^B_{\gamma\delta} 
\left[4(\alpha\beta|\gamma\delta) - (\alpha\delta|\gamma\beta) - 
(\beta\delta|\gamma\alpha) \right]  
\end{eqnarray}
The integrals in the first sum are distributed over $F$ as 
\begin{eqnarray}
 (\alpha\beta|\gamma\delta) 
 &\times & 4 P_{\alpha\beta} \rightarrow F_{\gamma\delta} \nonumber \\
 &\times & 4 P_{\gamma\delta} \rightarrow F_{\alpha\beta} \nonumber \\
 &\times & -P_{\beta\delta} \rightarrow F_{\alpha\gamma} \nonumber \\
 &\times & -P_{\alpha\delta} \rightarrow F_{\beta\gamma} \nonumber \\
 &\times & -P_{\beta\gamma} \rightarrow F_{\alpha\delta} \nonumber \\
 &\times & -P_{\beta\delta} \rightarrow F_{\alpha\gamma}
\end{eqnarray}
premultiplying the integrals with weight factors: 1/8 for 
$(\alpha\alpha|\alpha\alpha)$, 1/4 for $(\alpha\alpha|\beta\beta)$, 1/2 for 
$(\alpha\alpha|\alpha\beta)$, $(\alpha\alpha|\beta\gamma)$ and 
$(\alpha\beta|\alpha\beta)$ and 1 for all others. 

Orbitals and basis functions are stored in three levels: MOs are stored as 
linear combinations of symmetry-adapted cartesian AOs, which themselves are 
linear combinations of simple cartesian basis functions. Each symmetry-adapted 
AO is a combination of at most 8 basis functions. 


\section{The 4-index-transformation} 
Of course we may use a full 4-index-transformation, however, the matrix of 
the coefficients is very sparse. The {\tt quantum package} has a routine which 
calculates all AO-integrals $(\alpha\beta|\gamma\delta)$ for a given index 
triplet $\beta$, $\gamma$, $\delta$. 

Thus we loop over $\alpha \leq \beta$ and assemble in a $n\times 
n$ matrix all integrals $(\alpha\beta|\gamma\delta)$. 
From these we form the $(AA|AA)$ integrals, the 3-index contractions, and the 
3-integral contractions. The $(AA|AA)$ integrals will be premultiplied with 
appropriate weight factors. 

The transformation of 2-index quantities is particularly simple in cartesian 
AOs as all constituing AOs come in with identical weights:
$$ \langle i | \hat{O} | j \rangle = v_i v_j \sum_{i_k} \sum_{j_l} \langle i_k 
| \hat{O} | j_l \rangle \qquad . $$

\section{A model SCF program}
Once the symmetry-blocked matrices available, we do double contractions 
(for obtaining expectation values or the bielectronic contribution to 
the Fock matrix) block-wise as
\begin{verbatim}
 a=0.D0
 do ioper=1,noper
  do alpha=1,norb_sym(ioper)
   do beta=1,norb_sym(ioper)
    addr1D=blocked1Daddress(alpha,beta,ioper)
    a+=matrix(addr1D)*density(addr1D)
   end do
  end do
 end do
\end{verbatim}
All matrices are stored as upper triangles in 1D arrays, together with an 
address function which takes care of the index ordering. 

Molecular orbitals are not a symmetric matrix, which means that we have to 
store them as squares, again in a 1D array as the blocks have different sizes. 
The expansion in (cartesian) AO coefficients is trivial.

\end{document}



