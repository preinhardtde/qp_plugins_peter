! -*- F90 -*-
        subroutine write_sym
         implicit none
         integer :: i,j,isym
         
         write(6,*)
         write(6,*) ' symmmetry is ',group_name(group_index)
         write(6,*)

         write(6,*) ' Character table '
         write(6,'(3h   ,8(1h ,A8))') (name_oper(index_oper(i)),i=1,noper)
         do i=1,noper
          write(6,'(A3,8(3h    ,i2,4h     ))') name_irreps(i),(character_table(i,j),j=1,noper)
         end do
         write(6,*)

         write(6,*) ' Group multiplication table '
         write(6,'(3h   ,8(3h   ,A3))') (name_irreps(i),i=1,noper)
         do i=1,noper
          write(6,'(A3,8(3h   ,A3))') name_irreps(i),(name_irreps(group_multiplication_table(i,j)),j=1,noper)
         end do
         write(6,*)

        write(6,*)
        write(6,*) ' distributions among the IRREPS '
        if (noper.eq.1) then
         write(6,9903) ao_spher_num,(norb_sym_spher(isym),name_irreps(isym),isym=1,noper)
        else if (noper.eq.2) then
         write(6,9904) ao_spher_num,(norb_sym_spher(isym),name_irreps(isym),isym=1,noper)
        else if (noper.eq.4) then
         write(6,9905) ao_spher_num,(norb_sym_spher(isym),name_irreps(isym),isym=1,noper)
        else if (noper.eq.8) then
         write(6,9906) ao_spher_num,(norb_sym_spher(isym),name_irreps(isym),isym=1,noper)
        end if
9903    format(i4,' = ',i3,1x,A3)
9904    format(i4,' = ',i3,1x,A3,' +',i3,1x,A3)
9905    format(i4,' = ',i3,1x,A3,3(' +',i3,1x,A3))
9906    format(i4,' = ',i3,1x,A3,7(' +',i3,1x,A3))
        write(6,*)
         
        end subroutine write_sym

