program MP3_peter
  implicit none
  BEGIN_DOC
! Third-order Moller-Plesset, eithr from integrals or from the MP1 wavefunction
  END_DOC
  print *, '                          '
  print *, '    - - - - -             '
  print *, '    ^        ^            '
  print *, '   / \      / \           '
  print *, '   | |      | |           '
  print *, '   | |      \ /           '
  print *, '   | |       v            '
  print *, '   | |       - - -        '
  print *, '   | |            ^       '
  print *, '   | |           / \      '
  print *, '   | |           | |      '
  print *, '   \ /           \ /      '
  print *, '    v             v       '
  print *, '    - - - - - - - -       '
  print *, '                          '
  print *, '                          '
  print *, ''
  read_wf = .True.
  touch read_wf 
  if (N_det.eq.1) then
   call mp3_from_1det
  else
   call mp3_from_mp1
  end if
end
