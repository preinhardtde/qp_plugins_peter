! -*- F90 -*-
subroutine mp3_from_1det
 implicit none
 integer :: i,j,k,l
 double precision :: integral_mo, get_mo_bielec_integral,integral_bis

 PROVIDE fock_matrix_diag_mo

BEGIN_DOC
! for MP3 we need integrals and orbital energies, from a HF calculation
! 
! virtual orbitals are shifted by a level shifter, we correct  for this
!
! first loop : 
!   iajb -> (ic|jd)*(ac|bd)/(E_icjd)  = tcd
!   iajb -> (ik|jl)*(ka|lb)/(E_kalb)  = tkl
!
! second loop
!   iajb -> 2[ (2(ia|kc)-(ic|ka))(jb|kc)/E_ikac 
!              -(ac|jk)*(ic|kb)/E_ikbc 
!              -(bc|jk)*(ia|kc)/E_ikac ] = tkc
!
! E_MP3 = (tkl+tcd+tkc)*(2 (ia|jb) - (ib|ja))/E_iajb
!
! multiply 2 integral blocks with corresponding denominators
! we need integral blocks ij and ab for tcd and tkl
!
! 3 integral blocks 
! we need integral blocks 
!        (ia|jb),(ib|ja),(ia|kc),(ic|ka),(jb|kc)     ij,ik,jk
!         ov ov   ov ov   ov ov   ov ov   ov ov 
!        (ia|jb),(ib|ja),(ac|jk),(ic|kb)             ij,ik,ja
!         ov ov   ov ov   vv oo   ov ov 
!        (ia|jb),(ib|ja),(bc|jk),(ia|kc)             ij,ik,jb
!         ov ov   ov ov   vv oo   ov ov 
END_DOC

 double precision, allocatable :: integrals_array1(:,:)
 double precision, allocatable :: integrals_array2(:,:)
 double precision, allocatable :: integrals_array3(:,:)
 double precision, allocatable :: integrals_array4(:,:)
 double precision, allocatable :: integrals_array5(:,:)
 double precision, allocatable :: orben(:)

! 1 determinant, closed shell
 if (n_det.ne.1) then
  write(6,*) ' need 1 determinant for MP3 '
  write(6,*) ' closed shell en plus '
  write(6,*) ' Restricted Hartree-Fock, quoi '
  return
 end if

integer :: nocc
 nocc=elec_alpha_num
 write(6,*) ' my first try - MPdrei  '
 write(6,*) ' P Reinhardt (Paris, Nov 2018) '
 write(6,*) ' dimensions '
 write(6,*) '  MOs, nocc, nvirt ',mo_num,nocc,mo_num-nocc

 allocate(integrals_array1(mo_num,mo_num))
 allocate(integrals_array2(mo_num,mo_num))
 allocate(integrals_array3(mo_num,mo_num))
 allocate(integrals_array4(mo_num,mo_num))
 allocate(integrals_array5(mo_num,mo_num))
 allocate(orben(mo_num))

real*8 :: thirdkl, thirdkc,thirdcd,ei,ej,ek,el,ea,eb,ec,ed,tkl,tcd,tkc1,tkc2,tkc3
real*8 :: tijab,e_mp3,e_mp2
integer :: a,b,c,d

 write(6,*)
 write(6,*) ' supposed orbital energies '
 do i=1,nocc
  orben(i)=fock_matrix_diag_mo(i)
  write(6,*) i, orben(i)
 end do
 do i=1+nocc,mo_num
  orben(i)=fock_matrix_diag_mo(i)-level_shift
  write(6,*) i, orben(i)
 end do
 write(6,*)

 thirdkl=0.D0
 thirdcd=0.D0
 thirdkc=0.D0
 e_mp2=0.D0
real*8 :: eiiaa,eiiab,eijaa,eijab
 eiiaa=0.D0
 eiiab=0.D0
 eijaa=0.D0
 eijab=0.D0
logical :: lij,lab
 do i = 1, nocc
  ei=orben(i)
  write(6,*) ' i = ',i
  do j = 1, nocc
   ej=orben(j)
   lij=i.eq.j
   call get_mo_two_e_integrals_ij(i,j,mo_num,integrals_array1,mo_integrals_map) 
   do a=1+nocc,mo_num
    ea=orben(a)
    do b=1+nocc,mo_num
     eb=orben(b)
     lab=a.eq.b
real*8 :: denom,term,hiajb,hibja
     hiajb=integrals_array1(a,b)
     hibja=integrals_array1(b,a)
     term=hiajb*(2.D0*hiajb-hibja)
     denom=ei+ej-ea-eb
     tijab=term/denom
     if (lij) then
      if (lab) then
       eiiaa+=tijab
      else
       eiiab+=tijab
      end if
     else
      if (lab) then
       eijaa+=tijab
      else
       eijab+=tijab
      end if
     end if
     E_MP2+=tijab
    end do
   end do
  end do
 end do
 write(6,*) ' MP2 contribution is ',E_MP2
 write(6,*) '    iiaa : ',Eiiaa
 write(6,*) '    iiab : ',Eiiab
 write(6,*) '    ijaa : ',Eijaa
 write(6,*) '    ijab : ',Eijab
 write(6,*) 

 do i = 1, nocc
  ei=orben(i)
  write(6,*) ' tab tkl : i = ',i
  do j = 1, nocc
   ej=orben(j)
! all integrals (ix|jy) 
   call get_mo_two_e_integrals_ij(i,j,mo_num,integrals_array1,mo_integrals_map) 
   do a=1+nocc,mo_num
    ea=orben(a)
    do b=1+nocc,mo_num
     eb=orben(b)
     tijab=2.D0*integrals_array1(a,b)-integrals_array1(b,a)
     tijab*=1.D0/(ei+ej-ea-eb)
     call get_mo_two_e_integrals_ij(a,b,mo_num,integrals_array2,mo_integrals_map) 
     tkl=0.D0
     do k=1,nocc
      ek=orben(k)
      do l=1,nocc
       el=orben(l)
       tkl+=integrals_array1(k,l)*integrals_array2(k,l)/(ek+el-ea-eb)
      end do
     end do
     thirdkl+=tkl*tijab
     tcd=0.D0
     do c=1+nocc,mo_num
      ec=orben(c)
      do d=1+nocc,mo_num
       ed=orben(d)
       tcd+=integrals_array1(c,d)*integrals_array2(c,d)/(ei+ej-ec-ed)
      end do
     end do
     thirdcd+=tcd*tijab
    end do
   end do
  end do
 end do
 write(6,*) ' diagrams 1+2  ',thirdcd
 write(6,*) ' diagrams 3+4  ',thirdkl
!
! the more tricky one, kc
!   iajb -> 2[ (2(ia|kc)-(ic|ka))(jb|kc)/E_ikac 
!              -(ac|jk)*(ic|kb)/E_ikbc 
!              -(bc|jk)*(ia|kc)/E_ikac ] = tkc
!
 do i = 1, nocc
  ei=orben(i)
  write(6,*) ' tkc : i = ',i
  do j = 1, nocc
   ej=orben(j)
! all integrals (ix|jy) 
   call get_mo_two_e_integrals_ij(i,j,mo_num,integrals_array1,mo_integrals_map) 
   do a=1+nocc,mo_num
    ea=orben(a)
    call get_mo_two_e_integrals_ij(j,a,mo_num,integrals_array4,mo_integrals_map) 
    do b=1+nocc,mo_num
     eb=orben(b)
     call get_mo_two_e_integrals_ij(j,b,mo_num,integrals_array5,mo_integrals_map) 
     tijab=2.D0*integrals_array1(a,b)-integrals_array1(b,a)
     tijab*=1.D0/(ei+ej-ea-eb)

     tkc1=0.D0
     tkc2=0.D0
     tkc3=0.D0
     do k=1,nocc
      ek=orben(k)
      call get_mo_two_e_integrals_ij(i,k,mo_num,integrals_array2,mo_integrals_map) 
      call get_mo_two_e_integrals_ij(j,k,mo_num,integrals_array3,mo_integrals_map) 
      do c=1+nocc,mo_num
       ec=orben(c)
       tkc1+=(2.D0*integrals_array2(a,c)-integrals_array2(c,a))*integrals_array3(b,c)/(ei+ek-ea-ec)
      end do
      do c=1+nocc,mo_num
       ec=orben(c)
       tkc2-=integrals_array4(k,c)*integrals_array2(c,b)/(ei+ek-eb-ec)
      end do
      do c=1+nocc,mo_num
       ec=orben(c)
       tkc3-=integrals_array5(k,c)*integrals_array2(a,c)/(ei+ek-ea-ec)
      end do
     end do
     thirdkc+=2.D0*(tkc1+tkc2+tkc3)*tijab
    end do
   end do
  end do
 end do

 E_MP3=thirdkc+thirdkl+thirdcd
 write(6,*) ' HF  energy       is ',HF_energy
 write(6,*) ' MP2 contribution is ',E_MP2
 write(6,*) ' MP3 contribution is ',E_MP3
 write(6,*) ' ---------------------------------'
 write(6,*) ' MP3 energy is ',HF_energy+E_MP2+E_MP3
 write(6,*)
 write(6,*) ' diagrams 1+2  ',thirdcd
 write(6,*) ' diagrams 3+4  ',thirdkl
 write(6,*) ' diagrams 5-12 ',thirdkc
 write(6,*)
 deallocate(integrals_array1)
 deallocate(integrals_array2)
 deallocate(integrals_array3)
end subroutine MP3

subroutine mp3_from_mp1
 implicit none
 BEGIN_DOC
! assumes a ezfio folder with a MP1 WF stored
 END_DOC
 if (N_det.eq.1) then
  write(6,*) ' MP1 WF needed, but there is only 1 determinant '
  write(6,*) ' this seems Hartree-Fock .... '
  return
 end if

 read_wf = .true.
 touch read_wf
 call print_MP3_energy
end

subroutine print_MP3_energy
 implicit none
 double precision, allocatable :: psiHpsi(:)
 double precision, allocatable  :: i_H_psi_array(:)
 real*8 :: EMP3,EMP2,c0
 allocate (i_H_psi_array(N_states))
 allocate(psiHpsi(N_states))
 BEGIN_DOC
! if we have the MP1 wavefunction already, we can pass
! by <Psi|H|Psi> 
 END_DOC
 c0=psi_coef(1,1)

 call i_H_psi(ref_bitmask,psi_det,psi_coef,N_int,N_det,psi_det_size,N_states,i_H_psi_array)
 call u_0_H_u_0(psiHpsi,psi_coef,N_det,psi_det,N_int,N_states,psi_det_size)
 write(6,*) ' c0 = ',c0
 write(6,*) ' <0|H|Psi>/c0 - <0|H|0> = ',i_H_psi_array(1)/c0 - ref_bitmask_energy
 write(6,*) ' <Psi|H|Psi> = ',psiHpsi(1)+nuclear_repulsion
 write(6,*) ' ref_bitmask_energy = ',ref_bitmask_energy+nuclear_repulsion
 write(6,*) ' <Psi|H-E_HF|Psi>/c0/c0 = ',(psiHpsi(1)-ref_bitmask_energy)/c0/c0
 write(6,*)

 EMP2=i_H_psi_array(1)/c0 - ref_bitmask_energy
 EMP3=(psiHpsi(1)-ref_bitmask_energy)/c0/c0-EMP2

 write(6,9901) HF_energy,EMP2,HF_energy+EMP2,EMP3,HF_energy+EMP2+EMP3
 9901 format('  HF  ',10X,'= ',F20.8,/, '  EMP2',10X,'= ',2F20.8 &
                   ,/, '  EMP3',10X,'= ',2F20.8)
 write(6,*) 
end print_MP3_energy
