! -*- F90 -*-
BEGIN_PROVIDER [integer, nocc ]
         implicit none
         integer :: i
         nocc=(elec_alpha_num + elec_beta_num)/2
         if (elec_alpha_num.ne.elec_beta_num) then
          write(6,*) ' closed-shell program only '
          stop ' no open shells, please '
         end if
         write(6,*) ' number of occupied orbitals ',nocc
END_PROVIDER

BEGIN_PROVIDER [ real*8, ci, (ao_num,mo_num) ]
      implicit none
      integer :: ione,ierr,i,j,alpha,beta
      real*8  :: cdum,ss
      real*8 :: fock(ao_num,ao_num)
      real*8 :: fock_mo(mo_num,mo_num)

      write(6,*) ' constructing orbitals, iter No ',iter_scf

      if (iter_scf.eq.0) then

       ci=mo_coef
       do alpha=1,ao_num
        do beta=1,ao_num
         ss=0.D0
         do i=1,nocc
          ss+=ci(alpha,i)*ci(beta,i)
         end do
         density(alpha,beta)=ss
        end do
       end do
       density*=2.D0
       touch density

!      fock = ao_one_e_integrals

!      do i=1,ao_num
!       fock(i,i)=0.D0
!      end do

       fock = ao_one_e_integrals + xi_scf

!      call print_mat(fock,ao_num,ao_num)

       if (number_of_functionals.ne.0) then
        fock +=ksmat_libxc
       end if
       if (srPBE_qp2) then
        fock +=ksmat_srPBE_qp2
       end if
       if (srLDA_qp2) then
        fock +=ksmat_srLDA_qp2
       end if
       if (LDA_qp2) then
        fock +=ksmat_LDA_qp2
       end if
      else
       fock = ao_one_e_integrals + (1.D0-lambda_oda)*xi_old &
               + lambda_oda * xi_scf
       if (number_of_functionals.ne.0) then
        fock +=ksmat_libxc
       end if
       if (srPBE_qp2) then
        fock +=ksmat_srPBE_qp2
!       call print_mat(ksmat_srPBE_qp2,ao_num,ao_num)
       end if
       if (srLDA_qp2) then
        fock +=ksmat_srLDA_qp2
!       call print_mat(ksmat_srLDA_qp2,ao_num,ao_num)
       end if
       if (LDA_qp2) then
        fock +=ksmat_LDA_qp2
!       call print_mat(ksmat_LDA_qp2,ao_num,ao_num)
       end if
       

      end if
! transform F onto MOs
      call transf_ao_mo(fock,ao_num,fock_mo,mo_num,ci)

write(6,*) ' adding level shift of ',level_shift
      do i=nocc+1,mo_num
       fock_mo(i,i)+=level_shift
      end do

      write(6,*) ' diagonal of the Fock matrix '
      write(6,'(5(i5F16.8))') (i,fock_mo(i,i),i=1,mo_num)

      call lapack_diag(orben,eigen_vecs,fock_mo,mo_num,mo_num)
      
      WRITE(6,*) 
      WRITE(6,*) ' ORBITAL ENERGIES OF THE CANONICAL ORBITALS' 
      write(6,*) 
      write(6,'(4(i4,e14.5))') (i,orben(i),i=1,mo_num)
      write(6,*) 
!
! well, it should come down to a simple matrix multiplication ...
!
      do alpha=1,ao_num
       do i=1,mo_num
        cdum=0.d0
        do j=1,mo_num
! the j components of eigenvector i
         cdum=cdum+eigen_vecs(j,i)*ci(alpha,j)
        end do
        cwrk(alpha,i)=cdum
       end do
      end do

      ci=cwrk

      if (bavard) then 
       write(6,*) ' new occupied orbitals '
       do i=1,nocc   
        write(6,*) ' Orbital No ',i
        write(6,'(5(i4,F16.8))') (alpha,ci(alpha,i),alpha=1,ao_num)
       end do
      end if

END_PROVIDER

BEGIN_PROVIDER [ real*8, orben, (mo_num) ]
END_PROVIDER

BEGIN_PROVIDER [ real*8, cwrk, (ao_num,mo_num) ]
END_PROVIDER

BEGIN_PROVIDER [ real*8, eigen_vecs, (mo_num,mo_num) ]
END_PROVIDER

BEGIN_PROVIDER [ real*8, lambda_oda]
END_PROVIDER

BEGIN_PROVIDER [ logical, loda ]
   loda=.true.
END_PROVIDER

