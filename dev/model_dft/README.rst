=========
model_dft
=========

A model DFT program as interface for the xcfun package
 - it sets up the grid
 - performs KS iterations
 - calculates the total energy
 - stops

Hartree-Fock does not pass via libxc, but uses full Coulomb and Exchange
Range-separated PBE uses the routines in the QP2, not libxc

We have to calculate the Coulomb term with "normal" AO integrals,
the exchange part with long-range bielectronic integrals ( erf(mu r)/r )
and add the short-range xc functional.

CAM-B3LYP
E_x=lambda E_x(HF)+(1-lambda)[E_x^lr(HF)+E_x^sr(DFA)]
    = a E_x(HF) + b E_x^lr(HF) + (1-a) E_x(LDA) + (1-b) E_x^sr(LDA)
