! -*- F90 -*-
 BEGIN_PROVIDER [ integer, number_of_functionals]
&BEGIN_PROVIDER [ integer, choice_of_functional ]
BEGIN_DOC
! we choose a functional, which may be composed of 
! several functionals of different types
!
END_DOC
       implicit none
 100   continue
       write(6,*) ' please choose the functional from the following list '
       write(6,*) ' LDA            1 '
       write(6,*) ' PW91           2 '
       write(6,*) ' Colle-Salvetti 3 '
       write(6,*) ' PBE            4 '
       write(6,*) ' B3LYP          5 '
       write(6,*) ' Hartree-Fock   6 '
       write(6,*) ' M06 revised    7 '
       write(6,*) ' CAM-B3LYP      8 '
       write(6,*) ' RS-PBE Hybrid  9 '
       write(6,*) ' RS-LDA Hybrid 10 '
       write(6,*) ' LDA via qp2   11 '
       read(5,*,err=100) choice_of_functional
       if (choice_of_functional.lt.1.or.choice_of_functional.gt.11) then
        write(6,*) ' please choose a number between 1 and 11 '
        go to 100
       end if

       write(6,*) 
       write(6,*) ' using choice ',choice_of_functional
       write(6,*) 

       select case (choice_of_functional)
       case (1)
! LDA VWN5
         number_of_functionals=2
       case (2)
! PW91 XC
         number_of_functionals=2
       case (3)
! HF exchange, Colle-Salvetti correlation
         number_of_functionals=1
       case (4)
! PBE
         number_of_functionals=2
       case (5)
! B3LYP
         number_of_functionals=1
!        number_of_functionals=4
       case (6)
! Hartree-Fock
         number_of_functionals=0
         lexex=.true.
         exmul=1.D0
         srpbe_qp2=.false.
         srlda_qp2=.false.
       case (7)
! M06 L
         number_of_functionals=2
       case (8)
! CAM B3LYP
         number_of_functionals=1
         lexex=.true.
         lexex_lr=.true.
         exmul=0.19D0
         exmul_lr=0.65D0
       case (9)
! RS-PBE from the quantum package
         number_of_functionals=0
         lexex_lr=.true.
         exmul_lr=1.D0
         srpbe_qp2=.true.
   no_vvvv_integrals=.true.
         n_states=1
         write(6,*) ' please give the range-separation parameter mu:'
         read(5,*) mu_erf
         write(6,*)
         write(6,*) ' carrying out the calculation with mu_erf = ',mu_erf
         write(6,*)
       case (10)
! RS-LDA from the quantum package
         number_of_functionals=0
         lexex_lr=.true.
         exmul_lr=1.D0
         srlda_qp2=.true.
   no_vvvv_integrals=.true.
         n_states=1
         write(6,*) ' please give the range-separation parameter mu:'
         read(5,*) mu_erf
         write(6,*)
         write(6,*) ' carrying out the calculation with mu_erf = ',mu_erf
         write(6,*)
       case (11)
! LDA from the quantum package
         number_of_functionals=0
         lda_qp2=.true.
         n_states=1
       end select
    
END_PROVIDER

BEGIN_PROVIDER [logical, lexex]
&BEGIN_PROVIDER [logical, lexex_lr]
&BEGIN_PROVIDER [logical, srpbe_qp2]
&BEGIN_PROVIDER [logical, srlda_qp2]
&BEGIN_PROVIDER [logical, lda_qp2]
&BEGIN_PROVIDER [real*8, exmul]
&BEGIN_PROVIDER [real*8, exmul_lr]
        lexex     = .false.
        lexex_lr  = .false.
        srpbe_qp2 = .false.
        srlda_qp2 = .false.
        lda_qp2   = .false.
END_PROVIDER
                   
 BEGIN_PROVIDER [ integer, xcnum_libxc, (number_of_functionals)]
&BEGIN_PROVIDER [ real*8, weight_func, (number_of_functionals)]
BEGIN_DOC
! composition of different functionals
!
!
END_DOC
       implicit none
       integer :: i

       select case (choice_of_functional)
       case (1)
! LDA VWN5
         xcnum_libxc(1)=1
         xcnum_libxc(2)=7
         weight_func=1.D0
       case (2)
! PW91 XC
         xcnum_libxc(1)=109
         xcnum_libxc(2)=134
         weight_func=1.D0
       case (3)
! HF exchange, Colle-Salvetti correlation
         xcnum_libxc(1)=72
         weight_func=1.D0
         lexex=.true.
         exmul=1.D0
       case (4)
! PBE
         xcnum_libxc(1)=101
         xcnum_libxc(2)=216
         weight_func=1.D0
       case (5)
! B3LYP
         xcnum_libxc(1)=402
         weight_func(1)=1.D0
!        xcnum_libxc(1)=106
!        xcnum_libxc(2)=1
!        xcnum_libxc(2)=131
!        xcnum_libxc(4)=7
!        weight_func(1)=0.72D0
!        weight_func(2)=0.08D0
!        weight_func(3)=0.81D0
!        weight_func(4)=0.19D0
         lexex=.true.
         exmul=0.2D0
       case (6)
! Hartree-Fock
         write(6,*) ' we should not arrive here '
         stop ' Hartree-Fock: we should not arrive here '
       case (7)
! M06 L
         xcnum_libxc(1)=293
         xcnum_libxc(2)=294
         weight_func=1.D0
       case (8)
! CAM B3LYP
         xcnum_libxc(1)=433
         weight_func=1.D0
         lexex=.true.
         lexex_lr=.true.
         exmul=0.2D0
         exmul_lr=0.2D0
       case (9)
! srpbe - we do not pass via libxc
        write(6,*) ' we should not arrive here '
        stop ' srPBE: not via libxc'
       case (10)
! srlda - we do not pass via libxc
        write(6,*) ' we should not arrive here '
        stop ' sLDAE: not via libxc'
       case (11)
! lda of the qp2 - we do not pass via libxc
        write(6,*) ' we should not arrive here '
        stop ' LDA qp2: not via libxc'
       end select

       if (number_of_functionals.ne.0) write(6,*) ' using functionals No ',(xcnum_libxc(i),i=1,number_of_functionals)
    
END_PROVIDER
