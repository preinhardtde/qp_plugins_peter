#!/usr/bin/env bats

source $QP_ROOT/tests/bats/common.bats.sh
source $QP_ROOT/quantum_package.rc

# we create an ezfio for our purposes

function run() {
  thresh=1.e-8
  test_exe model_dft || skip
  EZFIO=$1.model_dft.ezfio
  LOG=$1.model_dft.log
  ILOG=$1.DFT.log
  rm -rf $EZFIO || skip
  qp_create_ezfio -b $3 -o $EZFIO $2
  qp set_file $EZFIO
  qp edit --check

  echo 1 > input_model_dft.tmp
  qp run model_dft < input_model_dft.tmp > $ILOG
  data="$(grep conver $ILOG | awk '{print $6}')"
  echo 'LDA ok'  $data $4 > $LOG
  eq $data $4 $thresh
  rm $ILOG
  shift

  echo 2 > input_model_dft.tmp
  qp run model_dft < input_model_dft.tmp > $ILOG
  data="$(grep conver $ILOG | awk '{print $6}')"
  echo 'PW91 ok'  $data $4 >> $LOG
  eq $data $4 $thresh
  rm $ILOG
  shift

  echo 3 > input_model_dft.tmp
  qp run model_dft < input_model_dft.tmp > $ILOG
  data="$(grep conver $ILOG | awk '{print $6}')"
  echo 'ColleSalvetti ok'  $data $4 >> $LOG
  eq $data $4 $thresh
  rm $ILOG
  shift

  echo 4 > input_model_dft.tmp
  qp run model_dft < input_model_dft.tmp > $ILOG
  data="$(grep conver $ILOG | awk '{print $6}')"
  echo 'PBE ok'  $data $4 >> $LOG
  eq $data $4 $thresh
  rm $ILOG
  shift

  echo 5 > input_model_dft.tmp
  qp run model_dft < input_model_dft.tmp > $ILOG
  data="$(grep conver $ILOG | awk '{print $6}')"
  echo 'B3LYP ok'  $data $4 >> $LOG
  eq $data $4 $thresh
  rm $ILOG
  shift

  echo 6 > input_model_dft.tmp
  qp run model_dft < input_model_dft.tmp > $ILOG
  data="$(grep conver $ILOG | awk '{print $6}')"
  echo 'Hartree-Fock ok'  $data $4 >> $LOG
  eq $data $4 $thresh
  rm $ILOG
  shift

  echo 7 > input_model_dft.tmp
  qp run model_dft < input_model_dft.tmp > $ILOG
  data="$(grep conver $ILOG | awk '{print $6}')"
  echo 'M06rev ok' $data $4 >> $LOG
  eq $data $4 $thresh
  rm $ILOG
  shift

  echo 8 > input_model_dft.tmp
  qp run model_dft < input_model_dft.tmp > $ILOG
  data="$(grep conver $ILOG | awk '{print $6}')"
  echo 'CAM-B3LYP ok'  $data $4 >> $LOG
  eq $data $4 $thresh
  rm $ILOG
  shift

  echo 9 > input_model_dft.tmp
  echo 0.4 >> input_model_dft.tmp
  qp run model_dft < input_model_dft.tmp > $ILOG
  data="$(grep conver $ILOG | awk '{print $6}')"
  echo 'RS-PBE 0.4 ok'  $data $4 >> $LOG
  eq $data $4 $thresh
  rm $ILOG
  shift

  echo 10 > input_model_dft.tmp
  echo 0.4 >> input_model_dft.tmp
  qp run model_dft < input_model_dft.tmp > $ILOG
  data="$(grep conver $ILOG | awk '{print $6}')"
  echo 'RS-LDA 0.4 ok'  $data $4 >> $LOG
  eq $data $4 $thresh
  rm $ILOG
  shift

  echo 11 > input_model_dft.tmp
  echo 0.4 >> input_model_dft.tmp
  qp run model_dft < input_model_dft.tmp > $ILOG
  data="$(grep conver $ILOG | awk '{print $6}')"
  echo 'LDA qp2 ok'  $data $4 >> $LOG
  eq $data $4 $thresh
  rm $ILOG

  rm -rf $LOG

}

@test "N2" { # 8.648100 13.754s
  run n2 n2.xyz 6-31g -107.928067398180 -108.779391560437 -109.272570848668 -109.356091028143 -109.468408177918 -108.867768924707 -109.421887764435 -109.122791207332 -109.324287861396 -108.644290109199 -108.581557355643 
}

 @test "B-B" { # 3s
   run b2_stretched b2_stretched.zmt cc-pvdz -48.462138367737 -49.009142639793 -48.914331598271 -48.961991464095 -49.048120852129 -48.696923797392 -48.967819639172 -48.762139295878 -48.873544253856 -48.465245984559 -48.460150505091
 }




