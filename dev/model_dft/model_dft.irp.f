! -*- F90 -*-
program model_dft
  implicit none
  BEGIN_DOC
! the SCF model we created with Fernand and Anthony, long ago
  END_DOC
         
real*8 :: cpu0,wall0
          call cpu_time(cpu0)
          call wall_time(wall0)
          call header
          call setup
          call create_guess
          call orthonormalize_mos
          call driver
          call cpustamp(cpu0,wall0,cpu0,wall0,'All ')
end program model_dft

     subroutine header
       write(6,*)
       write(6,*) ' the SCF model program from Toulouse, june 2009 '
       write(6,*) ' le sous-sol de Fernand en juin, avec Anthony '
       write(6,*) '  pour le qp2, P Reinhardt, may 2020 '
       write(6,*) '  version coupled to DFT via the libxc library '
       write(6,*)
       write(6,*)
       write(6,*) ' Canonical Restricted Kohn Sham '
       canonical_scf=.true.
       write(6,*)
     end subroutine header

     subroutine setup
       implicit none
       integer :: i
       read_wf=.true.
       touch read_wf
       n_points_radial_grid=75
       n_points_integration_angular=302

       level_shift=1.D0

       write(6,*) ' setting up the functionals rad, ang ',n_points_radial_grid  &
           ,n_points_integration_angular

       if (number_of_functionals.gt.0) then
        do i=1,number_of_functionals
         call setup_libxc(xcnum_libxc(i),func_name_xc(i) &
             ,is_lda_libxc(i),is_gga_libxc(i),is_metagga_libxc(i)      &
             ,is_exch_libxc(i),is_corr_libxc(i))
        end do
       end if

     end subroutine setup

     subroutine driver
       implicit none
       real*8 :: E_old,delta_P
       real*8 :: delta_E,pp
       integer :: i,j,iter       

       write(6,*) 
real*8 :: dmin
       dmin=1.D12
       do i=1,nucl_num
        do j=i+1,nucl_num
         dmin=min(dmin,nucl_dist(i,j))
        end do
       end do
       write(6,*) ' Minimum distance beween atom found is ',dmin
       write(6,*) 
       write(6,*) 
       
!      write(6,*) ' basis set information '
!      do i=1,ao_num
!       write(6,*) ' ao_l = ',i,ao_l(i)
!      end do

       dens_old=0.D0
       delta_e=huge(1.d0)
       delta_p=huge(1.d0)
       do while (delta_E.ge.thr_scf.or.delta_P.ge.thr_P)

       write(6,*) 
       write(6,*)  ' ============================================ '
       write(6,*)  '    Iteration No ',iter_scf
       write(6,*)  ' ============================================ '
       write(6,*)  '                Iteration No ',iter_scf
       write(6,*)  ' ============================================ '
       write(6,*) 
       e_old=total_energy
       if (number_of_functionals.ne.0) then
        e_xc_libxc_old=e_xc_libxc
       end if
       if (srPBE_qp2) then
        e_xc_srPBE_qp2_old=e_xc_srPBE_qp2
       end if
       if (srLDA_qp2) then
        e_xc_srLDA_qp2_old=e_xc_srLDA_qp2
       end if
       if (LDA_qp2) then
        e_xc_LDA_qp2_old=e_xc_LDA_qp2
       end if

       delta_P=0.d0
       do i=1,ao_num
        do j=1,ao_num
         pp=p_next(i,j)-density(i,j)
         delta_P=delta_P+pp*pp
        end do
       end do
       delta_P=delta_P/(ao_num*ao_num)
       
       total_energy = nuclear_repulsion + e_one + e_two
       if (number_of_functionals.ne.0) then
        do i=1,number_of_functionals
         write(6,*) ' DFT contribution       ',e_xc_libxc(i),'  (',trim(func_name_xc(i)),')'
         total_energy+=E_xc_libxc(i)
        end do
       end if
       if (srPBE_qp2) then
        provide e_xc_srPBE_qp2
        write(6,*) ' srDFT contribution       ',e_xc_srPBE_qp2,'  (srPBE functional)'
        total_energy+=e_xc_srPBE_qp2
       end if
       if (srLDA_qp2) then
        provide e_xc_srLDA_qp2
        write(6,*) ' srDFT contribution       ',e_xc_srLDA_qp2,'  (srLDA functional)'
        total_energy+=e_xc_srLDA_qp2
       end if
       if (LDA_qp2) then
        provide e_xc_LDA_qp2
        write(6,*) ' DFT contribution       ',e_xc_LDA_qp2,'  (LDA functional, qp2)'
        total_energy+=e_xc_LDA_qp2
       end if
       delta_e=abs(total_energy-e_old)
       write(6,*) ' nuclear repulsion   is ',nuclear_repulsion
       write(6,*) '          e_one  ',e_one
       write(6,*) '          e_two  ',e_two
       write(6,*) ' total energy is ',total_energy
       write(6,*) ' iter ',iter_scf, 'delta_E:', total_energy - E_old
       write(6,*) ' Delta P :',delta_P
       iter_scf=iter_scf+1

! the optimal damping algorithm of Eric Cancès and Claude Le Bris
       if (iter_scf.gt.2) then
real*8 :: a1,a2,b1,b2,b3
        a1=0.D0
        a2=0.D0
        b1=0.D0
        b2=0.D0
        b3=0.D0
integer :: alpha,beta
        do alpha=1,ao_num
         do beta=1,ao_num
          a1+=ao_one_e_integrals(alpha,beta)*dens_old(alpha,beta)
          a2+=ao_one_e_integrals(alpha,beta)*density(alpha,beta)
          if (number_of_functionals.ne.0) then
           do i=1,number_of_functionals
            a1+=e_xc_libxc_old(i)
            a2+=e_xc_libxc(i)
           end do
          end if
          if (srPBE_qp2) then
           a1+=e_xc_srPBE_qp2_old
           a2+=e_xc_srPBE_qp2
          end if
          if (srLDA_qp2) then
           a1+=e_xc_srLDA_qp2_old
           a2+=e_xc_srLDA_qp2
          end if
          if (LDA_qp2) then
           a1+=e_xc_LDA_qp2_old
           a2+=e_xc_LDA_qp2
          end if
          b1+=xi_old(alpha,beta)*dens_old(alpha,beta)
          b2+=xi_old(alpha,beta)*density(alpha,beta)
          b3+=xi_scf(alpha,beta)*density(alpha,beta)
         end do
        end do
        b1*=0.5D0
        b3*=0.5D0
real*8 :: oda_denom
        oda_denom=b1-b2+b3
        write(6,*) ' Optimal Damping Algorithm of Cances et al.'
        if ((oda_denom).lt.0.D0) then
         lambda_oda=1.D0
        else
         if (abs(oda_denom).lt.1.D-12) then
          lambda_oda=0.5D0
         else
          lambda_oda=-(a2-a1+b2-2.D0*b1)/oda_denom*0.5D0
         end if
        end if
        write(6,*) ' optimal lambda is ',lambda_oda
        if (lambda_oda.lt.0.D0.or.lambda_oda.gt.1.D0) then
         write(6,*) ' lambda outside [0,1], setting lambda to 1 '
         lambda_oda=1.0D0
       end if
       else
        lambda_oda=1.D0
       end if

!      lambda_oda=min(0.6,lambda_oda)
       lambda_oda=1.D0

       dens_old=density
       density = p_next
       xi_old=xi_scf

       touch density
         mo_coef=ci
       soft_touch mo_coef

       if (iter_scf.gt.max_iter) then
        write(6,*) ' max number of iterations ',max_iter,' attained '
        exit
       end if

       end do

       total_energy = nuclear_repulsion + e_one + e_two
       write(6,*) ' nuclear repulsion   is ',nuclear_repulsion
       write(6,*) ' one-electron energy is ',e_one
       write(6,*) ' two-electron energy is ',e_two
       if (number_of_functionals.ne.0) then
        do i=1,number_of_functionals
         total_energy+=E_xc_libxc(i)
        end do
       end if
       if (srPBE_qp2) then
        total_energy+=e_xc_srPBE_qp2
        write(6,*) ' range-separated Kohn-Sham total energy is ',total_energy
        write(6,*) ' adding long-range MP2 correlation energy  ',e_lrMP2
        total_energy+=e_lrMP2
       end if
       if (srLDA_qp2) then
        total_energy+=e_xc_srLDA_qp2
        write(6,*) ' range-separated Kohn-Sham total energy is ',total_energy
        write(6,*) ' adding long-range MP2 correlation energy  ',e_lrMP2
        total_energy+=e_lrMP2
       end if
       if (LDA_qp2) then
        total_energy+=e_xc_LDA_qp2
       end if
       write(6,*) 
       write(6,*) ' total energy is ',total_energy
 
       
       write(6,9904) total_energy,delta_E,delta_P,iter_scf
9904   format(//,70('='),//,' converged solution : E_DFT = ',F20.12,/, & 
            ' Delta E = ',E15.5,'  delta_P  =  ',E15.5,/, &
            ' at iteration No ',i4,//)

         mo_label = "Canonical"
         mo_coef=ci
         call save_mos
         write(6,*) ' saving orbitals,label =  ',mo_label
         write(6,*) ' all done '

     end subroutine driver
     
