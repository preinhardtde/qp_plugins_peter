! -*- F90 -*-
BEGIN_PROVIDER [ real*8, xi_old, (ao_num, ao_num) ]
END_PROVIDER

  BEGIN_PROVIDER [ real*8, xi_scf, (ao_num, ao_num) ]
 &BEGIN_PROVIDER [ real*8, xi_coul, (ao_num, ao_num) ]
 &BEGIN_PROVIDER [ real*8, xi_exch, (ao_num, ao_num) ]
 &BEGIN_PROVIDER [ real*8, xi_exch_lr, (ao_num, ao_num) ]
 use map_module
 implicit none
 BEGIN_DOC
 ! the 2-electron part of the Fock matrix
 END_DOC

 integer                        :: i,j,k,l,k1,r,s
 integer                        :: i0,j0,k0,l0
 integer*8                      :: p,q
 double precision               :: integral, c0, c1, c2
 double precision               :: ao_two_e_integral, local_threshold
 double precision, allocatable  :: ao_two_e_integral_coul(:,:)
 double precision, allocatable  :: ao_two_e_integral_exch(:,:)
 real*8 :: cpu0,wall0,cpu1,wall1,cpu2,wall2

   xi_coul = 0.d0
   xi_exch = 0.d0
   write(6,*) ' constructing 2el part xi '
   PROVIDE ao_two_e_integrals_in_map

   integer(omp_lock_kind) :: lck(ao_num)
   integer(map_size_kind)     :: i8
   integer                        :: ii(8), jj(8), kk(8), ll(8), k2
   integer(cache_map_size_kind)   :: n_elements_max, n_elements
   integer(key_kind), allocatable :: keys(:)
   double precision, allocatable  :: values(:)

   !$OMP PARALLEL DEFAULT(NONE)                                      &
       !$OMP PRIVATE(i,j,l,k1,k,integral,ii,jj,kk,ll,i8,keys,values,n_elements_max, &
       !$OMP  n_elements,ao_two_e_integral_coul, &
       !$OMP             ao_two_e_integral_exch) &
       !$OMP SHARED(ao_num,density,&
       !$OMP  ao_integrals_map, xi_coul, xi_exch)

   call get_cache_map_n_elements_max(ao_integrals_map,n_elements_max)
   allocate(keys(n_elements_max), values(n_elements_max))
   allocate(ao_two_e_integral_coul(ao_num,ao_num))
   allocate(ao_two_e_integral_exch(ao_num,ao_num))
   ao_two_e_integral_coul = 0.d0
   ao_two_e_integral_exch = 0.d0

   !$OMP DO SCHEDULE(static,1)
   do i8=0_8,ao_integrals_map%map_size
     n_elements = n_elements_max
     call get_cache_map(ao_integrals_map,i8,keys,values,n_elements)
     do k1=1,n_elements
       call two_e_integrals_index_reverse(kk,ii,ll,jj,keys(k1))

       do k2=1,8
         if (kk(k2)==0) then
           cycle
         endif
         i = ii(k2)
         j = jj(k2)
         k = kk(k2)
         l = ll(k2)
         integral = density(k,l) * values(k1)
         ao_two_e_integral_coul(i,j) += integral
         integral = values(k1)
         ao_two_e_integral_exch(l,j) -= 0.5D0*density(k,i) * integral
       enddo
     enddo
   enddo
   !$OMP END DO NOWAIT
   !$OMP CRITICAL
   xi_coul += ao_two_e_integral_coul
   xi_exch += ao_two_e_integral_exch
   !$OMP END CRITICAL
   deallocate(keys,values,ao_two_e_integral_coul)
   deallocate(ao_two_e_integral_exch)
   !$OMP END PARALLEL

   xi_scf=xi_coul
   if (lexex) then
    xi_scf+=exmul*xi_exch
   end if

   if (lexex_lr) then
! the same operations but now for the lr exchange only using the erf integrals
    xi_exch_lr=0.D0
! exchange part 
! F_ij = h_ij + sum_kl P_kl [(ij|kl) - (1/2)(ik|lj)]
!real*8 :: ao_two_e_integral_erf,ss,h_iklj
!   do i=1,ao_num
!    do j=1,ao_num
!     ss=0.D0
!     do k=1,ao_num
!      do l=1,ao_num
!       h_iklj=ao_two_e_integral_erf(i,k,l,j)
!       ss+=density(k,l)*h_iklj
!      end do
!     end do
!     xi_exch(i,j)=ss
!    end do
!   end do
!   xi_exch*=-0.5D0



   write(6,*) ' constructing 2el erf part xi_exch '
   PROVIDE ao_two_e_integrals_erf_in_map

   !$OMP PARALLEL DEFAULT(NONE)                                      &
       !$OMP PRIVATE(i,j,l,k1,k,integral,ii,jj,kk,ll,i8,keys,values,n_elements_max, &
       !$OMP  n_elements, &
       !$OMP             ao_two_e_integral_exch) &
       !$OMP SHARED(ao_num,density,&
       !$OMP  ao_integrals_erf_map, xi_exch_lr)

   call get_cache_map_n_elements_max(ao_integrals_erf_map,n_elements_max)
   allocate(keys(n_elements_max), values(n_elements_max))
   allocate(ao_two_e_integral_exch(ao_num,ao_num))
   ao_two_e_integral_exch = 0.d0

   !$OMP DO SCHEDULE(static,1)
   do i8=0_8,ao_integrals_erf_map%map_size
     n_elements = n_elements_max
     call get_cache_map(ao_integrals_erf_map,i8,keys,values,n_elements)
     do k1=1,n_elements
       call two_e_integrals_index_reverse(kk,ii,ll,jj,keys(k1))

       do k2=1,8
         if (kk(k2)==0) then
           cycle
         endif
         i = ii(k2)
         j = jj(k2)
         k = kk(k2)
         l = ll(k2)
         integral = values(k1)
         ao_two_e_integral_exch(l,j) -= 0.5D0*density(k,i) * integral
       enddo
     enddo
   enddo
   !$OMP END DO NOWAIT
   !$OMP CRITICAL
   xi_exch_lr += ao_two_e_integral_exch
   !$OMP END CRITICAL
   deallocate(keys,values)
   deallocate(ao_two_e_integral_exch)
   !$OMP END PARALLEL


    xi_scf+=exmul_lr*xi_exch_lr
   end if

END_PROVIDER

