! -*- F90 -*-
 BEGIN_PROVIDER [real*8, density_points, (n_points_final_grid,2)]
&BEGIN_PROVIDER [real*8, grad_density_points, (3,n_points_final_grid,2)]
BEGIN_DOC
!
! put the density and its gradient in two arrays, to be used later
!
END_DOC
  implicit none
  integer :: i,j

  do i=1,n_points_final_grid
   density_points(i,1)=one_e_dm_and_grad_alpha_in_r(4,i,1)
   density_points(i,2)=one_e_dm_and_grad_beta_in_r(4,i,1)
   do j=1,3
    grad_density_points(j,i,1)=one_e_dm_and_grad_alpha_in_r(j,i,1)
    grad_density_points(j,i,2)=one_e_dm_and_grad_beta_in_r(j,i,1)
   end do
  end do

  write(6,*) ' density and gradient available at the grid points '

END_PROVIDER

 BEGIN_PROVIDER [real*8, laplace_points, (n_points_final_grid,2)]
&BEGIN_PROVIDER [real*8, tau_points, (n_points_final_grid,2)]
    implicit none
    integer :: i,j
    real*8 :: r(3)
    real*8 :: dm_a(N_states),dm_b(N_states)
    real*8 :: grad_dm_a(3,N_states),grad_dm_b(3,N_states)
    real*8 :: lapl_dm_a(3,N_states),lapl_dm_b(3,N_states)
    real*8 :: aos_array(ao_num)
    real*8 :: grad_aos_array(3,ao_num)
    real*8 :: lapl_aos_array(3,ao_num)

    laplace_points=0.D0
    tau_points=0.D0
    do i=1,n_points_final_grid
     r(1) = final_grid_points(1,i)
     r(2) = final_grid_points(2,i)
     r(3) = final_grid_points(3,i)
     call density_and_grad_lapl_alpha_beta_and_all_aos_and_grad_aos_at_r(  &
       r,dm_a,dm_b, grad_dm_a, grad_dm_b, lapl_dm_a, lapl_dm_b, aos_array, grad_aos_array, lapl_aos_array)

     laplace_points(i,1)=lapl_dm_a(1,1)+lapl_dm_a(2,1)+lapl_dm_a(3,1)
     laplace_points(i,2)=lapl_dm_b(1,1)+lapl_dm_b(2,1)+lapl_dm_b(3,1)

integer :: ia
     do ia=1,elec_alpha_num
      tau_points(i,1)+=mos_grad_in_r_array(ia,i,1)*mos_grad_in_r_array(ia,i,1)  & 
                 + mos_grad_in_r_array(ia,i,2)*mos_grad_in_r_array(ia,i,2)  & 
                 + mos_grad_in_r_array(ia,i,3)*mos_grad_in_r_array(ia,i,3)
     end do
     do ia=1,elec_beta_num
      tau_points(i,2)+=mos_grad_in_r_array(ia,i,1)*mos_grad_in_r_array(ia,i,1)  & 
                 + mos_grad_in_r_array(ia,i,2)*mos_grad_in_r_array(ia,i,2)  & 
                 + mos_grad_in_r_array(ia,i,3)*mos_grad_in_r_array(ia,i,3)
     end do
    end do
    tau_points*=0.5D0

  write(6,*) ' laplacian and kinetic energy density available at the grid points '

END_PROVIDER
