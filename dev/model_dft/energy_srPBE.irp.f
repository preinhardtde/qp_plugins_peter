! -*- F90 -*-
BEGIN_PROVIDER [real*8, E_xc_srPBE_qp2_old]
END_PROVIDER

BEGIN_PROVIDER [real*8, E_xc_srPBE_qp2]
BEGIN_DOC
!
! evaluate the sr xc PBE functional on the grid points
! closed-shell, or unpolarized version
!
END_DOC
      implicit none
      integer :: i,j
      real*8  :: r(3)
      real*8  :: weight
      logical :: unpolarized_system

      e_xc_srPBE_qp2 = energy_x_sr_pbe(1)+energy_c_sr_pbe(1)
      write(6,*) ' E_x (srPBE) = ',energy_x_sr_pbe(1),'E_c (srPBE) = ',energy_c_sr_pbe(1)
 
END_PROVIDER

 BEGIN_PROVIDER [real*8, va_xc_srPBE_qp2, (ao_num,ao_num)]
&BEGIN_PROVIDER [real*8, vb_xc_srPBE_qp2, (ao_num,ao_num)]
     implicit none
     integer :: i,j
     logical :: unpolarized_system

     unpolarized_system=.true.
     do i=1,n_points_final_grid
      if (abs(density_points(i,1)-density_points(i,2)).gt.1.D-6) unpolarized_system=.false.
     end do

     write(6,*) ' unpolarized system ? ',unpolarized_system

     if (unpolarized_system) then
      do i=1,ao_num
       do j=1,ao_num
        va_xc_srPBE_qp2(i,j)=potential_xc_alpha_ao_sr_pbe(i,j,1)
       end do
      end do
      va_xc_srPBE_qp2*=0.5D0
      vb_xc_srPBE_qp2=va_xc_srPBE_qp2
     else
      stop ' we still have to think a little '
     end if
     write(6,*) ' provided the srPBE potential for mu = ',mu_erf
END_PROVIDER
