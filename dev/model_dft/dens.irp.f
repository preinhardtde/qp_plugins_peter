! -*- F90 -*-
BEGIN_PROVIDER [ real*8, p_next, (ao_num,ao_num) ]
      implicit none 
      integer :: ialph,ibeta,iorb
      real*8 :: cia
      
      write(6,*) ' constructing density p_next '
      
      p_next=0.d0
      
      do iorb=1,nocc
       do ialph=1,ao_num
        cia=ci(ialph,iorb)
        do ibeta=1,ao_num
         p_next(ialph,ibeta)+=cia*ci(ibeta,iorb)
        end do
       end do
      end do
      
      p_next*=2.D0

END_PROVIDER
