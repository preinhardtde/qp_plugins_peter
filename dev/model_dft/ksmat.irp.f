! -*- F90 -*-
BEGIN_PROVIDER [real*8, ksmat_libxc, (ao_num,ao_num)]
      implicit none
      ksmat_libxc=0.D0
      ksmat_libxc+=va_xc_libxc + vb_xc_libxc
!     call print_mat(ksmat_libxc,ao_num,ao_num)
!     ksmat_libxc*=2.D0
END_PROVIDER

BEGIN_PROVIDER [real*8, ksmat_srPBE_qp2, (ao_num,ao_num)]
      implicit none
      ksmat_srPBE_qp2=0.D0
      ksmat_srPBE_qp2=va_xc_srPBE_qp2 + vb_xc_srPBE_qp2
!     call print_mat(ksmat_srPBE_qp2,ao_num,ao_num)
END_PROVIDER

BEGIN_PROVIDER [real*8, ksmat_srLDA_qp2, (ao_num,ao_num)]
      implicit none
      ksmat_srLDA_qp2=0.D0
      ksmat_srLDA_qp2=va_xc_srLDA_qp2 + vb_xc_srLDA_qp2
END_PROVIDER

BEGIN_PROVIDER [real*8, ksmat_LDA_qp2, (ao_num,ao_num)]
      implicit none
      ksmat_LDA_qp2=0.D0
      ksmat_LDA_qp2=va_xc_LDA_qp2 + vb_xc_LDA_qp2
END_PROVIDER


