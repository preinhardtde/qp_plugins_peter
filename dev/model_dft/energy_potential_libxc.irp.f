! -*- F90 -*-
 BEGIN_PROVIDER [logical, is_corr_libxc, (number_of_functionals)]
&BEGIN_PROVIDER [logical, is_exch_libxc, (number_of_functionals)]
&BEGIN_PROVIDER [logical, is_lda_libxc, (number_of_functionals)]
&BEGIN_PROVIDER [logical, is_gga_libxc, (number_of_functionals)]
&BEGIN_PROVIDER [logical, is_metagga_libxc, (number_of_functionals)]
END_PROVIDER

BEGIN_PROVIDER [real*8, E_xc_libxc_old, (number_of_functionals)]
END_PROVIDER

 BEGIN_PROVIDER [real*8, E_xc_libxc, (number_of_functionals)]
&BEGIN_PROVIDER [real*8, aos_vxc_alpha_libxc, (ao_num,n_points_final_grid)]
&BEGIN_PROVIDER [real*8, aos_vxc_beta_libxc,  (ao_num,n_points_final_grid)]
BEGIN_DOC
!
! evaluate the exchange functional on the grid points
! for each functional we evaluate the energy contribution
! but add all contributions for the KS matrix
! closed-shell, or unpolarized version
!
END_DOC
      implicit none
      integer :: i,j
      real*8  :: r(3)
      real*8  :: weight
      real*8, allocatable :: excarray(:),vaxcarray(:),vbxcarray(:)
      logical :: unpolarized_system

      allocate (excarray(n_points_final_grid))
      allocate (vaxcarray(n_points_final_grid))
      allocate (vbxcarray(n_points_final_grid))

      e_xc_libxc = 0.d0
      aos_vxc_alpha_libxc=0.D0
      aos_vxc_beta_libxc =0.D0

      unpolarized_system=.true.
      do i=1,n_points_final_grid
       if (abs(density_points(i,1)-density_points(i,2)).gt.1.D-6) unpolarized_system=.false.
      end do


     do i=1,number_of_functionals
      if (unpolarized_system) then
! alpha and beta spin densities are equal
       if (is_lda_libxc(i)) then
        write(6,*) ' evaluating a LDA '
! we pass the density
        call calc_xc_lda_libxc(xcnum_libxc(i),n_points_final_grid,density_points,excarray,vaxcarray)
       else
        if (is_gga_libxc(i)) then
         write(6,*) ' evaluating a GGA '
! we pass the density and the gradient
         call calc_xc_gga_libxc(xcnum_libxc(i),n_points_final_grid,density_points &
             ,grad_density_points,excarray,vaxcarray)
        else
         if (is_metagga_libxc(i)) then
          write(6,*) ' evaluating a MGGA '
! we pass the density and the gradient
          call calc_xc_mgga_libxc(xcnum_libxc(i),n_points_final_grid,density_points &
             ,grad_density_points,laplace_points,tau_points,excarray,vaxcarray)
         else
          write(6,*) ' neither lda, nor gga, nor meta-gga, cannot handle yet '
          excarray =0.D0
          vaxcarray=0.D0
          vbxcarray=0.D0
         end if
        end if
       end if
       vbxcarray=0.5*vaxcarray
       vaxcarray=vbxcarray
      else
! we have a spin polarized system, with different alpha and beta spins
       if (is_lda_libxc(i)) then
        write(6,*) ' evaluating a LDA '
! we pass the density
        call calc_xc_lda_libxc_sp(xcnum_libxc(i),n_points_final_grid &
               ,density_points,excarray,vaxcarray,vbxcarray)
       else
        if (is_gga_libxc(i)) then
         write(6,*) ' evaluating a GGA '
! we pass the density and the gradient
         call calc_xc_gga_libxc_sp(xcnum_libxc(i),n_points_final_grid,density_points &
             ,grad_density_points,excarray,vaxcarray,vbxcarray)
        else
         if (is_metagga_libxc(i)) then
          write(6,*) ' evaluating a MGGA '
! we pass the density and the gradient
          call calc_xc_mgga_libxc_sp(xcnum_libxc(i),n_points_final_grid,density_points &
             ,grad_density_points,laplace_points,tau_points &
               ,excarray,vaxcarray,vbxcarray)
         else
          write(6,*) ' neither lda, nor gga, nor meta-gga, cannot handle yet '
          excarray =0.D0
          vaxcarray=0.D0
          vbxcarray=0.D0
         end if
        end if
       end if
      end if
      write(6,*) ' ... done  '
       
      e_xc_libxc(i) = 0.d0
      do j = 1, n_points_final_grid
!      if (abs(excarray(j)-energy_x_pbe_contrib(j,1)).gt.1.D-5) then
!         write(6,*) ' DIFFFF ',j,excarray(j),energy_x_pbe_contrib(j,1),excarray(j)-energy_x_pbe_contrib(j,1)
!      end if
       weight = final_weight_at_r_vector(j)
       e_xc_libxc(i) += weight * excarray(j)
       vaxcarray(j)*=weight_func(i)
       vbxcarray(j)*=weight_func(i)
!
! fill the array aos_vxc_alpha_libxc etc in the same time
!
integer :: k
       do k =1, ao_num
        aos_vxc_alpha_libxc(k,j) += vaxcarray(j) * aos_in_r_array(k,j)*weight
        aos_vxc_beta_libxc (k,j) += vbxcarray(j) * aos_in_r_array(k,j)*weight
       end do
      end do
     end do

      deallocate (excarray)
      deallocate (vaxcarray)
      deallocate (vbxcarray)

END_PROVIDER

 BEGIN_PROVIDER [real*8, va_xc_libxc, (ao_num,ao_num)]
&BEGIN_PROVIDER [real*8, vb_xc_libxc, (ao_num,ao_num)]
BEGIN_DOC
!
! assemble the XC part of the Kohn-Sham matrix in AOs
!
END_DOC
      implicit none
      integer :: istate

      call dgemm('N','T',ao_num,ao_num,n_points_final_grid,1.d0,         &
          aos_in_r_array,size(aos_in_r_array,1),                         &
          aos_vxc_alpha_libxc,size(aos_vxc_alpha_libxc,1),0.d0,&
          va_xc_libxc,size(va_xc_libxc,1))
      call dgemm('N','T',ao_num,ao_num,n_points_final_grid,1.d0,         &
          aos_in_r_array,size(aos_in_r_array,1),                         &
          aos_vxc_beta_libxc,size(aos_vxc_beta_libxc,1),0.d0,&
          vb_xc_libxc,size(vb_xc_libxc,1))

END_PROVIDER
