! -*- F90 -*-
BEGIN_PROVIDER [real*8, E_xc_srLDA_qp2_old]
END_PROVIDER

BEGIN_PROVIDER [real*8, E_xc_srLDA_qp2]
BEGIN_DOC
!
! evaluate the sr xc LDA functional on the grid points
! closed-shell, or unpolarized version
!
END_DOC
      implicit none
      integer :: i,j
      real*8  :: r(3)
      real*8  :: weight
      logical :: unpolarized_system

      e_xc_srLDA_qp2 = energy_x_sr_LDA(1)+energy_c_sr_LDA(1)
      write(6,*) ' E_x (srLDA) = ',energy_x_sr_LDA(1),'E_c (srLDA) = ',energy_c_sr_LDA(1)
 
END_PROVIDER

 BEGIN_PROVIDER [real*8, va_xc_srLDA_qp2, (ao_num,ao_num)]
&BEGIN_PROVIDER [real*8, vb_xc_srLDA_qp2, (ao_num,ao_num)]
     implicit none
     integer :: i,j
     do i=1,ao_num
      do j=1,ao_num
       va_xc_srLDA_qp2(i,j)=potential_xc_alpha_ao_sr_LDA(i,j,1)
!      vb_xc_srLDA_qp2(i,j)=potential_xc_beta_ao_sr_LDA(i,j,1)
      end do
     end do
     write(6,*) ' provided the srLDA potential '
     va_xc_srLDA_qp2*=0.5D0
     vb_xc_srLDA_qp2=va_xc_srLDA_qp2
END_PROVIDER
