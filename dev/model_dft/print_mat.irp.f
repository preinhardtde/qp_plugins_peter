
      subroutine print_mat(mat,ndim,nvec)
        implicit none
        integer :: ndim,nvec,i,j
        real*8 :: mat(ndim,ndim)

        write(6,*)
        write(6,*) ' printing a matrix '
        do i=1,nvec
         write(6,*) ' Line No ',i
         write(6,'(5(i4,F12.6))') (j,mat(i,j),j=1,ndim)
         write(6,*)
        end do
        write(6,*)
      end subroutine print_mat
