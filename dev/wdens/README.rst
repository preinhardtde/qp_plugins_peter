=====
wdens
=====

This plugin retrieves the information after a FCI and prepares the 
orbital-optimization step in the MCSCF procedure

- we generate a 1-particle and a 2-particle density matrix
- we create natural orbitals, and transform the 2-particle density matrix
- we extract from the MO integrals those necessary for the MCSCF 
  and transform them on the natural orbitals
- we store the natural orbitals on the ezfio file as MCSCF orbitals
