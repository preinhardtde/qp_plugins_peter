! -*- F90 -*-
use bitmasks ! you need to include the bitmasks_module.f90 features

BEGIN_PROVIDER [logical, three_particle_matrix]
END_PROVIDER 

 BEGIN_PROVIDER [real*8, D0tu, (n_act_orb,n_act_orb) ]
&BEGIN_PROVIDER [real*8, P0tuvx, (n_act_orb,n_act_orb,n_act_orb,n_act_orb) ]
BEGIN_DOC
! the first-order density matrix in the basis of the starting MOs
! the second-order density matrix in the basis of the starting MOs
! matrices are state averaged
!
! we use the spin-free generators of mono-excitations
! E_pq destroys q and creates p
! D_pq   =     <0|E_pq|0> = D_qp
! P_pqrs = 1/2 <0|E_pq E_rs - delta_qr E_ps|0>
!
END_DOC
      implicit none
      integer :: t,u,v,x,mu,nu,istate,ispin,jspin,ihole,ipart,jhole,jpart
      integer :: ierr
      integer(bit_kind), allocatable :: det_mu(:,:)
      integer(bit_kind), allocatable :: det_mu_ex(:,:)
      integer(bit_kind), allocatable :: det_mu_ex1(:,:)
      integer(bit_kind), allocatable :: det_mu_ex11(:,:)
      integer(bit_kind), allocatable :: det_mu_ex12(:,:)
      integer(bit_kind), allocatable :: det_mu_ex2(:,:)
      integer(bit_kind), allocatable :: det_mu_ex21(:,:)
      integer(bit_kind), allocatable :: det_mu_ex22(:,:)
      real*8 :: phase1,phase11,phase12,phase2,phase21,phase22
      integer :: nu1,nu2,nu11,nu12,nu21,nu22
      integer :: ierr1,ierr2,ierr11,ierr12,ierr21,ierr22
      real*8 :: cI_mu(N_states),term
      allocate(det_mu(N_int,2))
      allocate(det_mu_ex(N_int,2))
      allocate(det_mu_ex1(N_int,2))
      allocate(det_mu_ex11(N_int,2))
      allocate(det_mu_ex12(N_int,2))
      allocate(det_mu_ex2(N_int,2))
      allocate(det_mu_ex21(N_int,2))
      allocate(det_mu_ex22(N_int,2))

      write(6,*) ' providing density matrices D0 and P0 '

! set all to zero
       do t=1,n_act_orb
        do u=1,n_act_orb
         D0tu(u,t)=0.D0
         do v=1,n_act_orb
          do x=1,n_act_orb
           P0tuvx(x,v,u,t)=0.D0
          end do
         end do
        end do
       end do

! first loop: we apply E_tu, once for D_tu, once for -P_tvvu
      do mu=1,n_det
       call det_extract(det_mu,mu,N_int)
       do istate=1,n_states
        cI_mu(istate)=psi_coef(mu,istate)
       end do
       do t=1,n_act_orb
        ipart=list_act(t)
        do u=1,n_act_orb
         ihole=list_act(u)
! apply E_tu
         call det_copy(det_mu,det_mu_ex1,N_int)
         call det_copy(det_mu,det_mu_ex2,N_int)
         call do_spinfree_mono_excitation(det_mu,det_mu_ex1  &
            ,det_mu_ex2,nu1,nu2,ihole,ipart,phase1,phase2,ierr1,ierr2)
! det_mu_ex1 is in the list
         if (nu1.ne.-1) then
          do istate=1,n_states
           term=cI_mu(istate)*psi_coef(nu1,istate)*phase1
           D0tu(t,u)+=term
! and we fill P0_tvvu
           do v=1,n_act_orb
            P0tuvx(t,v,v,u)-=term
           end do
          end do
         end if
! det_mu_ex2 is in the list
         if (nu2.ne.-1) then
          do istate=1,n_states
           term=cI_mu(istate)*psi_coef(nu2,istate)*phase2
           D0tu(t,u)+=term
           do v=1,n_act_orb
            P0tuvx(t,v,v,u)-=term
           end do
          end do
         end if
        end do
       end do
      end do
! now we do the double excitation E_tu E_vx |0>
      do mu=1,n_det
       call det_extract(det_mu,mu,N_int)
       do istate=1,n_states
        cI_mu(istate)=psi_coef(mu,istate)
       end do
       do v=1,n_act_orb
        ipart=list_act(v)
        do x=1,n_act_orb
         ihole=list_act(x)
! apply E_vx
         call det_copy(det_mu,det_mu_ex1,N_int)
         call det_copy(det_mu,det_mu_ex2,N_int)
         call do_spinfree_mono_excitation(det_mu,det_mu_ex1  &
            ,det_mu_ex2,nu1,nu2,ihole,ipart,phase1,phase2,ierr1,ierr2)
! we apply E_tu to the first resultant determinant, thus E_tu E_vx |0>
         if (ierr1.eq.1) then
          do t=1,n_act_orb
           jpart=list_act(t)
           do u=1,n_act_orb
            jhole=list_act(u)
            call det_copy(det_mu_ex1,det_mu_ex11,N_int)
            call det_copy(det_mu_ex1,det_mu_ex12,N_int)
            call do_spinfree_mono_excitation(det_mu_ex1,det_mu_ex11  &
               ,det_mu_ex12,nu11,nu12,jhole,jpart,phase11,phase12,ierr11,ierr12)
            if (nu11.ne.-1) then
             do istate=1,n_states
              P0tuvx(t,u,v,x)+=cI_mu(istate)*psi_coef(nu11,istate) &
                   *phase11*phase1
             end do
            end if
            if (nu12.ne.-1) then
             do istate=1,n_states
              P0tuvx(t,u,v,x)+=cI_mu(istate)*psi_coef(nu12,istate) &
                   *phase12*phase1
             end do
            end if
           end do
          end do
         end if
          
! we apply E_tu to the second resultant determinant
         if (ierr2.eq.1) then
          do t=1,n_act_orb
           jpart=list_act(t)
           do u=1,n_act_orb
            jhole=list_act(u)
            call det_copy(det_mu_ex2,det_mu_ex21,N_int)
            call det_copy(det_mu_ex2,det_mu_ex22,N_int)
            call do_spinfree_mono_excitation(det_mu_ex2,det_mu_ex21  &
               ,det_mu_ex22,nu21,nu22,jhole,jpart,phase21,phase22,ierr21,ierr22)
            if (nu21.ne.-1) then
             do istate=1,n_states
              P0tuvx(t,u,v,x)+=cI_mu(istate)*psi_coef(nu21,istate)  &
                    *phase21*phase2
             end do
            end if
            if (nu22.ne.-1) then
             do istate=1,n_states
              P0tuvx(t,u,v,x)+=cI_mu(istate)*psi_coef(nu22,istate)  &
                    *phase22*phase2
             end do
            end if
           end do
          end do
         end if

        end do
       end do
      end do

! we average by just dividing by the number of states
      do x=1,n_act_orb
       do v=1,n_act_orb
        D0tu(v,x)*=1.0D0/dble(N_states)
        do u=1,n_act_orb
         do t=1,n_act_orb
          P0tuvx(t,u,v,x)*=0.5D0/dble(N_states)
         end do
        end do
       end do
      end do

      deallocate(det_mu)
      deallocate(det_mu_ex)
      deallocate(det_mu_ex1)
      deallocate(det_mu_ex11)
      deallocate(det_mu_ex12)
      deallocate(det_mu_ex2)
      deallocate(det_mu_ex21)
      deallocate(det_mu_ex22)
END_PROVIDER

BEGIN_PROVIDER [real*8, Q0vxyztu, (n_act_orb,n_act_orb,n_act_orb,n_act_orb,n_act_orb,n_act_orb) ]
BEGIN_DOC
! Q_vxyztu = 1/2 <0|(E_vx E_yz - delta_xy E_vz) E_tu |0>
! paper of Roos, eq A1 of the appendix
END_DOC
      implicit none
      integer :: v,x,y,z,t,u,mu
      integer :: istate,ispin,jspin,ihole,ipart,jhole,jpart,khole,kpart
      integer :: ierr
      integer(bit_kind), allocatable :: det_mu(:,:)
      integer(bit_kind), allocatable :: det_mu_ex(:,:)
      integer(bit_kind), allocatable :: det_mu_ex1(:,:)
      integer(bit_kind), allocatable :: det_mu_ex2(:,:)
      integer(bit_kind), allocatable :: det_mu_ex11(:,:)
      integer(bit_kind), allocatable :: det_mu_ex12(:,:)
      integer(bit_kind), allocatable :: det_mu_ex21(:,:)
      integer(bit_kind), allocatable :: det_mu_ex22(:,:)
      integer(bit_kind), allocatable :: det_mu_ex111(:,:)
      integer(bit_kind), allocatable :: det_mu_ex112(:,:)
      integer(bit_kind), allocatable :: det_mu_ex121(:,:)
      integer(bit_kind), allocatable :: det_mu_ex122(:,:)
      integer(bit_kind), allocatable :: det_mu_ex211(:,:)
      integer(bit_kind), allocatable :: det_mu_ex212(:,:)
      integer(bit_kind), allocatable :: det_mu_ex221(:,:)
      integer(bit_kind), allocatable :: det_mu_ex222(:,:)
      real*8 :: phase1,phase11,phase12,phase2,phase21,phase22
      real*8 :: phase111,phase112,phase121,phase122,phase211,phase212,phase221,phase222
      integer :: nu1,nu2,nu11,nu12,nu21,nu22
      integer :: nu111,nu112,nu121,nu122,nu211,nu212,nu221,nu222
      integer :: ierr1,ierr2,ierr11,ierr12,ierr21,ierr22
      integer :: ierr111,ierr112,ierr121,ierr122,ierr211,ierr212,ierr221,ierr222
      real*8 :: cI_mu(N_states),term
      allocate(det_mu(N_int,2))
      allocate(det_mu_ex(N_int,2))
      allocate(det_mu_ex1(N_int,2))
      allocate(det_mu_ex2(N_int,2))
      allocate(det_mu_ex11(N_int,2))
      allocate(det_mu_ex12(N_int,2))
      allocate(det_mu_ex21(N_int,2))
      allocate(det_mu_ex22(N_int,2))
      allocate(det_mu_ex111(N_int,2))
      allocate(det_mu_ex112(N_int,2))
      allocate(det_mu_ex121(N_int,2))
      allocate(det_mu_ex122(N_int,2))
      allocate(det_mu_ex211(N_int,2))
      allocate(det_mu_ex212(N_int,2))
      allocate(det_mu_ex221(N_int,2))
      allocate(det_mu_ex222(N_int,2))

      write(6,*) ' providing three-particle density matrix Q_vxyztu '

! set all to zero
       do v=1,n_act_orb
        do x=1,n_act_orb
         do y=1,n_act_orb
          do z=1,n_act_orb
           do t=1,n_act_orb
            do u=1,n_act_orb
             Q0vxyztu(v,x,y,z,t,u)=0.D0
            end do
           end do
          end do
         end do
        end do
       end do

! first we do the double excitation -E_vz E_tu |0> for all Q0vxyztu(v,x,x,z,t,u)
      do mu=1,n_det
       call det_extract(det_mu,mu,N_int)
       do istate=1,n_states
        cI_mu(istate)=psi_coef(mu,istate)
       end do
       do t=1,n_act_orb
        ipart=list_act(t)
        do u=1,n_act_orb
         ihole=list_act(u)
! apply E_tu
         call det_copy(det_mu,det_mu_ex1,N_int)
         call det_copy(det_mu,det_mu_ex2,N_int)
         call do_spinfree_mono_excitation(det_mu,det_mu_ex1  &
            ,det_mu_ex2,nu1,nu2,ihole,ipart,phase1,phase2,ierr1,ierr2)
! we apply E_vz to the first resultant determinant, thus E_vz E_tu |0>
         if (ierr1.eq.1) then
          do v=1,n_act_orb
           jpart=list_act(v)
           do z=1,n_act_orb
            jhole=list_act(z)
            call det_copy(det_mu_ex1,det_mu_ex11,N_int)
            call det_copy(det_mu_ex1,det_mu_ex12,N_int)
            call do_spinfree_mono_excitation(det_mu_ex1,det_mu_ex11  &
               ,det_mu_ex12,nu11,nu12,jhole,jpart,phase11,phase12,ierr11,ierr12)
            if (nu11.ne.-1) then
             do x=1,n_act_orb
              do istate=1,n_states
               Q0vxyztu(v,x,x,z,t,u)-=cI_mu(istate)*psi_coef(nu11,istate) &
                    *phase11*phase1
              end do
             end do
            end if
            if (nu12.ne.-1) then
             do x=1,n_act_orb
              do istate=1,n_states
               Q0vxyztu(v,x,x,z,t,u)-=cI_mu(istate)*psi_coef(nu12,istate) &
                    *phase12*phase1
              end do
             end do
            end if
           end do
          end do
         end if
          
! we apply E_vz to the second resultant determinant
         if (ierr2.eq.1) then
          do v=1,n_act_orb
           jpart=list_act(v)
           do z=1,n_act_orb
            jhole=list_act(z)
            call det_copy(det_mu_ex2,det_mu_ex21,N_int)
            call det_copy(det_mu_ex2,det_mu_ex22,N_int)
            call do_spinfree_mono_excitation(det_mu_ex2,det_mu_ex21  &
               ,det_mu_ex22,nu21,nu22,jhole,jpart,phase21,phase22,ierr21,ierr22)
            if (nu21.ne.-1) then
             do x=1,n_act_orb
              do istate=1,n_states
               Q0vxyztu(v,x,x,z,t,u)-=cI_mu(istate)*psi_coef(nu21,istate)  &
                     *phase21*phase2
              end do
             end do
            end if
            if (nu22.ne.-1) then
             do x=1,n_act_orb
              do istate=1,n_states
               Q0vxyztu(v,x,x,z,t,u)-=cI_mu(istate)*psi_coef(nu22,istate)  &
                     *phase22*phase2
              end do
             end do
            end if
           end do
          end do
         end if

        end do
       end do
      end do

!
! now the triple excitation, we may have 8 resulting determinants 111, 112 ... 222
!
      do mu=1,n_det
       call det_extract(det_mu,mu,N_int)
       do istate=1,n_states
        cI_mu(istate)=psi_coef(mu,istate)
       end do

       do t=1,n_act_orb
        ipart=list_act(t)
        do u=1,n_act_orb
         ihole=list_act(u)
! apply E_tu
         call det_copy(det_mu,det_mu_ex1,N_int)
         call det_copy(det_mu,det_mu_ex2,N_int)
         call do_spinfree_mono_excitation(det_mu,det_mu_ex1  &
            ,det_mu_ex2,nu1,nu2,ihole,ipart,phase1,phase2,ierr1,ierr2)
! we apply E_yz to the first resultant determinant, thus E_yz E_tu |0>
         if (ierr1.eq.1) then
          do y=1,n_act_orb
           jpart=list_act(y)
           do z=1,n_act_orb
            jhole=list_act(z)
            call det_copy(det_mu_ex1,det_mu_ex11,N_int)
            call det_copy(det_mu_ex1,det_mu_ex12,N_int)
            call do_spinfree_mono_excitation(det_mu_ex1,det_mu_ex11  &
               ,det_mu_ex12,nu11,nu12,jhole,jpart,phase11,phase12,ierr11,ierr12)
! excitation possible, but not necessarily in the list, as we apply another one
            if (ierr11.eq.1) then
             do v=1,n_act_orb
              kpart=list_act(v)
              do x=1,n_act_orb
               khole=list_act(x)
               call det_copy(det_mu_ex11,det_mu_ex111,N_int)
               call det_copy(det_mu_ex11,det_mu_ex112,N_int)
! we apply E_vx to this one, thus E_vx E_yz E_tu |0>
               call do_spinfree_mono_excitation(det_mu_ex11,det_mu_ex111  &
                  ,det_mu_ex112,nu111,nu112,khole,kpart,phase111,phase112,ierr111,ierr112)
! is this one possible and in the list ?
               if (nu111.ne.-1) then
                do istate=1,n_states
                 Q0vxyztu(v,x,y,z,t,u)+=cI_mu(istate)*psi_coef(nu111,istate) &
                      *phase111*phase11*phase1
                end do
               end if
               if (nu112.ne.-1) then
                do istate=1,n_states
                 Q0vxyztu(v,x,y,z,t,u)+=cI_mu(istate)*psi_coef(nu112,istate) &
                      *phase112*phase11*phase1
                end do
               end if
              end do
             end do
            end if
            if (ierr12.eq.1) then
             do v=1,n_act_orb
              kpart=list_act(v)
              do x=1,n_act_orb
               khole=list_act(x)
               call det_copy(det_mu_ex12,det_mu_ex121,N_int)
               call det_copy(det_mu_ex12,det_mu_ex122,N_int)
! we apply E_vx to this one, thus E_vx E_yz E_tu |0>
               call do_spinfree_mono_excitation(det_mu_ex12,det_mu_ex121  &
                  ,det_mu_ex122,nu121,nu122,khole,kpart,phase121,phase122,ierr121,ierr122)
! is this one possible and in the list ?
               if (nu121.ne.-1) then
                do istate=1,n_states
                 Q0vxyztu(v,x,y,z,t,u)+=cI_mu(istate)*psi_coef(nu121,istate) &
                      *phase121*phase12*phase1
                end do
               end if
               if (nu122.ne.-1) then
                do istate=1,n_states
                 Q0vxyztu(v,x,y,z,t,u)+=cI_mu(istate)*psi_coef(nu122,istate) &
                      *phase122*phase12*phase1
                end do
               end if
              end do
             end do
            end if
           end do
          end do
         end if
          
! we apply E_yz to the second resultant determinant, E_yz E_tu |0>
         if (ierr2.eq.1) then
          do y=1,n_act_orb
           jpart=list_act(y)
           do z=1,n_act_orb
            jhole=list_act(z)
            call det_copy(det_mu_ex2,det_mu_ex21,N_int)
            call det_copy(det_mu_ex2,det_mu_ex22,N_int)
            call do_spinfree_mono_excitation(det_mu_ex2,det_mu_ex21  &
               ,det_mu_ex22,nu21,nu22,jhole,jpart,phase21,phase22,ierr21,ierr22)
            if (ierr21.eq.1) then
             do v=1,n_act_orb
              kpart=list_act(v)
              do x=1,n_act_orb
               khole=list_act(x)
               call det_copy(det_mu_ex21,det_mu_ex211,N_int)
               call det_copy(det_mu_ex21,det_mu_ex212,N_int)
! we apply E_vx to this one, thus E_vx E_yz E_tu |0>
               call do_spinfree_mono_excitation(det_mu_ex21,det_mu_ex211  &
                  ,det_mu_ex212,nu211,nu212,khole,kpart,phase211,phase212,ierr211,ierr212)
! is this one possible and in the list ?
               if (nu211.ne.-1) then
                do istate=1,n_states
                 Q0vxyztu(v,x,y,z,t,u)+=cI_mu(istate)*psi_coef(nu211,istate) &
                      *phase211*phase21*phase2
                end do
               end if
               if (nu212.ne.-1) then
                do istate=1,n_states
                 Q0vxyztu(v,x,y,z,t,u)+=cI_mu(istate)*psi_coef(nu212,istate) &
                      *phase212*phase21*phase2
                end do
               end if
              end do
             end do
            end if
            if (ierr22.eq.1) then
             do v=1,n_act_orb
              kpart=list_act(v)
              do x=1,n_act_orb
               khole=list_act(x)
               call det_copy(det_mu_ex22,det_mu_ex221,N_int)
               call det_copy(det_mu_ex22,det_mu_ex222,N_int)
! we apply E_vx to this one, thus E_vx E_yz E_tu |0>
               call do_spinfree_mono_excitation(det_mu_ex22,det_mu_ex221  &
                  ,det_mu_ex222,nu221,nu222,khole,kpart,phase221,phase222,ierr221,ierr222)
! is this one possible and in the list ?
               if (nu221.ne.-1) then
                do istate=1,n_states
                 Q0vxyztu(v,x,y,z,t,u)+=cI_mu(istate)*psi_coef(nu221,istate) &
                      *phase221*phase22*phase2
                end do
               end if
               if (nu222.ne.-1) then
                do istate=1,n_states
                 Q0vxyztu(v,x,y,z,t,u)+=cI_mu(istate)*psi_coef(nu222,istate) &
                      *phase222*phase22*phase2
                end do
               end if
              end do
             end do

            end if
           end do
          end do
         end if

        end do
       end do
      end do

      do v=1,n_act_orb
       do x=1,n_act_orb
        do y=1,n_act_orb
         do z=1,n_act_orb
          do t=1,n_act_orb
           do u=1,n_act_orb
            Q0vxyztu(v,x,y,z,t,u)*=0.5D0/dble(n_states)
           end do
          end do
         end do
        end do
       end do
      end do

      deallocate(det_mu)
      deallocate(det_mu_ex)
      deallocate(det_mu_ex1)
      deallocate(det_mu_ex2)
      deallocate(det_mu_ex11)
      deallocate(det_mu_ex12)
      deallocate(det_mu_ex21)
      deallocate(det_mu_ex22)
      deallocate(det_mu_ex111)
      deallocate(det_mu_ex112)
      deallocate(det_mu_ex121)
      deallocate(det_mu_ex122)
      deallocate(det_mu_ex211)
      deallocate(det_mu_ex212)
      deallocate(det_mu_ex221)
      deallocate(det_mu_ex222)
END_PROVIDER


