====
acpf
====

We dress the Hamilton matrix by E_c(1-2/n) on the diagonal,
no dressing for 2 electrons (Full CI), CEPA-0 for an infinite 
number of electrons

MR-ACPF, block of the CAS + all Singles and Doubles
orbitals are - core inactive active virtual deleted

12 classes of determinants
1  i -> a 
2  i -> v
3  a -> v
4  ii -> aa
5  ii -> av
6  ii -> vv
7  ia -> aa
8  ia -> av
9  ia -> vv
10 aa -> av
11 aa -> vv

Once we have the list we call the Davidson, and we dress the diagonal 
of the small matrix. 
<Psi_i|H+Delta|Psi_j> = sum_IJ c_Ii c_Jj H_IJ + sum_I Delta c_Ii c_Ij 
                      = sum_IJ c_Ii c_Jj H_IJ + Delta <Phi_I|Phi_J>
                      = <Psi_i|H|Psi_j> + Delta delta_ij
