! -*- F90 -*-
  use bitmasks ! you need to include the bitmasks_module.f90 features

BEGIN_PROVIDER [integer, ndet_FullCAS]
END_PROVIDER


BEGIN_PROVIDER [logical, no_iivv]
&BEGIN_PROVIDER [logical, select_iivv]
&BEGIN_PROVIDER [real*8, thr_select_mrci]
BEGIN_DOC
! allow or not the ii -> vv excitations 
END_DOC
     no_iivv=.true.
     no_iivv=.false.

     select_iivv=.true.
     select_iivv=.false.

     if (select_iivv) no_iivv=.true.
     thr_select_mrci=1.D-6

END_PROVIDER
     
 BEGIN_PROVIDER [integer, n_act_el_alpha]
&BEGIN_PROVIDER [integer, n_act_el_beta ]
&BEGIN_PROVIDER [integer, n_act_hole_alpha]
&BEGIN_PROVIDER [integer, n_act_hole_beta]     
     n_act_el_alpha=elec_alpha_num-n_core_orb-n_inact_orb
     n_act_el_beta =elec_beta_num-n_core_orb-n_inact_orb
     n_act_hole_alpha=n_act_orb-n_act_el_alpha
     n_act_hole_beta=n_act_orb-n_act_el_beta
     END_PROVIDER

     BEGIN_PROVIDER [integer, ndet_CAS_alpha]
     &BEGIN_PROVIDER [integer, ndet_CAS_beta]
     implicit none
     integer :: i
     real*8 :: xcas

     ndet_CAS_alpha=1
     ndet_CAS_beta=1

     if (n_act_orb.gt.1) then
      xcas=1.d0
      do i=1,n_act_el_alpha
       xcas*=dble(n_act_orb-i+1)/dble(i)
      end do
      ndet_CAS_alpha=nint(xcas)

      xcas=1.d0
      do i=1,n_act_el_beta
       xcas*=dble(n_act_orb-i+1)/dble(i)
      end do
      ndet_CAS_beta=nint(xcas)
     end if

     write(6,*) ' ndet_CAS_alpha = ',ndet_CAS_alpha
     write(6,*) ' ndet_CAS_beta  = ',ndet_CAS_beta
     
     END_PROVIDER
     
 BEGIN_PROVIDER [integer, dim_mrci]
&BEGIN_PROVIDER [integer, n_det_mrci]
BEGIN_DOC
! dimension of the Hamilton matrix to be diagonalized 
! n_states is the minimum
! on this is added all determinants with mono- or di-excitations
! wrt the CAS space
!
! we face a dilemma as the CAS space is just one line per state in the 
! MRCI matrix, we do not need the whole space in the Hamilton matrix.
! However, we need the list and the coefficients for each state to loop
! over when producing the first line of the Hamilton matrix. So we add it to
! the list of determinants
!
END_DOC
     implicit none
     integer :: contr

     integer :: i,j,a,b,ii,jj,aa,bb,all_c
     real*8 :: delta_e,denom_i,denom_ij,denom_ija,denom_ijab
     real*8, allocatable :: integrals_array1(:,:)
          
     write(6,*) ' Generating the MR-CI space: counting determinants '
! the minimum dimension of the upper left corner
     n_det_mrci=n_det
     write(6,*) ' Minimal dimension = CAS: ',n_det_mrci,n_det_mrci
     
! mono
! i -> a
! not for all determinants in the CAS, n_inact_orb*(CAS+1) 
integer :: n_alpha,n_beta
     n_alpha=n_inact_orb*ndet_CAS_alpha*ndet_CAS_beta* &
          (n_act_orb-n_act_el_alpha)/(n_act_el_alpha+1)
     n_beta=n_inact_orb*ndet_CAS_alpha*ndet_CAS_beta*  &
          (n_act_orb-n_act_el_beta)/(n_act_el_beta+1)
     contr=n_alpha+n_beta
     n_det_mrci+=contr
     write(6,*) ' Counting mono-excitation i -> a ',contr,n_det_mrci
! for each determinant in the CAS we may mono excite from
! inact -> virt, in alpha or beta
     if (.not.no_iivv) then
      contr=n_det*n_inact_orb*n_virt_orb*2
      n_det_mrci+=contr
      write(6,*) ' Counting mono-excitation i -> v ',contr,n_det_mrci
     else
      write(6,*) ' no i -> v excitations '
     end if
     
! a -> v : (CAS-1)*n_virt_orb
     contr=n_virt_orb*ndet_CAS_alpha*ndet_CAS_beta* &
          n_act_el_alpha/(n_act_orb-n_act_el_alpha+1)
     contr+=n_virt_orb*ndet_CAS_alpha*ndet_CAS_beta* &
          n_act_el_beta/(n_act_orb-n_act_el_beta+1)
     n_det_mrci+=contr
     write(6,*) ' Counting mono-excitation a -> v ',contr,n_det_mrci
     
! diexcitations alpha-alpha, beta-beta, alpha-beta
     
! ii aa
     contr=n_inact_orb*n_inact_orb*ndet_CAS_alpha*ndet_CAS_beta &
          *(n_act_orb-n_act_el_alpha)/(n_act_el_alpha+1) &
          *(n_act_orb-n_act_el_beta)/(n_act_el_beta+1)
     n_det_mrci+=contr
     write(6,*) ' Counting di-excitations of opposite spin ii -> aa '&
          ,contr,n_det_mrci
     contr=n_inact_orb*(n_inact_orb-1)/2*ndet_CAS_alpha*ndet_CAS_beta &
          *(n_act_orb-n_act_el_alpha)*(n_act_orb-n_act_el_alpha-1)  &
          /(n_act_el_alpha+1)/(n_act_el_alpha+2)
     contr+=n_inact_orb*(n_inact_orb-1)/2*ndet_CAS_alpha*ndet_CAS_beta &
          *(n_act_orb-n_act_el_beta)*(n_act_orb-n_act_el_beta-1)  &
          /(n_act_el_beta+1)/(n_act_el_beta+2)
     n_det_mrci+=contr
     write(6,*) ' Counting di-excitations of     same spin ii -> aa '  &
          ,contr,n_det_mrci
! ii av
     contr=n_inact_orb*n_inact_orb*n_virt_orb &
          *ndet_CAS_alpha*ndet_CAS_beta              & 
          *(n_act_orb-n_act_el_alpha)/(n_act_el_alpha+1)
     contr+=n_inact_orb*n_inact_orb*n_virt_orb &
          *ndet_CAS_alpha*ndet_CAS_beta              & 
          *(n_act_orb-n_act_el_beta)/(n_act_el_beta+1)
     n_det_mrci+=contr
     write(6,*) ' Counting di-excitations of opposite spin ii -> av '  &
          ,contr,n_det_mrci
     contr=n_inact_orb*(n_inact_orb-1)/2*n_virt_orb &
          *ndet_CAS_alpha*ndet_CAS_beta              & 
          *(n_act_orb-n_act_el_alpha)/(n_act_el_alpha+1)
     contr+=n_inact_orb*(n_inact_orb-1)/2*n_virt_orb &
          *ndet_CAS_alpha*ndet_CAS_beta              &
          *(n_act_orb-n_act_el_beta)/(n_act_el_beta+1)
     n_det_mrci+=contr
     write(6,*) ' Counting di-excitations of     same spin ii -> av '  &
          ,contr,n_det_mrci
! ii - vv
!  di-excite a pair of opposite spin
     if (.not.no_iivv) then
      contr=n_det*n_inact_orb*n_virt_orb*n_inact_orb*n_virt_orb
      n_det_mrci+=contr
      write(6,*) ' Counting di-excitations of opposite spin ii -> vv '  &
           ,contr,n_det_mrci
! we may as well di-excite with same spins
      contr=n_det*n_inact_orb*n_virt_orb*(n_inact_orb-1)*(n_virt_orb-1)/2
      n_det_mrci+=contr
      write(6,*) ' Counting di-excitations of     same spin ii -> vv '  &
           ,contr,n_det_mrci
     else
      write(6,*) ' no ii -> vv excitations '
     end if
     
! ia aa, no contributions
! ia av
     contr=n_inact_orb*ndet_CAS_alpha*ndet_CAS_beta*n_virt_orb &
          *(n_act_orb-n_act_el_alpha)/(n_act_el_alpha+1) &
          *n_act_el_beta/(n_act_orb-n_act_el_beta+1)
     contr+=n_inact_orb*ndet_CAS_alpha*ndet_CAS_beta*n_virt_orb &
          *(n_act_orb-n_act_el_beta)/(n_act_el_beta+1) &
          *n_act_el_alpha/(n_act_orb-n_act_el_alpha+1)
     n_det_mrci+=contr
     write(6,*) ' Counting di-excitations of opposite spin ia -> av '     &
          ,contr,n_det_mrci
! ia vv
     contr=n_inact_orb*ndet_CAS_alpha*ndet_CAS_beta &
          *n_virt_orb*n_virt_orb &
          *n_act_el_beta/(n_act_orb-n_act_el_beta+1)
     contr+=n_inact_orb*ndet_CAS_alpha*ndet_CAS_beta &
          *n_virt_orb*n_virt_orb &
          *n_act_el_alpha/(n_act_orb-n_act_el_alpha+1)
     n_det_mrci+=contr
     write(6,*) ' Counting di-excitations of opposite spin ia -> vv '       &
          ,contr,n_det_mrci
     contr=n_inact_orb*ndet_CAS_alpha*ndet_CAS_beta &
          *n_virt_orb*(n_virt_orb-1)/2 &
          *n_act_el_beta/(n_act_orb-n_act_el_beta+1)
     contr+=n_inact_orb*ndet_CAS_alpha*ndet_CAS_beta &
          *n_virt_orb*(n_virt_orb-1)/2 &
          *n_act_el_alpha/(n_act_orb-n_act_el_alpha+1)
     n_det_mrci+=contr
     write(6,*) ' Counting di-excitations of     same spin ia -> vv '       &
          ,contr,n_det_mrci

! aa av, no, comes down to mono

! aa vv
     contr=n_virt_orb*n_virt_orb*ndet_CAS_alpha*ndet_CAS_beta &
          *n_act_el_alpha/(n_act_orb-n_act_el_alpha+1)          &
          *n_act_el_beta/(n_act_orb-n_act_el_beta+1)
     n_det_mrci+=contr
     write(6,*) ' Counting di-excitations of opposite spin aa -> vv '      &
          ,contr,n_det_mrci
     contr= n_virt_orb*(n_virt_orb-1)/2*ndet_CAS_alpha*ndet_CAS_beta &
          *n_act_el_beta/(n_act_orb-n_act_el_beta+1)  &
          *(n_act_el_beta-1)/(n_act_orb-n_act_el_beta+2)
     contr+= n_virt_orb*(n_virt_orb-1)/2*ndet_CAS_alpha*ndet_CAS_beta &
          *n_act_el_alpha/(n_act_orb-n_act_el_alpha+1)  &
          *(n_act_el_alpha-1)/(n_act_orb-n_act_el_alpha+2)
     n_det_mrci+=contr
     write(6,*) ' Counting di-excitations of     same spin aa -> vv '       &
          ,contr,n_det_mrci

! perturbative estimation of the ii -> vv contributions
! we need a Fock matrix which can be the inactive one, constructed with a
! state-averaged one-particle density matrix
! we use the MCSCF Fock matrix Fpq in molecular orbitals

     if (select_iivv) then
      allocate(integrals_array1(mo_num,mo_num))
      contr=0
      all_c=0

      do i=1,n_inact_orb
       ii=list_inact(i)
       denom_i=Fpq(ii,ii)
       do j=1,n_inact_orb
        jj=list_inact(j)
        denom_ij=denom_i+Fpq(jj,jj)
        call get_mo_two_e_integrals_ij(ii,jj,mo_num,integrals_array1 &
             ,mo_integrals_map) 
        do a=1,n_virt_orb
         aa=list_virt(a)
         denom_ija=denom_ij-Fpq(aa,aa)
         integer :: mult
         do b=1,n_virt_orb
          if (i.eq.j) then
           if (a.eq.b) then
! iiaa and ia
            mult=2
           else
! iIaB and iIAb
            mult=2
           end if
          else
           if (a.eq.b) then
! iJaA and IjaA
            mult=2
           else
! iJaB iJAb ijab IJAB IjaB IjAb
            mult=6
           end if
          end if
          all_c+=mult
          bb=list_virt(b)
          denom_ijab=denom_ija-Fpq(bb,bb)
          real*8 :: term,hiajb,hibja
          hiajb=integrals_array1(aa,bb)
          hibja=integrals_array1(bb,aa)
          term=hiajb*(2.D0*hiajb-hibja)/denom_ijab
          if (abs(term).gt.thr_select_mrci) then
           write(6,*) ' iajb, MP2= ',ii,jj,aa,bb,term
           contr+=mult
          end if
         end do
        end do
       end do
      end do
      real*8 :: xcontr
      xcontr=dble(contr)/dble(all_c)*100.D0
      contr*=n_det
      write(6,*) ' selected contributions : ',contr,' = ',xcontr,'%'
      
      n_det_mrci+=contr
      
      deallocate(integrals_array1)
     end if
     
     write(6,*) ' Number of determinants in the MRCI: ',n_det_mrci
     dim_MRCI=n_det_MRCI-n_det+n_states
     write(6,*) ' Dimension of the MRCI space       : ',dim_MRCI
     n_det_max=n_det_mrci
     touch n_det_max

     write(6,*) ' n_det_max = ',n_det_max
     
END_PROVIDER

subroutine generate_determinants_mrci
  use bitmasks ! you need to include the bitmasks_module.f90 features
  BEGIN_DOC
! we generate the list of determinants
  END_DOC
  implicit none
  integer :: i,j,idet
  integer(bit_kind), allocatable :: det_tmp1(:,:)
  integer(bit_kind), allocatable :: det_tmp2(:,:)
  allocate(det_tmp1(2,N_int))
  allocate(det_tmp2(2,N_int))

  write(6,*) ' the previously defined CAS space '
  ndet_FullCAS=n_det
  do i=1,n_det
   write(6,*) ' Determinant No ',i
   call det_extract(det_tmp1,i,n_int)
   call print_det(det_tmp1,n_int)
  end do

  write(6,*) ' filling the ',n_det_mrci,' determinants '
  write(6,*) ' Shape of psi_det: ',shape(psi_det)

  real*8 :: cpu0,wall0

  call cpu_time(cpu0)
  call wall_time(wall0)

! we generate the 11 excitation classes one after another
! as alternative we may loop over the CAS and create for each
! determinant all excitations
! i -> a
  call create_ia
  call cpustamp(cpu0,wall0,cpu0,wall0,'i->a')
! i -> v
  call create_iv
  call cpustamp(cpu0,wall0,cpu0,wall0,'i->v')
! a -> v
  call create_av
  call cpustamp(cpu0,wall0,cpu0,wall0,'a->v')
! ii -> aa
  call create_iiaa
  call cpustamp(cpu0,wall0,cpu0,wall0,'iiaa')
! ii -> av
  call create_iiav
  call cpustamp(cpu0,wall0,cpu0,wall0,'iiav')
! ii -> vv
  call create_iivv
  call cpustamp(cpu0,wall0,cpu0,wall0,'iivv')
! ia -> aa ... no, comes down to mono
! ia -> av ... only opposite spin
  call create_iaav
  call cpustamp(cpu0,wall0,cpu0,wall0,'iaav')
! ia -> vv
  call create_iavv
  call cpustamp(cpu0,wall0,cpu0,wall0,'iavv')
! aa -> av ... no, comes down to mono
! aa -> vv
  call create_aavv
  call cpustamp(cpu0,wall0,cpu0,wall0,'aavv')

   stop
  
  deallocate(det_tmp1)
  deallocate(det_tmp2)
end subroutine generate_determinants_mrci
     
  subroutine create_ia
    implicit none
    integer :: i,ii,t,tt,u,uu,b,bb,j,jj,a,aa,ispin,iii,ierr,nu,idet,indx
    integer :: n_det_start
    real*8 :: phase
    integer(bit_kind), allocatable :: det_tmp1(:,:)
    integer(bit_kind), allocatable :: det_tmp2(:,:)
    allocate(det_tmp1(2,N_int))
    allocate(det_tmp2(2,N_int))
    indx=0
     do i=1,n_inact_orb
      n_det_start=n_det
      ii=list_inact(i)
    do idet=1,ndet_FullCAS
     call det_extract(det_tmp1,idet,n_int)
     ierr=0
      do t=1,n_act_orb
       tt=list_act(t)
       do ispin=1,2
        call do_perhaps_new_signed_mono_excitation(det_tmp1,det_tmp2 &
             ,nu,ii,tt,ispin,phase,n_det_start,ierr)
!       write(6,*) ' exc - ',idet,ii,tt,ispin,nu,ierr
        if (ierr.eq.1) then
         if (nu.eq.-1) then
          n_det+=1
          indx+=1
!         write(6,*) ' new determinant: ',n_det
!         call print_det(det_tmp2,n_int)
          do iii=1,N_int
           psi_det(1,iii,n_det) = det_tmp2(1,iii)
           psi_det(2,iii,n_det) = det_tmp2(2,iii)
          end do
         end if
        end if
       end do
      end do
     end do
    end do
    write(6,*) ' found ',indx,' new determinants '
    write(6,*) ' created i->a mono-excitations, n_det is now ',n_det
    deallocate(det_tmp1)
    deallocate(det_tmp2)
  end subroutine create_ia
  
  subroutine create_iv
    implicit none
    integer :: i,ii,t,tt,u,uu,b,bb,j,jj,a,aa,ispin,iii,ierr,nu,idet,indx
    real*8 :: phase
    integer(bit_kind), allocatable :: det_tmp1(:,:)
    integer(bit_kind), allocatable :: det_tmp2(:,:)
    allocate(det_tmp1(2,N_int))
    allocate(det_tmp2(2,N_int))
    indx=0
    do idet=1,ndet_FullCAS
     call det_extract(det_tmp1,idet,n_int)
     ierr=0
     do i=1,n_inact_orb
      ii=list_inact(i)
      do a=1,n_virt_orb
       aa=list_virt(a)
       do ispin=1,2
        call do_new_signed_mono_excitation(det_tmp1,det_tmp2 &
             ,ii,aa,ispin,phase,ierr)
!       write(6,*) ' exc - ',idet,ii,aa,ispin,nu,ierr
        if (ierr.eq.1) then
         n_det+=1
         indx+=1
!        write(6,*) ' new determinant: ',n_det
!        call print_det(det_tmp2,n_int)
         do iii=1,N_int
          psi_det(1,iii,n_det) = det_tmp2(1,iii)
          psi_det(2,iii,n_det) = det_tmp2(2,iii)
         end do
        end if
       end do
      end do
     end do
    end do
    write(6,*) ' found ',indx,' new determinants '
    write(6,*) ' created i->v mono-excitations, n_det is now ',n_det
    deallocate(det_tmp1)
    deallocate(det_tmp2)
  end subroutine create_iv
  
  subroutine create_av
    implicit none
    integer :: i,ii,t,tt,u,uu,b,bb,j,jj,a,aa,ispin,iii,ierr,nu,idet,indx
    integer :: n_det_start
    real*8 :: phase
    integer(bit_kind), allocatable :: det_tmp1(:,:)
    integer(bit_kind), allocatable :: det_tmp2(:,:)
    allocate(det_tmp1(2,N_int))
    allocate(det_tmp2(2,N_int))
    indx=0
    do a=1,n_virt_orb
     n_det_start=n_det-1
     aa=list_virt(a)
     do idet=1,ndet_FullCAS
      call det_extract(det_tmp1,idet,n_int)
      ierr=0
      do t=1,n_act_orb
       tt=list_act(t)
       do ispin=1,2
        call do_perhaps_new_signed_mono_excitation(det_tmp1,det_tmp2 &
             ,nu,tt,aa,ispin,phase,n_det_start,ierr)
!       write(6,*) ' exc - ',idet,tt,aa,ispin,nu,nu2,ierr,n_det_start,n_det
        if (ierr.eq.1) then
         if (nu.eq.-1) then
          n_det+=1
          indx+=1
!         write(6,*) ' new determinant: ',n_det
!         call print_det(det_tmp2,n_int)
          do iii=1,N_int
           psi_det(1,iii,n_det) = det_tmp2(1,iii)
           psi_det(2,iii,n_det) = det_tmp2(2,iii)
          end do
         end if
        end if
       end do
      end do
     end do
    end do
    write(6,*) ' found ',indx,' new determinants '
    write(6,*) ' created a->v mono-excitations, n_det is now ',n_det
    deallocate(det_tmp1)
    deallocate(det_tmp2)
  end subroutine create_av
    
  subroutine create_iiaa
    implicit none
    integer :: i,ii,t,tt,u,uu,b,bb,j,jj,a,aa,ispin,jspin,iii,ierr,nu,idet,jerr
    integer :: mu,indx,n_det_start
    real*8 :: phase
    integer(bit_kind), allocatable :: det_tmp1(:,:)
    integer(bit_kind), allocatable :: det_tmp2(:,:)
    integer(bit_kind), allocatable :: det_tmp3(:,:)
    allocate(det_tmp1(2,N_int))
    allocate(det_tmp2(2,N_int))
    allocate(det_tmp3(2,N_int))
    indx=0
    do i=1,n_inact_orb
     n_det_start=n_det-1
     ii=list_inact(i)
     do idet=1,ndet_FullCAS
      call det_extract(det_tmp1,idet,n_int)
      ierr=0
      do t=1,n_act_orb
       tt=list_act(t)
! first excitation
       do ispin=1,2
        call det_copy(det_tmp1,det_tmp2,N_int)
        call do_single_excitation(det_tmp2,ii,tt,ispin,ierr)
        if (ierr.eq.1) then
! second excitation
         do j=i,n_inact_orb
          jj=list_inact(j)
          do u=1,n_act_orb
           uu=list_act(u)
           do jspin=1,2
            call do_perhaps_new_signed_mono_excitation(det_tmp2,det_tmp3 &
                 ,mu,jj,uu,jspin,phase,n_det_start,jerr)
            if (jerr.eq.1) then
!            write(6,*) ' exc - ',idet,ii,jj,tt,uu,ispin,jspin,mu,jerr
             if (mu.eq.-1) then
              n_det+=1
!             write(6,*) ' new determinant: ',n_det
              indx+=1
!             call print_det(det_tmp3,n_int)
              do iii=1,N_int
               psi_det(1,iii,n_det) = det_tmp3(1,iii)
               psi_det(2,iii,n_det) = det_tmp3(2,iii)
              end do
             end if
            end if
           end do
          end do
         end do
        end if
       end do
      end do
     end do
    end do
    write(6,*) ' found ',indx,' new determinants '
    write(6,*) ' created ii->aa di-excitations, n_det is now ',n_det
    deallocate(det_tmp1)
    deallocate(det_tmp2)
    deallocate(det_tmp3)
  end subroutine create_iiaa
  
  subroutine create_iiav
    implicit none
    integer :: i,ii,t,tt,u,uu,b,bb,j,jj,a,aa,ispin,jspin,iii,ierr,nu,idet,jerr
    integer :: mu,indx
    real*8 :: phase
    integer(bit_kind), allocatable :: det_tmp1(:,:)
    integer(bit_kind), allocatable :: det_tmp2(:,:)
    integer(bit_kind), allocatable :: det_tmp3(:,:)
    allocate(det_tmp1(2,N_int))
    allocate(det_tmp2(2,N_int))
    allocate(det_tmp3(2,N_int))
    indx=0
    do i=1,n_inact_orb
     ii=list_inact(i)
     do a=1,n_virt_orb
      aa=list_virt(a)
! first excitation
       do ispin=1,2
integer :: n_det_start
      n_det_start=n_det-1
      do idet=1,ndet_FullCAS
       call det_extract(det_tmp1,idet,n_int)
       ierr=0
        call det_copy(det_tmp1,det_tmp2,N_int)
        call do_single_excitation(det_tmp2,ii,aa,ispin,ierr)
        if (ierr.eq.1) then
! second excitation
           do jspin=1,2
 integer :: jstart
           if (jspin.eq.ispin) then
            jstart=i
           else
            jstart=1
           end if
         do j=jstart,n_inact_orb
          jj=list_inact(j)
          do t=1,n_act_orb
           tt=list_act(t)
            call do_perhaps_new_signed_mono_excitation(det_tmp2,det_tmp3 &
                 ,mu,jj,tt,jspin,phase,n_det_start,jerr)
            if (jerr.eq.1) then
!           write(6,*) ' exc - ',idet,ii,jj,tt,aa,ispin,jspin,mu,jerr,mu,mu2,n_det_start,n_det
             if (mu.eq.-1) then
              n_det+=1
!             write(6,*) ' new determinant: ',n_det
              indx+=1
!             call print_det(det_tmp3,n_int)
              do iii=1,N_int
               psi_det(1,iii,n_det) = det_tmp3(1,iii)
               psi_det(2,iii,n_det) = det_tmp3(2,iii)
              end do
             end if
            end if
           end do
          end do
         end do
        end if
       end do
      end do
     end do
    end do
    write(6,*) ' found ',indx,' new determinants '
    write(6,*) ' created ii->av di-excitations, n_det is now ',n_det
    deallocate(det_tmp1)
    deallocate(det_tmp2)
    deallocate(det_tmp3)
  end subroutine create_iiav
 
  subroutine create_iivv
    implicit none
    integer :: i,ii,t,tt,u,uu,b,bb,j,jj,a,aa,ispin,jspin,iii,ierr,nu,idet,jerr
    integer :: mu,indx
    real*8 :: phase
    integer(bit_kind), allocatable :: det_tmp1(:,:)
    integer(bit_kind), allocatable :: det_tmp2(:,:)
    integer(bit_kind), allocatable :: det_tmp3(:,:)
    allocate(det_tmp1(2,N_int))
    allocate(det_tmp2(2,N_int))
    allocate(det_tmp3(2,N_int))
BEGIN_DOC
! here we can have i<j, a<b and ijab, IJAB same spin excitations
! or iJaB, iJAb IjaB IjAb or iIaB iIAb iIaA
! all multiplied with the CAS
END_DOC
     indx=0
     do idet=1,ndet_FullCAS
      call det_extract(det_tmp1,idet,n_int)
      ierr=0
      do i=1,n_inact_orb
       ii=list_inact(i)
       do a=1,n_virt_orb
        aa=list_virt(a)
        do ispin=1,2
         call det_copy(det_tmp1,det_tmp2,N_int)
         call do_single_excitation(det_tmp2,ii,aa,ispin,ierr)
         if (ierr.eq.1) then
! same spin
          do j=i+1,n_inact_orb
           jj=list_inact(j)
           do b=a+1,n_virt_orb
            bb=list_virt(b)
            call do_new_signed_mono_excitation(det_tmp2,det_tmp3 &
                ,jj,bb,ispin,phase,jerr)
            if (jerr.eq.1) then
             n_det+=1
             indx+=1
             do iii=1,N_int
              psi_det(1,iii,n_det) = det_tmp3(1,iii)
              psi_det(2,iii,n_det) = det_tmp3(2,iii)
             end do
            end if
           end do
          end do
         end if
        end do
! opposite spin
        call det_copy(det_tmp1,det_tmp2,N_int)
        call do_single_excitation(det_tmp2,ii,aa,1,ierr)
        if (ierr.eq.1) then
         do j=1,n_inact_orb
          jj=list_inact(j)
          do b=1,n_virt_orb
           bb=list_virt(b)
           call do_new_signed_mono_excitation(det_tmp2,det_tmp3 &
               ,jj,bb,2,phase,jerr)
           if (jerr.eq.1) then
            n_det+=1
            indx+=1
            do iii=1,N_int
             psi_det(1,iii,n_det) = det_tmp3(1,iii)
             psi_det(2,iii,n_det) = det_tmp3(2,iii)
            end do
           end if
          end do
         end do
        end if
       end do
      end do
     end do

     write(6,*) ' found ',indx,' new determinants '
     write(6,*) ' created ii->vv di-excitations, n_det is now ',n_det
     deallocate(det_tmp1)
     deallocate(det_tmp2)
     deallocate(det_tmp3)
  end subroutine create_iivv
    
  subroutine create_iaav
    BEGIN_DOC
! there are only opposite-spin double excitations
    END_DOC
     implicit none
     integer :: i,ii,t,tt,u,uu,b,bb,j,jj,a,aa,ispin,jspin,iii,ierr,nu,idet,jerr
     integer :: mu,indx
     real*8 :: phase
     integer(bit_kind), allocatable :: det_tmp1(:,:)
     integer(bit_kind), allocatable :: det_tmp2(:,:)
     integer(bit_kind), allocatable :: det_tmp3(:,:)
     allocate(det_tmp1(2,N_int))
     allocate(det_tmp2(2,N_int))
     allocate(det_tmp3(2,N_int))
     indx=0
     do i=1,n_inact_orb
      ii=list_inact(i)
      do a=1,n_virt_orb
       aa=list_virt(a)
integer :: n_det_start
       n_det_start=n_det-1
       do idet=1,ndet_FullCAS
        call det_extract(det_tmp1,idet,n_int)
        ierr=0
        do t=1,n_act_orb
         tt=list_act(t)
! first excitation
         do ispin=1,2
          jspin=3-ispin
          call det_copy(det_tmp1,det_tmp2,N_int)
          call do_single_excitation(det_tmp2,ii,tt,ispin,ierr)
          if (ierr.eq.1) then
! second excitation
           do u=1,n_act_orb
            uu=list_act(u)
            call do_perhaps_new_signed_mono_excitation(det_tmp2,det_tmp3 &
                 ,mu,uu,aa,jspin,phase,n_det_start,jerr)
            if (jerr.eq.1) then
!            write(6,*) ' exc - ',idet,ii,tt,uu,aa,ispin,jspin,mu,jerr
             if (mu.eq.-1) then
              n_det+=1
!             write(6,*) ' new determinant: ',n_det
              indx+=1
!             call print_det(det_tmp3,n_int)
              do iii=1,N_int
               psi_det(1,iii,n_det) = det_tmp3(1,iii)
               psi_det(2,iii,n_det) = det_tmp3(2,iii)
              end do
             end if
            end if
           end do
          end if
         end do
        end do
       end do
      end do
     end do
    write(6,*) ' found ',indx,' new determinants '
    write(6,*) ' created ia->av di-excitations, n_det is now ',n_det
    deallocate(det_tmp1)
    deallocate(det_tmp2)
    deallocate(det_tmp3)
  end subroutine create_iaav
  
  subroutine create_iavv
    implicit none
    integer :: i,ii,t,tt,u,uu,b,bb,j,jj,a,aa,ispin,jspin,iii,ierr,nu,idet,jerr
    integer :: mu,indx
    real*8 :: phase
    integer(bit_kind), allocatable :: det_tmp1(:,:)
    integer(bit_kind), allocatable :: det_tmp2(:,:)
    integer(bit_kind), allocatable :: det_tmp3(:,:)
    allocate(det_tmp1(2,N_int))
    allocate(det_tmp2(2,N_int))
    allocate(det_tmp3(2,N_int))
    indx=0
    do i=1,n_inact_orb
     ii=list_inact(i)
     do a=1,n_virt_orb
      aa=list_virt(a)
! first excitation
      do ispin=1,2
integer :: n_det_start
       n_det_start=n_det
       do idet=1,ndet_FullCAS
        call det_extract(det_tmp1,idet,n_int)
        ierr=0
        call det_copy(det_tmp1,det_tmp2,N_int)
        call do_single_excitation(det_tmp2,ii,aa,ispin,ierr)
        if (ierr.eq.1) then
! second excitation
         do jspin=1,2
integer :: bstart
          if (ispin.eq.jspin) then
           bstart=a+1
          else
           bstart=1
          end if
          do t=1,n_act_orb
           tt=list_act(t)
           do b=bstart,n_virt_orb
            bb=list_virt(b)
            call do_perhaps_new_signed_mono_excitation(det_tmp2,det_tmp3 &
                 ,mu,tt,bb,jspin,phase,n_det_start,jerr)
            if (jerr.eq.1) then
!            write(6,*) ' exc - ',idet,ii,tt,aa,bb,ispin,jspin,mu,jerr
             if (mu.eq.-1) then
              n_det+=1
!             write(6,*) ' new determinant: ',n_det
              indx+=1
!             call print_det(det_tmp3,n_int)
              do iii=1,N_int
               psi_det(1,iii,n_det) = det_tmp3(1,iii)
               psi_det(2,iii,n_det) = det_tmp3(2,iii)
              end do
             end if
            end if
           end do
          end do
         end do
        end if
       end do
      end do
     end do
    end do
    write(6,*) ' found ',indx,' new determinants '
    write(6,*) ' created ia->vv di-excitations, n_det is now ',n_det
    deallocate(det_tmp1)
    deallocate(det_tmp2)
    deallocate(det_tmp3)
  end subroutine create_iavv
  
  subroutine create_aavv
    implicit none
    integer :: i,ii,t,tt,u,uu,b,bb,j,jj,a,aa,ispin,jspin,iii,ierr,nu,idet,jerr
    integer :: mu,indx
    real*8 :: phase
    integer(bit_kind), allocatable :: det_tmp1(:,:)
    integer(bit_kind), allocatable :: det_tmp2(:,:)
    integer(bit_kind), allocatable :: det_tmp3(:,:)
    allocate(det_tmp1(2,N_int))
    allocate(det_tmp2(2,N_int))
    allocate(det_tmp3(2,N_int))

    indx=0
    do a=1,n_virt_orb
     aa=list_virt(a)
     do ispin=1,2
 integer :: n_det_start
       n_det_start=n_det-1
! same spin
      do b=a+1,n_virt_orb
       bb=list_virt(b)
       do idet=1,ndet_FullCAS
        call det_extract(det_tmp1,idet,n_int)
        ierr=0
        do t=1,n_act_orb
         tt=list_act(t)
! first excitation
         call det_copy(det_tmp1,det_tmp2,N_int)
         call do_single_excitation(det_tmp2,tt,aa,ispin,ierr)
         if (ierr.eq.1) then
! second excitation
          do u=t+1,n_act_orb
           uu=list_act(u)
integer :: mu2
           call do_perhaps_new_signed_mono_excitation(det_tmp2,det_tmp3 &
                  ,mu,uu,bb,ispin,phase,n_det_start,jerr)
           if (jerr.eq.1) then
!           write(6,*) ' exc - ',idet,tt,uu,aa,bb,ispin,jspin,mu,mu2,n_det_start,n_det
            if (mu.eq.-1) then
             n_det+=1
!            write(6,*) ' new determinant: ',n_det
             indx+=1
!            call print_det(det_tmp3,n_int)
             do iii=1,N_int
              psi_det(1,iii,n_det) = det_tmp3(1,iii)
              psi_det(2,iii,n_det) = det_tmp3(2,iii)
             end do
            end if
           end if
          end do
         end if
        end do
       end do
      end do
     end do
    end do

    do a=1,n_virt_orb
     aa=list_virt(a)
     n_det_start=n_det-1
       do idet=1,ndet_FullCAS
        call det_extract(det_tmp1,idet,n_int)
        ierr=0
     do ispin=1,2
! opposite spin
      jspin=3-ispin
      do b=a,n_virt_orb
       bb=list_virt(b)
        do t=1,n_act_orb
         tt=list_act(t)
! first excitation
         call det_copy(det_tmp1,det_tmp2,N_int)
         call do_single_excitation(det_tmp2,tt,aa,ispin,ierr)
         if (ierr.eq.1) then
! second excitation
integer :: ustart
          if (a.eq.b) then
           ustart=t+1
          else
           ustart=1
          end if
          do u=ustart,n_act_orb
           uu=list_act(u)
           call do_perhaps_new_signed_mono_excitation(det_tmp2,det_tmp3 &
                  ,mu,uu,bb,jspin,phase,n_det_start,jerr)
           if (jerr.eq.1) then
!          write(6,*) ' exc2 - ',idet,tt,uu,aa,bb,ispin,jspin,mu,mu2,n_det_start,n_det
            if (mu.eq.-1) then
             n_det+=1
!            write(6,*) ' new determinant: ',n_det
             indx+=1
!            call print_det(det_tmp3,n_int)
             do iii=1,N_int
              psi_det(1,iii,n_det) = det_tmp3(1,iii)
              psi_det(2,iii,n_det) = det_tmp3(2,iii)
             end do
            end if
           end if
          end do
         end if
        end do
       end do
      end do
     end do
    end do
    write(6,*) ' found ',indx,' new determinants '
    write(6,*) ' created aa->vv di-excitations, n_det is now ',n_det
    deallocate(det_tmp1)
    deallocate(det_tmp2)
    deallocate(det_tmp3)
  end subroutine create_aavv
