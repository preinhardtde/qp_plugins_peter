program mracpf
  BEGIN_DOC
! 
  END_DOC
  implicit none
  real*8 :: cpu0,wall0
read_wf=.true.
touch read_wf
         call cpu_time(cpu0)
         call wall_time(wall0)
  call header
  call tests
  call driver
  call cpustamp(cpu0,wall0,cpu0,wall0,'ALL ')
end program mracpf

subroutine header
  write(6,*) 
  write(6,*) ' Multi-reference ACPF '
  write(6,*) ' P Reinhardt (Paris, 2020) '
  write(6,*) 
end subroutine header

subroutine tests
    implicit none
    integer :: i,n_recalc,n_mul,ist
    real*8 :: xcal

  ! Update array sizes
  if (psi_det_size < N_det_max) then
    psi_det_size = N_det_max
    TOUCH psi_det_size
  endif

    write(6,*) ' ndet        = ',n_det
    write(6,*) ' n_det_max   = ',n_det_max
    write(6,*) ' nstate      = ',n_states
    write(6,*) ' n_core_orb  = ',n_core_orb
    write(6,*) ' n_inact_orb = ',n_inact_orb
    write(6,*) ' n_act_orb   = ',n_act_orb
    write(6,*) ' n_virt_orb  = ',n_virt_orb
    write(6,*) ' n_del_orb   = ',n_del_orb
    write(6,*) ' n_alpha     = ',elec_alpha_num-n_core_orb-n_inact_orb
    write(6,*) ' n_beta      = ',elec_beta_num-n_core_orb-n_inact_orb

    provide list_inact 
    provide list_act 
    provide list_virt 

    xcal=1.d0
    do i=1,elec_alpha_num-n_core_orb-n_inact_orb
     xcal*=dble(n_act_orb+1-i)/dble(i)
    end do
    do i=1,elec_beta_num-n_core_orb-n_inact_orb
     xcal*=dble(n_act_orb+1-i)/dble(i)
    end do
    write(6,*) ' ndet_CAS    = ',nint(xcal)
    do ist=1,n_states
     write(6,*) ' State No ',ist
     write(6,'(5(i4,f12.6))') (i,psi_coef(i,ist),i=1,n_det)
    end do

    write(6,*) ' Dimension of the MRCI space ',dim_mrci
    
end subroutine tests

subroutine driver
      implicit none

      call generate_determinants_mrci
end subroutine driver
