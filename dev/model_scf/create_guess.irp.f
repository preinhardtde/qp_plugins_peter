! -*- F90 -*-
subroutine create_guess
  implicit none
  BEGIN_DOC
!   Create a MO guess if no MOs are present in the EZFIO directory
  END_DOC
  logical                        :: exists
  PROVIDE ezfio_filename
  call ezfio_has_mo_basis_mo_coef(exists)

  write(6,*) ' Entering <create_guess> '
  if (elec_alpha_num.ne.elec_beta_num) then
    write(6,*) ' this program can only do closed shells '
    stop
  end if
  if (.not.exists) then
    print*,'Creating a guess for the MOs'
    print*,'mo_guess_type = ',mo_guess_type
    if (mo_guess_type == "HCore") then
      mo_coef = ao_ortho_lowdin_coef
      TOUCH mo_coef
      mo_label = 'Guess'
      call mo_as_eigvectors_of_mo_matrix(mo_one_e_integrals,size(mo_one_e_integrals,1),size(mo_one_e_integrals,2),mo_label,.false.)
      SOFT_TOUCH mo_coef mo_label
    else if (mo_guess_type == "Huckel") then
      call huckel_guess
    else
      print *,  'Unrecognized MO guess type : '//mo_guess_type
      stop 1
    endif
  endif
  write(6,*) ' Starting orbitals '
  mo_label="Guess"
integer :: i,j
  do i=1,mo_num
   write(6,*) ' Orbital No ',i
   write(6,'(5(i4,F16.8))') (j,mo_coef(j,i),j=1,ao_num)
  end do
  write(6,*)
end


