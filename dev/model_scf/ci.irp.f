! -*- F90 -*-
BEGIN_PROVIDER [integer, nocc ]
         implicit none
         integer :: i
         nocc=(elec_alpha_num + elec_beta_num)/2
         if (elec_alpha_num.ne.elec_beta_num) then
          write(6,*) ' closed-shell program only '
          stop ' no open shells, please '
         end if
         write(6,*) ' number of occupied orbitals ',nocc
END_PROVIDER

BEGIN_PROVIDER [ real*8, ci, (ao_num,mo_num) ]
      implicit none
      integer :: ione,ierr,i,j,alpha,beta
      real*8  :: cdum,ss
      real*8 :: fock(ao_num,ao_num)
      real*8 :: fock_mo(mo_num,mo_num)

      write(6,*) ' constructing orbitals, iter No ',iter_scf

      if (iter_scf.eq.0) then

       ci=mo_coef
       do alpha=1,ao_num
        do beta=1,ao_num
         ss=0.D0
         do i=1,nocc
          ss+=ci(alpha,i)*ci(beta,i)
         end do
         density(alpha,beta)=ss
        end do
       end do
       density*=2.D0
       touch density

!      fock = ao_one_e_integrals

!      do i=1,ao_num
!       fock(i,i)=0.D0
!      end do

       fock = ao_one_e_integrals + xi
      else
       fock = ao_one_e_integrals + (1.D0-lambda_oda)*xi_old &
               + lambda_oda * xi
      end if
! transform F onto MOs
      call transf_ao_mo(fock,ao_num,fock_mo,mo_num,ci)

      if (canonical_scf) then

      write(6,*) ' diagonal of the Fock matrix '
      write(6,'(5(i5F16.8))') (i,fock_mo(i,i),i=1,mo_num)

      call lapack_diag(orben,eigen_vecs,fock_mo,mo_num,mo_num)
      
      WRITE(6,*) 
      WRITE(6,*) ' ORBITAL ENERGIES OF THE CANONICAL ORBITALS' 
      write(6,*) 
      write(6,'(4(i4,e14.5))') (i,orben(i),i=1,mo_num)
      write(6,*) 
!
! well, it should come down to a simple matrix multiplication ...
!
      do alpha=1,ao_num
       do i=1,mo_num
        cdum=0.d0
        do j=1,mo_num
! the j components of eigenvector i
         cdum=cdum+eigen_vecs(j,i)*ci(alpha,j)
        end do
        cwrk(alpha,i)=cdum
       end do
      end do

      ci=cwrk

! --------------------------------------------
     else
! SINGLES CI step
!
! we reorder the diagonal of the Fock matrix
!
      do i=1,mo_num
       orben(i)=fock_mo(i,i)
      end do
! we bubble sort inline 
real*8 :: odum,fdum
integer :: ii

      do i=1,mo_num-1
       do j=1,mo_num-i
        if (orben(j).gt.orben(j+1)) then
         do ii=1,ao_num
          cdum=ci(ii,j)
          ci(ii,j)=ci(ii,j+1)
          ci(ii,j+1)=cdum
         end do
         do ii=1,mo_num
          fdum=fock_mo(ii,j)
          fock_mo(ii,j)=fock_mo(ii,j+1)
          fock_mo(ii,j+1)=fdum
         end do
         do ii=1,mo_num
          fdum=fock_mo(j,ii)
          fock_mo(j,ii)=fock_mo(j+1,ii)
          fock_mo(j+1,ii)=fdum
         end do
         odum=orben(j)
         orben(j)=orben(j+1)
         orben(j+1)=odum
        end if
       end do
      end do
      write(6,*) ' diagonal of the Fock matrix '
      write(6,'(5(i5F16.8))') (i,fock_mo(i,i),i=1,mo_num)

! the local Singles-CI procedure
       CI_matrix=0.D0
! <0|H|ia>=F_ia*sqrt(2)
! <ia|H|jb>=F_ab \delta_ij - F_ij\delta_ab

integer :: indx,a,b
       indx=1
       do i=1,nocc
        do a=1+nocc,mo_num
         indx+=1
         CI_matrix(1,indx)=fock_mo(i,a)*sqrt(2.D0)
         CI_matrix(indx,1)=fock_mo(i,a)*sqrt(2.D0)
        end do
       end do
integer :: indxi, indxj
logical :: lij, lab
       indxi=1
       do i=1,nocc
        do a=1+nocc,mo_num
         indxi+=1
         indxj=1
         do j=1,nocc
          lij=i.eq.j
          do b=nocc+1,mo_num
           indxj+=1
           lab=a.eq.b
!          write(6,*) ' i j a b ',i,j,a,b,lij,lab,indxi,indxj
           if (lij) then
            CI_matrix(indxi,indxj)+=fock_mo(a,b)
!           write(6,*) ' F_ab =  ',fock_mo(a,b)
           end if
           if (lab) then
            CI_matrix(indxi,indxj)-=fock_mo(i,j)
!           write(6,*) ' F_ij =  ',fock_mo(i,j)
           end if
          end do
         end do
        end do
       end do
!      write(6,*) ' diagonal of the CI matrix '
!      write(6,'(5(I5,F15.5))') (i,CI_matrix(i,i),i=1,nMonoEx)
! brute force, no Davidson
     write(6,*) ' diagonal of the SCI matrix '
      write(6,'(5(i4,f15.5))') (i,CI_matrix(i,i),i=1,nMonoEx+1)

       IONE=1
       CALL lapack_diag(eigval_CI_matrix,eigvec_CI_matrix, &
           CI_matrix,nMonoEx+1,nMonoEx+1)
       write(6,*) ' energy lowering : ',eigval_CI_matrix(1)
! new orbitals and density
! intermediate normalization
       do i=2,nMonoEx
        eigvec_CI_matrix(i,1)*=1.D0/eigvec_CI_matrix(1,1)*ci_scaling
       end do
       eigvec_CI_matrix(1,1)=1.D0


       indx=1
       ss=0.D0
       do i=1,nocc
        do a=1+nocc,mo_num
         indx+=1
         if (abs(eigvec_CI_matrix(indx,1)).gt.1.D0) eigvec_CI_matrix(indx,1)= &
              eigvec_CI_matrix(indx,1)/abs(eigvec_CI_matrix(indx,1))*0.5D0
         if (abs(eigvec_CI_matrix(indx,1)).gt.1.D-4) write(6,*) ' excitation ' &
            ,i,' - ',a,' weight ',eigvec_CI_matrix(indx,1)
         ss+=eigvec_CI_matrix(indx,1)*eigvec_CI_matrix(indx,1)
         do alpha=1,ao_num
          ci(alpha,i)+=eigvec_CI_matrix(indx,1)*ci(alpha,a)
          ci(alpha,a)-=eigvec_CI_matrix(indx,1)*ci(alpha,i)
         end do
        end do
       end do
       ss=sqrt(ss)/nMonoEx
! dynamical scaling of the ci_scaling factor
!      ci_scaling=max(1.-sqrt(ss),0.5D0)
!      ci_scaling=1.D0-min(dble(iter_scf)*1.D-2,0.6D0)
       write(6,*) ' average excitation = ',ss,' scaling factor = ',ci_scaling
! normalize orbitals

       call ortho(ci)

     end if
! --------------------------------------------

      if (bavard) then 
       write(6,*) ' new occupied orbitals '
       do i=1,nocc   
        write(6,*) ' Orbital No ',i
        write(6,'(5(i4,F16.8))') (alpha,ci(alpha,i),alpha=1,ao_num)
       end do
      end if

END_PROVIDER

BEGIN_PROVIDER [ real*8, orben, (mo_num) ]
END_PROVIDER

BEGIN_PROVIDER [ real*8, cwrk, (ao_num,mo_num) ]
END_PROVIDER

BEGIN_PROVIDER [ real*8, eigen_vecs, (mo_num,mo_num) ]
END_PROVIDER

BEGIN_PROVIDER [ real*8, lambda_oda]
END_PROVIDER

BEGIN_PROVIDER [ logical, loda ]
   loda=.true.
END_PROVIDER
