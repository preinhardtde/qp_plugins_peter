! -*- F90 -*-
      subroutine shalf(start_indx,end_indx,orbs)
BEGIN_DOC
! orthogonalize by S^(-1/2) the set of orbitals between start and end
END_DOC
         implicit none
         real*8 :: orbs(ao_num,mo_num),ss
         integer :: alpha,i,j,beta,start_indx,end_indx,norb
         real*8, allocatable :: orb_tmp(:,:)

         norb=end_indx-start_indx+1
         write(6,*) ' entering Shalf ',start_indx,' - ',end_indx,' = ',norb,' orbitals '
         allocate(orb_tmp(ao_num,norb))
!
integer :: ii,jj
         do ii=1,norb
          i=start_indx+ii-1
          do alpha=1,ao_num
           orb_tmp(alpha,ii)=orbs(alpha,i)
          end do
         end do

         call transf2sq(ao_overlap,overlap_shalf,norb,orb_tmp)


         call lapack_diag(evals_shalf,evecs_shalf,overlap_shalf,mo_num,norb)
         write(6,*) start_indx,norb
         write(6,*) ' eigenvalues '
         write(6,'(5(i5,F12.6))') (i,evals_shalf(i),i=1,norb)

         if (evals_shalf(1).lt.1.D-12) then
          write(6,*) ' smallest eigenvalue for SHALF is ',evals_shalf(1)
          write(6,*) ' overlap matrix :'
          do i=1,norb
           do j=1,norb
            write(6,*) ' i,j,s ',i,j,overlap_shalf(i,j)
           end do
          end do
          stop ' Linear dependencies in SHALF '
         end if
         do i=1,norb
          do j=1,norb
           cwrk_ortho(j,i)=0.D0
          end do
         end do
! form S^(-1/2)
integer :: k
         do k=1,norb
          ss=1.D0/sqrt(evals_shalf(k))
          do i=1,norb
           do j=1,norb
            cwrk_ortho(i,j)+=evecs_shalf(i,k)*evecs_shalf(j,k)*ss
           end do
          end do
         end do
! transform the orbitals
         do alpha=1,ao_num
          do i=1,norb
           evals_shalf(i)=orbs(alpha,start_indx+i-1)
           orbs(alpha,start_indx+i-1)=0.D0
          end do
          do i=1,norb
           do j=1,norb
            orbs(alpha,start_indx+i-1)+=evals_shalf(j)*cwrk_ortho(i,j)
           end do
          end do
         end do
                                                             
! test orthogonality
!  write(6,*) ' verifying orthonormality'

!        do ii=1,norb
!         i=start_indx+ii-1
!         do jj=1,norb
!          j=start_indx+jj-1
!          ss=0.D0
!          write(6,*) ' i, j ',i,j
!          do alpha=1,ao_num
!           do beta=1,ao_num
!            ss+=orbs(alpha,i)*orbs(beta,j)*ao_overlap(alpha,beta)
!           end do
!          end do
!          if (i.eq.j) then
!           if (abs(ss-1.D0).gt.1.D-5) write(6,*) ' ii, jj, ss ',ii,jj,ss
!          else
!           if (abs(ss).gt.1.D-5) write(6,*) ' ii, jj, ss ',ii,jj,ss
!          end if
!         end do
!        end do
!     write(6,*) ' done '
      deallocate(orb_tmp)

      end subroutine shalf


 BEGIN_PROVIDER [real*8, overlap_shalf, (mo_num,mo_num)]
&BEGIN_PROVIDER [real*8, evals_shalf, (mo_num)]
&BEGIN_PROVIDER [real*8, evecs_shalf, (mo_num,mo_num)]
&BEGIN_PROVIDER [real*8, cwrk_ortho, (mo_num,mo_num)]
&BEGIN_PROVIDER [real*8, c3_ortho, (nocc)]
END_PROVIDER

      SUBROUTINE ORTHO(vector)
        implicit none
        real*8 :: vector(ao_num,mo_num)
        write(6,*) ' orthogonalizing orbitals'

!
! we orthogonalize the given set of orbitals
!
        CALL SHALF(1,nocc,vector)
        CALL SFULL(vector)
        CALL SHALF(nocc+1,mo_num,vector)
!
      END SUBROUTINE ORTHO

      subroutine sfull(vector)
!
! we project the NOCC occupied orbitals from the virtuals
!
! we use the helper arrays cwrk and c3
!
        implicit none

        real*8 :: vector(ao_num,mo_num),eps,sss,ccc
        integer :: iter,nov,i,j,k,alpha,beta

        eps=1.d-10

        call transf_ao_mo(ao_overlap,ao_num,cwrk_ortho,mo_num,vector)
!
! the corrected functions
!
        iter=0
        nov=nocc+1
! with iterative refinement
100     continue
        do i=nov,mo_num
         do j=1,nocc
          c3_ortho(j)=cwrk_ortho(i,j)
         end do
         do k=1,ao_num
          ccc=0.d0
          do j=1,nocc
           ccc=ccc+vector(k,j)*c3_ortho(j)
          end do
          vector(k,i)=vector(k,i)-ccc
         end do
        end do
!
        call transf_ao_mo(ao_overlap,ao_num,cwrk_ortho,mo_num,vector)
!
! calculate new overlaps
        sss=0.d0
        iter=iter+1
        do i=nov,mo_num
         do j=1,nocc
          sss=sss+cwrk_ortho(i,j)*cwrk_ortho(i,j)
        end do
        end do
        sss=sqrt(sss)/((mo_num-nocc)*nocc)
        write(6,*) ' iteration, sss:',iter,sss
        if (sss.gt.eps.and.iter.le.100) go to 100
!
! renormalize new virtual orbitals
!
        do i=nov,mo_num
         sss=0.d0
         do j=1,ao_num
          ccc=vector(j,i)
          do k=1,ao_num
           sss=sss+ao_overlap(j,k)*ccc*vector(k,i)
          end do
         end do
         sss=sqrt(sss)
         do j=1,ao_num
          vector(j,i)=vector(j,i)/sss
         end do
        end do

      end subroutine sfull
