! -*- F90 -*-
BEGIN_PROVIDER [integer, nMonoEx]
      implicit none
      nMonoEx=nocc*(mo_num-nocc)
      write(6,*) ' nonoEx = ',nMonoEx
END_PROVIDER

BEGIN_PROVIDER [real*8, CI_matrix, (nMonoEx+1,nMonoEx+1)]
      implicit none
      CI_matrix=0.D0
END_PROVIDER

 BEGIN_PROVIDER [real*8, eigvec_CI_matrix, (nMonoEx+1,nMonoEx+1)]
&BEGIN_PROVIDER [real*8, eigval_CI_matrix, (nMonoEx+1)]
END_PROVIDER

BEGIN_PROVIDER [real*8, ci_scaling ]
   ci_scaling=0.65D0
END_PROVIDER
