! -*- F90 -*-
use bitmasks ! you need to include the bitmasks_module.f90 features
 
BEGIN_PROVIDER [ integer, perturbation_order ]
BEGIN_DOC
! maximum order of perturbation theory
END_DOC
    implicit none
    write(6,*) ' desired order of perturbation ? '
    read(5,*) perturbation_order
END_PROVIDER

BEGIN_PROVIDER [integer, choice_series]
BEGIN_DOC
! kind of perturbation series         
! 1 = Moller-Plesset
! 2 = Epstein-Nesbet
END_DOC
    implicit none
    write(6,*) ' which kind of perturbation ? '
    write(6,*) '  1 = Moller-Plesset'
    write(6,*) '  2 = Epstein-Nesbet'
    read(5,*) choice_series
    if (choice_series.ne.1.and.choice_series.ne.2) then
     write(6,*) ' please choose the right series '
     stop
    end if
END_PROVIDER


BEGIN_PROVIDER [ real*8, pert_energy, (perturbation_order+1) ]
&BEGIN_PROVIDER [real*8, partial_sum, (perturbation_order)]
&BEGIN_PROVIDER [ real*8, Etot ]
BEGIN_DOC
! the energies of each order of perturbation
END_DOC
    implicit none
    integer :: i,iorder,jorder,idet
    do i=1,perturbation_order
     pert_energy(i)=0.D0
     partial_sum(i)=0.D0
    end do

! fill the first-order coefficients and store them on file
! we have all the <0|H|I> as bvect in memory
         write(6,*) ' E_core       = ',core_energy
         write(6,*) ' <0|H0-E0|0>  = ',H0E0(1)
         write(6,*) ' <0|H-E_HF|0> = ',bvect(1)
         write(6,*) ' Hartree-Fock energy = ',hf_energy
         write(6,*) ' E0pert              = ',E0pert
         write(6,*) ' <0|H-H0|0> = <0|V|0> = E(1) = ',bvect(1)-H0E0(1)+hf_energy-E0pert
         pert_energy(1)=hf_energy-E0pert
         pert_energy(2)=0.D0
         partial_sum(1)=hf_energy
         do idet=2,n_det_FullCAS
          c_n(idet)=-bvect(idet)/H0E0(idet)
          pert_energy(2)+=bvect(idet)*c_n(idet)
         end do
         open(unit=17,file='pertvect.tmp',status='unknown' &
            ,access='direct',recl=n_det_FullCAS*8)
         call putv(c_n,1,17)
         write(6,*) ' order, energy contribution, total energy, Aterm, Bterm '
         write(6,9901) E0pert,E0pert
         write(6,9911) pert_energy(1),E0pert+pert_energy(1),hf_energy
 9901 format('   0',2F20.12)
 9911 format('   1',2F20.12,' (HF = ',F20.12,')')
         iorder=2
         Aterm=pert_energy(iorder)
         Bterm=0.D0
         Etot=hf_energy+pert_energy(iorder)
         partial_sum(iorder)=Etot
         write(6,9902) iorder,pert_energy(iorder),Etot,Aterm,Bterm
 9902 format(i4,2F20.12,2E20.12)

         do iorder=3,perturbation_order
          call getv(civector,iorder-2,17)
          call hcalc
! multiply with the denominators
          do idet=2,n_det_FullCAS
           c_n(idet)=-hvect(idet)/H0E0(idet)
! on the diagonal we have <I|H-E_core|I>
! for having -<I|V|I> we have to have -diag(I)+<I|H0-E0|I>+E0-E_core
           c_n(idet)+=civector(idet)*(H0E0(idet)-core_energy+E0pert)/H0E0(idet)
           c_n1(idet)=0.D0
          end do
          do jorder=1,iorder-2
           call getv(civector,jorder,17)
           do idet=2,n_det_FullCAS
            c_n1(idet)+=pert_energy(iorder-jorder-1) &
                 *civector(idet)/H0E0(idet)
           end do
          end do
! calculate energy of order n+1
! Aterm is the direct term <0|V|I><I|V|J>c_J/<I|H0-E0|I>
! Bterm is the renormalization term <0|V|I>/<I|H0-E0|I>(sum_k E^k c_I^(n-k) )
real*8 :: Aterm, Bterm
          Aterm=0.D0
          Bterm=0.D0
          pert_energy(iorder)=0.D0
          do idet=1,n_det_FullCAS
           Aterm+=bvect(idet)*c_n(idet)
           Bterm+=bvect(idet)*c_n1(idet)
           c_n(idet)+=c_n1(idet)
          end do
          pert_energy(iorder)=Aterm+Bterm
          Etot+=pert_energy(iorder)
          partial_sum(iorder)=Etot
          call putv(c_n,iorder-1,17)
          write(6,9902) iorder,pert_energy(iorder),Etot,Aterm,Bterm
         end do
         close(17,status='delete')

END_PROVIDER

 BEGIN_PROVIDER [ real*8, diagH, (n_det_FullCAS) ]
&BEGIN_PROVIDER [ real*8, bvect, (n_det_FullCAS) ]
BEGIN_DOC
! the matrix elements <0|H|I> and <I|H|I>
END_DOC
     implicit none
     integer :: idet
     integer(bit_kind), allocatable :: det_mu(:,:),det_nu(:,:)
     real*8 :: hij
     allocate(det_mu(N_int,2))
     allocate(det_nu(N_int,2))

     call det_extract_FullCAS(det_mu,1,N_int)
     call i_H_j(det_mu,det_mu,N_int,hij)
     bvect(1)=hij-hf_energy+nuclear_repulsion
     write(6,*) ' ref: hij, hf_energy, E_nuc = ',hij,hf_energy,nuclear_repulsion
     diagH(1)=bvect(1)
     write(6,*) ' 1 bvect, diag = ',bvect(1),diagH(1)
     do idet=2,n_det_FullCAS
      call det_extract_FullCAS(det_nu,idet,N_int)
      call i_H_j(det_mu,det_nu,N_int,hij)
      bvect(idet)=hij
      call i_H_j(det_nu,det_nu,N_int,hij)
      diagH(idet)=hij-core_energy+nuclear_repulsion
!     write(6,*) ' idet  bvect, diag = ',idet,bvect(idet),diagH(idet)
     end do
END_PROVIDER

 BEGIN_PROVIDER [ real*8, H0E0, (n_det_FullCAS) ]
&BEGIN_PROVIDER [ real*8, E0pert ]
BEGIN_DOC
! the denominators <I|H0-E0|I>
END_DOC
    implicit none
    integer :: i,idet,ispin,iorb
    integer(bit_kind), allocatable :: det_ref(:,:),det_mu(:,:)
    allocate(det_mu(N_int,2))
    allocate(det_ref(N_int,2))
    if (elec_alpha_num.ne.elec_beta_num) then
     write(6,*) ' perturbation only for closed-shell cases coded '
     stop ' closed-shell reference needed '
    end if
    if (choice_series.eq.1) then
! Moller-Plesset
     E0pert=core_energy
     do iorb=n_core_orb+1,elec_num/2
      E0pert+=2.D0*orben(iorb)
     end do
! occupation valid for alpha and beta as closed shells assumed
! the lists contain the orbital indices of the occupied orbitals
integer :: list_det_ref(mo_num),nocc_hf
integer :: list_det_mu(mo_num),nocc_mu
     do i=1,elec_alpha_num
      list_det_ref(i)=0
      list_det_mu(i)=0
     end do
     call det_extract_FullCAS(det_ref,1,N_int)
     call bitstring_to_list(det_ref,list_det_ref,nocc_hf,N_int)
     do idet=1,n_det_FullCAS
      H0E0(idet)=0.D0
! look in the string which orbitals are occupied, subtract the reference
! alpha string
      call det_extract_FullCAS(det_mu,idet,N_int)
      call bitstring_to_list(det_mu(1,1),list_det_mu,nocc_mu,N_int)
      if (nocc_mu.ne.elec_alpha_num) then
       stop ' lost some electrons alpha '
      end if
! find the holes
      do i=1,nocc_mu
integer :: o1,j
logical :: lfound
       o1=list_det_ref(i)
       lfound=.false.
       do j=1,nocc_mu
        if (list_det_mu(j).eq.o1) lfound=.true.
       end do
       if (.not.lfound) then
! hole
        H0E0(idet)-=orben(o1)
       end if
      end do
         
! find the particles
      do i=1,nocc_mu
       o1=list_det_mu(i)
       lfound=.false.
       do j=1,nocc_mu
        if (list_det_ref(j).eq.o1) lfound=.true.
       end do
       if (.not.lfound) then
! particle
        H0E0(idet)+=orben(o1)
       end if
      end do

! beta string
      do i=1,mo_num
       list_det_mu(i)=0
      end do
      call bitstring_to_list(det_mu(1,2),list_det_mu,nocc_mu,N_int)
      if (nocc_mu.ne.elec_beta_num) then
       stop ' lost some electrons beta '
      end if

! find the holes
      do i=1,nocc_mu
       o1=list_det_ref(i)
       lfound=.false.
       do j=1,nocc_mu
        if (list_det_mu(j).eq.o1) lfound=.true.
       end do
       if (.not.lfound) then
! hole
        H0E0(idet)-=orben(o1)
       end if
      end do
         
! find the particles
      do i=1,nocc_mu
       o1=list_det_mu(i)
       lfound=.false.
       do j=1,nocc_mu
        if (list_det_ref(j).eq.o1) lfound=.true.
       end do
       if (.not.lfound) then
! particle
        H0E0(idet)+=orben(o1)
       end if
      end do

     end do
    else if (choice_series.eq.2) then
! Epstein-Nesbet
     E0pert=hf_energy
     do i=1,n_det_FullCAS
      H0E0(i)=diagH(i) ! +core_energy-hf_energy
     end do
    else 
     stop ' no choice yet '
    end if

!    write(6,*) ' The denominators '
!    do i=1,n_det_FullCAS
!     write(6,*) i,H0E0(i)
!    end do
END_PROVIDER

 BEGIN_PROVIDER [ real*8, C_n, (n_det_FullCAS) ]
&BEGIN_PROVIDER [ real*8, C_n1, (n_det_FullCAS) ]
BEGIN_DOC
! the coefficients of order n and n-1
END_DOC
    implicit none
    integer :: i
    do i=1,n_det_FullCAS
     C_n (i)=0.D0
     C_n1(i)=0.D0
    end do
END_PROVIDER

 BEGIN_PROVIDER [ real*8, civector, (n_det_FullCAS) ]
&BEGIN_PROVIDER [ real*8, hvect, (n_det_FullCAS) ]
END_PROVIDER

    subroutine hcalc
BEGIN_DOC
! we calculate H Psi, where Psi is given by the 
! coefficients in civector
END_DOC
     integer(bit_kind), allocatable :: det_mu(:,:),det_nu(:,:)
     real*8 :: hij
     allocate(det_mu(N_int,2))
     allocate(det_nu(N_int,2))

! initialize with the diagonal, then calculate the impact of the reference
! and finally any general element
!
     do idet=1,n_det_FullCAS
      hvect(idet)=civector(idet)*diagH(idet)
     end do

     do idet=2,n_det_FullCAS
      hvect(idet)+=civector(1)*bvect(idet)
      hvect(1)+=civector(idet)*bvect(idet)
     end do

     do idet=2,n_det_FullCAS
      call det_extract_FullCAS(det_mu,idet,N_int)
      do jdet=idet+1,n_det_FullCAS
       call det_extract_FullCAS(det_nu,jdet,N_int)
       call i_H_j(det_mu,det_nu,N_int,hij)
       hvect(idet)+=civector(jdet)*hij
       hvect(jdet)+=civector(idet)*hij
      end do
     end do
    end subroutine hcalc
