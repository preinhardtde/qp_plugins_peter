! -*- F90 -*- 
program perturbation
        call header
        call driver
        write(6,*)
        write(6,*) ' All done '
        write(6,*)
end program perturbation

       subroutine header 
         write(6,*) 
         write(6,*) ' Perturbation to any order  '
         write(6,*) '  single closed-shell reference ' 
         write(6,*) ' P Reinhardt 3/2019  '
         write(6,*) ' Coulanges-la-Vineuse, France'
         write(6,*) 
       end subroutine header 

       subroutine driver 
         implicit none
         integer :: i,ii
         integer :: iorder
         write(6,*) 
         write(6,*) ' defined core ',2*n_core_orb,' electrons in ' &
                 ,n_core_orb,' orbitals' 
         write(6,*) ' defined CAS ',elec_num-2*n_core_orb,' electrons in ' &
                 ,n_act_orb,' orbitals' 
         write(6,*) ' Number of determinants = ',n_det_FullCAS
         write(6,*) ' Number of atomic basis functions ',ao_num
         write(6,*) ' Total number of electrons is ',elec_num
         write(6,*) 
         write(6,*) ' perturbation series : '
         if (choice_series.eq.1) then
          write(6,*) ' Moller-Plesset '
         else if (choice_series.eq.2) then
          write(6,*) ' Epstein-Nesbet '
         else
          write(6,*) ' neither Moller-Plesset nor Epstein-Nesbet '
          stop
         end if

         write(6,*) ' total energy at order ',perturbation_order,' = ',Etot
         write(6,*)
         write(6,*) ' Shanks table '
         write(6,*) '  Shanks transformation '
         write(6,*) '  S = [S_(N+1)S_(N-1)-S_N^2] / [S_(N+1)+S_(N-1)-2 S_N] '
         write(6,*) '  order  partial sum   1st Shank   2nd Shank ' &
               ,'    3rd Shank '
         do iorder=1,perturbation_order
          write(6,*) iorder,partial_sum(iorder),shanks_table(iorder,1) &
              ,shanks_table(iorder,2),shanks_table(iorder,3)
         end do
         write(6,*)
         write(6,*) ' Richardson table '
         write(6,*) '      Richardson extrapolation'
         write(6,*) '      R_k(N) = [ (N+k)^k S_(N+k) - k (N+k-1)^k S_(N+k-1) ... N^k S_N ] / k!'
         write(6,*) '  order  partial sum   1st Richardson   2nd Richardson ' &
               ,'    3rd Richardson '
         do iorder=1,perturbation_order
          write(6,*) iorder,partial_sum(iorder),Richardson(iorder,1) &
              ,Richardson(iorder,2),Richardson(iorder,3)
         end do
       end subroutine driver 

