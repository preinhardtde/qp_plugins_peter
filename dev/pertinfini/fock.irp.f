! -*- F90 -*-
BEGIN_PROVIDER [real*8, orben, (mo_num)]
     implicit none
     integer :: i
     write(6,*) ' Orbital energies '
     do i=1,mo_num
      orben(i)=fock_matrix_diag_mo(i)
      write(6,*) i,orben(i)
     end do
 
 
END_PROVIDER
