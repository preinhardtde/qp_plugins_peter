=====
perturbation
=====

This plugin calculates the Moeller-Plesset or Epstein-Nesbet perturbation
series in the active space to any desired order, from a Hartree-Fock 
wavefunction. 

In a first step the complete active space is generated, and then, order by 
order we calculate E^n = <0|V|Psi(n-1)>, and the coefficients of Psi(n).

The coefficients of previous orders are stored on a growing direct-access 
file.

The program 'gen_cas' generates just the CAS without the perturbation series.

The program fci_peter produces a Full CI space (CAS), diagonalizes the Hamiltonian 
in that space, generates natural orbitals, and saves the 1-, 2- and 3-particle density
matrices together with the integrals PQxx, PxxQ and aaaP, P=all, a=act, x=core+act
The program has to be run twice for that the wavefunction is expressed in natural 
orbitals.
