! -* F90 -*-
  use bitmasks ! you need to include the bitmasks_module.f90 features
 
BEGIN_PROVIDER [integer, N_det_FullCAS ]
&BEGIN_PROVIDER [integer, N_alphastrings_FullCAS ]
BEGIN_DOC
! generating a complete CAS
! number of determinants = [(N-n)!/N!/n!]^2
END_DOC
   implicit none
   integer :: i,ii
   N_det_FullCAS=1
   if (elec_alpha_num.gt.n_core_orb) then
    do i=1,elec_alpha_num-n_core_orb
     N_det_FullCAS*=dble((n_act_orb-i+1))/dble(i)
    end do
   end if
   N_alphastrings_FullCAS=N_det_FullCAS
   N_det_FullCAS*=N_det_FullCAS
   write(6,*) ' generated Full CAS: N_det_FullCAS = ',N_det_FullCAS
   if (N_det_FullCAS.eq.0) then 
    write(6,*) ' please define some active orbitals '
    stop ' please define some active orbitals '
   end if
    
END_PROVIDER

BEGIN_PROVIDER [ integer(bit_kind), FullCAS_det, (N_int,2,n_det_FullCAS) ]
BEGIN_DOC
! the list of determinants in the Full CAS
! we create a separate list for this space 
END_DOC
        implicit none
        integer :: i,j,k,idet,jdet,indx
        integer(bit_kind), allocatable :: det_tmp(:)
        allocate(det_tmp(N_int))

        FullCAS_det=0_bit_kind
        do i=1,N_int
          FullCAS_det(i,1,1) = HF_bitmask(i,1)
          det_tmp(i) = HF_bitmask(i,1)
          FullCAS_det(i,2,1) = HF_bitmask(i,2)
        enddo
!
! we have to generate the bitstring and dump it into the list at the right place
!
        do idet=2,N_alphastrings_FullCAS
         call countup_FullCAS(det_tmp)
         do i=1,N_int
          FullCAS_det(i,1,idet) = det_tmp(i)
          FullCAS_det(i,2,idet) = HF_bitmask(i,2)
        end do
       end do
!
! we have the series of alpha strings, we copy for having the full CAS space in 
! alpha and beta electrons
!
        indx=N_alphastrings_FullCAS
        do jdet=2,N_alphastrings_FullCAS
         do idet=1,N_alphastrings_FullCAS
          indx+=1
          do i=1,N_int
           FullCAS_det(i,1,indx) = FullCAS_det(i,1,idet)
           FullCAS_det(i,2,indx) = FullCAS_det(i,1,jdet)
          end do
         end do
        end do

END_PROVIDER

        subroutine countup_FullCAS(det)
           implicit none 
           integer(bit_kind) :: det(N_int)
           integer :: occ(elec_alpha_num),i,ii,iii,i4,j
           logical :: found_empty
BEGIN_DOC
!
! from a given string we find the next one by exchanging a 1 with a zero 
! to the rhs. Only active orbitals are considered.
!
! 1 find the first empty active orbital
! 2 are there occ active orbitals below ? 
! 3 if yes, exchange and return the generated string.
! 4 if no, find the next higher empty active orbital, go to 2
!
END_DOC

! extract the indices of the occupied orbitals
         call bitstring_to_list( det, occ, elec_alpha_num, N_int)
         do i=1,n_act_orb
          ii=list_act(i)
          found_empty=.true.
          do j=1,elec_alpha_num
           if (occ(j).eq.ii) found_empty=.false.
          end do
          if (found_empty) then
! a candidate
! someone occupied to the lhs ? 
           do iii=i-1,1,-1
            i4=list_act(iii)
            do j=1,elec_alpha_num
             if (occ(j).eq.i4) then
! place ii in occ(j), encode the new list, and return
!             write(6,*) ' exchanged hole ',ii,' with particle ',i4
              occ(j)=ii
! migrate all lower occupied to the lowest possible places
integer :: i5,ii5,i6,ii6,jj
logical :: lexch
              do i5=1,i-2
               ii5=list_act(i5)
               found_empty=.true.
               do jj=1,elec_alpha_num
                if (ii5.eq.occ(jj)) found_empty=.false.
               end do
               if (found_empty) then
!               write(6,*) ' place No ',ii5,' is empty, trying to fill it'
! find a particle to exchange with
                lexch=.false.
                do i6=i5+1,i-1
                 if (.not.lexch) then
                  ii6=list_act(i6)
                  do jj=1,elec_alpha_num
                   if (ii6.eq.occ(jj)) then
                    occ(jj)=ii5
!                   write(6,*) ii6,' goes to place ',ii5
                    lexch=.true.
                   end if
                  end do
                 end if
                end do
               end if
              end do
                
              call list_to_bitstring( det, occ, elec_alpha_num, N_int)
              return
             end if
            end do
           end do
          end if
         end do
! we should never arrive here, as in this case, no count was possible
         write(6,*)  ' WARNING --- unable to count correctly '
         stop  '  unable to count correctly '
        end subroutine countup

       subroutine det_extract_FullCAS(key,nu,Nint)
BEGIN_DOC
! extract a determinant from the list of determinants
END_DOC
         implicit none
         integer :: ispin,i,nu,Nint
         integer(bit_kind) :: key(Nint,2)
         do ispin=1,2
          do i=1,Nint
           key(i,ispin)=FullCAS_det(i,ispin,nu)
          end do
         end do
       end subroutine det_extract_FullCAS


