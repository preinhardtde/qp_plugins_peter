! -*- F90 -*-
BEGIN_PROVIDER [real*8, shanks_table, (perturbation_order,3)]
BEGIN_DOC
! Shanks transformation
! S = [S_(N+1)S_(N-1)-S_N^2] / [S_(N+1)+S_(N-1)-2 S_N]
END_DOC
        implicit none
        integer :: i,j

        do i=1,3
         do j=1,perturbation_order
          shanks_table(j,i)=0.D0
         end do
        end do
! 1st order Shanks
real*8 :: s,ss
        do j=2,perturbation_order-1
         s=partial_sum(j+1)*partial_sum(j-1)
         s-=partial_sum(j)*partial_sum(j)
         ss=partial_sum(j+1)+partial_sum(j-1)-2.D0*partial_sum(j)
         shanks_table(j,1)=s/ss
        end do
! 2nd order Shanks
        do j=3,perturbation_order-2
         s=shanks_table(j+1,1)*shanks_table(j-1,1)
         s-=shanks_table(j,1)*shanks_table(j,1)
         ss=shanks_table(j+1,1)+shanks_table(j-1,1)-2.D0*shanks_table(j,1)
         shanks_table(j,2)=s/ss
        end do
! 3rd order Shanks
        do j=4,perturbation_order-3
         s=shanks_table(j+1,2)*shanks_table(j-1,2)
         s-=shanks_table(j,2)*shanks_table(j,2)
         ss=shanks_table(j+1,2)+shanks_table(j-1,2)-2.D0*shanks_table(j,2)
         shanks_table(j,3)=s/ss
        end do
END_PROVIDER

BEGIN_PROVIDER [real*8, richardson, (perturbation_order,3)]
BEGIN_DOC
!
! Richardson extrapolation
! R_k(N) = [ (N+k)^k S_(N+k) - k (N+k-1)^k S_(N+k-1) ... N^k S_N ] / k!
END_DOC
        implicit none
        integer :: i,j

        do i=1,3
         do j=1,perturbation_order
          richardson(j,i)=0.D0
         end do
        end do

! 1st order Richardson
        do j=1,perturbation_order-1
         richardson(j,1)=(j+1)*partial_sum(j+1)-j*partial_sum(j)
        end do
! 2nd order Richardson
        do j=1,perturbation_order-2
         richardson(j,2)=0.5D0*((j+2)*(j+2)*partial_sum(j+2) &
             -2.D0*(j+1)*(j+1)*partial_sum(j+1)+j*j*partial_sum(j))
        end do
! 3rd order Richardson
        do j=1,perturbation_order-3
         richardson(j,3)=((j+3)*(j+3)*(j+3)*partial_sum(j+3) &
                    -3.D0*(j+2)*(j+2)*(j+2)*partial_sum(j+2) &
                    +3.D0*(j+1)*(j+1)*(j+1)*partial_sum(j+1) &
                    -      j*    j*    j*   partial_sum(j))/6.D0
        end do
END_PROVIDER
