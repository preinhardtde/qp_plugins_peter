! -* F90 -*-
  use bitmasks ! you need to include the bitmasks_module.f90 features
 
program gencas
BEGIN_DOC
! Test program for generating a CAS 
! P Reinhardt, 6/2019 
END_DOC
   read_wf = .True.
   touch read_wf

   call header
   call driver

end program gencas

      subroutine header
          write(6,*) 
          write(6,*)  ' Test program for generating a CAS '
          write(6,*)  ' P Reinhardt, 6/2019 '
          write(6,*) 
      end subroutine header

      subroutine driver
        implicit none
        integer :: i
        integer(bit_kind), allocatable :: det_i(:,:)
        allocate(det_i(N_int,2))

        write(6,*) ' N_det = ',N_det
        write(6,*) ' n_act_orb = ',n_act_orb
        write(6,*) ' elec_num  = ',elec_num
        write(6,*) ' active electrons  = ',elec_num-2*n_core_orb
        write(6,*) ' active alpha electrons  = ',elec_num/2-n_core_orb, &
                        elec_alpha_num-n_core_orb
        do i=1,n_act_orb
         write(6,*) i,list_act(i)
        end do
        write(6,*) ' 1st determinant '
        call det_extract(det_i,1,N_int)
        call print_det(det_i,N_int)
        write(6,*) ' N_det_FullCAS = ',N_det_FullCAS
        do i=1,n_det_FullCAS
         write(6,*) ' Determinant No ',i
         call det_extract_FullCAS(det_i,i,N_int)
         call print_det(det_i,N_int)
        end do
      end subroutine driver
