! -*- F90 -*-
program model_scf
  implicit none
  BEGIN_DOC
! the SCF model we created with Fernand and Anthony, long ago
  END_DOC
         
          call header
          call setup 
          call driver
end

     subroutine header
       write(6,*)
       write(6,*) ' evaluate a given density and gradient with '
       write(6,*) ' functionals of the libxc and xcfun library '
       write(6,*) ' calculate total energy and the KS matrix elements '
       write(6,*) ' P Reinhardt, Paris,  may 2020' 
       write(6,*) ' in confinement we trust '
       write(6,*)
     end subroutine header

     subroutine setup
       implicit none
       integer :: i
       read_wf=.true.
       touch read_wf
       n_points_radial_grid=150
       n_points_integration_angular=302

       write(6,*) ' setting up the functionals rad, ang ',n_points_radial_grid,n_points_integration_angular

       if (number_of_functionals.gt.0) then
        do i=1,number_of_functionals
         call setup_libxc(xcnum_libxc(i),func_name_xc(i) &
             ,is_lda_libxc(i),is_gga_libxc(i),is_metagga_libxc(i),is_exch_libxc(i),is_corr_libxc(i))
        end do
       end if

     end subroutine setup

     subroutine driver
       implicit none
       integer :: i,j

       write(6,*) 
real*8 :: dmin
       dmin=1.D12
       do i=1,nucl_num
        do j=i+1,nucl_num
         dmin=min(dmin,nucl_dist(i,j))
        end do
       end do
       write(6,*) ' Minimum distance beween atom found is ',dmin,' bohr = ',dmin*0.529177,' Angstrom'
       write(6,*) 
       write(6,*) 

       write(6,*) ' number of grid points = ',n_points_final_grid
!      write(6,*) ' LDA x, qp2 = ',energy_x_lda(1)
!      write(6,*) ' LDA c, qp2 = ',energy_c_lda(1)
!      write(6,*)
!      write(6,*) ' PBE x, qp2 = ',energy_x_pbe(1)
!      write(6,*) ' PBE c, qp2 = ',energy_c_pbe(1)
!      write(6,*)

!      call calc_xc_lda(n_points_final_grid,final_weight_at_r_vector,density_points)

 real*8 :: numeleca,numelecb
       numeleca=0.D0
       numelecb=0.D0
       do j = 1, n_points_final_grid
        numeleca+=final_weight_at_r_vector(j)*density_points(j,1)
        numelecb+=final_weight_at_r_vector(j)*density_points(j,2)
       end do
       write(6,*)
       write(6,*) ' Number of electrons a, b = ',numeleca,numelecb
       write(6,*)
       
       write(6,*) 
       write(6,*)  ' number of functionals to evaluate = ',number_of_functionals
       if (number_of_functionals.gt.0) then
        do i=1,number_of_functionals
         if (is_exch_libxc(i)) then
          if (is_corr_libxc(i)) then 
           write(6,*) ' Exch-Corr    Functional: ',trim(func_name_xc(i))
          else
           write(6,*) ' Exchange     Functional: ',trim(func_name_xc(i))
          end if
         else
          if (is_corr_libxc(i)) then 
           write(6,*) ' Correlation  Functional: ',trim(func_name_xc(i))
          else
           write(6,*) ' dunno know   Functional: ',trim(func_name_xc(i))
          end if
         end if
!        write(6,*) ' energy contribution with the libxc library :',E_xc_libxc(i)
        end do
       end if
       write(6,*) 
       write(6,*)  ' Energy contributions:  '
       write(6,*)  ' Nuclear                ',nuclear_repulsion
       write(6,*)  ' kinetic energy         ',E_kin
       write(6,*)  ' Coulomb e-n            ',E_ham
       write(6,*)  ' Coulomb e-e            ',E_Coul
real*8 :: E_total
       E_total=nuclear_repulsion+E_kin+E_ham+E_Coul
       if (lexex) then
        write(6,*)  ' Exact exchange energy  ',E_exch*exmul,' = E_exch x weight',E_exch,exmul
        E_total+=E_exch*exmul
       end if
       if (number_of_functionals.gt.0) then
        do i=1,number_of_functionals
         write(6,*)  ' DFT  contribution      ',E_xc_libxc(i),'   (',trim(func_name_xc(i)),')'
         E_total+=E_xc_libxc(i)
        end do
       end if
       write(6,*) 
       write(6,*) ' Total energy is ',E_total
       write(6,*) 

 
      write(6,*)  ' Kohn Sham AO matrix elements '
      do i=1,10
       do j=1,10
        if (abs(ksmat_libxc(i,j)).gt.1.D-6) write(6,*) i,j,ksmat_libxc(i,j)  &
                 ,potential_xc_alpha_ao_lda(i,j,1)+potential_xc_beta_ao_lda(i,j,1)
       end do
      end do
       
     end subroutine driver
     
