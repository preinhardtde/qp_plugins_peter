! -*- F90 -*-
     subroutine calc_xc_lda_libxc(func_id_xc,n_points,density,excarray,vxcarray)
!EGIN_DOC
! interface for calculating energy and matrix elements for an unpolarized LDA
! type functional
! only an electron density is passed to the library routine
!ND_DOC
       use xc_f03_lib_m
       implicit none
       TYPE(xc_f03_func_t) :: xc_func
       TYPE(xc_f03_func_info_t) :: xc_info

       integer :: i
       integer*8 :: np8
       character(len=120) :: kind, family

       real*8, allocatable :: density_unpolarized(:)

       real*8 :: density(n_points,2)
       real*8 :: excarray(n_points)
       real*8 :: vxcarray(n_points)
       integer :: func_id_xc,n_points

       allocate (density_unpolarized(n_points))
       do i=1,n_points
        density_unpolarized(i)=density(i,1) + density(i,2)
       end do

       excarray=0.D0
       vxcarray=0.D0
 
       np8=n_points

       call xc_f03_func_init(xc_func, func_id_xc, XC_UNPOLARIZED)
       xc_info = xc_f03_func_get_info(xc_func)
       write(6,*) ' Functional: ',trim(xc_f03_func_info_get_name(xc_info))

       call xc_f03_lda_exc_vxc(xc_func, np8, density_unpolarized, excarray, vxcarray)
! multiply with the density in order to be coherent with the qp2 program
       do i=1,n_points
        excarray(i)=excarray(i)*density_unpolarized(i)
       end do

       call xc_f03_func_end(xc_func)
       deallocate (density_unpolarized)

     end subroutine calc_xc_lda_libxc 

     subroutine calc_xc_gga_libxc(func_id_xc,n_points,density,grad_density,excarray,vxcarray)
!EGIN_DOC
! interface for calculating energy and matrix elements for an unpolarized GGA
! type functional
! electron density and contracted gradient |grad n| are passed to the library routine
!ND_DOC
       use xc_f03_lib_m
       implicit none
       TYPE(xc_f03_func_t) :: xc_func
       TYPE(xc_f03_func_info_t) :: xc_info

       integer :: i,j
       integer*8 :: np8
       character(len=120) :: kind, family

       real*8, allocatable :: density_unpolarized(:)
       real*8, allocatable :: cont_gradient(:)
       real*8, allocatable :: vsigxcarray(:,:)

       real*8 :: density(n_points,2),grad_density(3,n_points,2)
       real*8 :: excarray(n_points)
       real*8 :: vxcarray(n_points)

       integer :: func_id_xc,n_points

       allocate (density_unpolarized(n_points))
       allocate (cont_gradient(n_points))
       allocate (vsigxcarray(3,n_points))

       cont_gradient=0.D0
       do i=1,n_points
        density_unpolarized(i)=density(i,1)+density(i,2)
        do j=1,3
! in the non-polarized calculation we need to pass |grad rho_a + grad rho_b|^2
         cont_gradient(i)=cont_gradient(i) &
              +grad_density(j,i,1)*grad_density(j,i,1)       &
              +grad_density(j,i,1)*grad_density(j,i,2)       &
              +grad_density(j,i,2)*grad_density(j,i,2)*2.D0
        end do
       end do
 
       np8=n_points

       call xc_f03_func_init(xc_func, func_id_xc, XC_UNPOLARIZED)

       call xc_f03_gga_exc_vxc(xc_func, np8,  &
           density_unpolarized, cont_gradient, excarray, vxcarray,vsigxcarray)

       do i=1,n_points
         excarray(i)=density_unpolarized(i)*excarray(i)
       end do

!      do i=1,100
!       write(6,*) ' gga ',i,density_unpolarized(i),cont_gradient_unpolarized(1,i),excarray(i),vxcarray(i)
!      end do

       call xc_f03_func_end(xc_func)
       deallocate (density_unpolarized)
       deallocate (cont_gradient)
       deallocate (vsigxcarray)

     end subroutine calc_xc_gga_libxc 

     subroutine calc_xc_mgga_libxc(func_id_xc,n_points,density    &
            ,grad_density,laplacian,tau,excarray,vxcarray)
!EGIN_DOC
! interface for calculating energy and matrix elements for an unpolarized meta-GGA
! type functional
! electron density and contracted gradient are passed to the library routine
! and as well laplacian and kinetic-energy density 
!ND_DOC
       use xc_f03_lib_m
       implicit none
       TYPE(xc_f03_func_t) :: xc_func
       TYPE(xc_f03_func_info_t) :: xc_info

       integer :: i,j
       integer*8 :: np8
       character(len=120) :: kind, family

       real*8, allocatable :: density_unpolarized(:)
       real*8, allocatable :: cont_gradient_unpolarized(:)
       real*8, allocatable :: laplacian_unpolarized(:)
       real*8, allocatable :: tau_unpolarized(:)
       real*8, allocatable :: vsigxcarray(:)
       real*8, allocatable :: vlaplarray(:)
       real*8, allocatable :: vtauarray(:)

       real*8 :: density(n_points,2),grad_density(3,n_points,2)
       real*8 :: laplacian(n_points,2),tau(n_points,2)
       real*8 :: excarray(n_points)
       real*8 :: vxcarray(n_points)

       integer :: func_id_xc,n_points

       allocate (density_unpolarized(n_points))
       allocate (cont_gradient_unpolarized(n_points))
       allocate (laplacian_unpolarized(n_points))
       allocate (tau_unpolarized(n_points))
       allocate (vsigxcarray(n_points))
       allocate (vlaplarray(n_points))
       allocate (vtauarray(n_points))

       do i=1,n_points
        density_unpolarized(i)=density(i,1)+density(i,2)
        cont_gradient_unpolarized(i)=0.D0
        do j=1,3
         cont_gradient_unpolarized(i)=cont_gradient_unpolarized(i) &
              +grad_density(j,i,1)*grad_density(j,i,1)             &
              +grad_density(j,i,2)*grad_density(j,i,2)             &
              +2.D0*grad_density(j,i,1)*grad_density(j,i,2)
        end do
        laplacian_unpolarized(i)=laplacian(i,1)+laplacian(i,2)
        tau_unpolarized(i)=tau(i,1)+tau(i,2)
       end do
 
       np8=n_points

       call xc_f03_func_init(xc_func, func_id_xc, XC_UNPOLARIZED)

       call xc_f03_mgga_exc_vxc(xc_func, np8,  &
          density_unpolarized, cont_gradient_unpolarized &
         ,laplacian_unpolarized,tau_unpolarized &
         ,excarray,vxcarray,vsigxcarray,vlaplarray,vtauarray)

       do i=1,n_points
         excarray(i)=density_unpolarized(i)*excarray(i)
       end do

       call xc_f03_func_end(xc_func)
       deallocate (density_unpolarized)
       deallocate (cont_gradient_unpolarized)
       deallocate (laplacian_unpolarized)
       deallocate (tau_unpolarized)

     end subroutine calc_xc_mgga_libxc 

     subroutine calc_xc_lda_libxc_sp(func_id_xc &
           ,n_points,density,excarray,vaxcarray,vbxcarray)
!EGIN_DOC
! interface for calculating energy and matrix elements for an unpolarized LDA
! type functional
! only an electron density is passed to the library routine
!ND_DOC
       use xc_f03_lib_m
       implicit none
       TYPE(xc_f03_func_t) :: xc_func
       TYPE(xc_f03_func_info_t) :: xc_info

       integer :: i
       integer*8 :: np8
       character(len=120) :: kind, family

       real*8, allocatable :: density_polarized(:,:)
       real*8, allocatable :: vxcarray(:,:)

       real*8 :: density(n_points,2)
       real*8 :: excarray(n_points)
       real*8 :: vaxcarray(n_points)
       real*8 :: vbxcarray(n_points)
       integer :: func_id_xc,n_points

       allocate (density_polarized(2,n_points))
       allocate (vxcarray(2,n_points))

       do i=1,n_points
        density_polarized(1,i)=density(i,1)
        density_polarized(2,i)=density(i,2)
       end do

       excarray=0.D0
       vaxcarray=0.D0
       vbxcarray=0.D0
 
       np8=n_points

       call xc_f03_func_init(xc_func, func_id_xc, XC_POLARIZED)
       xc_info = xc_f03_func_get_info(xc_func)
       write(6,*) ' Functional: ',trim(xc_f03_func_info_get_name(xc_info))

       call xc_f03_lda_exc_vxc(xc_func, np8, density_polarized, excarray, vxcarray)
! multiply with the density in order to be coherent with the qp2 program
       do i=1,n_points
        excarray(i)=excarray(i)*(density_polarized(1,i)+density_polarized(2,i))
        vaxcarray(i)=vxcarray(1,i)
        vbxcarray(i)=vxcarray(2,i)
       end do

       call xc_f03_func_end(xc_func)
       deallocate (density_polarized)
       deallocate (vxcarray)

     end subroutine calc_xc_lda_libxc_sp

     subroutine calc_xc_gga_libxc_sp(func_id_xc,n_points,density,grad_density,excarray,vxcarray)
!EGIN_DOC
! interface for calculating energy and matrix elements for an unpolarized GGA
! type functional
! electron density and contracted gradient |grad n| are passed to the library routine
!ND_DOC
       use xc_f03_lib_m
       implicit none
       TYPE(xc_f03_func_t) :: xc_func
       TYPE(xc_f03_func_info_t) :: xc_info

       integer :: i,j
       integer*8 :: np8
       character(len=120) :: kind, family

       real*8, allocatable :: density_unpolarized(:)
       real*8, allocatable :: cont_gradient(:)
       real*8, allocatable :: vsigxcarray(:,:)

       real*8 :: density(n_points,2),grad_density(3,n_points,2)
       real*8 :: excarray(n_points)
       real*8 :: vxcarray(n_points)

       integer :: func_id_xc,n_points

       allocate (density_unpolarized(n_points))
       allocate (cont_gradient(n_points))
       allocate (vsigxcarray(3,n_points))

       cont_gradient=0.D0
       do i=1,n_points
        density_unpolarized(i)=density(i,1)+density(i,2)
        do j=1,3
! in the non-polarized calculation we need to pass |grad rho_a + grad rho_b|^2
         cont_gradient(i)=cont_gradient(i) &
              +grad_density(j,i,1)*grad_density(j,i,1)       &
              +grad_density(j,i,1)*grad_density(j,i,2)       &
              +grad_density(j,i,2)*grad_density(j,i,2)*2.D0
        end do
       end do
 
       np8=n_points

       call xc_f03_func_init(xc_func, func_id_xc, XC_UNPOLARIZED)

       call xc_f03_gga_exc_vxc(xc_func, np8,  &
           density_unpolarized, cont_gradient, excarray, vxcarray,vsigxcarray)

       do i=1,n_points
         excarray(i)=density_unpolarized(i)*excarray(i)
       end do

!      do i=1,100
!       write(6,*) ' gga ',i,density_unpolarized(i),cont_gradient_unpolarized(1,i),excarray(i),vxcarray(i)
!      end do

       call xc_f03_func_end(xc_func)
       deallocate (density_unpolarized)
       deallocate (cont_gradient)
       deallocate (vsigxcarray)

     end subroutine calc_xc_gga_libxc_sp

     subroutine calc_xc_mgga_libxc_sp(func_id_xc,n_points,density    &
            ,grad_density,laplacian,tau,excarray,vxcarray)
!EGIN_DOC
! interface for calculating energy and matrix elements for an unpolarized meta-GGA
! type functional
! electron density and contracted gradient are passed to the library routine
! and as well laplacian and kinetic-energy density 
!ND_DOC
       use xc_f03_lib_m
       implicit none
       TYPE(xc_f03_func_t) :: xc_func
       TYPE(xc_f03_func_info_t) :: xc_info

       integer :: i,j
       integer*8 :: np8
       character(len=120) :: kind, family

       real*8, allocatable :: density_unpolarized(:)
       real*8, allocatable :: cont_gradient_unpolarized(:)
       real*8, allocatable :: laplacian_unpolarized(:)
       real*8, allocatable :: tau_unpolarized(:)
       real*8, allocatable :: vsigxcarray(:)
       real*8, allocatable :: vlaplarray(:)
       real*8, allocatable :: vtauarray(:)

       real*8 :: density(n_points,2),grad_density(3,n_points,2)
       real*8 :: laplacian(n_points,2),tau(n_points,2)
       real*8 :: excarray(n_points)
       real*8 :: vxcarray(n_points)

       integer :: func_id_xc,n_points

       allocate (density_unpolarized(n_points))
       allocate (cont_gradient_unpolarized(n_points))
       allocate (laplacian_unpolarized(n_points))
       allocate (tau_unpolarized(n_points))
       allocate (vsigxcarray(n_points))
       allocate (vlaplarray(n_points))
       allocate (vtauarray(n_points))

       do i=1,n_points
        density_unpolarized(i)=density(i,1)+density(i,2)
        cont_gradient_unpolarized(i)=0.D0
        do j=1,3
         cont_gradient_unpolarized(i)=cont_gradient_unpolarized(i) &
              +grad_density(j,i,1)*grad_density(j,i,1)             &
              +grad_density(j,i,2)*grad_density(j,i,2)             &
              +2.D0*grad_density(j,i,1)*grad_density(j,i,2)
        end do
        laplacian_unpolarized(i)=laplacian(i,1)+laplacian(i,2)
        tau_unpolarized(i)=tau(i,1)+tau(i,2)
       end do
 
       np8=n_points

       call xc_f03_func_init(xc_func, func_id_xc, XC_UNPOLARIZED)

       call xc_f03_mgga_exc_vxc(xc_func, np8,  &
          density_unpolarized, cont_gradient_unpolarized &
         ,laplacian_unpolarized,tau_unpolarized &
         ,excarray,vxcarray,vsigxcarray,vlaplarray,vtauarray)

       do i=1,n_points
         excarray(i)=density_unpolarized(i)*excarray(i)
       end do

       call xc_f03_func_end(xc_func)
       deallocate (density_unpolarized)
       deallocate (cont_gradient_unpolarized)
       deallocate (laplacian_unpolarized)
       deallocate (tau_unpolarized)

     end subroutine calc_xc_mgga_libxc_sp
