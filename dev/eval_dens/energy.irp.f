! -*- F90 -*-
 BEGIN_PROVIDER [ real*8, E_two ]
&BEGIN_PROVIDER [ real*8, E_coul ]
&BEGIN_PROVIDER [ real*8, E_exch ]
BEGIN_DOC
! the two-electron energy as contraction of XI with P
END_DOC
     implicit none  
     integer  :: ialph,ibeta

     e_two=0.d0
     e_coul=0.d0
     e_exch=0.d0
     do ialph=1,ao_num
      do ibeta=1,ao_num
       e_two+=density(ialph,ibeta)*xi_scf(ialph,ibeta)
       e_Coul+=density(ialph,ibeta)*xi_coul(ialph,ibeta)
       e_Exch+=density(ialph,ibeta)*xi_exch(ialph,ibeta)
      end do
     end do
     e_two*=0.5d0
     e_coul*=0.5d0
     e_exch*=0.5d0
     write(6,*) ' etwo = ',e_two,' = Coul ',e_coul,' + Excg ',E_exch
END_PROVIDER  

BEGIN_PROVIDER [ real*8, E_one ]
    E_one=E_kin + E_ham
     write(6,*) ' ekin = ',e_kin
     write(6,*) ' eham = ',e_ham
     write(6,*) ' eone = ',e_one
END_PROVIDER

 BEGIN_PROVIDER [ real*8, E_kin ]
&BEGIN_PROVIDER [ real*8, E_ham ]
    implicit none
    integer :: ialph,ibeta

    e_kin=0.d0
    e_ham=0.d0
    do ialph=1,ao_num
     do ibeta=1,ao_num
      e_kin+=density(ialph,ibeta)*ao_kinetic_integrals(ialph,ibeta)
      e_ham+=density(ialph,ibeta)*ao_integrals_n_e(ialph,ibeta)
     end do
    end do

END_PROVIDER
