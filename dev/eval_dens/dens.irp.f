! -*- F90 -*-
BEGIN_PROVIDER [ real*8, density, (ao_num,ao_num) ]
      implicit none 
      integer :: ialph,ibeta,iorb
      real*8 :: cia
      
      write(6,*) ' constructing density density '
      
      density=0.D0
      
      do iorb=1,elec_alpha_num
       do ialph=1,ao_num
        cia=mo_coef(ialph,iorb)
        do ibeta=1,ao_num
         density(ialph,ibeta)+=cia*mo_coef(ibeta,iorb)
        end do
       end do
      end do
      
      density*=2.D0

END_PROVIDER
