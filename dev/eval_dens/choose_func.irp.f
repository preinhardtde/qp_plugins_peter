! -*- F90 -*-
 BEGIN_PROVIDER [ integer, number_of_functionals]
&BEGIN_PROVIDER [ integer, choice_of_functional ]
BEGIN_DOC
! we choose a functional, which may be composed of 
! several functionals of different types
!
END_DOC
       implicit none
 100   continue
       write(6,*) ' please choose the functional from the following list '
       write(6,*) ' LDA            1 '
       write(6,*) ' PW91           2 '
       write(6,*) ' Colle-Salvetti 3 '
       write(6,*) ' PBE            4 '
       write(6,*) ' B3LYP          5 '
       write(6,*) ' Hartree-Fock   6 '
       read(5,*,err=100) choice_of_functional
       if (choice_of_functional.lt.1.or.choice_of_functional.gt.6) then
        write(6,*) ' please choose a number between 1 and 5 '
        go to 100
       end if

       select case (choice_of_functional)
       case (1)
! LDA VWN5
         number_of_functionals=2
       case (2)
! PW91 XC
         number_of_functionals=2
       case (3)
! HF exchange, Colle-Salvetti correlation
         number_of_functionals=1
       case (4)
! PBE
         number_of_functionals=2
       case (5)
! B3LYP
         number_of_functionals=1
       case (6)
! Hartree-Fock
         number_of_functionals=0
         lexex=.true.
         exmul=1.D0
       end select
    
END_PROVIDER

BEGIN_PROVIDER [logical, lexex]
&BEGIN_PROVIDER [real*8, exmul]
END_PROVIDER
                   
BEGIN_PROVIDER [ integer, xcnum_libxc, (number_of_functionals)]
BEGIN_DOC
! composition of different functionals
!
!
END_DOC
       implicit none

       select case (choice_of_functional)
       case (1)
! LDA VWN5
         xcnum_libxc(1)=1
         xcnum_libxc(2)=7
         lexex=.false.
       case (2)
! PW91 XC
         xcnum_libxc(1)=109
         xcnum_libxc(2)=134
         lexex=.false.
       case (3)
! HF exchange, Colle-Salvetti correlation
         xcnum_libxc(1)=72
         lexex=.true.
         exmul=1.D0
       case (4)
! PBE
         xcnum_libxc(1)=101
         xcnum_libxc(2)=216
         lexex=.false.
       case (5)
! B3LYP
         xcnum_libxc(1)=402
         lexex=.true.
         exmul=0.2D0
       case (6)
! Hartree-Fock
         write(6,*) ' we should not arrive here '
       end select
    
END_PROVIDER
