! -*- F90 -*-
     subroutine calc_xc_lda_libxc(func_id_xc,n_points,density,excarray,vxcarray)
!EGIN_DOC
! interface for calculating energy and matrix elements for an unpolarized LDA
! type functional
! only an electron density is passed to the library routine
!ND_DOC
       use xc_f03_lib_m
       implicit none
       TYPE(xc_f03_func_t) :: xc_func
       TYPE(xc_f03_func_info_t) :: xc_info

       integer :: i
       integer*8 :: np8
       character(len=120) :: kind, family

       real*8, allocatable :: density_unpolarized(:)

       real*8 :: density(n_points,2)
       real*8 :: excarray(n_points)
       real*8 :: vxcarray(n_points)
       integer :: func_id_xc,n_points

       allocate (density_unpolarized(n_points))
       do i=1,n_points
        density_unpolarized(i)=density(i,1) +density(i,2)
       end do

       excarray=0.D0
       vxcarray=0.D0
 
       np8=n_points

       call xc_f03_func_init(xc_func, func_id_xc, XC_UNPOLARIZED)
       xc_info = xc_f03_func_get_info(xc_func)
       write(6,*) ' Functional: ',trim(xc_f03_func_info_get_name(xc_info))

       call xc_f03_lda_exc_vxc(xc_func, np8, density_unpolarized, excarray, vxcarray)

!      do i=1,10
!       write(6,*) density_unpolarized(i),excarray(i),vxcarray(i)
!      end do

       call xc_f03_func_end(xc_func)
       deallocate (density_unpolarized)

     end subroutine calc_xc_lda_libxc 

     subroutine calc_xc_gga_libxc(func_id_xc,n_points,density,grad_density,excarray,vxcarray)
!EGIN_DOC
! interface for calculating energy and matrix elements for an unpolarized GGA
! type functional
! electron density and gradient are passed to the library routine
!ND_DOC
       use xc_f03_lib_m
       implicit none
       TYPE(xc_f03_func_t) :: xc_func
       TYPE(xc_f03_func_info_t) :: xc_info

       integer :: i,j
       integer*8 :: np8
       character(len=120) :: kind, family

       real*8, allocatable :: density_unpolarized(:)
       real*8, allocatable :: gradient_unpolarized(:,:)

       real*8 :: density(n_points,2),grad_density(3,n_points,2)
       real*8 :: excarray(n_points)
       real*8 :: vxcarray(n_points)
       real*8 :: vsigxcarray(3*n_points)

       integer :: func_id_xc,n_points

       allocate (density_unpolarized(n_points))
       allocate (gradient_unpolarized(3,n_points))

       do i=1,n_points
        density_unpolarized(i)=density(i,1)+density(i,2)
       end do
 
       do i=1,n_points
        do j=1,3
         gradient_unpolarized(j,i)=grad_density(j,i,1)+grad_density(j,i,2)
        end do
       end do
 
       np8=n_points

       call xc_f03_func_init(xc_func, func_id_xc, XC_UNPOLARIZED)

       call xc_f03_gga_exc_vxc(xc_func, np8,  &
           density_unpolarized, gradient_unpolarized, excarray, vxcarray,vsigxcarray)

       call xc_f03_func_end(xc_func)
       deallocate (density_unpolarized)
       deallocate (gradient_unpolarized)

     end subroutine calc_xc_gga_libxc 

