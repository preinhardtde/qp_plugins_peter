! -*- F90 -*-
     subroutine setup_libxc(func_id_xc,func_name,is_lda,is_gga,is_mgga,is_exch,is_corr)
       use xc_f03_lib_m
       implicit none
       logical :: is_lda,is_gga,is_exch,is_corr,is_mgga
       TYPE(xc_f03_func_t) :: xc_func
       TYPE(xc_f03_func_info_t) :: xc_info

       integer :: vmajor, vminor, vmicro, func_id_xc,i,n_param
       character(len=120) :: kind, family

       character*220 :: func_name

       write(6,*)
       write(6,*) ' ------------------------------ '
       write(6,*)
       call xc_f03_version(vmajor, vminor, vmicro)
       write(*,'("Libxc version: ",I1,".",I1,".",I1)') vmajor, vminor, vmicro

       call xc_f03_func_init(xc_func, func_id_xc, XC_UNPOLARIZED)
       xc_info = xc_f03_func_get_info(xc_func)

       is_exch=.false.
       is_corr=.false.
       select case(xc_f03_func_info_get_kind(xc_info))
       case (XC_EXCHANGE)
       write(kind, '(a)') 'an exchange functional'
       is_exch=.true.
       is_corr=.false.
       case (XC_CORRELATION)
       write(kind, '(a)') 'a correlation functional'
       is_corr=.true.
       is_exch=.false.
       case (XC_EXCHANGE_CORRELATION)
       is_exch=.true.
       is_corr=.true.
       write(kind, '(a)') 'an exchange-correlation functional'
       case (XC_KINETIC)
       write(kind, '(a)') 'a kinetic energy functional'
       case default
       write(kind, '(a)') 'of unknown kind'
       end select
       
       is_lda=.false.
       is_gga=.false.
       is_mgga=.false.
       select case (xc_f03_func_info_get_family(xc_info))
       case (XC_FAMILY_LDA);
       write(family,'(a)') "LDA"
       is_lda=.true.
       case (XC_FAMILY_GGA);
       write(family,'(a)') "GGA"
       is_gga=.true.
       case (XC_FAMILY_MGGA);
       write(family,'(a)') "MGGA"
       is_mgga=.true.
       case default;
       write(family,'(a)') "unknown"
       end select
       
       write(6,*)
       write(*,'("The functional ''", a, "'' is ", a, ",      &
           it belongs to the ''", a &
          , "'' family and is defined in the reference(s):")') &
       trim(xc_f03_func_info_get_name(xc_info)), trim(kind), trim(family)
       
       i = 0
       do while(i >= 0)
       write(*, '(a,i1,2a)') '[', i+1, '] ' &
           , trim(xc_f03_func_reference_get_ref( & 
           xc_f03_func_info_get_references(xc_info, i)))
       end do

       func_name=trim(xc_f03_func_info_get_name(xc_info))

       n_param=xc_f03_func_info_get_n_ext_params(xc_info)
       write(6,*) ' Number of ext param: ',n_param
       do i=0,n_param-1
        write(6,*) ' Parameter No ',i+1
        write(6,*) '  Name          :',xc_f03_func_info_get_ext_params_name(xc_info,i)
        write(6,*) '  Description   :',xc_f03_func_info_get_ext_params_description(xc_info,i)
        write(6,*) '  Default value :',xc_f03_func_info_get_ext_params_default_value(xc_info,i)
       end do
       write(6,*)
       
       call xc_f03_func_end(xc_func)
       
     end subroutine setup_libxc

