=========
model_dft
=========

A model DFT program as interface for the xcfun package
 - it sets up the grid
 - performs KS iterations
 - calculates the totel energy
 - stops
