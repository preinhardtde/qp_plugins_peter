! -*- F90 -*-
BEGIN_PROVIDER [real*8, d_orbital, (ao_num)]
END_PROVIDER

 BEGIN_PROVIDER [integer, indx_file, (abmax)]
&BEGIN_PROVIDER [integer, indx_ezfio, (abmax)]
END_PROVIDER

BEGIN_PROVIDER [integer, abmax]
      abmax=max(elec_alpha_num,elec_beta_num)+1
END_PROVIDER

BEGIN_PROVIDER [real*8, d_orbital_contrib, (ao_num)]
END_PROVIDER

           
          subroutine fill_d_orbital_contrib
BEGIN_DOC
! we have to extract from the matrix of the orbital overlaps
! the ones which are occupied, in det_string_0_1, and in 
! list_det_int2(1,j)
END_DOC
         implicit none
         integer :: indx1, indx2,i,j,common_elec_num
         integer :: other_elec_num


         d_orbital_contrib=0.D0

! calculate the common factor
         mo_overlap_tmp=0.D0
         indx1=0
         indx2=0
         if (e_is_alpha) then
          do i=1,mo_num
           if (det_string_0_1(i,2).eq.1) then
            indx1+=1
            indx_ezfio(indx1)=i
           end if
           if (list_det_int2(2,i).eq.1) then
            indx2+=1
            indx_file(indx2)=i
           end if
          end do
          common_elec_num=elec_beta_num
         else
          do i=1,mo_num
           if (det_string_0_1(i,1).eq.1) then
            indx1+=1
            indx_ezfio(indx1)=i
           end if
           if (list_det_int2(1,i).eq.1) then
            indx2+=1
            indx_file(indx2)=i
           end if
          end do
          common_elec_num=elec_alpha_num
         end if
! get the determinant
real*8 :: common_factor,other_factor
         do i=1,common_elec_num
          do j=1,common_elec_num
           mo_overlap_tmp(i,j)=mo_overlap_1_2(indx_ezfio(i),indx_file(j))
          end do
         end do
         call calc_det(common_elec_num,common_factor)

! now the loop over the spin with the difference


         mo_overlap_tmp=0.D0
         indx1=0
         indx2=0
         indx_ezfio=0
         indx_file=0
          
         if (e_is_alpha) then
          do i=1,mo_num
           if (det_string_0_1(i,1).eq.1) then
            indx1+=1
            indx_ezfio(indx1)=i
           end if
           if (list_det_int2(1,i).eq.1) then
            indx2+=1
            indx_file(indx2)=i
           end if
          end do
          common_elec_num=max(indx1,indx2)
         else
          do i=1,mo_num
           if (det_string_0_1(i,2).eq.1) then
            indx1+=1
            indx_ezfio(indx1)=i
           end if
           if (list_det_int2(2,i).eq.1) then
            indx2+=1
            indx_file(indx2)=i
           end if
          end do
          common_elec_num=max(indx1,indx2)
         end if
         write(6,*) '  common_elec_num = ', common_elec_num

         if (e_is_ezfio) then
! the extra electron is in the ezfio, we run over the this array
real*8 :: perm
          perm=1.D0
          do indx1=1,common_elec_num
           write(6,*) ' indx1 = ',indx1
           do i=1,indx1-1
            do j=1,common_elec_num-1
             mo_overlap_tmp(i,j)=mo_overlap_1_2(indx_ezfio(i),indx_file(j))
             if (mo_overlap_tmp(i,j).ne.0.D0) write(6,*) 'I- ',i,j,indx_ezfio(i),indx_file(j),mo_overlap_tmp(i,j)
            end do
           end do
           do i=indx1+1,common_elec_num
            do j=1,common_elec_num-1
             mo_overlap_tmp(i-1,j)=mo_overlap_1_2(indx_ezfio(i),indx_file(j))
             if (mo_overlap_tmp(i-1,j).ne.0.D0) write(6,*) 'II- ',i-1,j,mo_overlap_tmp(i-1,j)
            end do
           end do
           call calc_det(common_elec_num-1,other_factor)
           do i=1,ao_num
            d_orbital_contrib(i)+=perm*other_factor*mo_coef(i,indx1)
           end do
           perm=-perm
          end do
         else
          perm=1.D0
          do indx1=1,common_elec_num
           write(6,*) ' indx1 = ',indx1
           do i=1,indx1-1
            do j=1,common_elec_num-1
             mo_overlap_tmp(i,j)=mo_overlap_1_2(indx_ezfio(j),indx_file(i))
             if (mo_overlap_tmp(i,j).ne.0.D0) write(6,*) 'III- ',i,j,mo_overlap_tmp(i,j)
            end do
           end do
           do i=indx1+1,common_elec_num
            do j=1,common_elec_num-1
             mo_overlap_tmp(i-1,j)=mo_overlap_1_2(indx_ezfio(j),indx_file(i))
             if (mo_overlap_tmp(i-1,j).ne.0.D0) write(6,*) 'IV- ',i-1,j,mo_overlap_tmp(i-1,j)
            end do
           end do
           call calc_det(common_elec_num-1,other_factor)
           do i=1,ao_num
            d_orbital_contrib(i)+=perm*other_factor*mo_coef2(i,indx1)
           end do
           perm=-perm
          end do
         end if
         d_orbital_contrib*=common_factor
   
!      if (abs(common_factor).gt.1.D-7) then
         write(6,*) ' calculating contribution of the two determinants '
         write(6,*) ' contribution to dyson orbital:'
         write(6,'(6(i4,F12.6))') (i,d_orbital_contrib(i),i=1,ao_num )
!      end if
real*8 :: sss
        sss=0.D0
        do i=1,ao_num
         do j=1,ao_num
          sss+=d_orbital_contrib(i)*d_orbital_contrib(j)*ao_overlap(i,j)
         end do
        end do
        sss=sqrt(sss)
        write(6,*) ' Norm of the correction ',sss
    
          end subroutine fill_d_orbital_contrib
