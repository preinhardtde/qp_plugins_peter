! -*- F90 -*-
use bitmasks

 BEGIN_PROVIDER [real*8, nz2, (nucl_num)]
&BEGIN_PROVIDER [real*8, pos2, (3,nucl_num)]
END_PROVIDER

 BEGIN_PROVIDER [integer, list_alpha, (n_int*bit_kind_size)]
&BEGIN_PROVIDER [integer, list_beta,  (n_int*bit_kind_size)]
END_PROVIDER

 BEGIN_PROVIDER [integer, mo_num2]
&BEGIN_PROVIDER [integer, ao_num2]
&BEGIN_PROVIDER [integer, elec_alpha_num2]
&BEGIN_PROVIDER [integer, elec_beta_num2]
&BEGIN_PROVIDER [integer, n_det2]
&BEGIN_PROVIDER [real*8, e_nuc2]
      implicit none
      integer :: i,idum,jdum,kdum,j
      open(unit=12,file='Basis_and_MOs.dat',status='old',form='formatted')
      e_nuc2=0.D0
      read(12,*) idum
      do i=1,idum
       read(12,*) nz2(i),(pos2(j,i),j=1,3)
       do j=1,i-1
real*8 :: distij
        distij=sqrt((pos2(1,i)-pos2(1,j))*(pos2(1,i)-pos2(1,j))   &
                   +(pos2(2,i)-pos2(2,j))*(pos2(2,i)-pos2(2,j))   &
                   +(pos2(3,i)-pos2(3,j))*(pos2(3,i)-pos2(3,j)))
        e_nuc2+=dble(nz2(i)*nz2(j))/distij
       end do
      end do
      read(12,*) ao_num2,idum,mo_num2
      read(12,*) elec_alpha_num2,elec_beta_num2
      close(12)
      write(6,*) ' read information from file <Basis_and_MOs.dat>:'
      write(6,*) ' aos,mos, alpha, beta electrons= ',ao_num2,mo_num2, elec_alpha_num2,elec_beta_num2
!
      open(unit=12,file='Wavefunction.dat',status='old',form='formatted')
      read(12,*) idum,n_det2,jdum,kdum
      close(12)
      if ((idum.ne.mo_num2).or.(jdum.ne.elec_alpha_num2).or.(kdum.ne.elec_beta_num2)) then
       write(6,*) ' mo_num2 = ',idum,mo_num2
       write(6,*) '  elec_alpha_num2 = ',jdum,elec_alpha_num2
       write(6,*) '  elec_beta_num2  = ',kdum,elec_beta_num2
       write(6,*) ' Wavefunction and orbitals do not correspond '
       stop ' inconsistencies '
      end if
      write(6,*) ' read information from file <Wavefunction.dat>:'
      write(6,*) ' ndet = ',n_det2

END_PROVIDER

BEGIN_PROVIDER [real*8, mo_coef2, (ao_num, mo_num)]
BEGIN_DOC
! the orbitals read from file
! we have to read the basis set first, then the orbitals
END_DOC
      implicit none 
      integer :: i,j,k,l,idum
      real*8 :: rdum,xdum,cdum
      write(6,*) 
      write(6,*)  ' reading orbitals from file '
      write(6,*) 
      mo_coef2=0.D0

      open(unit=12,file='Basis_and_MOs.dat',status='old',form='formatted')
! read geometry
      read(12,*) idum
      do i=1,idum
       read(12,*)
      end do
! read two dummy lines
      read(12,*) idum
      read(12,*) idum
! read basis set
      do i=1,ao_num
       read(12,*) idum,idum,idum,idum,k
       do l=1,k
        read(12,*) rdum
       end do
      end do
! now we can read the orbitals
      do k=1,mo_num
       read(12,*)
       read(12,*)
       read(12,*) (mo_coef2(l,k),l=1,ao_num)
      end do
      close(12)
!
! are they orthonormal ?
real*8 :: ss
integer :: alpha,beta
      do i=1,mo_num
       ss=0.D0
       do alpha=1,ao_num
        do beta=1,ao_num
         ss+=mo_coef2(alpha,i)*mo_coef2(beta,i)*ao_overlap(alpha,beta)
        end do
       end do
!      write(6,*) ' norm of orbital ',i,' is ',sqrt(ss)
       if (abs(sqrt(ss)-1.D0).gt.1.D-6) then
         write(6,*) ' norm of orbital ',i,' read from file is ',sqrt(ss)
         stop ' bad norm '
       end if
      end do
!
END_PROVIDER 

BEGIN_PROVIDER [integer, det_string_0_1, (mo_num,2)]
      implicit none
      det_string_0_1=0
END_PROVIDER 

!   subroutine dyson_per_orbital(dyson_orb)
!EGIN_DOC
!  from the filled det_string_0_1 we generate a Dyson orbital
!  with respect to the wavefunction of the ezfio
! we have to run over the list of determinants, get the occupation
! pattern, set up the overlap matrices, calculate determinants, 
! and assemble all for having in the end just one orbital.
!ND_DOC
!     implicit none
!     real*8 :: dyson_orb(ao_num)
!     dyson_orb=0

!     do idet=1,n_det
!BEGIN_DOC
! we extract the determinant
! transform it to a string and read the string one-by-one
!END_DOC
!     integer(bit_kind), allocatable :: det_i(:,:)
!     allocate(det_i(N_int,2))

!     integer :: idet,iorb
!     integer*8 :: imask,i_nibble,ipos

!     write(6,*) ' Treating determinant No ',idet
!     det_string_0_1=0
!     call det_extract(det_i,idet,n_int)
!     call print_det(det_i,n_int)

!     write(6,*) ' N_int = ',n_int
!     do iorb=1,mo_num
! which integer to look at ?
!      i_nibble = shiftr(iorb-1,bit_kind_shift)+1
!      ipos = iorb-shiftl(i_nibble-1,bit_kind_shift)-1
!      imask = ibset(0_bit_kind,ipos)
!      write(6,*) ' iorb, i_nibble, ipos, imask = ',iorb, i_nibble, ipos, imask 
! is there a alpha spin ?
!      if (iand(det_i(i_nibble,1),imask) /= 0_bit_kind) then
!       det_string_0_1(iorb,1)=1
!       write(6,*) ' found alpha electron at position ',iorb
!      end if
! is there a beta spin ?
!      if (iand(det_i(i_nibble,2),imask) /= 0_bit_kind) then
!       det_string_0_1(iorb,2)=1
!       write(6,*) ' found beta electron at position ',iorb
!      end if
!     end do

!     deallocate(det_i)

!   
!   end subroutine dyson_per_orbital
