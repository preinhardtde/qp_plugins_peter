=============
dyson_orbital
=============

For creating a Dyson orbital we need two wavefunctions
which differ in one electron. We have to calculate 
<Psi_(n-1)|Psi_n> = int_(n-1) Psi_(n-1)(r_1, r_2, ..., r_(n-1)) 
     Psi(r_1, r_2, r_(n-1), r) dr_1 ... dr_(n-1) leaving a function 
of r_n, i.e. an orbital.

Of course, spins in the n-1 part have to match.

One wavefunction we get from the ezfio, and the other one with 
corresponding orbitals from the files 'Basis_and_MOs.dat' and 
'Wavefunction.dat'.

We need the overlap matrix between the two orbital 
sets (which run over the same atomic basis). Within each set orbitals 
are orthogonal, but not between orbital sets.

Each wavefunction is a sum over determinants.

We have to take a pair of determinants, and calculate their overlap.

- check all dimensions and matches
- read xata from the ezfio
- read data from the files
- normalize all orbitals
- calculate the overlap matrix between the two orbital sets
- normalize the wavefunctions

