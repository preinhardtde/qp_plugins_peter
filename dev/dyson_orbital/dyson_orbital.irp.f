program dyson_orbital
  implicit none
  BEGIN_DOC
! TODO : Put the documentation of the program here
  END_DOC
 read_wf = .True.
 touch read_wf
real*8 :: cpu0,wall0
  call cpu_time(cpu0)
  call wall_time(wall0)

  call header
  call setup
  call driver

  call cpustamp(cpu0,wall0,cpu0,wall0,'All ')
end

   subroutine header
     write(6,*)
     write(6,*) ' Determining a Dyson orbital from two wavefunctions '
     write(6,*) ' P Reinhardt (Coulanges-la-Vineuse, December 2020) '
     write(6,*)
     write(6,*)
   end subroutine header
 
   subroutine setup
      implicit none
      integer :: i,j
      real*8 :: ss,frob

      write(6,*)
      write(6,*) ' First wavefunction (ezfio): '
      write(6,*) ' ao_num, mo_num, elec_alpha_num, elec_beta_num, n_det ' &
            ,ao_num, mo_num, elec_alpha_num, elec_beta_num, n_det
      if (n_states.ne.1) then
       write(6,*) ' First WF contains ',n_states,' states. We can handle only one'
       stop ' Too many states '
      end if
      write(6,*)
      write(6,*) ' Second wavefunction (from files): '
      write(6,*) ' ao_num, mo_num, elec_alpha_num, elec_beta_num, n_det '  &
           ,ao_num2, mo_num2, elec_alpha_num2, elec_beta_num2, n_det2
      write(6,*)
!
! we have to test whether the geometry is the same. 
! the easiest comparison is the nuclear repulsion
!
       if (nucl_num.eq.1) then
        write(6,*) ' 1 atom: nuclear charge ',nucl_charge(1),nz2(1),nucl_charge(1)-nz2(1)
       else
        write(6,*) ' nuclear repulsion ',nuclear_repulsion,e_nuc2,nuclear_repulsion-e_nuc2
       end if
       write(6,*) 

       write(6,*) ' diagonal of the overlap matrix between the two orbital sets '
       write(6,'(5(i4,F12.5))') (i,mo_overlap_1_2(i,i),i=1,mo_num)

       frob=0.D0
       do i=1,mo_num
        do j=1,mo_num
         frob+=mo_overlap_1_2(i,j)*mo_overlap_1_2(i,j)
        end do
       end do
       frob*=1.D0/mo_num
       write(6,*) 
       write(6,*) ' Frobenius inner product: ',frob,' (should be 1) '
       write(6,*) 

       write(6,*) ' - - - - - - - - - - - - - - - - - - - - - - - - - -'
       if (e_is_alpha) then 
        write(6,*) ' the difference is in the number of alpha electrons '
       else
        write(6,*) ' the difference is in the number of beta electrons '
       end if
       if (e_is_ezfio) then 
        write(6,*) ' the excess electron is on the ezfio '
       else
        write(6,*) ' the excess electron is on the external orbital file '
       end if
       write(6,*) ' - - - - - - - - - - - - - - - - - - - - - - - - - -'

   end subroutine setup

   subroutine driver
      implicit none
      integer :: i,j,idet,jdet,idum
      real*8 :: det,comfac,psi_coef2

      d_orbital=0.D0
      open(unit=12,file='Wavefunction.dat',status='old',form='formatted')
      read(12,*) idum
! a double loop, 
! jdet we take as sequence of 0 and 1 from the file
      do jdet=1,n_det2
       read(12,*) idum,psi_coef2
       if (idum.ne.jdet) stop ' idum, jdet : inconsistency '
       read(12,*) (list_det_int2(1,j),j=1,mo_num)
       read(12,*) (list_det_int2(2,j),j=1,mo_num)
       do j=1,mo_num
        if (list_det_int2(1,j).eq.1) then
         list_det_char(1,j)='+'
        else
         list_det_char(1,j)='-'
        end if
        if (list_det_int2(2,j).eq.1) then
         list_det_char(2,j)='+'
        else
         list_det_char(2,j)='-'
        end if
       end do
       write(6,*) ' jdet = ',jdet,psi_coef2
       write(6,'(10(1H|,10A1))') (list_det_char(1,j),j=1,mo_num)
       write(6,'(10(1H|,10A1))') (list_det_char(2,j),j=1,mo_num)
! idet we have from the ezfio, it is in det_string_0_1
       do idet=1,n_det
        call code_det(idet)
        do j=1,mo_num
         if (det_string_0_1(j,1).eq.1) then
          list_det_char(1,j)='+'
         else
          list_det_char(1,j)='-'
         end if
         if (det_string_0_1(j,2).eq.1) then
          list_det_char(2,j)='+'
         else
          list_det_char(2,j)='-'
         end if
        end do
        call fill_d_orbital_contrib
!       write(6,*) 'idet = ',idet,psi_coef(idet,1)
!       write(6,'(5HIDET ,10(1H|,10A1))') (list_det_char(1,j),j=1,mo_num)
!       write(6,'(5HIDET ,10(1H|,10A1))') (list_det_char(2,j),j=1,mo_num)
        d_orbital+=d_orbital_contrib*psi_coef(idet,1)*psi_coef2
       end do
      end do
      close(12)
! normalize the orbital
real*8 :: sss

      sss=0.D0
      do i=1,ao_num
       do j=1,ao_num
        sss+=d_orbital(i)*d_orbital(j)*ao_overlap(i,j)
       end do
      end do
      sss=1.D0/sqrt(sss)
      d_orbital*=sss

      write(6,*) ' The obtained Dyson orbital:'
      do i=1,ao_num
       write(6,*) i,d_orbital(i)
      end do
      
   end subroutine driver
