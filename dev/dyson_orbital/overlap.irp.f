! -*- F90 -*-
BEGIN_PROVIDER [real*8, mo_overlap_1_2, (mo_num,mo_num)]
BEGIN_DOC
!
! < phi_i(ezfio) | phi_j(files) >
!
END_DOC
          implicit none
          integer :: i,j,k,alpha,beta
          real*8 :: ss,cia,cjb

          open(unit=12,file='MO_OVERLAP_1_2',status='unknown',form='formatted')
          mo_overlap_1_2=0.D0
          do i=1,mo_num
           do j=1,mo_num
            ss=0.D0
            do alpha=1,ao_num
             cia=mo_coef(alpha,i)
             do beta=1,ao_num
              cjb=mo_coef2(beta,j)
              ss+=cia*cjb*ao_overlap(alpha,beta)
             end do
            end do
            mo_overlap_1_2(i,j)=ss
            if (abs(mo_overlap_1_2(i,j)).gt.1.D-8) write(12,*) i,j,mo_overlap_1_2(i,j)
           end do
          end do
          close(12)

! best match
          write(6,*)
          write(6,*) ' looking of best matches '
real*8 :: maxS
integer :: indx
          do i=1,mo_num
           maxS=0.D0
           do j=1,mo_num
            if (abs(mo_overlap_1_2(i,j)).gt.maxS) then
             maxS=abs(mo_overlap_1_2(i,j))
             indx=j
            end if
           end do
           write(6,*) ' Orbital No ',i,' matches orbital ',indx,' with overlap ',maxS
          end do
          write(6,*)
END_PROVIDER

BEGIN_PROVIDER [real*8, mo_overlap_tmp, (mo_num,mo_num)]
END_PROVIDER

 BEGIN_PROVIDER [real*8, eigval, (mo_num)]
&BEGIN_PROVIDER [real*8, eigvec, (mo_num,mo_num)]
END_PROVIDER

     subroutine calc_det(nval,det)
BEGIN_DOC
! determinant via LU decomposition
! LAPACK has this as dgetrf
!
END_DOC
       implicit none
       integer :: nval,i,info,j
       real*8 :: det
       integer, allocatable :: ipiv(:)
       allocate (ipiv(nval))
!
!      write(6,*) 'A={{'
!      do i=1,nval
!       do j=1,nval
!        if (abs(mo_overlap_tmp(i,j)).gt.1.D-5) then
!         write(6,'(F12.5,1h,)') mo_overlap_tmp(i,j)
!        else
!         write(6,*) ' 0.,'
!        end if
!       end do
!       write(6,*) '},{'
!      end do
!      write(6,*) '}'

       call DGETRF(nval, nval, mo_overlap_tmp, mo_num, IPIV, INFO )
!      write(6,*) ipiv
!      write(6,*) 

       det=1.D0
       do i=1,nval
        det*=mo_overlap_tmp(i,i)
       end do
       deallocate (ipiv)

!      if (abs(det).gt.1.D-7) write(6,*) ' calculated determinant is ',det,' nval = ',nval
       write(6,*) ' calculated determinant is ',det,' nval = ',nval

     end subroutine calc_det
!
