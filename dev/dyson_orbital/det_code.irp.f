! -*- F90 -*-
 BEGIN_PROVIDER [integer, list_det_int2, (2,mo_num)]
&BEGIN_PROVIDER [character*1, list_det_char, (2,mo_num)]
END_PROVIDER

     subroutine code_det(idet)
      use bitmasks
      implicit none
      integer :: i,idet
BEGIN_DOC
! we get the occupation pattern for idet and store it in a list
END_DOC
      integer(bit_kind), allocatable :: det_i(:,:)
      allocate(det_i(N_int,2))

      integer :: iorb
      integer*8 :: imask,i_nibble,ipos

!     write(6,*) ' Treating determinant No ',idet
      det_string_0_1=0
      call det_extract(det_i,idet,n_int)
!     call print_det(det_i,n_int)

      do iorb=1,mo_num
! which integer to look at ?
       i_nibble = shiftr(iorb-1,bit_kind_shift)+1
       ipos = iorb-shiftl(i_nibble-1,bit_kind_shift)-1
       imask = ibset(0_bit_kind,ipos)
!      write(6,*) ' iorb, i_nibble, ipos, imask = ',iorb, i_nibble, ipos, imask 
! is there a alpha spin ?
       if (iand(det_i(i_nibble,1),imask) /= 0_bit_kind) then
        det_string_0_1(iorb,1)=1
!       write(6,*) ' found alpha electron at position ',iorb
       end if
! is there a beta spin ?
       if (iand(det_i(i_nibble,2),imask) /= 0_bit_kind) then
        det_string_0_1(iorb,2)=1
!       write(6,*) ' found beta electron at position ',iorb
       end if
      end do

     end subroutine code_det

