! -*- F90 -*-
     subroutine cpustamp(cpu_in,wall_in,cpu_out,wall_out,label)
BEGIN_DOC
!
! CPU and wall time since incoming cpu and wall 
! returns cpu and wall of the moment
!
END_DOC
         implicit none
         real*8 :: cpu_in,wall_in,cpu_out,wall_out,cpu_tmp,wall_tmp
         character*4 :: label

         call cpu_time(cpu_tmp)
         call wall_time(wall_tmp)
         write(6,7702) label,cpu_tmp-cpu_in,wall_tmp-wall_in,(cpu_tmp-cpu_in)/(wall_tmp-wall_in+1.D-11)
  7702 format(A4,' --- CPU and Wall time:',2F14.6,' ratio:',F10.5)
         cpu_out=cpu_tmp
         wall_out=wall_tmp
     end subroutine cpustamp

