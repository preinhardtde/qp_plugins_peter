! -*- F90 -*-
 BEGIN_PROVIDER [logical, e_is_alpha]
&BEGIN_PROVIDER [logical, e_is_ezfio]
          e_is_alpha=(elec_alpha_num.ne.elec_alpha_num2)
          e_is_ezfio=(elec_alpha_num.gt.elec_alpha_num2).or.(elec_beta_num.gt.elec_beta_num2)
          if (e_is_alpha) then
           if (elec_beta_num.ne.elec_beta_num2) then
            write(9901) elec_alpha_num,elec_beta_num,elec_alpha_num2,elec_beta_num2
            stop ' I more than 1 difference '
           end if
           if (abs(elec_alpha_num-elec_alpha_num2).gt.1) then
            write(9901) elec_alpha_num,elec_beta_num,elec_alpha_num2,elec_beta_num2
            stop ' II more than 1 difference '
           end if
          else
           if (abs(elec_beta_num-elec_beta_num2).ne.1) then
            write(9901) elec_alpha_num,elec_beta_num,elec_alpha_num2,elec_beta_num2
            stop ' III more or less than 1 difference '
           end if
          end if
END_PROVIDER
