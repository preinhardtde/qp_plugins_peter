! -*- F90 -*-

use bitmasks ! you need to include the bitmasks_module.f90 features

BEGIN_PROVIDER [real*8, SX_hamil, (nMonoEx,nMonoEx)]
BEGIN_DOC
! Hamiltonian matrix elements between SX states from integrals 
! and density matrices
END_DOC
       implicit none
       integer :: i,j,a,b,t,u,ustart,bstart,indx,jndx

       real*8 :: SX_hamil_itju
       real*8 :: SX_hamil_itja
       real*8 :: SX_hamil_itua
       real*8 :: SX_hamil_iajb
       real*8 :: SX_hamil_iatb
       real*8 :: SX_hamil_taub

       write(6,*) ' providing Hamilton matrix between SX states '
       write(6,*) '  nMonoEx = ',nMonoEx
       write(6,*) '  approx_first_order = ',approx_first_order

       indx=1
       do i=1,n_core_orb
        do t=1,n_act_orb
         jndx=indx
         do j=i,n_core_orb
          if (i.eq.j) then
           ustart=t
          else
           ustart=1
          end if
          do u=ustart,n_act_orb
           SX_hamil(indx,jndx)=SX_hamil_itju(i,t,j,u)
           SX_hamil(jndx,indx)=SX_hamil(indx,jndx)
!          write(6,9901) excit_class(indx),list_core(i),list_act(t)      &
!             ,excit_class(jndx),list_core(j),list_act(u)    &
!              ,SX_hamil(indx,jndx),SXmat_exact(indx,jndx)
! 9901   format('tst : ',a3,2i4,5x,a3,2i4,2F20.12)
           jndx+=1
          end do
         end do
         do j=1,n_core_orb
          do a=1,n_virt_orb
           SX_hamil(indx,jndx)=SX_hamil_itja(i,t,j,a)
           SX_hamil(jndx,indx)=SX_hamil(indx,jndx)
!          write(6,9901) excit_class(indx),list_core(i),list_act(t)      &
!             ,excit_class(jndx),list_core(j),list_virt(a)    &
!              ,SX_hamil(indx,jndx),SXmat_exact(indx,jndx)
           jndx+=1
          end do
         end do
         do u=1,n_act_orb
          do a=1,n_virt_orb
           SX_hamil(indx,jndx)=SX_hamil_itua(i,t,u,a)
           SX_hamil(jndx,indx)=SX_hamil(indx,jndx)
!          write(6,9901) excit_class(indx),list_core(i),list_act(t)      &
!             ,excit_class(jndx),list_core(j),list_virt(a)      &
!              ,SX_hamil(indx,jndx),SXmat_exact(indx,jndx)
           jndx+=1
          end do
         end do
         indx+=1
        end do
       end do

       do i=1,n_core_orb
        do a=1,n_virt_orb
         jndx=indx
         do j=i,n_core_orb
          if (i.eq.j) then
           bstart=a
          else
           bstart=1
          end if
          do b=bstart,n_virt_orb
           SX_hamil(indx,jndx)=SX_hamil_iajb(i,a,j,b)
           SX_hamil(jndx,indx)=SX_hamil(indx,jndx)
!          write(6,9901) excit_class(indx),list_core(i),list_virt(a)      &
!             ,excit_class(jndx),list_core(j),list_virt(b)    &
!             ,SX_hamil(indx,jndx),SXmat_exact(indx,jndx)
           jndx+=1
          end do
         end do
         do t=1,n_act_orb
          do b=1,n_virt_orb
           SX_hamil(indx,jndx)=SX_hamil_iatb(i,a,t,b)
           SX_hamil(jndx,indx)=SX_hamil(indx,jndx)
!          write(6,9901) excit_class(indx),list_core(i),list_virt(a)      &
!             ,excit_class(jndx),list_act(t),list_virt(b)     &
!             ,SX_hamil(indx,jndx),SXmat_exact(indx,jndx)
           jndx+=1
          end do
         end do
         indx+=1
        end do
       end do
       
       do t=1,n_act_orb
        do a=1,n_virt_orb
         jndx=indx
         do u=t,n_act_orb
          if (t.eq.u) then
           bstart=a
          else
           bstart=1
          end if
          do b=bstart,n_virt_orb
           SX_hamil(indx,jndx)=SX_hamil_taub(t,a,u,b)
           SX_hamil(jndx,indx)=SX_hamil(indx,jndx)
!          write(6,9901) excit_class(indx),list_act(t),list_virt(a)   &
!              ,excit_class(jndx),list_act(u),list_virt(b)     &
!              ,SX_hamil(indx,jndx),SXmat_exact(indx,jndx)
           jndx+=1
          end do
         end do
         indx+=1
        end do
       end do

       write(6,*) ' the SX Hamiltonian is ready '
       do i=1,nMonoEx
        write(6,*) ' excitation No ',i,': ',excit(1,i),excit(2,i),'  (',excit_class(i),') has diagonal element ',SX_hamil(i,i)
       end do 
!      write(6,*) ' adding a level shift if 20 Hartree '
!      do i=1,nMonoEx
!       SX_hamil(i,i)+=20.D0
!      end do 

END_PROVIDER

       real*8 function SX_hamil_itju(ii,tt,jj,uu)
BEGIN_DOC
! the Hamilton matrix between SX states for core->act,core->act
END_DOC
        implicit none
        integer :: i,ii,j,jj,t,tt,u,uu,v,vv,x,xx,y,yy
        integer :: a,aa,b,bb
        real*8 :: term,nt,mt,mu,nu
        
        SX_hamil_itju=0.D0
        i=list_core(ii)
        t=list_act(tt)
        j=list_core(jj)
        u=list_act(uu)
        nt=occnum(t)
        mt=2.D0-nt
        nu=occnum(u)
        mu=2.D0-nu
!       write(6,*) '  < core, active |H| core, active > ',i,t,j,u
        
        if (approx_first_order) then
         if (i.eq.j) then
          if (t.eq.u) then
! it it 
           term=Fpq(t,t)-Fpq(i,i)
           term+=0.5D0*bielecCI(tt,tt,tt,t)*nt
          else
! it iu
           term=0.5D0*Fpq(t,u)
           term+=0.25D0*(nt*bielecCI(tt,tt,tt,u)+nu*bielecCI(uu,uu,uu,t))
           term*=sqrt(mt*mu)
          end if
          SX_hamil_itju=term
          write(6,9901) i,t,u,j,term*sqrt(mt*mu),term
         else
! it/ju
          if (t.eq.u) then
! only it/jt survives
           SX_hamil_itju=-Fpq(i,j)
          end if
         end if
        else
! with full 2P (and 3P) density matrix
! for all it ju
real*8 :: t1,t2,t3,t4,t5,t6,t7,t8,t9
         t1=0.D0
         t2=0.D0
         t3=0.D0
         t4=0.D0
         t5=0.D0
         t6=0.D0
         t7=0.D0
         t8=0.D0
         t9=0.D0
real*8 :: t2a,t2b,t2c,t2d,t1a,t1b,t7a,t7b
 t2a=0.D0
 t2b=0.D0
 t2c=0.D0
 t2d=0.D0
 t7a=0.D0
 t7b=0.D0
!        t1 = -(2.D0-nt-nu)*(bielec_pqxx(u,t,ii,jj) - &
!             2.D0 * bielec_pxxq(u,jj,ii,t))
         t1 = -(2.D0-nt-nu)*(bielec_pqxx(u,t,ii,jj) - &
              2.D0 * bielec_pxxq(u,jj,ii,t))
!        write(6,*) ' t1 : ',t1a,t1b,t1,nt,nu,2.D0-nt-nu
real*8 :: nv
         do vv=1,n_act_orb
          v=list_act(vv)
          nv=occnum(v)
          if (t.eq.u) then
           t2a+=nv*(2.D0*bielec_pqxx(v,v,ii,jj) - bielec_pxxq(v,jj,ii,v))
          end if
          do xx=1,n_act_orb
           x=list_act(xx)
           t2b+=2.D0*P0tuvx_no(tt,uu,vv,xx)*bielec_pqxx(v,x,ii,jj)
           t2c+=2.D0*P0tuvx_no(vv,uu,tt,xx)*bielec_pxxq(v,jj,ii,x)
          end do
         end do
!        write(6,*) ' t2 : ',t2a,t2b,t2c,-t2a+t2b+t2c,t2
         t2=-t2a+t2b+t2c

         if (i.eq.j) then
          if (t.eq.u) then
! only for it it 
           do vv=1,n_act_orb
            v=list_act(vv)
            t3+=nt*occnum(v)*Fipq(v,v)
           end do
          end if
! for all it iu
integer :: u3
          u3=n_core_orb+uu
          t4+=2.D0*Fapq(t,u)+2.D0*Fipq(u,t)-nt*Fipq(u,t)-nu*Fipq(t,u)
          do vv=1,n_act_orb
           v=list_act(vv)
           do xx=1,n_act_orb
            x=list_act(xx)
            t5-=2.D0*P0tuvx_no(tt,uu,vv,xx)*Fipq(v,x)
            do yy=1,n_act_orb
integer :: y3,z,zz,tt8
             y3=n_core_orb+yy
             t6-=2.D0*P0tuvx_no(vv,xx,tt,yy)*bielecCI(vv,xx,yy,u)
             if (three_particle_matrix) then
              do zz=1,n_act_orb
               z=list_act(zz)
               if (t.eq.u) then
                t7a+=P0tuvx_no(vv,xx,yy,zz)*nt*bielecCI(vv,xx,yy,z)
               end if
               t7b+=Q0vxyztu_no(vv,xx,yy,zz,tt,uu)*bielecCI(vv,xx,yy,z)
              end do
             end if
            end do
           end do
          end do
         end if
         t7=t7a-t7b
         if (t.eq.u) then
! for all it jt
          t8-=(2.D0-nt)*Fipq(i,j)
         end if
!        write(6,*) 
!        write(6,*) ' contributions '
!        write(6,*) ' t1 : ',t1,t1/sqrt(mt*mu)
!        write(6,*) ' t2 : ',t2,t2/sqrt(mt*mu)
!        write(6,*) ' t3 : ',t3,t3/sqrt(mt*mu)
!        write(6,*) ' t4 : ',t4,t4/sqrt(mt*mu)
!        write(6,*) ' t5 : ',t5,t5/sqrt(mt*mu)
!        write(6,*) ' t6 : ',t6,t6/sqrt(mt*mu)
!        write(6,*) ' t7 : ',t7,t7/sqrt(mt*mu)
!        write(6,*) ' t8 : ',t8,t8/sqrt(mt*mu)
         term=t1+t2+t3+t4+t5+t6+t7+t8+t9
!        write(6,9901) i,t,u,j,t1,t2,t3,t4,t5,t6,t7,t8,t9,term,term/sqrt(mt*mu), sqrt(mt*mu) 
 9901  format(' itju ',4i4,9F13.6,' -> ',3F23.8)
         SX_hamil_itju=term/sqrt(mt*mu)
        end if
       end function SX_hamil_itju

       real*8 function SX_hamil_itja(ii,tt,jj,aa)
BEGIN_DOC
! the Hamilton matrix between SX states for core->act,core->virt
END_DOC
        implicit none
        integer :: i,ii,j,jj,t,tt,u,uu,v,vv,x,xx,y,yy
        integer :: a,aa,b,bb
        real*8 :: term,nt,mt,mu,nu
        SX_hamil_itja=0.D0
        i=list_core(ii)
        t=list_act(tt)
        j=list_core(jj)
        a=list_virt(aa)
!       write(6,*) '  < core, active |H| core, virtual > ',i,t,j,a
        
real*8 :: mmt
        nt=occnum(t)
        mt=(2.D0-nt)
        mmt=sqrt(mt/2.D0)
        if (approx_first_order) then
! it/ja
         term=0.5D0*nt*bielecCI(tt,tt,tt,a)
         if (i.eq.j) then
          term+=Fpq(t,a)
         end if
         term*=mmt
         SX_hamil_itja=term
        else
! with full 2P density matrix
         nt=occnum(t)
         term=(nt-2.D0)*(bielec_pqxx(a,t,i,j)-2.D0*bielec_pxxq(a,j,t,i))
         if (i.eq.j) then
          term+=2.D0*(Fipq(a,t)+Fapq(a,t))-nt*Fipq(a,t)
          do vv=1,n_act_orb
           do xx=1,n_act_orb
            do yy=1,n_act_orb
             term-=2.D0*P0tuvx_no(tt,vv,xx,yy)*bielecCI(xx,yy,vv,a)
            end do
           end do
          end do
         end if
         SX_hamil_itja=term/sqrt(2.D0*mt)
        end if
       end function SX_hamil_itja

       real*8 function SX_hamil_itua(ii,tt,uu,aa)
BEGIN_DOC
! the Hamilton matrix between SX states for core->act,act->virt
END_DOC
        implicit none
        integer :: i,ii,j,jj,t,tt,u,uu,v,vv,x,xx,y,yy,t3,u3,v3,x3
        integer :: a,aa,b,bb
        real*8 :: term,nt,mt,mu,nu
        SX_hamil_itua=0.D0
!       write(6,*) '  < core, active |H| core, virtual > ',i,t,u,a
! nothing when approx, all are zero
        if (approx_first_order) return
! with full 2P density matrix
        i=list_core(ii)
        t=list_act(tt)
        u=list_act(uu)
        a=list_virt(aa)
        t3=n_core_orb+tt
        u3=n_core_orb+uu
! the block it/ua
        nt=occnum(t)
        nu=occnum(u)
        term=-nu*(bielec_pxxq(a,t3,u3,i)-2.D0*bielec_pxxq(a,u3,t3,i))
        do vv=1,n_act_orb
         do xx=1,n_act_orb
          x3=xx+n_core_orb
          x=list_act(xx)
          v3=vv+n_core_orb
!         term-=2.D0*P0tuvx_no(tt,vv,uu,xx)*bielec_pxxq(a,x3,v3,i)
          term-=2.D0*P0tuvx_no(tt,vv,uu,xx)*bielec_pqxx(a,x,v3,ii)
         end do
        end do
        term*=1.D0/sqrt(nu*(2.D0-nt))
        SX_hamil_itua=term
       end function SX_hamil_itua

       real*8 function SX_hamil_iajb(ii,aa,jj,bb)
BEGIN_DOC
! the Hamilton matrix between SX states for core->virt,core->virt
END_DOC
        implicit none
        integer :: i,ii,j,jj,t,tt,u,uu,v,vv,x,xx,y,yy
        integer :: a,aa,b,bb
        real*8 :: term,nt,mt,mu,nu
        SX_hamil_iajb=0.D0
        i=list_core(ii)
        a=list_virt(aa)
        j=list_core(jj)
        b=list_virt(bb)
!       write(6,*) '  < core, virtual |H| core, virtual > ',i,a,j,b
        
        if (approx_first_order) then
         if (i.eq.j) then
          if (a.eq.b) then
           SX_hamil_iajb=Fpq(a,a)-Fpq(i,i)
          else
           SX_hamil_iajb=Fpq(a,b)
          end if
         else
          if (a.eq.b) then
           SX_hamil_iajb=-Fpq(i,j)
          end if
         end if
        else
! with full 2P density matrix
         term=2.D0*bielec_pxxq(a,ii,jj,b)-bielec_pqxx(a,b,ii,jj)
         if (i.eq.j) then
          term+=Fpq(a,b)
         end if
         if (a.eq.b) then
          term-=Fpq(i,j)
         end if
         SX_hamil_iajb=term
        end if
       end function SX_hamil_iajb

       real*8 function SX_hamil_iatb(ii,aa,tt,bb)
BEGIN_DOC
! the Hamilton matrix between SX states for core->virt,act->virt
END_DOC
        implicit none
        integer :: i,ii,j,jj,t,tt,u,uu,v,vv,x,xx,y,yy
        integer :: a,aa,b,bb
        real*8 :: term,nt,mt,mu
        SX_hamil_iatb=0.D0
        i=list_core(ii)
        a=list_virt(aa)
        t=list_act(tt)
        b=list_virt(bb)
!       write(6,*) '  < core, virtual |H| active, virtual > ',i,a,t,b
        
        if (approx_first_order) then
         if (a.eq.b) then
! only ia/ta survives
          term=-Fpq(i,t)*occnum(t)
          term+=0.5D0*BielecCI(tt,tt,tt,i)*(2.D0-occnum(t))
          term*=1.D0/sqrt(2.D0*occnum(t))
          SX_hamil_iatb=term
         end if
        else
! with full 2P density matrix
         nt=occnum(t)
integer :: t3
         t3=n_core_orb+tt
         term=nt*(2.D0*bielec_pxxq(a,ii,t3,b)-bielec_pqxx(a,b,t3,ii))
         if (a.eq.b) then
          term-=nt*Fipq(t,i)
          do vv=1,n_act_orb
           do xx=1,n_act_orb
            do yy=1,n_act_orb
             term-=2.D0*P0tuvx_no(tt,vv,xx,yy)*BielecCI(xx,yy,vv,i)
            end do
           end do
          end do
         end if
         term*=1.D0/sqrt(2.D0*occnum(t))
         SX_hamil_iatb=term
        end if
       end function SX_hamil_iatb

       real*8 function SX_hamil_taub(tt,aa,uu,bb)
BEGIN_DOC
! the Hamilton matrix between SX states for act->virt,act->virt
END_DOC
        implicit none
        integer :: i,ii,j,jj,t,tt,u,uu,v,vv,x,xx,y,yy
        integer :: a,aa,b,bb
        real*8 :: term,nt,mt,mu,den,nu
        SX_hamil_taub=0.D0
        t=list_act(tt)
        a=list_virt(aa)
        u=list_act(uu)
        b=list_virt(bb)
!       write(6,*) '  < active, virtual |H| active, virtual > ',t,a,u,b
        
        nt=occnum(t)
        nu=occnum(u)
        mt=2.D0-occnum(t)
        if (approx_first_order) then
         if (t.eq.u) then
          if (a.eq.b) then
           SX_hamil_taub=Fpq(a,a)-Fpq(t,t)
           SX_hamil_taub+=0.5D0*mt*bielecCI(tt,tt,tt,t)
          else
! ta/tb
           SX_hamil_taub=Fpq(a,b)
          end if
         else
          if (a.eq.b) then
           mu=2.D0-occnum(u)
           term=-0.5D0*Fpq(t,u)
           term+= 0.25D0*( &
                mt*bielecCI(tt,tt,tt,u)+ &
                mu*bielecCI(uu,uu,uu,t))
           term*=sqrt(nt*nu)
           SX_hamil_taub=term
          end if
         end if
        else
! with full 2P density matrix
         term=0.D0
! contribution for all ta ub
integer :: v3,x3
real*8 :: t1,t2,t3,t4,t5,t6
         t1=0.D0
         t2=0.D0
         t3=0.D0
         t4=0.D0
         t5=0.D0
         t6=0.D0
real*8 :: t6a,t6b
         t6a=0.D0
         t6b=0.D0
         do vv=1,n_act_orb
          v3=n_core_orb+vv
          do xx=1,n_act_orb
           x3=n_core_orb+xx
           term+=2.D0*P0tuvx_no(tt,uu,vv,xx)*bielec_pqxx(a,b,v3,x3)
           term+=2.D0*P0tuvx_no(tt,xx,vv,uu)*bielec_pxxq(a,x3,v3,b)
          end do
         end do
!        t1=term
         if (a.eq.b) then
! for ta ta only
          if (t.eq.u) then
           do vv=1,n_act_orb
            v=list_act(vv)
            term-=nt*occnum(v)*Fipq(v,v)
!           t2-=nt*occnum(v)*Fipq(v,v)
           end do
          end if
! for all ta ua
          do vv=1,n_act_orb
           v=list_act(vv)
           v3=n_core_orb+vv
           do xx=1,n_act_orb
            x=list_act(xx)
            x3=n_core_orb+xx
            term+=2.D0*P0tuvx_no(tt,uu,vv,xx)*Fipq(v,x)
!           t3+=2.D0*P0tuvx_no(tt,uu,vv,xx)*Fipq(v,x)
            do yy=1,n_act_orb
             y=list_act(yy)
             term-=2.D0*P0tuvx_no(uu,vv,xx,yy)*bielecCI(tt,vv,xx,y)
!            t4-=2.D0*P0tuvx_no(uu,vv,xx,yy)*bielecCI(tt,vv,xx,y)
           if (three_particle_matrix) then
integer :: z,zz
             do zz=1,n_act_orb
!             t6a+=Q0vxyztu_no(vv,xx,yy,zz,tt,uu)*bielecCI(vv,xx,zz,y)
              term+=Q0vxyztu_no(vv,xx,yy,zz,tt,uu)*bielecCI(vv,xx,zz,y)
              if (t.eq.u) then
!              t6b-=nt*P0tuvx_no(vv,xx,yy,zz)*bielecCI(vv,xx,zz,y)
               term-=nt*P0tuvx_no(vv,xx,yy,zz)*bielecCI(vv,xx,zz,y)
              end if
             end do
           end if
            end do
           end do
          end do
         end if
         if (t.eq.u) then
! for all ta tb
          term+=nt*Fipq(a,b)
!         t5+=nt*Fipq(a,b)
         end if
         SX_hamil_taub=term/sqrt(nt*nu)
!        t1*=1.D0/sqrt(nt*nu)
!        t2*=1.D0/sqrt(nt*nu)
!        t3*=1.D0/sqrt(nt*nu)
!        t4*=1.D0/sqrt(nt*nu)
!        t5*=1.D0/sqrt(nt*nu)
!        t6*=1.D0/sqrt(nt*nu)
!        t6a*=1.D0/sqrt(nt*nu)
!        t6b*=1.D0/sqrt(nt*nu)
!        if (a.eq.b) write(6,9901) t,a,u,b,t1,t2,t3,t4,t5,t6a,t6b,SX_hamil_taub
 9901 format(' taub : contrib ',4I4,7F12.6,' -> ',F12.6)
        end if
       end function SX_hamil_taub

BEGIN_PROVIDER [real*8, SXhamdiag, (nMonoEx)]
BEGIN_DOC
! the diagonal of the SX matrix, needed for the Davidson procedure
END_DOC
       implicit none
       integer :: i,t,a,indx
       real*8 :: SX_hamil_itju,SX_hamil_iajb,SX_hamil_taub

       indx=0
       do i=1,n_core_orb
        do t=1,n_act_orb
         indx+=1
         SXhamdiag(indx)=SX_hamil_itju(i,t,i,t)
        end do
       end do

       do i=1,n_core_orb
        do a=1,n_virt_orb
         indx+=1
         SXhamdiag(indx)=SX_hamil_iajb(i,a,i,a)
        end do
       end do

       do t=1,n_act_orb
        do a=1,n_virt_orb
         indx+=1
         SXhamdiag(indx)=SX_hamil_taub(t,a,t,a)
        end do
       end do

END_PROVIDER

BEGIN_PROVIDER [real*8, SXmat_exact, (nMonoEx,nMonoEx)]
BEGIN_DOC
! calculate the SX matrix from the wavefunction
! we assume that we have natural active orbitals
END_DOC
     implicit none
     integer :: indx,ihole,ipart
     integer :: jndx,jhole,jpart
     character*3 :: iexc,jexc
     real*8 :: res

     write(6,*) ' providing exact SX matrix SXmat_exact '
     write(6,*) '  nMonoEx = ',nMonoEx

     do indx=1,nMonoEx
      do jndx=1,nMonoEx
       SXmat_exact(indx,jndx)=1.D0
      end do
     end do

     return

     open(unit=13,file='SX_exact.dat',status='old',form='formatted',err=100)
     write(6,*) ' found file <SX_exact.dat>, reading matrix '
 200 continue
integer :: idum,jdum
real*8 :: sss
     read(13,*,err=200,end=300) idum,jdum,sss
     SXmat_exact(idum,jdum)=sss
     SXmat_exact(jdum,idum)=sss
     go to 200
 300 continue
     close(13)
     return

  100 continue

     write(6,*) ' no file <SX_exact.dat>, calculating matrix '
     write(6,*) ' we will calculate ',nMonoEx*(nMonoEx+1)/2,' matrix elements '
     do indx=1,nMonoEx
      if (indx.eq.2) write (6,*) ' indx =',indx
      if (indx.eq.10) write (6,*) ' indx =',indx
      if (dble(indx*100/nMonoEx)-dble(indx)*100./dble(nMonoEx).eq.0.D0) write (6,*) indx*100/nMonoEx,' %'
      ihole=excit(1,indx)
      ipart=excit(2,indx)
      iexc=excit_class(indx)
real*8 :: xmi,xmj
      if (first_order) then
       if (excit_class(indx).eq.'c-a') then
        xmi=1.D0/sqrt(2.D0-occnum(ipart))
       else if (excit_class(indx).eq.'c-v') then
        xmi=1.D0/sqrt(2.D0)
       else if (excit_class(indx).eq.'a-v') then
        xmi=1.D0/sqrt(occnum(ihole))
       else
        stop ' SXmat_exct : indx, class not possible '
       end if
      end if
      do jndx=indx,nMonoEx
       jhole=excit(1,jndx)
       jpart=excit(2,jndx)
       jexc=excit_class(jndx)
       call calc_SX_elem(ihole,ipart,jhole,jpart,res)
!      write(6,*) ' SXian ',ihole,'->',ipart &
!           ,' (',iexc,')',jhole,'->',jpart,' (',jexc,')',res
       SXmat_exact(indx,jndx)=res
       SXmat_exact(jndx,indx)=res
       if (first_order) then
        if (excit_class(jndx).eq.'c-a') then
         xmj=1.D0/sqrt(2.D0-occnum(jpart))
        else if (excit_class(jndx).eq.'c-v') then
         xmj=1.D0/sqrt(2.D0)
        else if (excit_class(jndx).eq.'a-v') then
         xmj=1.D0/sqrt(occnum(jhole))
        else
         stop ' SXmat_exct : jndx, class not possible '
        end if
        SXmat_exact(indx,jndx)*=0.5D0*xmi*xmj
        SXmat_exact(jndx,indx)=SXmat_exact(indx,jndx)
       end if
      end do
     end do
     do indx=1,nMonoEx
      SXmat_exact(indx,indx)-=psi_energy(1)
     end do

     open(unit=13,file='SX_exact.dat',form='formatted',status='unknown')
     do indx=1,nMonoEx
      do jndx=indx,nMonoEx
       if (abs(SXmat_exact(indx,jndx)).gt.1.D-10) write(13,9956) indx,jndx,SXmat_exact(indx,jndx)   &
         ,excit_class(indx),excit(1,indx),excit(2,indx),excit_class(jndx),excit(1,jndx),excit(2,jndx)
 9956 format(2I6,F20.12,5x,a3,2I4,5x,a3,2i4)
      end do
     end do
     close(13)
     write(6,*) ' wrote SX_exact.dat'
      
END_PROVIDER

     subroutine calc_SX_elem(ihole,ipart,jhole,jpart,res)
BEGIN_DOC
! eq 19 of Siegbahn et al, Physica Scripta 1980
! we calculate  <Psi| E_pq (H-E_0) E_rs |Psi>
! average over all states is performed.
! no transition between states.
END_DOC
       implicit none 
       integer :: ihole,ipart,ispin,mu,istate
       integer :: jhole,jpart,jspin
       integer :: mu_pq, nu_rs,nu
       real*8 :: res
       integer(bit_kind), allocatable :: det_mu(:,:)
       integer(bit_kind), allocatable :: det_nu(:,:)
       integer(bit_kind), allocatable :: det_mu_pq(:,:)
       integer(bit_kind), allocatable :: det_nu_rs(:,:)
       real*8 :: i_H_psi_array(N_states),phase,phase2,phase3
       real*8 :: i_H_j_element
       allocate(det_mu(N_int,2))
       allocate(det_nu(N_int,2))
       allocate(det_mu_pq(N_int,2))
       allocate(det_nu_rs(N_int,2))
       integer :: mu_pq_possible
       integer :: nu_rs_possible

       res=0.D0
integer :: nterm
        nterm=0

real*8 :: EHE
          EHE=0.D0
! the terms <0|E E H |0>
       do mu=1,n_det
! get the string of the determinant
        call det_extract(det_mu,mu,N_int)
        do ispin=1,2
! do the monoexcitation pq on it
         call det_copy(det_mu,det_mu_pq,N_int)
         call do_signed_mono_excitation(det_mu,det_mu_pq,mu_pq &
            ,ihole,ipart,ispin,phase,mu_pq_possible)
! the determinant is necessarily outside the CAS, so mu_pq = -1
!
! the operator E H E, we have to do a double loop over the determinants 
!  we still have the determinant mu_pq and the phase in memory
! this we need in first and second order
         if (mu_pq_possible.eq.1) then
          do nu=1,N_det
           call det_extract(det_nu,nu,N_int)
           do jspin=1,2
            call det_copy(det_nu,det_nu_rs,N_int)
            call do_signed_mono_excitation(det_nu,det_nu_rs,nu_rs &
               ,jhole,jpart,jspin,phase2,nu_rs_possible)
! excitation possible ? 
            if (nu_rs_possible.eq.1) then
! the determinant is necessarily outside the CAS, so nu_rs = -1
             call i_H_j(det_mu_pq,det_nu_rs,N_int,i_H_j_element)
             do istate=1,N_states
              EHE+=2.D0*i_H_j_element*psi_coef(mu,istate) &
                 *psi_coef(nu,istate)*phase*phase2
              nterm+=1
             end do
            end if
           end do
          end do
         end if
        end do
       end do
       res=EHE

! state-averaged SX exact
       res*=1.D0/dble(N_states)
       write(6,*) ' SX exact ',ihole,ipart,jhole,jpart,res,nterm

     end subroutine calc_SX_elem

