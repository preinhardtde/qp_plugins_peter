! -*- F90 -*-
program optorb
  implicit none
  BEGIN_DOC
! calculating an orbital gradient on a wavefunction
  END_DOC
 read_wf = .True.
 touch read_wf

real*8 :: cpu0,wall0
         call cpu_time(cpu0)
         call wall_time(wall0)
         call header
         first_order=.true.
         approx_first_order=.false.
   write(6,*) ' -------- - - - - - - ----------- '
   write(6,*) '     first_order = ',first_order
   write(6,*) ' -------- - - - - - - ----------- '
   write(6,*) ' -------- - - - - - - ----------- '
   write(6,*) '     approx_first_order = ',approx_first_order
   write(6,*) ' -------- - - - - - - ----------- '
   three_particle_matrix=.true.
   write(6,*) ' -------- - - - - - - ----------- '
   write(6,*) '     three particle matrix Q = ',three_particle_matrix
   write(6,*) ' -------- - - - - - - ----------- '

         call driver
         call cpustamp(cpu0,wall0,cpu0,wall0,'All ')
end program optorb

       subroutine header
         write(6,*) 
         write(6,*) ' the orbital optimization step for the MCSCF '
         write(6,*) ' first order'
         write(6,*) 
         write(6,*) ' P. Reinhardt (Paris, feb 2019) '
         write(6,*) 
         write(6,*) 
       end subroutine header

       subroutine driver
         implicit none
         integer :: i,j

         call print_geom

         write(6,*)
         write(6,*) ' <0|H|0>               = ',eone+etwo+ecore
         write(6,*) ' energy improvement    = ',energy_improvement
         write(6,*) ' new energy            = ',eone+etwo+ecore+energy_improvement
         write(6,*)

         write(6,*)
         write(6,*) '   creating new orbitals '
         do i=1,mo_num
          write(6,*) ' Orbital No ',i
          write(6,'(5F14.6)') (NewOrbs(j,i),j=1,mo_num)
          write(6,*)
         end do

         mo_label = "Natural"
         write(6,*) '   storing new orbitals in mo_coef'
         do i=1,mo_num
          do j=1,ao_num
           mo_coef(j,i)=NewOrbs(j,i)
          end do
         end do
         write(6,*) '   saving new orbitals '
         call save_mos
 
         write(6,*)
         write(6,*) '   ... all done '

       end subroutine driver
