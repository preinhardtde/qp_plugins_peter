! -*- F90 -*-
      subroutine print_geom
        implicit none
        integer :: i,j
        write(6,*) ' interatomic distances '
        do i=1,nucl_num
         do j=i+1,nucl_num
          write(6,*) ' distance atom ',i,' and ',j,' is ',nucl_dist(i,j)
         end do
        end do
        write(6,*)
      end subroutine print_geom
