============
casscf_peter
============

THe orbital-optimization step in the CASSCF. Two algorithms, 
either the 2nd-order Augmented Hessian method, or 1st-order SuperCI. 
The latter as full implementation with 3-particle matrices,
or without, or with approximated 2-particle matrices. The program 
uses density matrices and integral files prepared by wdens or the 
complete_fci plugins.
