! -*- F90 -*-
BEGIN_PROVIDER [real*8, norm_wf ]
BEGIN_DOC
! calculate the norm of the WF as sum_I c_I^2
END_DOC
     implicit none
     integer :: idet
     norm_wf=0.D0
     do idet=1,n_det
      norm_wf+=psi_coef(idet,1)*psi_coef(idet,1)
     end do
END_PROVIDER
