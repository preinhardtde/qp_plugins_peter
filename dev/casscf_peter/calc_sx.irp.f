! -*- F90 -*-
program cgrad
  implicit none
  BEGIN_DOC
! calculating an orbital gradient on a wavefunction
  END_DOC
 read_wf = .True.
 touch read_wf

         approx_first_order=.false.
         first_order=.true.
         three_particle_matrix=.true.
         three_particle_matrix=.false.
         write(6,*) '        first_order = ',first_order
         write(6,*) ' approx_first_order = ',approx_first_order
         call header
         call driver
end program cgrad

       subroutine header
         write(6,*) 
         write(6,*) ' the SX hamiltonian for the 1st-order CASSCF calculation '
         write(6,*) 
         write(6,*) ' P. Reinhardt (Paris, feb 2019) '
         write(6,*) 
         write(6,*) 
       end subroutine header

       subroutine driver
         implicit none
         integer :: i,j


         write(6,*) ' mo_num = ',mo_num
         write(6,*) ' ao_num = ',ao_num
         write(6,*)
         write(6,*) ' <0|H|0>     (qp)      = ',psi_energy_with_nucl_rep(1)
         write(6,*)
         write(6,*) ' ecore  = ',ecore
         write(6,*) ' number of determinants in the wavefunction ',n_det

         first_order=.true.
         approx_first_order=.false.
         
         write(6,*) 
         write(6,*) ' the Hamiltonian between SX states '
         open(unit=16,file='SXcompar_withP.dat',status='unknown',form='formatted')
         write(16,*) '     I, J     exact,  <0|E_I H E_J |0> from D, P and integrals'
         do i=1,nMonoEx
          do j=i,nMonoEx
           if (abs(SXmat_exact(i,j)).gt.1.D-2) then
           if (SX_hamil(i,j).ne.0.D0) then
            if (abs(1.D0-SXmat_exact(i,j)/SX_hamil(i,j)).gt.1.D-2) then
             if (abs(SXmat_exact(i,j)).gt.1.D-2) then
              write(16,9913) i,excit(1,i),excit(2,i),excit_class(i), &
               j,excit(1,j),excit(2,j),excit_class(j),SXmat_exact(i,j),SX_hamil(i,j),SXmat_exact(i,j)/SX_hamil(i,j),SXmat_exact(i,j)-SX_hamil(i,j)
             else
              if (abs(SXmat_exact(i,j)-SX_hamil(i,j)).lt.1.D-8) then
               write(6,9914) i,excit(1,i),excit(2,i),excit_class(i), &
                j,excit(1,j),excit(2,j),excit_class(j),SXmat_exact(i,j),SX_hamil(i,j),SXmat_exact(i,j)/SX_hamil(i,j),SXmat_exact(i,j)-SX_hamil(i,j)
              else
               write(16,9912) i,excit(1,i),excit(2,i),excit_class(i), &
                j,excit(1,j),excit(2,j),excit_class(j),SXmat_exact(i,j),SX_hamil(i,j),SXmat_exact(i,j)/SX_hamil(i,j),SXmat_exact(i,j)-SX_hamil(i,j)
              end if
             end if
            else
             write(6,9911) i,excit(1,i),excit(2,i),excit_class(i), &
              j,excit(1,j),excit(2,j),excit_class(j),SXmat_exact(i,j),SX_hamil(i,j),SXmat_exact(i,j)/SX_hamil(i,j),SXmat_exact(i,j)-SX_hamil(i,j)
            end if
           else 
            if (SXmat_exact(i,j).ne.0.D0) then
             if (abs(SXmat_exact(i,j)).gt.1.D-2) then
              write(16,9903) i,excit(1,i),excit(2,i),excit_class(i), &
               j,excit(1,j),excit(2,j),excit_class(j),SXmat_exact(i,j),SX_hamil(i,j)
             else
              if (abs(SXmat_exact(i,j)-SX_hamil(i,j)).lt.1.D-8) then
               write(6,9904) i,excit(1,i),excit(2,i),excit_class(i), &
                j,excit(1,j),excit(2,j),excit_class(j),SXmat_exact(i,j),SX_hamil(i,j)
              else
               write(16,9902) i,excit(1,i),excit(2,i),excit_class(i), &
                j,excit(1,j),excit(2,j),excit_class(j),SXmat_exact(i,j),SX_hamil(i,j)
              end if
             end if
            else
             write(6,9901) i,excit(1,i),excit(2,i),excit_class(i), &
              j,excit(1,j),excit(2,j),excit_class(j),SXmat_exact(i,j),SX_hamil(i,j)
            end if
           end if

9901 format('SX hamiltonian ',i4,': ',i2,' -> ',i2,' (',A3,') ;'  &
               ,i4,': ',i2,' -> ',i2,' (',A3,') ;',2E23.14,' ok ')
9902 format('SX hamiltonian ',i4,': ',i2,' -> ',i2,' (',A3,') ;'  &
               ,i4,': ',i2,' -> ',i2,' (',A3,') ;',2E23.14,' wrong ')
9903 format('SX hamiltonian ',i4,': ',i2,' -> ',i2,' (',A3,') ;'  &
               ,i4,': ',i2,' -> ',i2,' (',A3,') ;',2E23.14,' wrong !!!')
9904 format('SX hamiltonian ',i4,': ',i2,' -> ',i2,' (',A3,') ;'  &
               ,i4,': ',i2,' -> ',i2,' (',A3,') ;',2E23.14,' wrong unimportant')
9911 format('SX hamiltonian ',i4,': ',i2,' -> ',i2,' (',A3,') ;'  &
               ,i4,': ',i2,' -> ',i2,' (',A3,') ;',4E23.14,' ok ')
9912 format('SX hamiltonian ',i4,': ',i2,' -> ',i2,' (',A3,') ;'  &
               ,i4,': ',i2,' -> ',i2,' (',A3,') ;',4E23.14,' wrong ')
9913 format('SX hamiltonian ',i4,': ',i2,' -> ',i2,' (',A3,') ;'  &
               ,i4,': ',i2,' -> ',i2,' (',A3,') ;',4E23.14,' wrong !!!')
9914 format('SX hamiltonian ',i4,': ',i2,' -> ',i2,' (',A3,') ;'  &
               ,i4,': ',i2,' -> ',i2,' (',A3,') ;',4E23.14,' wrong unimportant')
      end if
           end do
          end do
          close(16)

         approx_first_order=.true.
         touch SX_hamil

         call write_time(6)
         write(6,*) 
         write(6,*) ' the Hamiltonian between SX states '
         open(unit=16,file='SXcompar_approx.dat',status='unknown',form='formatted')
         write(16,*) '     I, J     exact,  <0|E_I H E_J |0> from D, P and integrals'
         do i=1,nMonoEx
          do j=i,nMonoEx
           if (SX_hamil(i,j).ne.0.D0) then
            if (abs(1.D0-SXmat_exact(i,j)/SX_hamil(i,j)).gt.1.D-2) then
             if (abs(SXmat_exact(i,j)).gt.1.D-2) then
              write(16,9913) i,excit(1,i),excit(2,i),excit_class(i), &
               j,excit(1,j),excit(2,j),excit_class(j),SXmat_exact(i,j),SX_hamil(i,j),SXmat_exact(i,j)/SX_hamil(i,j),SXmat_exact(i,j)-SX_hamil(i,j)
             else
              if (abs(SXmat_exact(i,j)-SX_hamil(i,j)).lt.1.D-8) then
               write(16,9914) i,excit(1,i),excit(2,i),excit_class(i), &
                j,excit(1,j),excit(2,j),excit_class(j),SXmat_exact(i,j),SX_hamil(i,j),SXmat_exact(i,j)/SX_hamil(i,j),SXmat_exact(i,j)-SX_hamil(i,j)
              else
               write(16,9912) i,excit(1,i),excit(2,i),excit_class(i), &
                j,excit(1,j),excit(2,j),excit_class(j),SXmat_exact(i,j),SX_hamil(i,j),SXmat_exact(i,j)/SX_hamil(i,j),SXmat_exact(i,j)-SX_hamil(i,j)
              end if
             end if
            else
             write(16,9911) i,excit(1,i),excit(2,i),excit_class(i), &
              j,excit(1,j),excit(2,j),excit_class(j),SXmat_exact(i,j),SX_hamil(i,j),SXmat_exact(i,j)/SX_hamil(i,j),SXmat_exact(i,j)-SX_hamil(i,j)
            end if
           else 
            if (SXmat_exact(i,j).ne.0.D0) then
             if (abs(SXmat_exact(i,j)).gt.1.D-2) then
              write(16,9903) i,excit(1,i),excit(2,i),excit_class(i), &
               j,excit(1,j),excit(2,j),excit_class(j),SXmat_exact(i,j),SX_hamil(i,j)
             else
              write(16,9902) i,excit(1,i),excit(2,i),excit_class(i), &
               j,excit(1,j),excit(2,j),excit_class(j),SXmat_exact(i,j),SX_hamil(i,j)
             end if
            else
             write(16,9901) i,excit(1,i),excit(2,i),excit_class(i), &
              j,excit(1,j),excit(2,j),excit_class(j),SXmat_exact(i,j),SX_hamil(i,j)
            end if
           end if

           end do
          end do

          close(16)
  
         write(6,*)
         write(6,*) '   ... all done '

       end subroutine driver
