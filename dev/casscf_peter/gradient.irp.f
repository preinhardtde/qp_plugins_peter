! -*- F90 -*-

use bitmasks ! you need to include the bitmasks_module.f90 features

BEGIN_PROVIDER [ integer, nMonoEx ]
BEGIN_DOC
! 
END_DOC
     implicit none
     nMonoEx=n_core_orb*n_act_orb+n_core_orb*n_virt_orb+n_act_orb*n_virt_orb
     write(6,*) ' nMonoEx = ',nMonoEx
END_PROVIDER

 BEGIN_PROVIDER [integer, excit, (2,nMonoEx)]
&BEGIN_PROVIDER [character*3, excit_class, (nMonoEx)]
BEGIN_DOC
! a list of the orbitals involved in the excitation
END_DOC

     implicit none
     integer :: i,t,a,ii,tt,aa,indx
     indx=0
     do ii=1,n_core_orb
      i=list_core(ii)
      do tt=1,n_act_orb
       t=list_act(tt)
       indx+=1
       excit(1,indx)=i
       excit(2,indx)=t
       excit_class(indx)='c-a'
      end do
     end do

     do ii=1,n_core_orb
      i=list_core(ii)
      do aa=1,n_virt_orb
       a=list_virt(aa)
       indx+=1
       excit(1,indx)=i
       excit(2,indx)=a
       excit_class(indx)='c-v'
      end do
     end do

     do tt=1,n_act_orb
      t=list_act(tt)
      do aa=1,n_virt_orb
       a=list_virt(aa)
       indx+=1
       excit(1,indx)=t
       excit(2,indx)=a
       excit_class(indx)='a-v'
      end do
     end do

     if (bavard) then
      write(6,*) ' Filled the table of the Monoexcitations '
      do indx=1,nMonoEx
        write(6,*) ' ex ',indx,' : ',excit(1,indx),' -> ' &
                  ,excit(2,indx),'  ',excit_class(indx)
      end do
     end if

END_PROVIDER

BEGIN_PROVIDER [real*8, norm_grad]
END_PROVIDER

BEGIN_PROVIDER [real*8, gradvec_exact, (nMonoEx)]
BEGIN_DOC
! calculate the orbital gradient <Psi| H E_pq |Psi> by hand, i.e. for 
! each determinant I we determine the string E_pq |I> (alpha and beta 
! separately) and generate <Psi|H E_pq |I>
! sum_I c_I <Psi|H E_pq |I> is then the pq component of the orbital
! gradient
! E_pq = a^+_pa_q + a^+_Pa_Q
END_DOC
     implicit none
     integer :: ii,tt,aa,indx,ihole,ipart,istate
     real*8 :: res

     do indx=1,nMonoEx
      ihole=excit(1,indx)
      ipart=excit(2,indx)
      call calc_grad_elem(ihole,ipart,res)
      gradvec_exact(indx)=res
      if (first_order) then
       if (excit_class(indx).eq.'c-a') then
        gradvec_exact(indx)*=1.D0/sqrt(2.D0-occnum(ipart))
       else if  (excit_class(indx).eq.'c-v') then
        gradvec_exact(indx)*=1.D0/sqrt(2.D0)
       else if  (excit_class(indx).eq.'a-v') then
        gradvec_exact(indx)*=1.D0/sqrt(occnum(ihole))
       else
        stop ' excit class impossible '
       end if
      end if
 
     end do

     norm_grad=0.d0
     do indx=1,nMonoEx
      norm_grad+=gradvec_exact(indx)*gradvec_exact(indx)
     end do
     norm_grad=sqrt(norm_grad)
     write(6,*)
     write(6,*) ' Norm of the exact orbital gradient (via <0|EH|0>) : ', norm_grad 
     write(6,*)
     if (norm_grad.gt.1.D-7) then
      casscf_converged=.false.
     else
      casscf_converged=.true.
     end if

END_PROVIDER

BEGIN_PROVIDER [logical, casscf_converged]
BEGIN_DOC
! flag for convergence, we do no go beyond 1.E-7
END_DOC
     casscf_converged=.false.
END_PROVIDER

     subroutine calc_grad_elem(ihole,ipart,res)
BEGIN_DOC
! eq 18 of Siegbahn et al, Physica Scripta 1980
! we calculate 2 <Psi| H E_pq | Psi>, q=hole, p=particle
END_DOC
       implicit none 
       integer :: ihole,ipart,mu,iii,ispin,ierr,nu,istate
       real*8 :: res
       integer(bit_kind), allocatable :: det_mu(:,:),det_mu_ex(:,:)
       real*8 :: i_H_psi_array(N_states),phase
       allocate(det_mu(N_int,2))
       allocate(det_mu_ex(N_int,2))

       res=0.D0

       do mu=1,n_det
! get the string of the determinant
        call det_extract(det_mu,mu,N_int)
        do ispin=1,2
! do the monoexcitation on it
         call det_copy(det_mu,det_mu_ex,N_int)
         call do_signed_mono_excitation(det_mu,det_mu_ex,nu &
            ,ihole,ipart,ispin,phase,ierr)
         if (ierr.eq.1) then
!       write(6,*)
!       write(6,*) ' mu = ',mu
!       call print_det(det_mu,N_int)
!         write(6,*) ' generated nu = ',nu,' for excitation ',ihole,' -> ',ipart,' ierr = ',ierr,' phase = ',phase,' ispin = ',ispin
!         call print_det(det_mu_ex,N_int)
          call i_H_psi(det_mu_ex,psi_det,psi_coef,N_int &
                  ,N_det,N_det,N_states,i_H_psi_array)
          do istate=1,N_states
           res+=i_H_psi_array(istate)*psi_coef(mu,istate)*phase
          end do
!         write(6,*) ' contribution = ',i_H_psi_array(1)*psi_coef(mu,1)*phase,res
         end if
        end do
       end do

! state-averaged gradient
       res*=2.D0/dble(N_states)
       if (first_order) then
        res*=0.5D0
       end if

     end subroutine calc_grad_elem

BEGIN_PROVIDER [real*8, gradvec, (nMonoEx)]
BEGIN_DOC
! calculate the orbital gradient <Psi| H E_pq |Psi> from density
! matrices and integrals; Siegbahn et al, Phys Scr 1980
! eqs 14 a,b,c
END_DOC
     implicit none
     integer :: i,t,a,indx,aa,uu,ii,u,tt
     real*8 :: gradvec_it,gradvec_ia,gradvec_ta
     real*8 :: nt,t2,term,mt
     real*8 :: berthier

     do i=1,nMonoEx
      gradvec(i)=0.D0
     end do

write(6,*) ' providing the gradient from D, P and integrals '

     if (first_order.and.approx_first_order) then
! approximate gradient
      berthier=0.D0
      indx=0
! 0 -> it
      do ii=1,n_core_orb
       i=list_core(ii)
       do tt=1,n_act_orb
        t=list_act(tt)
        indx+=1
        nt=occnum(t)
        mt=(2.D0-nt)
        term=2.D0*Fapq(i,t)+mt*Fipq(i,t)
! write(6,*) ' F term ',i,t,term/sqrt(mt)
! 2 Ptttt (it|tt) = nt(nt-1)(it|tt)
        t2=(nt-1.D0)*bielecCI(tt,tt,tt,i)
! correction for the diagonal terms in the sum ttuu and tuut
! so we can run over all u, including t
! we have to add -nt (tt|ti) + 0.5 nt (tt|ti) = -0.5 nt (tt|ti) 
        t2-=0.5*nt*bielecCI(tt,tt,tt,i)
        do uu=1,n_act_orb
         u=list_act(uu)
! 2 Pttuu (it|uu) = nt nu (it|uu)
! 2 Ptuut (iu|ut) = -1/2 nt nu (iu|ut) for t.ne.u
         t2+=occnum(u)*(bielecCI(uu,uu,tt,i)-0.5D0*bielecCI(tt,uu,uu,i))
        end do
write(6,*) ' P term ',i,t,-nt*t2/sqrt(mt)
        term-=nt*t2
        term*=1.D0/sqrt(mt)
        gradvec(indx)=term
!       write(6,*) ' grad ',i,' -> ',t,gradvec_exact(indx),gradvec(indx)
        berthier+=abs(term)
       end do
      end do
! 0 -> ia
      do ii=1,n_core_orb
       i=list_core(ii)
       do aa=1,n_virt_orb
        a=list_virt(aa)
        indx+=1
        term=sqrt(2.D0)*Fpq(i,a)
        gradvec(indx)=term
!       write(6,*) ' grad ',i,' -> ',a,gradvec_exact(indx),gradvec(indx)
        berthier+=abs(term)
       end do
      end do
! 0 -> ta
      do tt=1,n_act_orb
       t=list_act(tt)
       nt=occnum(t)
       do aa=1,n_virt_orb
        a=list_virt(aa)
        indx+=1
        term=Fipq(a,t)
! 2Ptttt (at|tt) = nt(nt-1)(at|tt)
! correction for the terms in the sum ttuu and tuut
        t2=-(1.D0-0.5D0*nt)*bielecCI(tt,tt,tt,a)
        do uu=1,n_act_orb
         u=list_act(uu)
! 2Pttuu (at|uu) = nt nu (at|uu)
! 2Ptuut (au|ut) = -1/2 nt nu (au|ut) for t.ne.u
         t2+=occnum(u)*(bielecCI(uu,uu,tt,a)-0.5D0*bielecCI(tt,uu,uu,a))
        end do
        term+=t2
        gradvec(indx)=term
!       write(6,*) ' grad ',t,' -> ',a,gradvec_exact(indx),gradvec(indx)
        berthier+=abs(term)
       end do
      end do
     else
! the complete gradient with the full 2P density matrix
      indx=0
      do i=1,n_core_orb
       do t=1,n_act_orb
        indx+=1
        gradvec(indx)=gradvec_it(i,t)
!       write(6,*) ' grad ',list_core(i),' -> ',list_act(t),gradvec_exact(indx),gradvec(indx)*0.5D0
       end do
      end do
    
      do i=1,n_core_orb
       do a=1,n_virt_orb
        indx+=1
        gradvec(indx)=gradvec_ia(i,a)
!       write(6,*) ' grad ',list_core(i),' -> ',list_virt(a),gradvec_exact(indx),gradvec(indx)*0.5D0
       end do
      end do
    
      do t=1,n_act_orb
       do a=1,n_virt_orb
        indx+=1
        gradvec(indx)=gradvec_ta(t,a)
!       write(6,*) ' grad ',list_act(t),' -> ',list_virt(a),gradvec_exact(indx),gradvec(indx)*0.5D0
       end do
      end do
      if (first_order) then
       do indx=1,nMonoEx
integer :: ihole,ipart
        if (excit_class(indx).eq.'c-a') then
         ipart=excit(2,indx)
         gradvec(indx)*=0.5D0/sqrt(2.D0-occnum(ipart))
        else if  (excit_class(indx).eq.'c-v') then
         gradvec(indx)*=0.5D0/sqrt(2.D0)
        else if  (excit_class(indx).eq.'a-v') then
         ihole=excit(1,indx)
         gradvec(indx)*=0.5D0/sqrt(occnum(ihole))
        else
         stop ' excit class impossible '
        end if
        berthier+=gradvec(indx)
       end do
      end if
     end if

     if (first_order) then
      write(6,*)
      write(6,*) ' Brillouin-Levy-Berthier condition ', abs(berthier)
      write(6,*)
     else
      norm_grad=0.d0
      do indx=1,nMonoEx
       norm_grad+=gradvec(indx)*gradvec(indx)
      end do
      norm_grad=sqrt(norm_grad)
      write(6,*)
      write(6,*) ' Norm of the orbital gradient (via D, P and integrals): ', norm_grad 
      write(6,*)
     end if

END_PROVIDER

      real*8 function gradvec_it(i,t)
BEGIN_DOC
! the orbital gradient core -> active
! we assume natural orbitals
END_DOC
         implicit none
         integer :: i,t

         integer :: ii,tt,v,vv,x,y
         integer :: x3,y3,t3
         real*8 :: t2,t2d,nt

         t2=0.D0
         t2d=0.D0
         ii=list_core(i)
         tt=list_act(t)
         t3=n_core_orb+t
         nt=occnum(tt)
         gradvec_it=2.D0*(Fipq(tt,ii)+Fapq(tt,ii))
         gradvec_it-=occnum(tt)*Fipq(ii,tt)
! write(6,*) ' F term ',ii,tt,gradvec_it/sqrt(2.D0-occnum(tt))
! calculate only tttt and ttuu, tuut terms, add rest
         do v=1,n_act_orb
          vv=list_act(v)
integer :: v3
          v3=v+n_core_orb
          do x=1,n_act_orb
integer :: xx
           xx=list_act(x)
           x3=x+n_core_orb
           do y=1,n_act_orb
            y3=y+n_core_orb
            if (v.eq.t.and.x.eq.t.and.y.eq.t) then
             t2d-=2.D0*P0tuvx_no(t,t,t,t)*bielec_PQxx(ii,tt,t3,t3)
!            write(6,*) ' P_tttt ',P0tuvx_no(t,t,t,t),0.5D0*nt*(nt-1.D0)
            else 
             if (t.eq.v.and.x.eq.y) then
              t2d-=2.D0*P0tuvx_no(t,t,x,x)*bielec_PQxx(ii,tt,x3,x3)
!             write(6,*) ' P_ttxx ',P0tuvx_no(t,t,x,x),0.5D0*nt*occnum(xx)
             else
              if (t.eq.y.and.x.eq.v) then
               t2d-=2.D0*P0tuvx_no(t,v,v,t)*bielec_PQxx(ii,vv,v3,t3)
!              write(6,*) ' P_tvvt ',P0tuvx_no(t,v,v,t),0.5D0*nt*occnum(vv)
              else
               t2-=2.D0*P0tuvx_no(t,v,x,y)*bielec_PQxx(ii,vv,x3,y3)
              end if
             end if
            end if
           end do
          end do
         end do
! write(6,*) ' P term tttt, ttuu, tuut ',ii,tt,t2d/sqrt(2.D0-occnum(tt))
         gradvec_it+=t2d
! write(6,*) ' P term others ',ii,tt,t2/sqrt(2.D0-occnum(tt))
         gradvec_it+=t2
! write(6,*) ' total for grad SX ',ii,tt,gradvec_it/sqrt(2.D0-occnum(tt))
         gradvec_it*=2.D0
      end function gradvec_it

      real*8 function gradvec_ia(i,a)
BEGIN_DOC
! the orbital gradient core -> virtual
END_DOC
         implicit none
         integer :: i,a,ii,aa

         ii=list_core(i)
         aa=list_virt(a)
         gradvec_ia=2.D0*(Fipq(aa,ii)+Fapq(aa,ii))
         gradvec_ia*=2.D0

      end function gradvec_ia

      real*8 function gradvec_ta(t,a)
BEGIN_DOC
! the orbital gradient active -> virtual
! we assume natural orbitals
END_DOC
         implicit none
         integer :: t,a,tt,aa,v,vv,x,y

         tt=list_act(t)
         aa=list_virt(a)
         gradvec_ta=0.D0
         gradvec_ta+=occnum(tt)*Fipq(aa,tt)
         do v=1,n_act_orb
          do x=1,n_act_orb
           do y=1,n_act_orb
            gradvec_ta+=2.D0*P0tuvx_no(t,v,x,y)*bielecCI(x,y,v,aa)
           end do
          end do
         end do
         gradvec_ta*=2.D0

      end function gradvec_ta

BEGIN_PROVIDER [real*8, gradvec_Fonly, (nMonoEx)]
BEGIN_DOC
! calculate the orbital gradient <Psi| H E_pq |Psi> from the
! Fock matrix only, no explicit integrals are implied
END_DOC
     implicit none
     integer :: i,t,a,indx,aa,uu,ii,u,tt
     real*8 :: nt,t2,term,mt
     real*8 :: berthier

     do i=1,nMonoEx
      gradvec_Fonly(i)=0.D0
     end do

     write(6,*) ' providing the gradient from Fock-matrix elements only '

     if (first_order.and.approx_first_order) then
! approximate gradient
      berthier=0.D0
      indx=0
! 0 -> it
      do ii=1,n_core_orb
       i=list_core(ii)
       do tt=1,n_act_orb
        t=list_act(tt)
        indx+=1
        nt=occnum(t)
        mt=(2.D0-nt)
        term=2.D0*Fapq(i,t)+mt*Fipq(i,t)
        term*=1.D0/sqrt(mt)
        gradvec_Fonly(indx)=term
!       write(6,*) ' grad ',i,' -> ',t,gradvec_exact(indx),gradvec_Fonly(indx)
        berthier+=abs(term)
       end do
      end do
! 0 -> ia
      do ii=1,n_core_orb
       i=list_core(ii)
       do aa=1,n_virt_orb
        a=list_virt(aa)
        indx+=1
        term=sqrt(2.D0)*Fpq(i,a)
        gradvec_Fonly(indx)=term
!       write(6,*) ' grad ',i,' -> ',a,gradvec_exact(indx),gradvec_Fonly(indx)
        berthier+=abs(term)
       end do
      end do
! 0 -> ta
      do tt=1,n_act_orb
       t=list_act(tt)
       nt=occnum(t)
       do aa=1,n_virt_orb
        a=list_virt(aa)
        indx+=1
        term=Fipq(a,t)
        gradvec_Fonly(indx)=term
!       write(6,*) ' grad ',t,' -> ',a,gradvec_exact(indx),gradvec_Fonly(indx)
        berthier+=abs(term)
       end do
      end do
      if (first_order) then
       do indx=1,nMonoEx
integer :: ihole,ipart
        if (excit_class(indx).eq.'c-a') then
         ipart=excit(2,indx)
         gradvec_Fonly(indx)*=0.5D0/sqrt(2.D0-occnum(ipart))
        else if  (excit_class(indx).eq.'c-v') then
         gradvec_Fonly(indx)*=0.5D0/sqrt(2.D0)
        else if  (excit_class(indx).eq.'a-v') then
         ihole=excit(1,indx)
         gradvec_Fonly(indx)*=0.5D0/sqrt(occnum(ihole))
        else
         stop ' excit class impossible '
        end if
        berthier+=gradvec_Fonly(indx)
       end do
      end if
     end if

     if (first_order) then
      write(6,*)
      write(6,*) ' Brillouin-Levy-Berthier condition ', abs(berthier)
      write(6,*)
     else
      norm_grad=0.d0
      do indx=1,nMonoEx
       norm_grad+=gradvec_Fonly(indx)*gradvec_Fonly(indx)
      end do
      norm_grad=sqrt(norm_grad)
      write(6,*)
      write(6,*) ' Norm of the orbital gradient (via D, P and integrals): ', norm_grad 
      write(6,*)
     end if

END_PROVIDER

