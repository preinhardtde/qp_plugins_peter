! -*- F90 -*-
BEGIN_PROVIDER [logical, three_particle_matrix]
END_PROVIDER

BEGIN_PROVIDER [real*8, occnum, (mo_num)]
    implicit none
    integer :: i,kk,j
    logical :: lread
    real*8 :: rdum
    do i=1,mo_num
     occnum(i)=0.D0
    end do
    do i=1,n_core_orb
     occnum(list_core(i))=2.D0
    end do

    open(unit=12,file='D0tu.dat',form='formatted',status='old')
    lread=.true.
    do while (lread)
     read(12,*,iostat=kk) i,j,rdum
     if (kk.ne.0) then
      lread=.false.
     else
      if (i.eq.j) then
       occnum(list_act(i))=rdum
      else
       write(6,*) ' WARNING - no natural orbitals !'
       write(6,*) i,j,rdum
      end if
     end if
    end do
    close(12)
    write(6,*) 
    write(6,*) ' read occupation numbers '
    write(6,9902)
  9902 format('  i',12x,'occ',13x,'2-occ',11x,'sqrt(occ)',9x,'sqrt(2-occ) ')
    do i=1,mo_num
     if (occnum(i).ne.2.D0.and.occnum(i).ne.0.D0) write(6,9901) i,occnum(i),2.D0-occnum(i),     &
              sqrt(occnum(i)),sqrt(2.D0-occnum(i))
 9901 format(i4,4F18.12)
    end do
    write(6,*) 
END_PROVIDER

BEGIN_PROVIDER [real*8, P0tuvx_no, (n_act_orb,n_act_orb,n_act_orb,n_act_orb)]
      implicit none
      integer :: i,j,k,l,kk
      real*8 :: rdum
      logical :: lread

      do i=1,n_act_orb
       do j=1,n_act_orb
        do k=1,n_act_orb
         do l=1,n_act_orb
          P0tuvx_no(l,k,j,i)=0.D0
         end do
        end do
       end do
      end do

      open(unit=12,file='P0tuvx.dat',form='formatted',status='old')
      lread=.true.
      do while (lread)
       read(12,*,iostat=kk) i,j,k,l,rdum
       if (kk.ne.0) then
        lread=.false.
       else
        P0tuvx_no(i,j,k,l)=rdum
       end if
      end do
      close(12)
      write(6,*) ' read the 2-particle density matrix '
END_PROVIDER

BEGIN_PROVIDER [real*8, Q0vxyztu_no, (n_act_orb,n_act_orb,n_act_orb,n_act_orb,n_act_orb,n_act_orb)]
      implicit none
      integer :: t,u,v,x,y,z,kk
      real*8 :: rdum
      logical :: lread

      do v=1,n_act_orb
       do x=1,n_act_orb
        do y=1,n_act_orb
         do z=1,n_act_orb
          do t=1,n_act_orb
           do u=1,n_act_orb
            Q0vxyztu_no(v,x,y,z,t,u)=0.D0
           end do
          end do
         end do
        end do
       end do
      end do

      open(unit=12,file='Q0vxyztu.dat',form='formatted',status='old')
      lread=.true.
      do while (lread)
       read(12,*,iostat=kk) v,x,y,z,t,u,rdum
       if (kk.ne.0) then
        lread=.false.
       else
        Q0vxyztu_no(v,x,y,z,t,u)=rdum
       end if
      end do
      close(12)
      write(6,*) ' read the 3-particle density matrix '
END_PROVIDER
