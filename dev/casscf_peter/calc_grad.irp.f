! -*- F90 -*-
program cgrad
  implicit none
  BEGIN_DOC
! calculating an orbital gradient on a wavefunction
  END_DOC
 read_wf = .True.
 touch read_wf

         call header
         call driver
end program cgrad

       subroutine header
         write(6,*) 
         write(6,*) ' the orbital gradient for the CASSCF calculation '
         write(6,*) 
         write(6,*) ' P. Reinhardt (Paris, feb 2019) '
         write(6,*) 
         write(6,*) 
       end subroutine header

       subroutine driver
         implicit none
         integer :: i,j


         write(6,*) ' mo_num = ',mo_num
         write(6,*) ' ao_num = ',ao_num
         write(6,*)
         write(6,*) ' <0|H|0>     (qp)      = ',psi_energy_with_nucl_rep(1)
         write(6,*)
         write(6,*) ' ecore  = ',ecore
         write(6,*) ' number of determinants in the wavefunction ',n_det

         write(6,*) 
         write(6,*) ' the orbital gradient '
         write(6,*) ' No p -> q   type <0|HE|0>   from D, P and integrals'
         do i=1,nMonoEx
          if (gradvec(i).ne.0.D0) then
           if (abs(gradvec_exact(i)/gradvec(i)-1.D0).lt.1.D-4) then
            write(6,9911) i,excit(1,i),excit(2,i),excit_class(i),gradvec_exact(i),gradvec(i),gradvec_exact(i)/gradvec(i)
9911 format(i3,' ',i3,' ->',i3,' (',a3,') : ',3E13.4,' ok ')
9912 format(i3,' ',i3,' ->',i3,' (',a3,') : ',3E13.4,' wrong ')
9913 format(i3,' ',i3,' ->',i3,' (',a3,') : ',3E13.4,' wrong !!!')
           else
            if (abs(gradvec(i)).gt.1.D-4) then
             write(6,9913) i,excit(1,i),excit(2,i),excit_class(i),gradvec_exact(i),gradvec(i),gradvec_exact(i)/gradvec(i)
            else
             write(6,9912) i,excit(1,i),excit(2,i),excit_class(i),gradvec_exact(i),gradvec(i),gradvec_exact(i)/gradvec(i)
            end if
           end if
          else
           if (gradvec_exact(i).ne.0.D0) then
            if (abs(gradvec(i)).gt.1.D-4) then
             write(6,9913) i,excit(1,i),excit(2,i),excit_class(i),gradvec_exact(i),gradvec(i),gradvec_exact(i)/gradvec(i)
            else
             write(6,9912) i,excit(1,i),excit(2,i),excit_class(i),gradvec_exact(i),gradvec(i)
            end if
           else
            write(6,9911) i,excit(1,i),excit(2,i),excit_class(i),gradvec_exact(i),gradvec(i)
           end if
          end if
         end do

         call write_time(6)
         approx_first_order=.true.
         touch approx_first_order

         write(6,*) 
         write(6,*) ' the orbital gradient '
         write(6,*) ' No p -> q   type <0|HE|0>   from D, P and integrals'
         do i=1,nMonoEx
          if (gradvec(i).ne.0.D0) then
           if (abs(gradvec_exact(i)/gradvec(i)-1.D0).lt.1.D-4) then
            write(6,9911) i,excit(1,i),excit(2,i),excit_class(i),gradvec_exact(i),gradvec(i),gradvec_exact(i)/gradvec(i)
           else
            if (abs(gradvec(i)).gt.1.D-4) then
             write(6,9913) i,excit(1,i),excit(2,i),excit_class(i),gradvec_exact(i),gradvec(i),gradvec_exact(i)/gradvec(i)
            else
             write(6,9912) i,excit(1,i),excit(2,i),excit_class(i),gradvec_exact(i),gradvec(i),gradvec_exact(i)/gradvec(i)
            end if
           end if
          else
           if (gradvec_exact(i).ne.0.D0) then
            if (abs(gradvec(i)).gt.1.D-4) then
             write(6,9913) i,excit(1,i),excit(2,i),excit_class(i),gradvec_exact(i),gradvec(i),gradvec_exact(i)/gradvec(i)
            else
             write(6,9912) i,excit(1,i),excit(2,i),excit_class(i),gradvec_exact(i),gradvec(i)
            end if
           else
            write(6,9911) i,excit(1,i),excit(2,i),excit_class(i),gradvec_exact(i),gradvec(i)
           end if
          end if
         end do
 
         write(6,*)
         write(6,*) '   ... all done '

       end subroutine driver
