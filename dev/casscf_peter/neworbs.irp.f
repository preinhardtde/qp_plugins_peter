! -*- F90 -*-
BEGIN_PROVIDER [real*8, SXmatrix, (nMonoEx+1,nMonoEx+1)]
      implicit none
      integer :: i,j
      if (casscf_converged) then
        write(6,*) ' CASSCF orbital gradient converged, we return '
        return
      end if
      do i=1,nMonoEx+1
       do j=1,nMonoEx+1
        SXmatrix(i,j)=0.D0
       end do
      end do

      if (first_order) then
! first order, i.e. the Hamiltonian between SX states
       do i=1,nMonoEx
        SXmatrix(1,i+1)=0.5D0*gradvec(i)
        SXmatrix(1+i,1)=0.5D0*gradvec(i)
       end do
       do i=1,nMonoEx
        do j=1,nMonoEx
         SXmatrix(i+1,j+1)=SX_hamil(i,j)
         SXmatrix(j+1,i+1)=SX_hamil(i,j)
        end do
       end do
       if (bavard) then
        do i=1,nMonoEx
         write(6,*) ' diagonal of the SX Hamilton matrix : ',i,SX_hamil(i,i)
        end do
       end if
      else
! second order, i.e. real gradient and Hessian
       do i=1,nMonoEx
        SXmatrix(1,i+1)=gradvec(i)
        SXmatrix(1+i,1)=gradvec(i)
       end do
       
       do i=1,nMonoEx
        do j=1,nMonoEx
         SXmatrix(i+1,j+1)=hessmat(i,j)
         SXmatrix(j+1,i+1)=hessmat(i,j)
        end do
       end do
      
       if (bavard) then
        do i=1,nMonoEx
         write(6,*) ' diagonal of the Hessian : ',i,hessmat(i,i)
        end do
       end if
      end if

END_PROVIDER

 BEGIN_PROVIDER [real*8, SXeigenvec, (nMonoEx+1,nMonoEx+1)]
&BEGIN_PROVIDER [real*8, SXeigenval, (nMonoEx+1)]
 END_PROVIDER

 BEGIN_PROVIDER [real*8, SXvector, (nMonoEx+1)]
&BEGIN_PROVIDER [real*8, energy_improvement]
      implicit none
      integer :: ierr,matz,i
      real*8 :: c0

      if (casscf_converged) then
       c0=1.D0
       SXvector(1)=1.D0
       do i=2,nMonoEx+1
        SXvector(i)=0.D0
       end do
       return
      end if
      call lapack_diag(SXeigenval,SXeigenvec,SXmatrix,nMonoEx+1,nMonoEx+1)
      write(6,*) ' SXdiag : lowest 5 eigenvalues '
      write(6,*) ' 1 - ',SXeigenval(1),SXeigenvec(1,1)
      write(6,*) ' 2 - ',SXeigenval(2),SXeigenvec(1,2)
      write(6,*) ' 3 - ',SXeigenval(3),SXeigenvec(1,3)
      write(6,*) ' 4 - ',SXeigenval(4),SXeigenvec(1,4)
      write(6,*) ' 5 - ',SXeigenval(5),SXeigenvec(1,5)
      write(6,*) 
      write(6,*) ' SXdiag : lowest eigenvalue = ',SXeigenval(1)
      energy_improvement = SXeigenval(1)

      if (SXeigenval(1).gt.0.D0) then
       write(6,*) ' no improvement found '
       stop ' nothing to do any more '
      end if

integer :: best_vector
real*8 :: best_overlap
      best_overlap=0.D0
      do i=1,nMonoEx+1
       if (SXeigenval(i).lt.0.D0) then
        if (abs(SXeigenvec(1,i)).gt.best_overlap) then
         best_overlap=abs(SXeigenvec(1,i))
         best_vector=i
        end if
       end if
      end do

      write(6,*) ' SXdiag : eigenvalue for best overlap with ' 
      write(6,*) '  previous orbitals = ',SXeigenval(best_vector)
      energy_improvement = SXeigenval(best_vector)
      
      c0=SXeigenvec(1,best_vector)
      SXvector(1)=c0

      if (first_order) then
       if (c0.lt.0.D0) then
        do i=1,nMonoEx+1
         SXvector(i)=-SXeigenvec(i,best_vector)
        end do
       else
        do i=1,nMonoEx+1
         SXvector(i)=SXeigenvec(i,best_vector)
        end do
       end if
! scale the entries of the SX CI vector with the occupation numbers
! from the initial Full CI, i.e. the D^{(0)}
integer :: ii,a,aa,indx,t,tt
       indx=1
       write(6,*) ' reference vector 0-0',SXvector(indx)
       write(6,*) ' weight of the excitations, a_I, c_I (unscaled)'
       do ii=1,n_core_orb
        i=list_core(ii)
        do tt=1,n_act_orb
         t=list_act(tt)
         indx+=1
         SXvector(indx)*=1.D0/sqrt(2.D0-occnum(t))
         if (abs(SXvector(indx)).gt.1.D-6) then
          write(6,9901) i,t,SXvector(indx),SXvector(indx)*sqrt(2.D0-occnum(t))
  9901 format('   core -> active',2i4,2F16.8)
         end if
        end do
       end do
       do ii=1,n_core_orb
        i=list_core(ii)
        do aa=1,n_virt_orb
         a=list_virt(aa)
         indx+=1
         SXvector(indx)*=1.D0/sqrt(2.D0)
         if (abs(SXvector(indx)).gt.1.D-6) then
          write(6,9902) i,a,SXvector(indx),SXvector(indx)*sqrt(2.D0)
  9902 format('   core -> virtual',2i4,2F16.8)
         end if
        end do
       end do
       do tt=1,n_act_orb
        t=list_act(tt)
        do aa=1,n_virt_orb
         a=list_virt(aa)
         indx+=1
         SXvector(indx)*=1.D0/sqrt(occnum(t))
         if (abs(SXvector(indx)).gt.1.D-6) then
          write(6,9903) t,a,SXvector(indx),SXvector(indx)*sqrt(occnum(t))
  9903 format('   active -> virtual',2i4,2F16.8)
         end if
        end do
       end do
      else
! for 2nd order we have to use an intermediate normalization
       write(6,*) ' weight of the 1st element ',c0
       write(6,*) ' appplying a damping faactor of ',damping
       do i=1,nMonoEx+1
        SXvector(i)=damping*SXeigenvec(i,best_vector)/c0
       end do
      end if
END_PROVIDER

BEGIN_PROVIDER [real*8, NewOrbs, (ao_num,mo_num) ]
      implicit none
      integer :: i,j,ialph

      if (first_order) then
!      call form_SXorbs
       call use_SXcoefs
      else
! form the exponential of the Orbital rotations
       call get_orbrotmat
      end if
! form the new orbitals from the rotation matrix Umat
      do ialph=1,ao_num
       do j=1,mo_num
        NewOrbs(ialph,j)=0.D0
       end do
      end do
    if (.not.first_order) then
! we sort the eigenvectors for that U(i,i) is maximized

integer :: ipiv
real*8 :: vpiv,vdum
      do i=1,mo_num
! in which eigenvector we find the largest element at position i ?
       ipiv=1
       vpiv=abs(umat(i,1))
       do j=1,mo_num
        if (abs(Umat(i,j)).gt.vpiv) then
         ipiv=j
         vpiv=abs(umat(i,j))
        end if
       end do
! we found it in ipiv
! we exchange vectors ipiv and i
       if (i.ne.ipiv) then
        write(6,*) ' exchanging ',ipiv,' and ',i
        do j=1,mo_num
         vdum=umat(j,ipiv)
         umat(j,ipiv)=umat(j,i)
         umat(j,i)=vdum
        end do
       end if
      end do
    end if

      do j=1,mo_num
       if (Umat(j,j).lt.0.D0) then
        do i=1,mo_num
         Umat(i,j)=-Umat(i,j)
        end do
       end if
      end do

!     write(6,*) ' the diagonal of Umat - this should be the overlap <new|old> '
!     write(6,'(5(i5,F12.4))') (i,Umat(i,i),i=1,mo_num)
!     write(6,*)

      open(unit=97,file='Umat.tmp',form='formatted',status='unknown')
      do i=1,mo_num
       do j=1,mo_num
        if (abs(Umat(i,j)).gt.1.D-10) write(97,*) i,j,umat(i,j)
       end do
      end do
      close(97)
  

      do ialph=1,ao_num
       do i=1,mo_num
        wrkline(i)=mo_coef(ialph,i)
       end do
       do i=1,mo_num
        do j=1,mo_num
         NewOrbs(ialph,i)+=Umat(i,j)*wrkline(j)
        end do
       end do
      end do

! calculate overlap of old and new vectors, 
! we suppose that there is no exchange of orbitals
! we may even reorder orbitals for having the best overlap

real*8 :: ss,ssum
integer :: alpha,beta
     ssum=0.D0
     do i=1,mo_num
      ss=0.D0
      do alpha=1,ao_num
       do beta=1,ao_num
        ss+=NewOrbs(alpha,i)*mo_coef(beta,i)*ao_overlap(alpha,beta)
       end do
      end do
      if (ss.lt.0.D0) then
       do alpha=1,ao_num
        NewOrbs(alpha,i)=-NewOrbs(alpha,i)
       end do
       ss=-ss
      end if
      write(6,*) ' overlap <new|old> = ',i,ss
      ssum+=ss
     end do


     write(6,9803) ssum/dble(mo_num)*100.D0
  9803 format(' relative sum of all overlaps is ',F10.4,' %')
     write(6,*)

END_PROVIDER

 BEGIN_PROVIDER [real*8, Tpotmat, (mo_num,mo_num) ]
&BEGIN_PROVIDER [real*8, Umat, (mo_num,mo_num) ]
&BEGIN_PROVIDER [real*8, wrkline, (mo_num) ]
&BEGIN_PROVIDER [real*8, Tmat, (mo_num,mo_num) ]
END_PROVIDER

BEGIN_PROVIDER [real*8, cwrk_SXorbs, (ao_num,mo_num)]
END_PROVIDER

      subroutine use_SXcoefs
BEGIN_DOC
! we use directly the eigenvector of the lowest SX state
! we avoid completely cc, aa or vv rotations
!
! as we need a temporary array to store the changes, we fill 
! Umat withe overlaps of old and new vectors.
! the two orbital sets, mo_coef and cwrk, are orthogonal sets 
END_DOC
      implicit none
      integer :: i,j,alpha
      real*8 :: ss 

! sxvector should be in intermediate normalization
! so either we muliply initially with c0, or we divide 
! all elements of SXvector by c0
      ss=sxvector(1)
      do i=1,mo_num
       do alpha=1,ao_num
        cwrk_SXorbs(alpha,i)=mo_coef(alpha,i)*ss
       end do
      end do

integer :: ii,tt,aa,t,a
integer :: address_it,address_ia,address_ta
! core-active
      do ii=1,n_core_orb
       i=list_core(ii)
       do tt=1,n_act_orb
        t=list_act(tt)
        ss=sxvector(address_it(i,t))
        do alpha=1,ao_num
         cwrk_SXorbs(alpha,i)+=ss*cwrk_SXorbs(alpha,t)
         cwrk_SXorbs(alpha,t)-=ss*cwrk_SXorbs(alpha,i)
        end do
       end do
! core-virtual
       do aa=1,n_virt_orb
        a=list_virt(aa)
        ss=sxvector(address_ia(i,a))
        do alpha=1,ao_num
         cwrk_SXorbs(alpha,i)+=ss*cwrk_SXorbs(alpha,a)
         cwrk_SXorbs(alpha,a)-=ss*cwrk_SXorbs(alpha,i)
        end do
       end do
      end do
! active-virtual
      do tt=1,n_act_orb
       t=list_act(tt)
       do aa=1,n_virt_orb
        a=list_virt(aa)
        ss=sxvector(address_ta(t,a))
        do alpha=1,ao_num
         cwrk_SXorbs(alpha,t)+=ss*cwrk_SXorbs(alpha,a)
         cwrk_SXorbs(alpha,a)-=ss*cwrk_SXorbs(alpha,t)
        end do
       end do
      end do

      call orthogonalize_CAS(cwrk_SXorbs)
integer :: beta
      do i=1,mo_num
       do j=1,mo_num
        ss=0.D0
        do alpha=1,ao_num
         do beta=1,ao_num
          ss+=cwrk_SXorbs(alpha,j)*mo_coef(beta,i)*ao_overlap(alpha,beta)
         end do
        end do
        Umat(j,i)=ss
       end do
      end do
      
      end subroutine use_SXcoefs

      subroutine form_SXorbs
BEGIN_DOC
! we diagonalize the SX density matrix - our Umat is 
! the ensemble of eigenvectors
! eigenvalues are occupation numbers, which we do not need 
! however, as we redo the CAS in the next iteration
! occupation numbers are not stored
END_DOC
      implicit none
      integer :: i,j
      real*8 :: dum

      call lapack_diag(wrkline,Umat,SXdensmat,mo_num,mo_num)
! we have to reorder the data as eigenvalues are in ascending order - 
! we need them in descending order
! Umat(i,j) is component i of vector j
      do i=1,mo_num/2
       dum=wrkline(i)
       wrkline(i)=wrkline(mo_num+1-i)
       wrkline(mo_num+1-i)=dum
       do j=1,mo_num
! well ... i,j or j,i ?
        dum=Umat(j,i)
        Umat(j,i)=Umat(j,mo_num+1-i)
        Umat(j,mo_num+1-i)=dum
       end do
      end do
      write(6,*) ' new occupation numbers '
      write(6,'(5(i5,F12.4))') (i,wrkline(i),i=1,mo_num)
      write(6,*) 

      end subroutine form_SXorbs

      subroutine get_orbrotmat
BEGIN_DOC
! form the exponential e^T from the amplitudes
! we use the summation of the series without diagonalization 
! and complex algebra     
END_DOC
      implicit none
      integer :: i,j,indx,k,iter,t,a,ii,tt,aa
      real*8 :: sum
      logical :: converged


! the orbital rotation matrix T
      do i=1,mo_num
       do j=1,mo_num
        Tmat(i,j)=0.D0
        Umat(i,j)=0.D0
        Tpotmat(i,j)=0.D0
       end do
       Tpotmat(i,i)=1.D0
      end do

      indx=1
      do i=1,n_core_orb
       ii=list_core(i)
       do t=1,n_act_orb
        tt=list_act(t)
        indx+=1
        Tmat(ii,tt)= SXvector(indx)
        Tmat(tt,ii)=-SXvector(indx)
       end do
      end do
      do i=1,n_core_orb
       ii=list_core(i)
       do a=1,n_virt_orb
        aa=list_virt(a)
        indx+=1
        Tmat(ii,aa)= SXvector(indx)
        Tmat(aa,ii)=-SXvector(indx)
       end do
      end do
      do t=1,n_act_orb
       tt=list_act(t)
       do a=1,n_virt_orb
        aa=list_virt(a)
        indx+=1
        Tmat(tt,aa)= SXvector(indx)
        Tmat(aa,tt)=-SXvector(indx)
       end do
      end do

      write(6,*) ' the T matrix '
      do indx=1,nMonoEx
       i=excit(1,indx)
       j=excit(2,indx)
       if (bavard.and.abs(Tmat(i,j)).gt.1.D-9) then
               write(6,9901) i,j,excit_class(indx),Tmat(i,j)
       end if
  9901 format('   ',i4,' -> ',i4,' (',A3,') : ',E14.6)
      end do

      write(6,*) 
      write(6,*) ' forming the matrix exponential '
      write(6,*) 
! form the exponential
      iter=0
      converged=.false.
      do while (.not.converged)
       iter+=1
! add the next term
       do i=1,mo_num
        do j=1,mo_num
         Umat(i,j)+=Tpotmat(i,j)
        end do
       end do
! next power of T, we multiply Tpotmat with Tmat/iter
       do i=1,mo_num
        do j=1,mo_num
         wrkline(j)=Tpotmat(i,j)/dble(iter)
         Tpotmat(i,j)=0.D0
        end do
        do j=1,mo_num
         do k=1,mo_num
          Tpotmat(i,j)+=wrkline(k)*Tmat(k,j)
         end do
        end do
       end do
! Convergence test
       sum=0.D0
       do i=1,mo_num
        do j=1,mo_num
         sum+=abs(Tpotmat(i,j))
        end do
       end do
       write(6,*) ' Iteration No ',iter,' Sum = ',sum
       if (sum.lt.1.D-6) then
        converged=.true.
       end if
       if (iter.ge.NItExpMax) then
        stop ' no convergence '
       end if
      end do
      write(6,*)
      write(6,*) ' Converged ! '
      write(6,*)

      end subroutine get_orbrotmat

BEGIN_PROVIDER [integer, NItExpMax]
   NItExpMax=100
END_PROVIDER

BEGIN_PROVIDER [real*8, SXdensmat, (mo_num,mo_num) ]

       implicit none
       integer :: i,j,ii,jj,t,tt,a,aa,u,uu,b,bb,v,vv,x,xx
       integer :: address_ia,address_it,address_ta
       real*8 :: a0, aia, aib, ait, aiu, aja, ajt, ata, atb, aua, Dtt
       real*8 :: ss

       do i=1,mo_num
        do j=1,mo_num
         SXdensmat(i,j)=0.D0
        end do
       end do

! equations B.3 of the article of Roos, Taylor, Siegbahn 
! with diagonal D
! core-core
      a0=SXvector(1)
      do ii=1,n_core_orb
       i=list_core(ii)
       SXdensmat(i,i)+=2.D0
       do aa=1,n_virt_orb
        a=list_virt(aa)
        aia=SXvector(address_ia(i,a))
        SXdensmat(i,i)-=2.D0*aia*aia
        do jj=ii+1,n_core_orb
         j=list_core(jj)
         aja=SXvector(address_ia(j,a))
         SXdensmat(i,j)-=2.D0*aia*aja
         SXdensmat(j,i)-=2.D0*aia*aja
        end do
       end do
       do tt=1,n_act_orb
        t=list_act(tt)
        ait=SXvector(address_it(i,t))
        SXdensmat(i,i)-=(2.D0-occnum(t))*ait*ait
        do jj=ii+1,n_core_orb
         j=list_core(jj)
         ajt=SXvector(address_it(j,t))
         SXdensmat(i,j)-=ait*ajt*(2.D0-occnum(t))
         SXdensmat(j,i)-=ait*ajt*(2.D0-occnum(t))
        end do
       end do
      end do
! core-active
      do tt=1,n_act_orb
       t=list_act(tt)
       Dtt=occnum(t)
       do ii=1,n_core_orb
        i=list_core(ii)
        ait=SXvector(address_it(i,t))
        SXdensmat(i,t)+=a0*ait*(2.D0-Dtt)
        SXdensmat(t,i)+=a0*ait*(2.D0-Dtt)
        ss=0.D0
        do aa=1,n_virt_orb
         a=list_virt(aa)
         aia=SXvector(address_ia(i,a))
         ata=SXvector(address_ta(t,a))
         ss+=aia*ata
        end do
        SXdensmat(i,t)-=ss*Dtt
        SXdensmat(t,i)-=ss*Dtt
       end do
      end do
! core-virtual
      do ii=1,n_core_orb
       i=list_core(ii)
       do aa=1,n_virt_orb
        a=list_virt(aa)
        aia=SXvector(address_ia(i,a))
        SXdensmat(i,a)=2.D0*a0*aia
        SXdensmat(a,i)=2.D0*a0*aia
       end do
      end do

! active-active
!
! the part without the term containing P
!
      do tt=1,n_act_orb
       t=list_act(tt)
       dtt=occnum(t)
       SXdensmat(t,t)=dtt
       do uu=1,n_act_orb
        u=list_act(uu)
        ss=0.D0
        do ii=1,n_core_orb
         i=list_core(ii)
         aiu=SXvector(address_it(i,u))
         ait=SXvector(address_it(i,t))
         ss+=ait*aiu
        end do
        SXdensmat(t,u)+=ss*(2.D0-dtt-occnum(u))
        if (t.ne.u) then
         SXdensmat(u,t)+=ss*(2.D0-dtt-occnum(u))
        end if
       end do
      end do
!
! we add the term containing P
! first an intermediate quantity,  sum_a a_va a_xa - sum_i a_iv a_ix
real*8 :: Avx(n_act_orb,n_act_orb)
      do vv=1,n_act_orb
       v=list_act(vv)
       do xx=1,n_act_orb
        x=list_act(xx)
        ss=0.D0
        do ii=1,n_core_orb
         i=list_core(ii)
         aix=SXvector(address_it(i,x))
         aiv=SXvector(address_it(i,v))
         ss+=aiv*aix
        end do
        do aa=1,n_virt_orb
         a=list_virt(aa)
         axa=SXvector(address_ta(x,a))
         ava=SXvector(address_ta(v,a))
         ss+=ava*axa
        end do
        Avx(vv,xx)=ss
       end do
      end do

      if (approx_first_order) then
!
! P_ttuu=1/2 n_t(n_u-delta_tu)
! P_tuut=-1/4 n_tn_u for t.ne.u
!        
! we need P_vxtu, thus for t=u this gives a sum over v and v=x
! otherwise v=u and x=t 
! think on a white page first ... 
! sum_vx (2P_vxtu-D_vx D_tu)*(sum_a - sum_i) = ... 
! = sum_v (2P_vvtt - D_vv D_tt) * ( ... ) if t=u
!    =0 for v.ne.t
! thus only the term t=u=v=x : -n_t survives
! otherwise (2 P_tuut - D_ut D_ut) = -1/2 n_t n_u for t.ne.u
!
       do tt=1,n_act_orb
        t=list_act(tt)
        dtt=occnum(t)
        SXdensmat(t,t)-=dtt*Avx(tt,tt)
        do uu=tt+1,n_act_orb
         u=list_act(uu)
         SXdensmat(t,u)-=0.5D0*dtt*occnum(u)*Avx(tt,uu)
         SXdensmat(u,t)-=0.5D0*dtt*occnum(u)*Avx(tt,uu)
        end do
       end do
      else
! the real P matrix
real*8 :: aiv, aix, axa, ava, nt, nv
       ss=0.D0
       do vv=1,n_act_orb
        v=list_act(vv)
        nv=occnum(v)
        do xx=1,n_act_orb
         x=list_act(xx)
         do tt=1,n_act_orb
          t=list_act(tt)
          nt=occnum(t)
          SXdensmat(t,t)+=2.D0*P0tuvx_no(vv,xx,tt,tt)*Avx(vv,xx)
          if (v.eq.x) then
           SXdensmat(t,t)-=nt*nv*Avx(vv,xx)
          end if
! t .ne. u          
          do uu=tt+1,n_act_orb
           u=list_act(uu)
           SXdensmat(t,u)+=2.D0*P0tuvx_no(vv,xx,tt,uu)*Avx(vv,xx)
           SXdensmat(u,t)+=2.D0*P0tuvx_no(vv,xx,uu,tt)*Avx(vv,xx)
          end do
         end do
        end do
       end do

      end if
! active-virtual
      do tt=1,n_act_orb
       t=list_act(tt)
       dtt=occnum(t)
       do aa=1,n_virt_orb
        a=list_virt(aa)
        ata=SXvector(address_ta(t,a))
        SXdensmat(t,a)+=a0*ata*dtt
        SXdensmat(a,t)+=a0*ata*dtt
       end do
       do ii=1,n_core_orb
        i=list_core(ii)
        ait=SXvector(address_it(i,t))*(2.D0-dtt)
        do aa=1,n_virt_orb
         a=list_virt(aa)
         aia=SXvector(address_ia(i,a))
         SXdensmat(t,a)=aia*ait
         SXdensmat(a,t)=aia*ait
        end do
       end do
      end do
! virtual-virtual
      do aa=1,n_virt_orb
       a=list_virt(aa)
       do bb=aa,n_virt_orb
        b=list_virt(bb)
        ss=0.D0
 
        do ii=1,n_core_orb
         i=list_core(ii)
         aia=SXvector(address_ia(i,a))
         aib=SXvector(address_ia(i,b))
         ss+=aia*aib
        end do
        SXdensmat(a,b)+=2.D0*ss
        if (a.ne.b) then
         SXdensmat(b,a)+=2.D0*ss
        end if
        ss=0.D0
        do tt=1,n_act_orb
         t=list_act(tt)
         ata=SXvector(address_ta(t,a))
         atb=SXvector(address_ta(t,b))
         ss+=ata*atb*occnum(t)
        end do
        SXdensmat(a,b)+=ss
        if (a.ne.b) then
         SXdensmat(b,a)+=ss
        end if
       end do
      end do

      write(6,*) 
      write(6,*) ' the diagonal of the SXdensmat matrix '
      write(6,'(5(i4,F12.6))') (i,SXdensmat(i,i),i=1,mo_num)
      ss=0.D0
      do i=1,mo_num
       ss+=SXdensmat(i,i)
      end do
      write(6,*) ' and the trace : '
      write(6,*) ' sum_p D_pp = ',ss
      write(6,*)


      write(6,*) 
      write(6,*) ' the intriguing elements D^SX_tt '
      write(6,9901) 
 9901 format(9x,'t',5x,'with P',5x,'without P',5x,'delta',5x,'our D^SX elements')
real*8 :: delta,deltas,ss1,ss2,nel0,n1,n2,n11,n22
logical :: lvt,lvx
      deltas=0.D0
      n11=0.D0
      n22=0.D0
      nel0=0.D0
      do tt=1,n_act_orb
       t=list_act(tt)
       nt=occnum(t)
       ss=0.D0
       do ii=1,n_core_orb
        i=list_core(ii)
        ait=SXvector(address_it(i,t))
        ss+=ait*ait
       end do
       nel0+=nt
       delta=0.D0
       n1=nt+2.D0*ss*(1.D0-nt)
       n2=nt+2.D0*ss*(1.D0-nt)
       ss=0.D0
       do vv=1,n_act_orb
        v=list_act(vv)
        lvt=v.eq.t
        nv=occnum(v)
        do xx=1,n_act_orb
         x=list_act(xx)
         lvx=x.eq.v
         ss1=2.D0*P0tuvx_no(vv,xx,tt,tt)
         ss2=0.D0
         if (lvx) then
          ss1-=nv*nt
          if (lvt) then
           ss2+=nt
          end if
         end if
         ss=ss1+ss2
         delta+=Avx(vv,xx)*ss
         n1+=ss1*Avx(vv,xx)
         n2-=ss2*Avx(vv,xx)
        end do 
       end do 
       write(6,9902) tt,n1,n2,delta,SXdensmat(t,t)
 9902 format(I10,F12.6,F12.6,E14.4,F12.6)
       n11+=n1
       n22+=n2
       deltas+=delta
      end do 
      write(6,*) ' the sum of all the deltas                     ',deltas
      write(6,*) ' nummber of active electrons, with P matrix    ',n11
      write(6,*) ' nummber of active electrons, without P matrix ',n22
      write(6,*) ' nummber of active electrons, sum n_t :        ',nel0
      write(6,*) 

real*8 :: si,st,sa
! D_ii
      si=0.D0
      do ii=1,n_core_orb
       i=list_core(ii)
       si+=SXdensmat(i,i)
      end do
      write(6,*) ' sum over occ orbitals       ',si
! D_tt
      st=0.D0
      do tt=1,n_act_orb
       t=list_act(tt)
       st+=SXdensmat(t,t)
      end do
      write(6,*) ' sum over act orbitals       ',st

! D_aa
      sa=0.D0
      do aa=1,n_virt_orb
       a=list_virt(aa)
       sa+=SXdensmat(a,a)
      end do
      write(6,*) ' sum over virt orbitals      ',sa
      write(6,*) ' number of electrons         ',si+st+sa

      ss=2.D0*n_core_orb
      do tt=1,n_act_orb
       t=list_act(tt)
       ss+=occnum(t)
      end do
      write(6,*) ' 2*n_core_orb + sum_t n_t  = ',ss
      write(6,*)

      open(unit=12,file='SXdensmat.tmp',form='formatted',status='unknown')
      do i=1,mo_num
       do j=1,mo_num
        if (abs(SXdensmat(i,j)).gt.1.D-8) write(12,*) i,j,SXdensmat(i,j)
       end do
      end do
      close(12)
END_PROVIDER

       integer function address_it(i,t)
         implicit none
         integer :: i,t
BEGIN_DOC
! address of a couple core-active
END_DOC
         address_it=offset_it+(i-1)*n_act_orb  &
               +(t-n_core_orb)
       end function address_it

       integer function address_ia(i,a)
         implicit none
         integer :: i,a
BEGIN_DOC
! address of a couple core-virtual
END_DOC
         address_ia=offset_ia+(i-1)*n_virt_orb  &
               +(a-n_core_orb-n_act_orb)
       end function address_ia

       integer function address_ta(t,a)
         implicit none
         integer :: t,a
BEGIN_DOC
! address of a couple core-active
END_DOC
         address_ta=offset_ta+(t-n_core_orb-1)*n_virt_orb  &
               +(a-n_core_orb-n_act_orb)
       end function address_ta

 BEGIN_PROVIDER [integer, offset_it]
&BEGIN_PROVIDER [integer, offset_ia]
&BEGIN_PROVIDER [integer, offset_ta]
        implicit none
        offset_it=1
        offset_ia=1+n_core_orb*n_act_orb
        offset_ta=offset_ia+n_core_orb*n_virt_orb
END_PROVIDER

