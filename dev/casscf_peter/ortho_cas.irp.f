! -*- F90 -*-

      subroutine orthogonalize_CAS(orbs)
BEGIN_DOC
! hierarchially orthogonalize a set of orbitals
! 3 classes: core, active, virtual
! 5 steps:  
!    - core with S^(-1/2)
!    - active wrt core 
!    - active with S^(-1/2)
!    - virt wrt to core+act
!    - virt with S^(-1/2)
END_DOC
         implicit none
         real*8 :: orbs(ao_num,mo_num)
         integer :: nc,na,nv,nca,nav

         nc=n_core_orb
         na=n_act_orb
         nv=n_virt_orb
         nav=na+nv
         nca=nc+na

write(6,*) ' Orthogonalization '
write(6,*) ' I   Shalf'
         call shalf(1,nc,orbs)
write(6,*) ' II  SFull'
         call sfull(nc,nav,orbs)
write(6,*) ' III Shalf'
         call shalf(nc+1,nca,orbs)
write(6,*) ' IV  SFull'
         call sfull(nca,nv,orbs)
write(6,*) ' V   Shalf'
         call shalf(nca+1,mo_num,orbs)
write(6,*) '   ... done'

! renormalize all orbitals
 integer :: i,alpha,beta
 real*8 :: ss
    do i=1,mo_num
     ss=0.D0
     do alpha=1,ao_num
      do beta=1,ao_num
       ss+=orbs(alpha,i)*orbs(beta,i)*ao_overlap(alpha,beta)
      end do
     end do
     ss=1.D0/sqrt(ss)
     if (abs(ss-1.d0).gt.1.D-5) write(6,*) ' bad norm ? ',i,1.D0/ss
     do alpha=1,ao_num
      orbs(alpha,i)*=ss
     end do
    end do

    
      end subroutine orthogonalize_CAS

 BEGIN_PROVIDER [real*8, overlap_shalf, (mo_num,mo_num)]
&BEGIN_PROVIDER [real*8, evals_shalf, (mo_num)]
&BEGIN_PROVIDER [real*8, evecs_shalf, (mo_num,mo_num)]
&BEGIN_PROVIDER [real*8, cwrk_shalf, (mo_num,mo_num)]
END_PROVIDER

      subroutine shalf(start_indx,end_indx,orbs)
BEGIN_DOC
! orthogonalize by S^(-1/2) the set of orbitals between start and end
END_DOC
         implicit none
         real*8 :: orbs(ao_num,mo_num),ss
         integer :: alpha,i,j,beta,start_indx,end_indx,norb

         norb=end_indx-start_indx+1
         write(6,*) ' entering Shalf ',start_indx,' - ',end_indx,' = ',norb,' orbitals '

integer :: ii,jj
         do ii=1,norb
          i=start_indx+ii-1
          do jj=1,norb
           j=start_indx+jj-1
           ss=0.D0
!          write(6,*) ' i, j ',i,j
           do alpha=1,ao_num
            do beta=1,ao_num
             ss+=orbs(alpha,i)*orbs(beta,j)*ao_overlap(alpha,beta)
            end do
           end do
           overlap_shalf(ii,jj)=ss
!          write(6,*) ' ii, jj, ss ',ii,jj,ss
          end do
         end do

         call lapack_diag(evals_shalf,evecs_shalf,overlap_shalf,mo_num,norb)
         write(6,*) start_indx,norb
         write(6,*) ' eigenvalues '
         write(6,'(5(i5,F12.6))') (i,evals_shalf(i),i=1,norb)

         if (evals_shalf(1).lt.1.D-12) then
          write(6,*) ' smallest eigenvalue for SHALF is ',evals_shalf(1)
          write(6,*) ' overlap matrix :'
          do i=1,norb
           do j=1,norb
            write(6,*) ' i,j,s ',i,j,overlap_shalf(i,j)
           end do
          end do
          stop ' Linear dependencies in SHALF '
         end if
         do i=1,norb
          do j=1,norb
           cwrk_shalf(j,i)=0.D0
          end do
         end do
! form S^(-1/2)
integer :: k
         do k=1,norb
          ss=1.D0/sqrt(evals_shalf(k))
          do i=1,norb
           do j=1,norb
            cwrk_shalf(i,j)+=evecs_shalf(i,k)*evecs_shalf(j,k)*ss
           end do
          end do
         end do
! transform the orbitals
         do alpha=1,ao_num
          do i=1,norb
           evals_shalf(i)=orbs(alpha,start_indx+i-1)
           orbs(alpha,start_indx+i-1)=0.D0
          end do
          do i=1,norb
           do j=1,norb
            orbs(alpha,start_indx+i-1)+=evals_shalf(j)*cwrk_shalf(i,j)
           end do
          end do
         end do

! test orthogonality
   write(6,*) ' verifying orthogonality'
            
         do ii=1,norb
          i=start_indx+ii-1
          do jj=1,norb
           j=start_indx+jj-1
           ss=0.D0
!          write(6,*) ' i, j ',i,j
           do alpha=1,ao_num
            do beta=1,ao_num
             ss+=orbs(alpha,i)*orbs(beta,j)*ao_overlap(alpha,beta)
            end do
           end do
           if (i.eq.j) then 
            if (abs(ss-1.D0).gt.1.D-5) write(6,*) ' ii, jj, ss ',ii,jj,ss
           else
            if (abs(ss).gt.1.D-5) write(6,*) ' ii, jj, ss ',ii,jj,ss
           end if
          end do
         end do
      write(6,*) ' done '

      end subroutine shalf

      subroutine sfull(nlower,northo,orbs)
BEGIN_DOC
!  projection routine. 
! projects out the first nlower orbitals from the northo next ones,
! sort of generalized Gram-Schmidt
! orbitals are normalized at the end
!
! the procedure may be applied several times for numerical precision
END_DOC
        implicit none
        integer :: nlower,northo
        real*8 :: orbs(ao_num,mo_num)

        integer :: i,alpha,j,beta,iter
        real*8 :: ss
        logical :: lwork

        lwork=.true.
        iter=0

        do while (lwork) 

         do i=nlower+1,nlower+northo
          do j=1,nlower
! calculate overlap
           ss=0.D0
           do alpha=1,ao_num
            do beta=1,ao_num
             ss+=orbs(alpha,i)*orbs(beta,j)*ao_overlap(alpha,beta)
            end do
           end do
!          write(6,*) ' overlap i,j is ',i,j,ss
! subtract overlap * orbital_j from orbital_i
           do alpha=1,ao_num
            orbs(alpha,i)-=ss*orbs(alpha,j)
           end do
! close loop over j
          end do
! normalize the resulting orbital to 1
          ss=0.D0
          do alpha=1,ao_num
           do beta=1,ao_num
            ss+=orbs(alpha,i)*orbs(beta,i)*ao_overlap(alpha,beta)
           end do
          end do
          ss=sqrt(ss)
          if (ss.lt.1.D-12) then
           write(6,*) ' norm of obtained orbital for SFULL is ',ss
           stop ' Linear dependencies detected in SFULL '
          end if
          do alpha=1,ao_num
           orbs(alpha,i)*=1.D0/ss
          end do
! close loop over i
         end do

         iter+=1
! calculate remaining non-orthogonality
real*8 :: remaining_s
         remaining_s=0.D0
         do i=nlower+1,nlower+northo
          do j=1,nlower
! calculate overlap
           ss=0.D0
           do alpha=1,ao_num
            do beta=1,ao_num
             ss+=orbs(alpha,i)*orbs(beta,j)*ao_overlap(alpha,beta)
            end do
           end do
           remaining_s+=ss*ss
          end do
         end do
         remaining_s=sqrt(remaining_s)

         if (remaining_s.lt.1.D-10.or.iter.gt.10) lwork=.false.

        end do

      end subroutine sfull
