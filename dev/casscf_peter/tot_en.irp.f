! -*- F90 -*-
 BEGIN_PROVIDER [real*8, etwo]
&BEGIN_PROVIDER [real*8, eone]
&BEGIN_PROVIDER [real*8, ecore]
       implicit none
       integer :: t,u,v,x,i,ii,tt,uu,vv,xx,j,jj,t3,u3,v3,x3
real*8 :: e_one_all,e_two_all
       ecore    =nuclear_repulsion
       do i=1,n_core_orb
        ii=list_core(i)
        ecore    +=2.D0*one_ints(ii,ii)
        do j=1,n_core_orb
         jj=list_core(j)
         ecore    +=2.D0*bielec_PQxx(ii,ii,j,j)-bielec_PQxx(ii,jj,j,i)
        end do
       end do
       eone    =0.D0
       etwo    =0.D0
       do t=1,n_act_orb
        tt=list_act(t)
        t3=t+n_core_orb
        eone    +=occnum(tt)*one_ints(tt,tt)
        do i=1,n_core_orb
         ii=list_core(i)
         eone    +=occnum(tt)*(2.D0*bielec_PQxx(tt,tt,i,i) &
           -bielec_PQxx(tt,ii,i,t3))
        end do
        do u=1,n_act_orb
         uu=list_act(u)
         u3=u+n_core_orb
         do v=1,n_act_orb
          vv=list_act(v)
          v3=v+n_core_orb
          do x=1,n_act_orb
           xx=list_act(x)
           x3=x+n_core_orb
real*8 :: h1
           h1=bielec_PQxx(tt,uu,v3,x3)
           etwo    +=P0tuvx_no(t,u,v,x)*h1
          end do
         end do
        end do
       end do

       write(6,*) '     sum of all        = ',eone+etwo+ecore
       write(6,*)
       
END_PROVIDER
