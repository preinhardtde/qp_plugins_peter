! -*- F90 -*-
program optorb
  implicit none
  BEGIN_DOC
! calculating an orbital gradient on a wavefunction
  END_DOC
 read_wf = .True.
 touch read_wf
real*8 :: cpu0,wall0
         call cpu_time(cpu0)
         call wall_time(wall0)


         call header
         first_order=.false.
   write(6,*) ' -------- - - - - - - ----------- '
   write(6,*) '     first_order = ',first_order
   write(6,*) ' -------- - - - - - - ----------- '

         call driver
         call cpustamp(cpu0,wall0,cpu0,wall0,'All ')
end program optorb

       subroutine header
         write(6,*) 
         write(6,*) ' the orbital optimization step for the MCSCF '
         write(6,*) 
         write(6,*) ' P. Reinhardt (Paris, feb 2019) '
         write(6,*) 
         write(6,*) 
       end subroutine header

       subroutine driver
         implicit none
         integer :: i,j

         call print_geom


         write(6,*)
         write(6,*) '   creating new orbitals '
         do i=1,mo_num
          write(6,*) ' Orbital No ',i
          write(6,'(5F14.6)') (NewOrbs(j,i),j=1,mo_num)
          write(6,*)
         end do

         mo_label = "Natural"
         do i=1,mo_num
          do j=1,ao_num
           mo_coef(j,i)=NewOrbs(j,i)
          end do
         end do
         call save_mos
 
!        if (norm_grad.gt.1.D-7) then
         if (abs(energy_improvement).gt.1.D-10) then
          casscf_converged=.false.
         else
          casscf_converged=.true.
         end if

         write(6,*)
         write(6,*) ' <0|H|0>               = ',ecore+eone+etwo
         write(6,*) ' energy improvement    = ',energy_improvement
         write(6,*) ' new energy            = ',ecore+eone+etwo+energy_improvement
         write(6,*)
         write(6,*) ' norm orbital gradient = ',norm_grad
         write(6,*)
         write(6,*)
         write(6,*) ' orbital gradient converged ? ',casscf_converged
         write(6,*)
         write(6,*) '   ... all done '

       end subroutine driver
