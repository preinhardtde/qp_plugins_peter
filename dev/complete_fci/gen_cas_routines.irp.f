! -* F90 -*-
  use bitmasks ! you need to include the bitmasks_module.f90 features

BEGIN_PROVIDER [logical, need_to_generate_CAS]
   implicit none

!  write(6,*)  ' III n_det, n_det_FullCAS ',n_det,n_det_FullCAS

   if (n_det.eq.n_det_FullCAS) then
     need_to_generate_CAS=.false.
   else 
     need_to_generate_CAS=.true.
     n_det=n_det_FullCAS
   end if

END_PROVIDER
 
BEGIN_PROVIDER [integer, N_det_FullCAS ]
&BEGIN_PROVIDER [integer, N_alphastrings_FullCAS ]
&BEGIN_PROVIDER [integer, N_betastrings_FullCAS ]
BEGIN_DOC
! generating a complete CAS
! number of determinants = [(N-n)!/N!/n!]^2
END_DOC
   implicit none
   integer :: i,ii

real*8 dntmp
! alpha electrons
   dntmp=1.D0
   if (elec_alpha_num.gt.n_core_orb) then
    do i=1,elec_alpha_num-n_core_orb
     dntmp*=dble((n_act_orb-i+1))/dble(i)
     if (dntmp.gt.10000) then
      write(6,*) ' more than 10000 alpha strings '
      write(6,*) ' active space too large, please reduce '
      stop ' explosion of CAS space '
     end if
    end do
   end if
   N_alphastrings_FullCAS=Nint(dntmp)
   write(6,*) ' Number of alpha strings in the Full CAS: N_det_FullCAS = ',N_alphastrings_FullCAS

! beta electrons
   dntmp=1.D0
   if (elec_beta_num.gt.n_core_orb) then
    do i=1,elec_beta_num-n_core_orb
     dntmp*=dble((n_act_orb-i+1))/dble(i)
     if (dntmp.gt.10000) then
      write(6,*) ' more than 10000 beta strings '
      write(6,*) ' active space too large, please reduce '
      stop ' explosion of CAS space '
     end if
    end do
   end if
   N_betastrings_FullCAS=Nint(dntmp)
   write(6,*) ' Number of beta  strings in the Full CAS: N_det_FullCAS = ',N_betastrings_FullCAS

   N_det_FullCAS=N_betastrings_FullCAS*N_alphastrings_FullCAS

   write(6,*) ' Number of dets in the Full CAS: N_det_FullCAS = ',N_det_FullCAS
   if (N_det_FullCAS.eq.0) then 
    write(6,*) ' strange .... n_det=0  '
    stop '  strange .... n_det=0  '
   end if
    
END_PROVIDER

     subroutine gen_cas
BEGIN_DOC
!
! we create a CAS and insert it into the wavefunction psi
! place for the determinants is already created
END_DOC

       implicit none
       integer :: i,j,k,idet,jdet,indx
       integer(bit_kind), allocatable :: det_tmp_alpha(:)
       integer(bit_kind), allocatable :: det_tmp_beta(:)
       integer, allocatable :: occ_alpha(:)
       integer, allocatable :: occ_beta(:)
       allocate(det_tmp_alpha(N_int))
       allocate(det_tmp_beta(N_int))
       allocate(occ_alpha(elec_alpha_num))
       allocate(occ_beta(elec_beta_num))

!      n_states=1
       n_det=n_det_FullCAS

        psi_det=0_bit_kind
! HF is not the right determinant
!       do i=1,N_int
!         psi_det(i,1,1) = HF_bitmask(i,1)
!         psi_det(i,2,1) = HF_bitmask(i,2)
!         det_tmp_alpha(i) = HF_bitmask(i,1)
!         det_tmp_beta(i)  = HF_bitmask(i,2)
!       enddo
       do i=1,n_core_orb
        occ_alpha(i)=list_core(i)
        occ_beta(i)=list_core(i)
       end do
       do i=1,elec_alpha_num-n_core_orb
        occ_alpha(n_core_orb+i)=list_act(i)
       end do
       do i=1,elec_beta_num-n_core_orb
        occ_beta(n_core_orb+i)=list_act(i)
       end do
       call list_to_bitstring( psi_det(1,1,1), occ_alpha, elec_alpha_num, N_int)
       call list_to_bitstring( psi_det(1,2,1), occ_beta, elec_beta_num, N_int)
       call list_to_bitstring( det_tmp_alpha, occ_alpha, elec_alpha_num, N_int)
       call list_to_bitstring( det_tmp_beta, occ_beta, elec_beta_num, N_int)




       write(6,*) ' Primary alpha determinant '
       call print_spindet(det_tmp_alpha,n_int)
       write(6,*) ' Primary beta determinant '
       call print_spindet(det_tmp_beta,n_int)
!
! we have to generate the bitstring and dump it into the list at the right place
!
        do idet=2,N_alphastrings_FullCAS
         call countup_FullCAS(det_tmp_alpha,elec_alpha_num)
         call print_spindet(det_tmp_alpha,n_int)
!        call print_det(1,n_int,det_tmp_alpha)
!        write(6,*) ' generated string No ',idet
         do i=1,N_int
          psi_det(i,1,idet) = det_tmp_alpha(i)
!         psi_det(i,2,idet) = HF_bitmask(i,2)
          psi_det(i,2,idet) = det_tmp_beta(i) 
         end do
        end do
        write(6,*) ' generated alpha strings '

!
! we have to count through the beta strings
!
        indx=N_alphastrings_FullCAS
        do jdet=2,N_betastrings_FullCAS
         call countup_FullCAS(det_tmp_beta,elec_beta_num)
         call print_spindet(det_tmp_beta,n_int)
         do idet=1,N_alphastrings_FullCAS
          indx+=1
          do i=1,N_int
           psi_det(i,1,indx) = psi_det(i,1,indx-N_alphastrings_FullCAS)
           psi_det(i,2,indx) = det_tmp_beta(i)
          end do
         end do
        end do
        write(6,*) ' generated beta strings '

        write(6,*) ' filled the list of determinants ',indx,n_det,n_det_FullCAS
       deallocate(det_tmp_alpha,det_tmp_beta,occ_alpha,occ_beta)
        end subroutine gen_cas

        subroutine countup_FullCAS(det,elec_x_num)
           implicit none 
           integer(bit_kind) :: det(N_int)
           integer :: elec_x_num,occ(elec_x_num),i,ii,iii,i4,j
           logical :: found_empty
BEGIN_DOC
!
! from a given string we find the next one by exchanging a 1 with a zero 
! to the rhs. Only active orbitals are considered.
!
! 1 find the first empty active orbital
! 2 are there occ active orbitals below ? 
! 3 if yes, exchange and return the generated string.
! 4 if no, find the next higher empty active orbital, go to 2
!
END_DOC

! extract the indices of the occupied orbitals
         call bitstring_to_list( det, occ, elec_x_num, N_int)
         do i=1,n_act_orb
          ii=list_act(i)
          found_empty=.true.
          do j=1,elec_x_num
           if (occ(j).eq.ii) found_empty=.false.
          end do
          if (found_empty) then
! a candidate
! someone occupied to the lhs ? 
           do iii=i-1,1,-1
            i4=list_act(iii)
            do j=1,elec_x_num
             if (occ(j).eq.i4) then
! place ii in occ(j), encode the new list, and return
!             write(6,*) ' exchanged hole ',ii,' with particle ',i4
              occ(j)=ii
! migrate all lower occupied to the lowest possible places
integer :: i5,ii5,i6,ii6,jj
logical :: lexch
              do i5=1,i-2
               ii5=list_act(i5)
               found_empty=.true.
               do jj=1,elec_x_num
                if (ii5.eq.occ(jj)) found_empty=.false.
               end do
               if (found_empty) then
!               write(6,*) ' place No ',ii5,' is empty, trying to fill it'
! find a particle to exchange with
                lexch=.false.
                do i6=i5+1,i-1
                 if (.not.lexch) then
                  ii6=list_act(i6)
                  do jj=1,elec_x_num
                   if (ii6.eq.occ(jj)) then
                    occ(jj)=ii5
!                   write(6,*) ii6,' goes to place ',ii5
                    lexch=.true.
                   end if
                  end do
                 end if
                end do
               end if
              end do
                
              call list_to_bitstring( det, occ, elec_x_num, N_int)
              return
             end if
            end do
           end do
          end if
         end do
! if we are here, there is no hole to use for counting. 
! we have to return
        end subroutine countup

