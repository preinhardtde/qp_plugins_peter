! -*- F90 -*-
subroutine select_Nstates_Sz
BEGIN_DOC
!
! we fill psi(:,N_states) with CSFs of the right Sz
!
END_DOC
          implicit none
BEGIN_DOC
! we run through all determinants of the generated CAS
! and take the spin part with the right S.
! S can be between S_z and (1/2)'(n_alpha + n_beta) 
! where n_alpha, n_beta are the active electrons
! the N_states lowest H_ii of the right spin form our new 
! MC wavefunctions
END_DOC
           integer :: idet,jdet,i,j,ss
           real*8 :: ss1,smax

           integer(bit_kind) :: det_mu(N_int,2)
           integer(bit_kind) :: det_nu(N_int,2)
           integer :: istate,ivec
           real*8 :: sss
           real*8 :: s_compare

           real*8, allocatable :: e_select_tmp(:)
           allocate (e_select_tmp(n_states))
           integer(bit_kind), allocatable :: keys_tmp(:,:,:)
           allocate (keys_tmp(N_int,2,n_det_FullCAS))

           real*8, allocatable :: coeff_cas(:),coeff_S2(:)
           allocate (coeff_cas(n_det_FullCAS),coeff_S2(n_det_FullCAS))

           logical :: high_spin,is_S2_eigenfunction,no_need_for_this_one
           integer :: a_before_b
           logical :: rumer_construction
           integer :: n_agrafes,n_copies
           integer, allocatable :: agrafes(:,:),sign_copies(:)
           character*1, allocatable :: spin_copies(:,:) 
           integer :: indx, indx_max
           real*8 :: e_0,s_0

           do idet=1,n_det_FullCAS
            call det_extract(det_mu,idet,n_int)
            do i=1,2
             do j=1,n_int
              keys_tmp(j,i,idet)=det_mu(j,i)
             end do
            end do
           end do

           rumer_construction=.true.

           Smax=(elec_alpha_num+elec_beta_num-2*n_core_orb)*0.5D0
!          write(6,*) ' Sz= ',S_z
!          write(6,*) ' Smax= ',(elec_alpha_num+elec_beta_num-2*n_core_orb)*0.5D0

           s_compare=1.D0/(S_z*(S_z+1.D0))
           e_select_tmp=1.D10
           psi_coef=0.D0
           istate=0
           do idet=1,n_det_FullCAS
            call det_extract(det_mu,idet,n_int)
!            call print_det(det_mu,n_int)
            call bitstring_to_csfstring(det_mu,n_int,csf_string)
!           write(6,*) idet,csf_string
            high_spin=.true.
            no_need_for_this_one=.false.
            a_before_b=0
            do i=1,n_act_orb
             if (.not.no_need_for_this_one) then
              if (csf_string(i).eq.'a') then
               a_before_b+=1
              end if
              if (csf_string(i).eq.'b') then
               high_spin=.false.
               a_before_b-=1
              end if
              if (a_before_b.lt.0) no_need_for_this_one=.true.
             end if
            end do
!           write(6,*) ' a_before_b = ',a_before_b
            if (no_need_for_this_one) then 
!            write(6,*) ' determinant not needed '
             continue
            else
             coeff_cas=0.D0
             if (high_spin) then
!             write(6,*) ' high spin, no need for for projection'
              coeff_cas(idet)=1.D0
             else
              if (rumer_construction) then
               n_agrafes=0
               n_copies=1
               do i=1,n_act_orb
                if (csf_string(i).eq.'b') then
                 n_agrafes+=1
                 n_copies*=2
                end if
               end do
!              write(6,*) ' n_agrafes, n_copies = ',n_agrafes, n_copies
               allocate (agrafes(2,n_agrafes))
               allocate (spin_copies(n_act_orb,n_copies))
               allocate (sign_copies(n_copies))
! the tricky thing, how to find the spin pairing
               agrafes=0
               indx=0
               indx_max=0
               do i=1,n_act_orb
                if (csf_string(i).eq.'a') then
                 if (indx_max.lt.n_agrafes) then
! we open a new spin pair
                  do while (agrafes(1,indx+1).ne.0)
                   indx+=1
                   indx_max=max(indx,indx_max)
                  end do
                  indx+=1
                  indx_max=max(indx,indx_max)
                  agrafes(1,indx)=i
!                  write(6,*) ' we open at level ',indx,' at position ',i
                 end if
                end if
                if (csf_string(i).eq.'b') then
! close a spin pair
                 agrafes(2,indx)=i
!                 write(6,*) ' we close at level ',indx,' at position ',i
                 do while (agrafes(2,indx-1).ne.0)
                  indx-=1
                 end do
                 indx-=1
                end if
               end do

!               write(6,*) ' agrafes = ',(agrafes(1,i),agrafes(2,i),i=1,n_agrafes)
               sign_copies=1
               do i=1,n_copies
                do j=1,n_act_orb
                 spin_copies(j,i)=csf_string(j)
                end do
               end do
               integer :: indx_incr,indx_start,indx_a,indx_b,k
               integer :: start
               indx_start=1
               indx_incr=2
               do k=1,n_agrafes
                indx_a=agrafes(1,k)
                indx_b=agrafes(2,k)
                do start=indx_start,n_copies,indx_incr
                 do i=1,indx_start
                  sign_copies(start+i)*=-1
                  spin_copies(indx_a,start+i)='b'
                  spin_copies(indx_b,start+i)='a'
                 end do
                end do
                indx_start*=2
                indx_incr*=2
               end do

               integer :: indx_csf_string
               do i=1,n_copies
                indx=indx_csf_string(spin_copies(1,i),keys_tmp)
!                write(6,*) ' i, sign, string = ',indx,sign_copies(i)  &
!                     ,(spin_copies(j,i),j=1,n_act_orb)
                if (indx.le.0) then
!                write(6,*) ' string ',(spin_copies(j,i),j=1,n_act_orb) &
!                     ,' is not in our list of determinants '
                 stop
                end if
                coeff_cas(indx)=dble(sign_copies(i))
               end do
               deallocate (agrafes,spin_copies,sign_copies)
              else             
! construct the projector S^2-S(S+1)
               coeff_cas(idet)=1.D0
               is_S2_eigenfunction=.false.
               do ss=nint(2*(S_z+1)),nint(2*Smax),2
                if (.not.is_s2_eigenfunction) then 
!              write(6,*) ' projecting out the component with S = ',SS*0.5D0
                 call S2_u_0(coeff_s2,coeff_cas,n_det_FullCAS,keys_tmp,N_int)
                 is_S2_eigenfunction=.true.
!               write(6,*) ' S2 of the moment '
                 do jdet=1,n_det_FullCAS
                  if ((coeff_s2(jdet).ne.0.D0).and.    &
                       (abs(coeff_cas(jdet)/coeff_s2(jdet) &
                       -S_compare).gt.1.D-6)) then
                   is_S2_eigenfunction=.false.
                  end if
!                if (coeff_s2(jdet).ne.0.D0) then
!                call bitstring_to_csfstring(keys_tmp(1,1,jdet),n_int,csf_string)
!                 write(6,*) jdet,coeff_s2(jdet),csf_string,coeff_cas(jdet)/coeff_s2(jdet)-S_compare
!                end if
                 end do
! S(S+1)     
                 if (.not.is_S2_eigenfunction) then
!                  write(6,*) ' projecting out the component with S = ',SS*0.5D0
                  ss1=ss*(ss+2)*0.25D0
                  do jdet=1,n_det_FullCAS
                   coeff_s2(jdet)-=ss1*coeff_cas(jdet)
                  end do
                  coeff_cas=coeff_s2
                  real*8 :: nrm_csf
                  nrm_csf=0.D0
                  do jdet=1,n_det_FullCAS
                   nrm_csf+=coeff_cas(jdet)*coeff_cas(jdet)
                  end do
                  nrm_csf=1.D0/sqrt(nrm_csf)
                  do jdet=1,n_det_FullCAS
                   if (abs(coeff_cas(jdet)).gt.1.D-6) then
                    coeff_cas(jdet)*=nrm_csf
                   end if
                  end do
                 end if
                end if
               end do
              end if
             end if
! did we create a new state ?
! we Schmidt orthogonalize to all previous
             do ivec=1,istate
              sss=0.D0
              do jdet=1,n_det_FullCAS
!             write(6,*) ' istate, idet, jdet, ivec =',istate, idet, jdet, ivec
               sss+=psi_coef(jdet,ivec)*coeff_cas(jdet)
              end do
              do jdet=1,n_det_FullCAS
               coeff_CAS(jdet)-=sss*psi_coef(jdet,ivec)
              end do
             end do
! the remaining vector has a non-zero length ?
             sss=0.D0
             do jdet=1,n_det_FullCAS
              sss+=coeff_CAS(jdet)*coeff_CAS(jdet)
             end do
             sss=sqrt(sss)
             if (sss.gt.1.D-6) then
              do jdet=1,n_det_FullCAS
               coeff_CAS(jdet)*=1.D0/sss
              end do
! use i_h_j instead
!!$              call u_0_H_u_0(e_0,s_0,coeff_cas,n_det_FullCAS   &
!!$                   ,keys_tmp,n_int,1,n_det_FullCAS)
              e_0=ham_diag(idet)       
              if (e_select_tmp(n_states).gt.e_0) then
! we put the vector in the last place
               istate+=1
               if (istate.gt.n_states) then
!                write(6,*) ' istate goes too far ',istate
                istate=n_states
               end if
!               write(6,*) ' this seems a good candidate ',e_0
               do i=1,n_det_FullCAS
                psi_coef(i,n_states)=coeff_cas(i)
               end do
               e_select_tmp(n_states)=e_0
!              else
!               write(6,*) ' energy ',e_0,' not interesting '
              end if
! we bubble down
              i=n_states
              do while (i.gt.1.and.e_select_tmp(i-1).gt.e_select_tmp(i))
               i-=1
               e_0=e_select_tmp(i)
               e_select_tmp(i)=e_select_tmp(i+1)
               e_select_tmp(i+1)=e_0
               do jdet=1,n_det_FullCAS
                e_0=psi_coef(jdet,i)
                psi_coef(jdet,i)=psi_coef(jdet,i+1)
                psi_coef(jdet,i+1)=e_0
               end do
              end do
!            else
!             write(6,*) ' this one we had already '
             end if
!             write(6,9901) 1.D0/s_compare
!9901         format(' Eigenfunction with S(S+1) = ',F4.1)
!             do jdet=1,n_det_FullCAS
!              if (abs(coeff_cas(jdet)).gt.1.D-6) then
!               call bitstring_to_csfstring(keys_tmp(1,1,jdet),n_int,csf_string)
!              write(6,9233) jdet,coeff_cas(jdet),coeff_cas(jdet)*coeff_cas(jdet),csf_string
!9233           format(I5,F12.6,' x  = sqrt(',f12.6,') ',20A1 ) 
!              end if
!             end do
            end if ! closes if (no_need ...)
           end do

           if (istate.lt.N_states) then
            write(6,*) ' you demanded ',N_states,' states '
            write(6,*) ' but I found only ',istate,' states of the right spin '
            stop ' not enough states of the right Sz available '
           end if
           deallocate (keys_tmp)
           deallocate (coeff_cas,coeff_S2)
           
         end subroutine select_Nstates_Sz

         integer function indx_csf_string(csf,key_tmp)
           use bitmasks
           implicit none
           character*1 :: csf(n_act_orb)
           integer(bit_kind) :: det_mu(N_int,2),key_tmp(n_int,2,n_det_FullCAS)
           integer :: occ_list(mo_num),i,j,k
           logical :: lfound
           det_mu=0_bit_kind
           
           occ_list=0
           do i=1,n_core_orb
            occ_list(i)=list_core(i)
           end do
           i=n_core_orb
           do j=1,n_act_orb
            if (csf(j).eq.'2'.or.csf(j).eq.'a') then
             i+=1
             occ_list(i)=list_act(j)
            end if
           end do
           call list_to_bitstring(det_mu(1,1),occ_list,i,n_int)
           
           occ_list=0
           do i=1,n_core_orb
            occ_list(i)=list_core(i)
           end do
           i=n_core_orb
           do j=1,n_act_orb
            if (csf(j).eq.'2'.or.csf(j).eq.'b') then
             i+=1
             occ_list(i)=list_act(j)
            end if
           end do
           call list_to_bitstring(det_mu(1,2),occ_list,i,n_int)
!           write(6,*) ' transformed csf string ',csf 
!           call print_det(det_mu,n_int)
           do i=1,n_det_FullCAS
            if (.not.lfound) then
             lfound=.true.
             do j=1,n_int
              do k=1,2
               if (det_mu(j,k).ne.key_tmp(j,k,i)) lfound=.false.
              end do
             end do
             if (lfound) then
              indx_csf_string=i
              return
             end if
            end if
           end do

           if (.not.lfound) then
            indx_csf_string=0
            return
           end if
           
         end function indx_csf_string
