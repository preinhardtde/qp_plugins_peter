! -*- F90 -*-
program make_csf
 read_wf = .True.
 touch read_wf
real*8 :: cpu0,wall0
         call cpu_time(cpu0)
         call wall_time(wall0)

         call header
         call setup
         call driver
         call cpustamp(cpu0,wall0,cpu0,wall0,'All ')
end program make_csf

subroutine header
          write(6,*)
          write(6,*) ' transform a CAS from '
          write(6,*) ' determinants to Configuration State Functions '
          write(6,*) ' we use the RRumer string etchnique here '
          write(6,*) ' P Reinhardt (Paris, janv 2020) '
          write(6,*)
end subroutine header

subroutine setup
          implicit none

integer :: possible_Sz,Smin,Smax,Sk,i,j

! do we have any orbitals defined ?
         if (n_core_orb+n_act_orb.eq.0) then
          write(6,*) ' you did not specify core and/or active orbitals '
          write(6,*) ' I found n_core_orb = ',n_core_orb,' and n_act_orb = ',n_act_orb
          stop ' II please define orbital classes with qp_set_mo_class'
         else
          write(6,*) ' n_core_orb = ',n_core_orb
          write(6,*) ' n_act_orb  = ',n_act_orb
         end if
! can we hold the cas ?
  ! Update array sizes
         if (psi_det_size < N_det_FullCAS) then
           psi_det_size = N_det_FullCAS
           TOUCH psi_det_size
         endif

         write(6,*) ' N_states = ',N_states,N_det
         if (n_states.gt.n_det_FUllCAS) then
          write(6,*) ' you asked for more states than determinants, impossible '
          stop ' too many states demanded '
         end if
!
! as we do a Full CI we do not need the wavefunction as input
!        read_wf = .True.
!        touch read_wf
!        write(6,*) ' II N_states = ',N_states, N_det
         if (need_to_generate_CAS) then
          write(6,*) '  we will generate the CAS '
          call gen_cas
         end if

         Smin=nint(2.D0*S_z)
         Smax=elec_alpha_num+elec_beta_num-2*n_core_orb
         Smax=min(2*n_act_orb-smax,smax)
         write(6,*) ' active alpha electrons ',elec_alpha_num-n_core_orb
         write(6,*) ' active beta  electrons ',elec_beta_num-n_core_orb
         write(6,*)

         if (Smax.eq.Smin) then
          write(6,*) ' there is only one possible spin, nothing to do here '
          write(6,*) ' no need for a MCSCF calculation, RHF is sufficient '   
          write(6,*) ' any high-spin determinant is a S2 eigenfunction '   
          stop ' Nothing to do '
         else
         write(6,9901) 0.5D0*dble(Smin),0.5D0*dble(Smax )
  9901 format(' We may have a spin between S = ',F4.1,' and S= ',F4.1)
         end if

       integer(bit_kind), allocatable :: det_mu(:,:)
       integer(bit_kind), allocatable :: det_nu(:,:)
       allocate(det_mu(N_int,2))
       allocate(det_nu(N_int,2))

integer :: idet, jdet
real*8 :: s2,hij

!      do idet=1,n_det_FullCAS
!       call det_extract(det_mu,idet,n_int)
!       call print_act_det(det_mu,N_int)
!       do jdet=1,n_det_FullCAS
!        call det_extract(det_nu,jdet,n_int)
!        call i_H_j_s2(det_mu,det_nu,N_int,hij,s2)
!        if (s2.ne.0.D0) then
!         write(6,*) ' i, j, H_ij, s2_ij ',idet, jdet, Hij, s2
!        end if
!       end do
!      end do


       do idet=1,n_det_FullCAS
        call det_extract(det_mu,idet,n_int)
        call bitstring_to_csfstring(det_mu,n_int,csf_string)
        write(6,*) idet,' det is ',csf_string
        call print_det(det_mu,n_int)
       end do

       deallocate(det_nu,det_mu)

end subroutine setup

subroutine driver
          use bitmasks
          implicit none
BEGIN_DOC
! we run through all determinants of the generated CAS
! and take the spin part with the right S.
! S can be between S_z and (1/2)'(n_alpha + n_beta) 
! where n_alpha, n_beta are the active electrons
! the N_states lowest H_ii of the right spin form our new 
! MC wavefunctions
END_DOC
           integer :: idet,jdet,i,j,ss
           real*8 :: ss1,smax
           integer(bit_kind) :: det_mu(N_int,2)
           integer(bit_kind) :: det_nu(N_int,2)
           integer(bit_kind) :: keys_tmp(N_int,2,n_det_FullCAS)
           real*8 :: coeff_cas(n_det_FullCAS),coeff_S2(n_det_FullCAS)
           integer, allocatable :: indx_S2_packed(:,:)
           real*8,  allocatable :: coeff_S2_packed(:,:)
           integer :: len_S2(n_det_FullCAS)

           logical :: high_spin,is_S2_eigenfunction,no_need_for_this_one
           integer :: a_before_b
           logical :: rumer_construction
           integer :: n_agrafes,n_copies
           integer, allocatable :: agrafes(:,:),sign_copies(:)
           character*1, allocatable :: spin_copies(:,:) 
           integer :: indx, indx_max,len_max
           real*8 :: cpu0,wall0,sss,s_compare

           do idet=1,n_det_FullCAS
            call det_extract(det_mu,idet,n_int)
            do i=1,2
             do j=1,n_int
              keys_tmp(j,i,idet)=det_mu(j,i)
             end do
            end do
           end do

           len_max=0
           len_S2=0
           s_compare=1.D0/(s_z*(s_z+1.D0))

           Smax=(elec_alpha_num+elec_beta_num-2*n_core_orb)*0.5D0
           write(6,*) ' Sz= ',S_z
           write(6,*) ' Smax= ',(elec_alpha_num+elec_beta_num    &
                -2*n_core_orb)*0.5D0

!!$! longest expansion may be (n_alpha+n_beta)!/(n_alpha!)/(n_beta!)
!!$! n_act_orb, S_z
           integer :: nov,nam,nbm
           real*8 :: fact
           nov=max(0,nint(2.D0*SMax)-n_act_orb)
           nam=elec_alpha_num-n_core_orb-nov
           nbm=elec_beta_num-n_core_orb-nov
           len_max=fact(nam+nbm)/fact(nam)/fact(nbm)
           write(6,*) ' nov, nam, nbm, len_max ',nov, nam, nbm, len_max
           allocate (coeff_S2_packed(len_max,n_det_FullCAS))
           allocate (indx_S2_packed(len_max,n_det_FullCAS))
           integer :: num_csf
           len_max=0
           num_csf=0
           do idet=1,n_det_FullCAS
            call det_extract(det_mu,idet,n_int)
            call print_det(det_mu,n_int)
            call bitstring_to_csfstring(det_mu,n_int,csf_string)
            no_need_for_this_one=.false.
            a_before_b=0
            do i=1,n_act_orb
             if (.not.no_need_for_this_one) then
              if (csf_string(i).eq.'a') then
               a_before_b+=1
              end if
              if (csf_string(i).eq.'b') then
               high_spin=.false.
               a_before_b-=1
              end if
              if (a_before_b.lt.0) no_need_for_this_one=.true.
             end if
            end do
!            write(6,*) ' a_before_b = ',a_before_b
            if (no_need_for_this_one) then 
             write(6,*) ' determinant not needed '
            else

            high_spin=.true.
            do i=1,n_act_orb
             if (csf_string(i).eq.'b') high_spin=.false.
            end do
            coeff_cas=0.D0
            coeff_cas(idet)=1.D0
            if (high_spin) then
             coeff_cas(idet)=1.D0
!             write(6,*) ' high spin, no need for for projections'
            else
             n_agrafes=0
             n_copies=1
             do i=1,n_act_orb
              if (csf_string(i).eq.'b') then
               n_agrafes+=1
               n_copies*=2
              end if
             end do
!             write(6,*) ' n_agrafes, n_copies = ',n_agrafes, n_copies
             allocate (agrafes(2,n_agrafes))
             allocate (spin_copies(n_act_orb,n_copies))
             allocate (sign_copies(n_copies))
! the tricky thing, how to find the spin pairing
             agrafes=0
             indx=0
             indx_max=0
             do i=1,n_act_orb
              if (csf_string(i).eq.'a') then
               if (indx_max.lt.n_agrafes) then
! we open a new spin pair
                do while (agrafes(1,indx+1).ne.0)
                 indx+=1
                 indx_max=max(indx,indx_max)
                end do
                indx+=1
                indx_max=max(indx,indx_max)
                agrafes(1,indx)=i
!                write(6,*) ' we open at level ',indx,' at position ',i
               end if
              end if
              if (csf_string(i).eq.'b') then
! close a spin pair
               agrafes(2,indx)=i
!               write(6,*) ' we close at level ',indx,' at position ',i
               do while (agrafes(2,indx-1).ne.0)
                indx-=1
               end do
               indx-=1
              end if
             end do
             
!             write(6,*) ' agrafes = ',(agrafes(1,i),agrafes(2,i),i=1,n_agrafes)
             sign_copies=1
             do i=1,n_copies
              do j=1,n_act_orb
               spin_copies(j,i)=csf_string(j)
              end do
             end do
             integer :: indx_incr,indx_start,indx_a,indx_b,k
             integer :: start
             indx_start=1
             indx_incr=2
             do k=1,n_agrafes
              indx_a=agrafes(1,k)
              indx_b=agrafes(2,k)
              do start=indx_start,n_copies,indx_incr
               do i=1,indx_start
                sign_copies(start+i)*=-1
                spin_copies(indx_a,start+i)='b'
                spin_copies(indx_b,start+i)='a'
               end do
              end do
              indx_start*=2
              indx_incr*=2
             end do
             
             integer :: indx_csf_string
             do i=1,n_copies
              indx=indx_csf_string(spin_copies(1,i),keys_tmp)
!              write(6,*) ' i, sign, string = ',indx,sign_copies(i)  &
!                   ,(spin_copies(j,i),j=1,n_act_orb)
              if (indx.le.0) then
               write(6,*) ' string ',(spin_copies(j,i),j=1,n_act_orb) &
                    ,' is not in our list of determinants '
               stop
              end if
              coeff_cas(indx)=dble(sign_copies(i))
             end do
             deallocate (agrafes,spin_copies,sign_copies)
            end if
! we orthogonalize with Schmidt wrt to the rest
            do jdet=1,idet-1
             integer :: kdet,indxx
             sss=0.D0
             do k=1,len_S2(jdet)
              indxx=indx_S2_packed(k,jdet)
              if (indxx.ne.0) then
!!$               write(6,*) ' jdet, indxx ',jdet,indxx,coeff_cas(indxx),coeff_S2_packed(k,jdet)
               if (coeff_cas(indxx).ne.0.D0) then
                sss=sss+coeff_cas(indxx)*coeff_S2_packed(k,jdet)
               end if
              end if
             end do
             if (abs(sss).gt.1.D-12) then
!!$              write(6,*) ' overlap between ',idet,' and ',jdet,' is ',sss
!!$              write(6,*) ' len_S2(jdet) = ',len_S2(jdet)
              do k=1,len_S2(jdet)
               indxx=indx_S2_packed(k,jdet)
               if (indxx.ne.0) then
                coeff_cas(indxx)-=sss*coeff_S2_packed(k,jdet)
               end if
              end do
             end if
            end do
! normalize the resulting vector
            sss=0.D0
            do jdet=1,n_det_FullCAS
             sss+=coeff_cas(jdet)*coeff_cas(jdet)
            end do
            if (abs(sss).ge.1.D-6) then
             num_csf+=1
             len_S2(num_csf)=0
! ok, we save the vector
             sss=1.D0/sqrt(sss)
             write(6,9901) num_csf,1.D0/s_compare
9901         format(' Eigenfunction No ',I6,' with S(S+1) = ',F4.1)
             
             do jdet=1,n_det_FullCAS
              if (abs(coeff_cas(jdet)).gt.1.D-12) then
               len_S2(num_csf)+=1
               coeff_cas(jdet)*=sss
               coeff_S2_packed(len_S2(num_csf),num_csf)=coeff_cas(jdet)
               indx_S2_packed(len_S2(num_csf),num_csf)=jdet
               call bitstring_to_csfstring(keys_tmp(1,1,jdet),n_int,csf_string)
               write(6,*) jdet,coeff_cas(jdet),csf_string      &
                    ,coeff_cas(jdet)*coeff_cas(jdet) 
              end if
             end do
             len_max=max(len_s2(num_csf),len_max)
            else
             write(6,*) ' this one we had already '
            end if
9233        format(I5,F12.6,' x ',A,' = sqrt(',f12.6,' )' )
           end if
          end do
          write(6,*)
          
          call cpustamp(cpu0,wall0,cpu0,wall0,'CSF ')
          write(6,*)
          write(6,*) ' finished, we found ',num_csf,' independent CSFs'
          write(6,*) ' longest expansion runs over ',len_max,' determinants '
          write(6,*)
          
        end subroutine driver

