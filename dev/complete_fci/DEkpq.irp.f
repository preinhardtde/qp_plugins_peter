 BEGIN_PROVIDER [real*8, DKpq, (n_det,n_act_orb,n_act_orb) ]
&BEGIN_PROVIDER [real*8, EKpq, (n_det,n_act_orb,n_act_orb) ]
      implicit none
      integer :: i,p,q
      do p=1,n_act_orb
       do q=1,n_act_orb
        do i=1,n_det
         DKpq(i,q,p)=0.D0
         EKpq(i,q,p)=0.D0
        end do
       end do
      end do
END_PROVIDER

      subroutine Fill_Dkpq
        implicit none
        integer :: p,q,mu,kappa,istate
        real*8 :: aaa
! D^kappa_pq = sum_mu <kappa|E_pq|mu> c_mu
        do p=1,n_act_orb
         do q=1,n_act_orb
          do kappa=1,n_det
           DKpq(kappa,p,q)=0.D0
           mu=Aindx_mpq(kappa,q,p,1)
           if (mu.ne.-1) then
            aaa=Aval_mpq(kappa,q,p,1)
            DKpq(kappa,p,q)+=aaa*psi_coef(mu,1)
!           DKpq(kappa,p,q)+=aaa*vect(mu)
            if (secnd_Aval_mpq(kappa,q,p)) then
             mu=Aindx_mpq(kappa,q,p,2)
             aaa=Aval_mpq(kappa,q,p,2)
             DKpq(kappa,p,q)+=aaa*psi_coef(mu,1)
!            DKpq(kappa,p,q)+=aaa*vect(mu)
!            DKpq(kappa,q,p)+=aaa*psi_coef(mu,1)
            end if
           end if
          end do
         end do
        end do
!       open(unit=12,file='Dkpq.dat',form='formatted',status='unknown')
!       do kappa=1,n_det
!        do p=1,n_act_orb
!         do q=1,n_act_orb
!          write(12,*) ' Dkpq: k p q Dkpq ',kappa,p,q,DKpq(kappa,p,q)
!         end do
!        end do
!       end do
!       close(12)

      end subroutine Fill_Dkpq
 
      subroutine Fill_Ekpq
        implicit none
        integer :: p,q,kappa,r,s,ss,istate
        call Fill_Dkpq

       call dgemm('N','T',n_det, n_act_orb*n_act_orb, n_act_orb*n_act_orb, 1.d0, &
               Dkpq, size(Dkpq,1), bielecCI_act, n_act_orb*n_act_orb, &
               0.d0, Ekpq, size(Ekpq,1))

!       do kappa=1,n_det
!        do p=1,n_act_orb
!         do q=1,n_act_orb
!          Ekpq(kappa,p,q)=0.D0
!          do r=1,n_act_orb
!           do s=1,n_act_orb
!            Ekpq(kappa,p,q)+=bielecCI_act(p,q,r,s)*Dkpq(kappa,r,s)
!           end do
!          end do
!         end do
!        end do
!       end do
        write(6,*) ' ... done '
      end subroutine Fill_Ekpq

BEGIN_PROVIDER [ double precision, bielecCI_act, (n_act_orb,n_act_orb,n_act_orb,n_act_orb) ]
  implicit none
  integer :: p,q,r,s,ss
  do p=1,n_act_orb
   do q=1,n_act_orb
    do r=1,n_act_orb
     do s=1,n_act_orb
      ss=list_act(s)
      bielecCI_act(p,q,r,s) = bielecCI(p,q,r,ss)
     end do
    end do
   end do
  end do

END_PROVIDER
