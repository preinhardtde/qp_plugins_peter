============
complete_fci
============

A straight-forward Full CI on the CAS space, generating as 
well natural orbitals and density matrices of 1, 2 and 3 particles.

We operate via a formula tape, the matrix of all <mu|E_pq|nu>
within the CAS. As no selection is performed we can insert the 
identity |nu><nu| in the expressions of 2- and 3-particle 
density matrices for instance, and the formation of the 
sigma vector.

We have an own Davidson procedure, as we can just multiply 
two matrices for the sigma vector. Perhaps we can use the 
Davidson in the program already present, and just replace the 
routine which calculates H Psi.
