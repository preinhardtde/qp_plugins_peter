! -*- F90 -*-
          use bitmasks
          real*8 function hijcalc(i,j)
BEGIN_DOC
! return matrix element H_ij 
END_DOC
           implicit none
           
          integer :: i,j
          real*8 :: hhh
          integer(bit_kind), allocatable :: det_i(:,:)
          integer(bit_kind), allocatable :: det_j(:,:)
          allocate(det_i(N_int,2))
          allocate(det_j(N_int,2))

          call det_extract(det_i,i,N_int)
          call det_extract(det_j,j,N_int)
          call i_H_j(det_i,det_j,N_int,hhh)
real*8 :: hm,hd,ph
!         call i_H_j_verbose(det_i,det_j,N_int,hhh,hm,hd,ph)
!         write(6,*) hm,hd,ph
          hijcalc=hhh
          
          deallocate(det_i)
          deallocate(det_j)
          
          end function hijcalc

! -*- F90 -*-
          use bitmasks
          subroutine hijcalc_v(i,j,hhh,hm,hd,ph)
BEGIN_DOC
! return matrix element H_ij, detailed in mono, bi, and phase
END_DOC
           implicit none
           
          integer :: i,j,deg
          real*8 :: hhh
          real*8 :: hm,hd,ph
          integer(bit_kind), allocatable :: det_i(:,:)
          integer(bit_kind), allocatable :: det_j(:,:)
          allocate(det_i(N_int,2))
          allocate(det_j(N_int,2))

          call det_extract(det_i,i,N_int)
          call det_extract(det_j,j,N_int)
          write(6,*) ' element ',i,j
          call print_det(det_i,N_int)
          write(6,*) ' det j = ',j
          call print_det(det_j,N_int)
          call get_excitation_degree(det_i,det_j,deg,N_int)
integer :: exc(0:2,2,2)
          exc=0
          call get_excitation(det_i,det_j,exc,deg,ph,N_int)

          write(6,*) ' excitation degree = ',deg
          if (exc(0,1,1).ne..0) write(6,*) ' excitation alpha ',exc(1,1,1),' -> ',exc(1,2,1)
          if (exc(0,1,2).ne..0) write(6,*) ' excitation beta  ',exc(1,1,2),' -> ',exc(1,2,2)
          write(6,*) ' excitation = ',exc
          call i_H_j_verbose(det_i,det_j,N_int,hhh,hm,hd,ph)
          
          deallocate(det_i)
          deallocate(det_j)
          
          end function hijcalc

