! -*- F90 -*-
BEGIN_PROVIDER [real*8, ecorr, (N_states)]
END_PROVIDER

 BEGIN_PROVIDER [real*8, E_FCI, (N_states) ]
&BEGIN_PROVIDER [real*8, E_FCI_average ]
&BEGIN_PROVIDER [real*8, solvector, (n_det_FullCAS) ]
       implicit none
       real*8 :: scalp
       integer :: i,istate
       integer(bit_kind) :: det_mu(n_int,2)

!      do i=1,n_det_FullCAS
!       psi_coef(i,1)=0.D0
!      end do
!      psi_coef(1,1)=1.D0

       E_FCI=0.D0
       if (n_act_orb.ne.0) then
! for a single state we have our Davidson routine
! it should work for N_states > 1 as well
        Call davidson_casdi
        do istate=1,N_states
         E_FCI(istate)+=ecorr(istate)
! reference is not the starting vector !!
         write(6,*) ' weight of the reference ',psi_coef(psi_coef_indx_max_start(istate),istate)
         do i=1,n_det
          if (abs(psi_coef(i,istate)).gt.1.D-4) then
           call det_extract(det_mu,i,n_int)
           call bitstring_to_csfstring(det_mu,n_int,csf_string)
           write(6,*) ' psi_coef ',i,psi_coef(i,istate),'   ',csf_string
          end if
          solvector(i)=psi_coef(i,istate)
         end do
        end do
       end if
!
! the average total energy is stores in EFCI(1)
       E_FCI_average=0.D0
       E_FCI+=Ecore
       do istate=1,n_states
        E_FCI_average+=E_FCI(istate)
       end do
       E_FCI_average*=1.D0/dble(N_states)

       open(unit=17,file='EFCI',status='unknown',form='formatted')
       write(17,9901)
 9901  format('# Enuc Ecore EFCI - nuclear repulsion already in core energy')
       write(17,'(3E21.12)') nuclear_repulsion,Ecore,E_FCI_average
       close(17)

END_PROVIDER

