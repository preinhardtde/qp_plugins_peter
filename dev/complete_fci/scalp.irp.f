! -*- F90 -*-
     real*8 function scalp(a,b,n)
BEGIN_DOC
! scalar product of a abd b, should be replaced by library routine
END_DOC
       implicit none
       integer :: n,i
       real*8 :: a(n),b(n)
       scalp=0.d0
       do i=1,n
        scalp=scalp+a(i)*b(i)
       end do
     end function scalp


