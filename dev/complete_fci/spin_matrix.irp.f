! -*- F90 -*-
          use bitmasks
BEGIN_PROVIDER [real*8, S2diag_own, (n_det)]
BEGIN_DOC
! diagonal element is 1/4((n_a-n_b)^2 + 2(n_a+n_b))
! counting singly occupied orbitals
END_DOC
         implicit none 
         integer :: idet
          integer(bit_kind), allocatable :: det_mu(:,:)
          allocate(det_mu(N_int,2))
          integer(bit_kind)              :: imask

          integer :: ii,i_nibble,ipos,iorb,n_alpha,n_beta

         S2diag_own=0.D0

         do idet=1,n_det
           n_alpha=0
           n_beta=0
           call det_extract(det_mu,idet,N_int)
!          write(6,*) ' idet = ',idet
!          call print_det(det_mu,N_int)
           do iorb=1,n_act_orb
            ii=list_act(iorb)
            i_nibble = shiftr(ii-1,bit_kind_shift)+1
            ipos = ii-shiftl(i_nibble-1,bit_kind_shift)-1
            imask = ibset(0_bit_kind,ipos)
! is there a alpha spin and no beta spin ?
            if ((iand(det_mu(i_nibble,1),imask) /= 0_bit_kind).and. &
                (iand(det_mu(i_nibble,2),imask) == 0_bit_kind)) then
              n_alpha+=1
            end if
! is there a beta spin and no alpha spin ?
            if ((iand(det_mu(i_nibble,2),imask) /= 0_bit_kind).and. &
                (iand(det_mu(i_nibble,1),imask) == 0_bit_kind)) then
              n_beta+=1
            end if
           end do
           S2diag_own(idet)=0.25D0*dble((n_alpha-n_beta)*(n_alpha-n_beta)+2*(n_alpha+n_beta))
!          write(6,*) 'idet, n_alpha, n_beta, s2diag = ',idet,n_alpha,n_beta,s2diag_own(idet)
          end do

END_PROVIDER

BEGIN_PROVIDER [real*8, S2mat_own, (n_det,n_det)]
          S2mat_own=0.D0
END_PROVIDER

subroutine fill_S2mat_own
BEGIN_DOC
!
! we take a determinant and exchange an alpha with a beta spin
! in the active orbitals
!
END_DOC
          implicit none
          integer :: idet,jdet
          logical :: lfound

          integer(bit_kind), allocatable :: det_mu(:,:)
          integer(bit_kind), allocatable :: det_mu_ex1(:,:)
          integer(bit_kind), allocatable :: key_tmp(:,:)

          allocate(det_mu(N_int,2))
          allocate(det_mu_ex1(N_int,2))
          allocate(key_tmp(N_int,2))

          integer(bit_kind)              :: imask,jmask

          integer :: ii,i_nibble,ipos,iorb
          integer :: jj,j_nibble,jpos,jorb

          S2mat_own=0.D0
        
          do idet=1,n_det
           S2mat_own(idet,idet)=S2diag_own(idet)
           call det_extract(det_mu,idet,N_int)
!          write(6,*) ' idet = ',idet
!          call print_det(det_mu,N_int)

           do iorb=1,n_act_orb
            ii=list_act(iorb)
! is there a alpha spin and no beta spin ?
            i_nibble = shiftr(ii-1,bit_kind_shift)+1
            ipos = ii-shiftl(i_nibble-1,bit_kind_shift)-1
            imask = ibset(0_bit_kind,ipos)
            if ((iand(det_mu(i_nibble,1),imask) /= 0_bit_kind).and. &
                (iand(det_mu(i_nibble,2),imask) == 0_bit_kind)) then
             do jorb=1,n_act_orb
              if (iorb.ne.jorb) then
               jj=list_act(jorb)
! is there a beta spin and no alpha spin ? 
               j_nibble = shiftr(jj-1,bit_kind_shift)+1
               jpos = jj-shiftl(j_nibble-1,bit_kind_shift)-1
               jmask = ibset(0_bit_kind,jpos)
               if ((iand(det_mu(j_nibble,1),jmask) == 0_bit_kind).and. &
                   (iand(det_mu(j_nibble,2),jmask) /= 0_bit_kind)) then
! if so, we exchange the two spins and look where the new determinant is
                call det_copy(det_mu,det_mu_ex1,N_int)
! 4 actions needed
                det_mu_ex1(i_nibble,1) = ibclr(det_mu_ex1(i_nibble,1),ipos)
                det_mu_ex1(i_nibble,2) = ibset(det_mu_ex1(i_nibble,2),ipos)
                det_mu_ex1(j_nibble,2) = ibclr(det_mu_ex1(j_nibble,2),jpos)
                det_mu_ex1(j_nibble,1) = ibset(det_mu_ex1(j_nibble,1),jpos)
!               write(6,*) ' spin permuted determinant ',ii,' <-> ',jj
!               call print_det(det_mu_ex1,N_int)
! get the number in the list
                lfound=.false.
                jdet=0
                do while (.not.lfound)
                 jdet+=1
                 if (jdet.gt.N_det) then
! the determinant is possible, but not in the list
                  lfound=.true.
                  jdet=-1
                  stop ' should never arrive here if a CAS is used '
                 else
                  call det_extract(key_tmp,jdet,N_int)
                  lfound=.true.
integer :: j3,j4
                  do j4=1,2
                   do j3=1,N_int
                    if (key_tmp(j3,j4).ne.det_mu_ex1(j3,j4)) then
                     lfound=.false.
                    end if
                   end do
                  end do

                 end if
                end do
! get the phase of the excitation
integer :: exc(0:2,2,2),degree
real*8 :: phase
                call get_excitation(det_mu,det_mu_ex1,exc,degree,phase,N_int)
                S2mat_own(idet,jdet)=-phase
! symmetrization not necessary, we loop over all possible idet
! and jdet can be < idet 
!               S2mat_own(jdet,idet)=1.D0
               end if
              end if
             end do
            end if 
           end do
          end do
     
          deallocate(det_mu)
          deallocate(det_mu_ex1)
          deallocate(key_tmp)
          
end subroutine fill_S2mat_own

          subroutine spin_projector(psi_in,target_spin,ierr)
               implicit none
               integer :: ierr
               real*8 :: psi_in(n_det),target_spin
BEGIN_DOC
!
! S2 is sum_P P_ab + 1/4( (n_a-n_b)^2 + 2(n_a+n_b) )
! 1) is the target spin compatible with S_z and the CAS ?
!      target spin >= S_z, <= 2n_act-n_a-n_b, S_z+n, with an integer n
! 2) project out all components with S other than the target spin
! 3) normalize the result to 1
!
! we use hvect and qvect as temporary storage
!
END_DOC
          integer(bit_kind), allocatable :: det_mu(:,:)
          integer(bit_kind), allocatable :: det_mu_ex1(:,:)
          integer(bit_kind), allocatable :: key_tmp(:,:)

          integer(bit_kind)              :: imask,jmask
          
          integer :: ii,i_nibble,ipos,iorb
          integer :: jj,j_nibble,jpos,jorb

real*8 :: maxposs,ss
integer :: is
logical :: lfound
integer :: ns
          
          
          if (s_z.gt.target_spin) then
           write(6,*) ' we have S_z = ',s_z &
                ,' and you want a target spin of ',target_spin
           stop ' target spin too low '
          end if
          maxposs=0.5D0*min(elec_alpha_num+elec_beta_num  &
               ,2*n_act_orb-elec_alpha_num-elec_beta_num)
          if (target_spin.gt.maxposs) then
           write(6,*) ' we have place for a maximum spin of',maxposs   &
                ,' and you want a target spin of ',target_spin
           stop ' target spin too high '
          end if
          if (abs(nint(target_spin-s_z)-(target_spin-s_z)).gt.0.1) then
           write(6,*) ' S_z of ',s_z,' and target spin of '    &
                ,target_spin,' are incompatible '
           stop ' S_z and target spin incompatible '
          end if
          
          if (abs(S_z-maxposs).le.1.D-4) then
           write(6,*) ' There is only the possibility S = S_z, '   &
                ,'nothing to project out '
          else
           ns=nint(maxposs-s_z)
           allocate(det_mu(N_int,2))
           allocate(det_mu_ex1(N_int,2))
           allocate(key_tmp(N_int,2))

           do is=0,ns
            if (abs(s_z+is-target_spin).gt.0.1D0) then
             write(6,*) ' projecting out S=',S_z+ns
! we projet out
integer :: idet,jdet
             do idet=1,n_det
              hvect(idet)=(S2diag_own(idet)-dble((s_z+ns)*(s_z+ns+1)))*psi_in(idet)
! the hard part, the projector
              
              
              call det_extract(det_mu,idet,N_int)
!              write(6,*) ' idet = ',idet
!              call print_det(det_mu,N_int)
              
              do iorb=1,n_act_orb
               ii=list_act(iorb)
! is there a alpha spin and no beta spin ?
               i_nibble = shiftr(ii-1,bit_kind_shift)+1
               ipos = ii-shiftl(i_nibble-1,bit_kind_shift)-1
               imask = ibset(0_bit_kind,ipos)
               if ((iand(det_mu(i_nibble,1),imask) /= 0_bit_kind).and. &
                    (iand(det_mu(i_nibble,2),imask) == 0_bit_kind)) then
                do jorb=1,n_act_orb
                 if (iorb.ne.jorb) then
                  jj=list_act(jorb)
! is there a beta spin and no alpha spin ? 
                  j_nibble = shiftr(jj-1,bit_kind_shift)+1
                  jpos = jj-shiftl(j_nibble-1,bit_kind_shift)-1
                  jmask = ibset(0_bit_kind,jpos)
                  if ((iand(det_mu(j_nibble,1),jmask) == 0_bit_kind).and. &
                       (iand(det_mu(j_nibble,2),jmask) /= 0_bit_kind)) then
! if so, we exchange the two spins and look where the new determinant is
                   call det_copy(det_mu,det_mu_ex1,N_int)
! 4 actions needed
                   det_mu_ex1(i_nibble,1) = ibclr(det_mu_ex1(i_nibble,1),ipos)
                   det_mu_ex1(i_nibble,2) = ibset(det_mu_ex1(i_nibble,2),ipos)
                   det_mu_ex1(j_nibble,2) = ibclr(det_mu_ex1(j_nibble,2),jpos)
                   det_mu_ex1(j_nibble,1) = ibset(det_mu_ex1(j_nibble,1),jpos)
!                  write(6,*) ' spin permuted determinant ',ii,' <-> ',jj
!                  call print_det(det_mu_ex1,N_int)
! get the number in the list
                   lfound=.false.
                   jdet=0
                   do while (.not.lfound)
                    jdet+=1
                    if (jdet.gt.N_det) then
! the determinant is possible, but not in the list
                     lfound=.true.
                     jdet=-1
                     stop ' should never arrive here if a CAS is used '
                    else
                     call det_extract(key_tmp,jdet,N_int)
                     lfound=.true.
                     integer :: j3,j4
                     do j4=1,2
                      do j3=1,N_int
                       if (key_tmp(j3,j4).ne.det_mu_ex1(j3,j4)) then
                        lfound=.false.
                       end if
                      end do
                     end do
         
                    end if
                   end do
! get the phase of the excitation
integer :: exc(0:2,2,2),degree
real*8 :: phase
                   call get_excitation(det_mu,det_mu_ex1,exc,degree,phase,N_int)
                   hvect(jdet)-=phase*psi_in(idet)
! symmetrization not necessary, we loop over all possible idet
                  end if
                 end if
                end do
               end if
              end do
              psi_in=hvect
             end do

             
             ss=0.D0
             do idet=1,N_det
              ss+=psi_in(idet)*psi_in(idet)
             end do
             if (ss.le.1.D-12) then
              write(6,*) ' Incoming vector had no component of target spin = ' &
                   ,target_spin
              stop ' Nothing left after projection '
             else
              ss=1.D0/sqrt(ss)
              do idet=1,n_det
               psi_in(idet)*=ss
              end do
             end if
             
            end if
           end do
           deallocate(det_mu)
           deallocate(det_mu_ex1)
           deallocate(key_tmp)
          end if
        end subroutine spin_projector
               
          
