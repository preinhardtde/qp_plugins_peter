! -*- F90 -*- 
program CompleteFullCI
BEGIN_DOC
! FullCI program
! works on the given wavefunction if present
! and of right dimension 
! writing density matrices over natural FCI orbitals 
! onto disk
! 1-, 2-particle density matrix
END_DOC

        implicit none
        real*8 :: cpu0,wall0

        call cpu_time(cpu0)
        call wall_time(wall0)
        call header
        call tests
        call driver
        write(6,*)
        write(6,*) ' All done '
        write(6,*)
        call cpustamp(cpu0,wall0,cpu0,wall0,'All ')
end program CompleteFullCI

       subroutine header 
         write(6,*) 
         write(6,*) ' Brute force FCI program  '
         write(6,*) ' P Reinhardt 4/2020  '
         write(6,*) ' in confinement we trust, France'
         write(6,*) 
         three_particle_matrix=.false.
       end subroutine header 
 
       subroutine tests 
         implicit none
! do we have any orbitals defined ?
         if (n_core_orb+n_act_orb.eq.0) then
          write(6,*) ' you did not specify core and/or active orbitals '
          write(6,*) ' I found n_core_orb = ',n_core_orb,' and n_act_orb = ',n_act_orb
          stop ' II please define orbital classes with qp_set_mo_class'
         else
          write(6,*) ' n_core_orb = ',n_core_orb
          write(6,*) ' n_act_orb  = ',n_act_orb
         end if
! can we hold the cas ?
         if (n_det_FullCAS.gt.100000) then
          write(6,*) ' more than 100000 determinants, CAS too large'
          stop ' more than 100000 determinants, CAS too large'
         end if

         write(6,*) ' S_z = ',S_z,' S_z2_Sz = ',S_z2_Sz
         write(6,*) ' n_alpha = ',elec_alpha_num,' n_beta = ',elec_beta_num

!        write(6,*) ' N_states = ',N_states,N_det
         if (n_states.gt.n_det_FUllCAS) then
          write(6,*) ' you asked for more states than determinants, impossible '
          stop ' too many states demanded '
         end if
         read_wf = .false.
!        touch read_wf
!        write(6,*) ' II N_states = ',N_states, N_det
         if (n_det.ne.n_det_FUllCAS) then
          n_det=0
          need_to_generate_CAS=.true.
         end if
         if (need_to_generate_CAS) call gen_cas
       end subroutine tests 

       subroutine driver 
         implicit none
         integer :: i,ii
         integer :: iorder
         integer :: istate,p,q,r,s,indx,j,t,u,v,x,y,z

         real*8, allocatable :: hmat(:,:)
         real*8, allocatable :: eval(:)
         real*8, allocatable :: evec(:,:)
         allocate (hmat(n_det_FullCAS,n_det_FullCAS))
         allocate (eval(n_det_FullCAS))
         allocate (evec(n_det_FullCAS,n_det_FullCAS))

!
! what can we construct as spins with the present determinants ? 
! this should come down to the Kotani branching diagram.
!
         write(6,*) ' the possible spin states '
!        call lapack_diag(eval,evec,S2_matrix_all_dets,n_det_FullCAS,n_det_FullCAS)
         call fill_S2mat_own
         call lapack_diag(eval,evec,S2mat_own,n_det_FullCAS,n_det_FullCAS)
         write(6,*) ' Spin eigenvalues S(S+1)'
         write(6,'(10(i6,f7.2))') (i,eval(i),i=1,n_det)
         write(6,*) 
         write(6,*) ' Spin S'
         write(6,'(10(i6,f7.2))') (i,-0.5D0+sqrt(0.25D0+eval(i)),i=1,n_det)
         write(6,*) 
         write(6,*) ' constructing the Hamilton matrix '
         write(6,*) 
         do i=1,n_det
!         write(6,*) ' treating determinant No = ',i
          do ii=1,n_det
           psi_coef(ii,1)=0.D0
          end do
          psi_coef(i,1)=1.D0
          call hcalc
          do ii=1,n_det
            hmat(i,ii)=hvect(ii)
real*8 :: hhh,hm,hd,ph,deg
!           hhh=hijcalc(i,ii)
!           if (i.gt.ii.and.abs(hhh-hmat(i,ii)).gt.1.D-4) then
!             call hijcalc_v(i,ii,hhh,hm,hd,ph)
!             write(6,*) ' we, qp calculation : ',i,ii,hmat(i,ii),hhh,' diff = ',hmat(i,ii)-hhh,hm,hd,ph
!           end if
          end do
         end do

!        write(6,*)
!        write(6,*) ' comparing diagonal elements '
!        write(6,*)
!eal*8 :: hijcalc
!        do i=1,N_det
!         j=i
!         if (abs(H_matrix_all_dets(i,j)-hmat(i,j)+nuclear_repulsion-ecore).gt.1.D-10) then
!          write(6,*) i,j,hmat(i,j)+ecore-nuclear_repulsion,H_matrix_all_dets(i,j)   &
!               ,hijcalc(i,i),H_matrix_all_dets(i,j)-hmat(i,j)+nuclear_repulsion-ecore
!         end if
!        end do
!        write(6,*)
!        write(6,*) ' comparing off-diagonal elements '
!        write(6,*)
!        do i=1,N_det
!         do j=i+1,N_det
!          if (abs(H_matrix_all_dets(i,j)-hmat(i,j)).gt.1.D-4) then
!           write(6,*) i,j,hmat(i,j),H_matrix_all_dets(i,j)   &
!                ,H_matrix_all_dets(i,j)-hmat(i,j)
!          end if
!         end do
!        end do

         call lapack_diag(eval,evec,Hmat,n_det_FullCAS,n_det_FullCAS)
!        call lapack_diag(eval,evec,h_matrix_all_dets,n_det_FullCAS,n_det_FullCAS)
!        renormalize the eigenvectors
         do istate=1,N_states
          ss=0.D0
          do ii=1,n_det
           ss+=evec(ii,istate)*evec(ii,istate)
          end do
          ss=1.D0/sqrt(ss)
          do ii=1,n_det
           evec(ii,istate)*=ss
          end do
         end do

!         do i=1,n_det_FullCAS
!          write(6,*) ' spin matrix : own qp2 diff ',i,i,S2mat_own(i,i)  &
!             ,S2_matrix_all_dets(i,i)+s_z2_sz,S2mat_own(i,i)-S2_matrix_all_dets(i,i) -s_z2_sz
!          do j=i+1,n_det_FullCAS
!         if (S2mat_own(i,j).ne.0.D0.or. &
!               S2_matrix_all_dets(i,j).ne.0.D0) write(6,*) & 
!              ' spin matrix : own qp2 diff ',i,j,S2mat_own(i,j)  &
!             ,S2_matrix_all_dets(i,j),S2mat_own(i,j)-S2_matrix_all_dets(i,j) 
!         end do
!         write(6,*)
!        end do

!   stop ' SSSS '

         write(6,*) ' Lowest eigenvalues : '
real*8 :: emoy,ss
         emoy=0.D0
         do istate=1,N_states
          emoy+=eval(istate)
          do ii=1,n_det_FullCAS
           psi_coef(ii,istate)=evec(ii,istate)
          end do
          ss=0.D0
          do i=1,n_det_FullCAS
           do j=1,n_det_FullCAS
            ss+=psi_coef(i,istate)*psi_coef(j,istate)*S2mat_own(i,j)
           end do
          end do
          write(6,9821)  istate,eval(istate)+ecore,ss
   9821 format('   Energy of state No ',i3,' is ',F15.7,' with <S2> of ',F7.2)
!         write(6,*)  ' Energy of state No ',istate,' is ',eval(istate)+nuclear_repulsion,' with <S2> of ',ss
         end do
         emoy*=1.D0/dble(N_states)
         deallocate (hmat)
         deallocate (eval)
         deallocate (evec)

         touch psi_coef

         write(6,*) ' S_z = ',S_z,' S_z2_Sz = ',S_z2_Sz
         write(6,*) ' n_alpha = ',elec_alpha_num,' n_beta = ',elec_beta_num

!        do i=1,n_states
!         write(6,9822) i,nuclear_repulsion+psi_energy(i),psi_S2(i)+s_z2_sz
!        end do
!  9822 format('   qp2 : Energy of state No ',i3,' is ',F15.7,' with <S2> of ',F7.2)
    
         write(6,*) ' defined core ',2*n_core_orb,' electrons in ' &
                 ,n_core_orb,' orbitals' 
         write(6,*) '  defined CAS ',elec_num-2*n_core_orb,' electrons in ' &
                 ,n_act_orb,' orbitals' 
         write(6,*) ' Number of states               = ',n_states
         write(6,*) ' Number of determinants         = ',n_det,n_det_FullCas
         write(6,*) ' Number of atomic basis functions ',ao_num
         write(6,*) ' Total number of electrons is     ',elec_num
         write(6,*) ' S_z                              ',(elec_alpha_num-elec_beta_num)*0.5D0
         write(6,*) 
         write(6,*) '                total energy    = ',eone+etwo+ecore
         write(6,*) '                Full CI energy  = ',Emoy,Emoy+ecore
         write(6,*) ' generating natural orbitals '
         write(6,*)
         write(6,*) '                           Ndet = ',n_det
         call save_wavefunction
         write(6,*) ' Saved wavefunction'

         call trf_to_natorb
         write(6,*)

         write(6,*) ' all data available ! '
         write(6,*) '    writing out files '

         open(unit=12,file='D0tu.dat',form='formatted',status='unknown')
         do p=1,n_act_orb
          do q=1,n_act_orb
           if (abs(D0tu(p,q)).gt.1.D-12) then
            write(12,'(2i8,E20.12)') p,q,D0tu(p,q)
!           write(6,'(2i8,E20.12)') p,q,D0tu(p,q)
           end if
          end do
         end do 
         close(12)
         write(6,*) '    wrote <D0tu.dat>'
         
real*8 :: approx,np,nq,nr,ns
logical :: lpq,lrs,lps,lqr
         open(unit=12,file='P0tuvx.dat',form='formatted',status='unknown')
         do p=1,n_act_orb
          np=D0tu(p,p)
          do q=1,n_act_orb
           lpq=p.eq.q
           nq=D0tu(q,q)
           do r=1,n_act_orb
            lqr=q.eq.r
            nr=D0tu(r,r)
            do s=1,n_act_orb
             lrs=r.eq.s
             lps=p.eq.s
             approx=0.D0
             if (lpq.and.lrs) then
              if (lqr) then
! pppp
               approx=0.5D0*np*(np-1.D0)
              else
! pprr
               approx=0.5D0*np*nr
              end if
             else
              if (lps.and.lqr.and..not.lpq) then
! pqqp
               approx=-0.25D0*np*nq
              end if
             end if
!            if (abs(P0tuvx(p,q,r,s)).gt.1.D-12) then
!             write(12,'(4i4,2E20.12)') p,q,r,s,P0tuvx(p,q,r,s),approx
             if (abs(P0tuvx(p,q,r,s)).gt.1.D-12) then
              write(12,'(4i4,2E20.12)') p,q,r,s,P0tuvx(p,q,r,s),approx
             end if
            end do
           end do
          end do 
         end do 
         close(12)
         write(6,*) '    wrote <P0tuvx.dat>'
         
         open(unit=12,form='formatted',status='unknown',file='onetrf.tmp')
         indx=0
         do q=1,mo_num
          do p=q,mo_num
            if (abs(onetrf(p,q)).gt.1.D-12) then
             write(12,'(2i6,E20.12)') p,q,onetrf(p,q)
             indx+=1
            end if
          end do 
         end do 
         write(6,*) ' wrote ',indx,' mono-electronic integrals'
         close(12)
 
         
         open(unit=12,form='formatted',status='unknown',file='bielec_PQxx.tmp')
         indx=0
         do p=1,mo_num
          do q=p,mo_num
           do r=1,n_core_orb+n_act_orb
            do s=r,n_core_orb+n_act_orb
             if (abs(bielec_PQxxtmp(p,q,r,s)).gt.1.D-12) then
              write(12,'(4i8,E20.12)') p,q,r,s,bielec_PQxxtmp(p,q,r,s)
              indx+=1
             end if
            end do
           end do
          end do 
         end do 
         write(6,*) ' wrote ',indx,' integrals (PQ|xx)'
         close(12)
 
         open(unit=12,form='formatted',status='unknown',file='bielec_PxxQ.tmp')
         indx=0
         do p=1,mo_num
          do q=1,n_core_orb+n_act_orb
           do r=q,n_core_orb+n_act_orb
integer ::s_start
            if (q.eq.r) then
             s_start=p
            else
             s_start=1
            end if
            do s=s_start,mo_num
             if (abs(bielec_PxxQtmp(p,q,r,s)).gt.1.D-12) then
              write(12,'(4i8,E20.12)') p,q,r,s,bielec_PxxQtmp(p,q,r,s)
              indx+=1
             end if
            end do
           end do
          end do 
         end do 
         write(6,*) ' wrote ',indx,' integrals (Px|xQ)'
         close(12)
 
         open(unit=12,form='formatted',status='unknown',file='bielecCI.tmp')
         indx=0
         do p=1,n_act_orb
          do q=p,n_act_orb
           do r=1,n_act_orb
            do s=1,mo_num
             if (abs(bielecCI(p,q,r,s)).gt.1.D-12) then
              write(12,'(4i8,E20.12)') p,q,r,s,bielecCI(p,q,r,s)
              indx+=1
             end if
            end do
           end do
          end do 
         end do 
         write(6,*) ' wrote ',indx,' integrals (tu|xP)'
         close(12)

         if (three_particle_matrix) then
          open(unit=12,form='formatted',status='unknown',file='Q0vxyztu.dat')
          indx=0
          do v=1,n_act_orb
           do x=1,n_act_orb
            do y=1,n_act_orb
             do z=1,n_act_orb
              do t=1,n_act_orb
               do u=1,n_act_orb
                if (abs(Q0vxyztu(v,x,y,z,t,u)).gt.1.D-12) then
                 write(12,'(6i8,E20.12)') v,x,y,z,t,u,Q0vxyztu(v,x,y,z,t,u)
                 indx+=1
                end if
               end do
              end do
             end do 
            end do 
           end do 
          end do 
          write(6,*) ' wrote ',indx,' matrix elements Q0vxyztu '
          close(12)
         end if

         write(6,*)
         write(6,*) '   Original orbitals '
         do i=1,mo_num
          write(6,*) ' Orbital No ',i
          write(6,'(5F14.6)') (mo_coef(j,i),j=1,ao_num)
          write(6,*)
         end do

         write(6,*)
         write(6,*) '   creating new orbitals '
         do i=1,mo_num
          write(6,*) ' Orbital No ',i
          write(6,'(5F14.6)') (NatOrbsFCI(j,i),j=1,ao_num)
          write(6,*)
         end do

         mo_label = "MCSCF"
         mo_label = "Natural"
         mo_label = "Natural"
         do i=1,mo_num
          do j=1,ao_num
           mo_coef(j,i)=NatOrbsFCI(j,i)
          end do
         end do
         call save_mos
         write(6,*) ' Saved MOs as <',trim(mo_label),'>'


         write(6,*)
         write(6,*) ' Finally: Full CI energy  = ',Emoy+Ecore
         write(6,*)
 
         write(6,*) '   ... done '

       end subroutine driver
