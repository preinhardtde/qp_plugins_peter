! -*- F90 -*-
 BEGIN_PROVIDER [real*8, hone_CAS_mo, (n_act_orb,n_act_orb)]
&BEGIN_PROVIDER [real*8, hone_eff_mo, (n_act_orb,n_act_orb)]
BEGIN_DOC
! effective one-electron integrals 
END_DOC
      implicit none
      integer :: p,q,r,pp,qq
      real*8 :: t1,t2,t3
!
       do p=1,n_act_orb
        pp=list_act(p)
        do q=1,n_act_orb
         qq=list_act(q)
         t1=mo_one_e_integrals(pp,qq)
!        hone_CAS_mo(p,q)=mo_one_e_integrals(pp,qq)
         t2=0.D0
         do r=1,n_core_orb
          t2+=bielec_pqxxtmp(pp,qq,r,r)-0.5D0 * bielec_pxxqtmp(pp,r,r,qq)
!         hone_CAS_mo(p,q)+=2.D0*(bielec_pqxxtmp(pp,qq,r,r)-0.5D0 * bielec_pxxqtmp(pp,r,r,qq))
         end do
         t2*=2.0D0
!        hone_eff_mo(p,q)=hone_CAS_mo(p,q)
! we have to add -1/2 sum_{r\in active} (pr|rq) to the common 1e integral for the sigma vector
         t3=0.D0
         do r=1,n_act_orb
          t3-=bielecCI(p,r,r,qq)
!         hone_CAS_mo(p,q)-=0.5D0*bielecCI(p,r,r,qq)
         end do
         t3*=0.5D0
         hone_eff_mo(p,q)=t1+t2
         hone_CAS_mo(p,q)=t1+t2+t3
!        if (p.ge.q) write(6,*) ' one-e integral ',pp,qq,t1,t2,t3,t1+t2,t1+t2+t3
        end do
       end do

END_PROVIDER
