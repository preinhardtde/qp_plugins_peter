! -*- F90 -*-
      subroutine vnorm(howto)
       implicit none
       integer :: i,howto,idet
       real*8 :: sum,xmlt,xnrm,scalp
!
! three types of norms for the ci vector vect
! howto=1: normalize to one
!      =2: intermediate norm, i.e. c(1)=1
!      =3: complementary norm, all but c(1) normed to one
       
       if (howto.eq.1) then
        write(6,*) ' normalizing CI vector to 1'
        write(6,*) ' Psi_coef(1,1)  = ',Psi_coef(1,1)
        sum=sqrt(scalp(psi_coef(1,1),psi_coef(1,1),n_det_FullCAS))
        do i=1,n_det_FullCAS
         psi_coef(i,1)=psi_coef(i,1)/sum
        end do
       else 
        if (howto.eq.2) then
         xmlt=1.d0/psi_coef(1,1)
         do i=1,n_det_FullCAS
          psi_coef(i,1)=psi_coef(i,1)*xmlt
         end do
        else
         if (howto.eq.3) then
          xnrm=scalp(psi_coef(1,1),psi_coef(1,1),n_det_FullCAS) &
            -psi_coef(1,1)*psi_coef(1,1)
          if (xnrm.gt.1.d-16) then
           xnrm=1.d0/sqrt(xnrm)
           do idet=1,n_det_FullCAS
            psi_coef(idet,1)=xnrm*psi_coef(idet,1)
           end do
           write(6,*) ' K0 = ',psi_coef(1,1)
           write(6,*) 
          else
           psi_coef(1,1)=1.d10
           write(6,*) ' no complement available ' 
           write(6,*) ' setting k0 to 10000 ' 
           write(6,*) 
          end if
         else
          write(6,*) ' call to VNORM should have 1, 2 or 3.'
          stop       ' call to VNORM should have 1, 2 or 3'
         end if
        end if
       end if
      end subroutine vnorm
