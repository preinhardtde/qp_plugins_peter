! -*- F90 -*-
BEGIN_PROVIDER [real*8, ham_diag, (n_det)]
BEGIN_DOC
! the diagonal of the Hamilton matrix in determinants
! E_core is subtracted, or, better, not added
END_DOC
      implicit none
      integer :: p,q,r,s,mu,kappa
      real*8 :: aaa,bbb

      ham_diag=0.D0
      do p=1,n_act_orb
       do q=1,n_act_orb
        do mu=1,n_det
! we use the packed formula tape !
         if (Aindx_mpq(mu,p,q,1).eq.mu) then
          ham_diag(mu)+=hone_CAS_mo(p,q)*Aval_mpq(mu,p,q,1)
         else
          if (Aindx_mpq(mu,p,q,2).eq.mu) then
           ham_diag(mu)+=hone_CAS_mo(p,q)*Aval_mpq(mu,p,q,2)
          end if
         end if
        end do
       end do
      end do
!     write(6,*) ' the one-electron part of the diagonal of the Hamilton matrix is ready '
!integer :: idet
!      do idet=1,n_det
!       write(6,*) ' diag',idet,ham_diag(idet)
!      end do

! sequence (mu,kappa)*(kappa,mu), may be 11, 12, 21, 22

integer :: pp,qq
      do p=1,n_act_orb
       do q=1,n_act_orb
        do mu=1,n_det
! we use the packed formula tape !
integer :: ss
         kappa=Aindx_mpq(mu,p,q,1)
         if (kappa.ne.-1) then
          aaa=0.5D0*aval_mpq(mu,p,q,1)
          do r=1,n_act_orb
           do s=1,n_act_orb
!           ss=list_act(s)
            if (Aindx_mpq(kappa,r,s,1).eq.mu) then
             bbb=aval_mpq(kappa,r,s,1)
!            ham_diag(mu)+=aaa*bbb*bielecCI(p,q,r,ss)
             ham_diag(mu)+=aaa*bbb*bielecCI_act(p,q,r,s)
            else
             if (Aindx_mpq(kappa,r,s,2).eq.mu) then
              bbb=aval_mpq(kappa,r,s,2)
              ham_diag(mu)+=aaa*bbb*bielecCI_act(p,q,r,s)
!             ham_diag(mu)+=aaa*bbb*bielecCI(p,q,r,ss)
             end if
            end if
           end do
          end do
! we may have a second one
          if (secnd_Aval_mpq(mu,p,q)) then
           kappa=Aindx_mpq(mu,p,q,2)
           aaa=0.5D0*aval_mpq(mu,p,q,2)
           do r=1,n_act_orb
            do s=1,n_act_orb
!            ss=list_act(s)
             if (Aindx_mpq(kappa,r,s,1).eq.mu) then
              bbb=aval_mpq(kappa,r,s,1)
!             ham_diag(mu)+=aaa*bbb*bielecCI(p,q,r,ss)
              ham_diag(mu)+=aaa*bbb*bielecCI_act(p,q,r,s)
             else
              if (Aindx_mpq(kappa,r,s,2).eq.mu) then
               bbb=aval_mpq(kappa,r,s,2)
!              ham_diag(mu)+=aaa*bbb*bielecCI(p,q,r,ss)
               ham_diag(mu)+=aaa*bbb*bielecCI_act(p,q,r,s)
              end if
             end if
            end do
           end do
          end if
         end if
        end do
       end do
      end do
!     write(6,*) ' the diagonal of the Hamilton matrix is ready '
!      do idet=1,n_det
!       write(6,*) ' diag',idet,ham_diag(idet)
!      end do
END_PROVIDER

      subroutine Hcalc
BEGIN_DOC
! calculate H.C in determinants
END_DOC
        implicit none
        integer :: p,q,mu,kappa,pp,qq
        real*8 :: aaa

!       write(6,*) ' forming H.C '
 
        call fill_EKpq
 
! we should use the packed formula tape !
        hvect=0.D0
        do p=1,n_act_orb
         do q=1,n_act_orb
          do mu=1,n_det
           kappa=Aindx_mpq(mu,p,q,1)
           if (kappa.ne.-1) then
            aaa=Aval_mpq(mu,p,q,1)
            hvect(mu)+=aaa*Ekpq(kappa,p,q)
!        if (mu.eq.2.and.aaa*Ekpq(kappa,p,q).ne.0) write(6,*) ' t2 I  mu kappa p q hvect ',mu,kappa,p,q,aaa*Ekpq(kappa,p,q)*0.5D0,hvect(mu)*0.5D0
            if (secnd_Aval_mpq(mu,p,q)) then
             kappa=Aindx_mpq(mu,p,q,2)
             aaa=Aval_mpq(mu,p,q,2)
             hvect(mu)+=aaa*Ekpq(kappa,p,q)
!        if (mu.eq.2.and.aaa*Ekpq(kappa,p,q).ne.0) write(6,*) ' t2 II mu kappa p q hvect ',mu,kappa,p,q,aaa*Ekpq(kappa,p,q)*0.5D0,hvect(mu)*0.5D0
            end if
           end if
!          do kappa=1,n_det
!           hvect(mu)+=Amnpq(mu,kappa,p,q)*Ekpq(kappa,p,q)
!          end do
          end do
         end do
        end do
! pushed to the dgemv call
!       hvect*=0.5D0

! we have to add the 1-el contributions
!       do mu=1,n_det
!        do p=1,n_act_orb
!         pp=list_act(p)
!         do q=1,n_act_orb
!          qq=list_act(q)
! real*8 :: t1
!          t1=hone_CAS_mo(p,q)*DKpq(mu,p,q)
!          hvect(mu)+=hone_CAS_mo(p,q)*DKpq(mu,p,q)
!          if (mu.eq.2.and.t1.ne.0) write(6,*) ' t1 mu p q hone, Dkpq',mu,p,q,hone_CAS_mo(p,q),Dkpq(mu,p,q),t1,hvect(mu)
!         end do
!        end do
!       end do

! the same calling dgemv hvect = 0.5*hvect + DKpq.hone_CAS_mo
        call dgemv ('N', n_det, n_act_orb*n_act_orb, 1.d0, DKpq, n_det, &
                     hone_CAS_mo, 1, 0.5D0, hvect, 1)


!integer :: i
!      write(6,*) ' ... H.C. ready '
!      write(6,*) ' the CI vector C '
!      write(6,'(5(i4,e12.5))') (i,psi_coef(i,1),i=1,n_det)
!      write(6,*) ' the resulting vector H.C '
!      write(6,'(5(i4,e12.5))') (i,hvect(i),i=1,n_det)

!      stop ' HCALC '

      end subroutine Hcalc
