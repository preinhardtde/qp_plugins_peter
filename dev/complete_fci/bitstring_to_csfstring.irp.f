! -*- F90 -*-
use bitmasks
BEGIN_PROVIDER [character*1, csf_string, (n_act_orb)]
END_PROVIDER

       subroutine bitstring_to_csfstring(key,nint,csfstring)
BEGIN_DOC
! print a determinant only by its active orbitals
END_DOC
          implicit none
          character*1 :: csfstring(n_act_orb)
          integer :: n_elements,i,j,nint
          integer, allocatable :: list_occ(:,:)
          allocate(list_occ(N_int*bit_kind_size,2))
          integer(bit_kind) :: key(nint,2)
          call bitstring_to_list(key, list_occ, n_elements, Nint)
          call bitstring_to_list(key(1,2), list_occ(1,2), n_elements, Nint)
          csfstring='0'
          do i = 1, elec_alpha_num
           do j=1,n_act_orb
            if (list_occ(i,1).eq.list_act(j)) csfstring(j)='a'
           end do
          end do
          do i = 1, elec_beta_num
           do j=1,n_act_orb
            if (list_occ(i,2).eq.list_act(j)) then
             if (csfstring(j).eq.'a') then
              csfstring(j)='2'
             else
              csfstring(j)='b'
             end if
            end if
           end do
          end do
       end subroutine bitstring_to_csfstring
          
