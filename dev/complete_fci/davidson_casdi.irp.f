! -*- F90 -*-
 BEGIN_PROVIDER [integer, maxdav]
&BEGIN_PROVIDER [integer, maxiter]
&BEGIN_PROVIDER [real*8, thrdav]
&BEGIN_PROVIDER [integer, psi_coef_indx_max_start,(N_states)]
   maxdav=30*N_states
   maxiter=30
   thrdav=1.D-8
END_PROVIDER

 BEGIN_PROVIDER[real*8, bab, (maxdav, maxdav)]
&BEGIN_PROVIDER[real*8, alpha_dav, (maxdav, maxdav)]
&BEGIN_PROVIDER[real*8, alam, (maxdav)]
END_PROVIDER

       subroutine davidson_casdi
BEGIN_DOC
! this is the Davidson procedure for one single state, as programmed 
! by Daniel Maynau in the CASDI series of programs in Toulouse
! originally multistate, reduced to 1 state, re-expanded to multistate
!
! we suppose that we have N_state guess vectors in psi_coef
! we hand back the lowest N_state eigenvectors and eigenvalues
!
END_DOC
         implicit none 
         integer :: niter
         real*8 :: thr_david,u_dot_v
         
         logical :: loop
         real*8 :: zero,sumq,sum,alp
         integer :: iunit1,iunit2,iter,i,matz,ierr,l,m,ll,iq,j,imode
         integer :: nele,nvi
         real*8 :: babl,scalp,aval,avec,diffi
!        real*8 :: olddress,newdress,xml,newdress_diff

         integer :: ii,k
!
!     diagonalization subroutine for large symmetric matrices
!     E. R. Davidson, J. Comp. Phys 17 (185) 87
!
! structure from the casdi program of D. Maynau, completely rewritten
!
         write(6,*) ' entering the Davidson procedure, N_states, ndet =  ' &
              , N_states,n_det,n_det_FullCAS
10       format (3x,'iter=',i3,'  eigenvalue=',f17.12,', convergence =',d10.2)
         
         zero=0.d0
         thr_david=thrdav
!
! we open 2 direct-access files, we open them in the /dev/shm ram-disk
!
         iunit1=76
         iunit2=87
         open(unit=iunit1,file='/dev/shm/VECTOR.TMP',status='unknown' &
           ,access='direct',recl=n_det*8)
         open(unit=iunit2,file='/dev/shm/HVECT.TMP',status='unknown'  &
           ,access='direct',recl=n_det*8)
!
         iter=0
!
integer :: istate,jstate,idet
real*8 :: ss
      integer(bit_kind), allocatable :: det_mu(:,:)
      allocate(det_mu(N_int,2))


! starting guess vectors
!logical :: lused(N_det)

         if (.not.read_wf) then
          call select_Nstates_Sz
         end if
           do istate=1,N_states
            ss=0.D8
            do i=1,N_det
             if (abs(psi_coef(i,istate)).gt.ss) then
              idet=i
              ss=abs(psi_coef(i,istate))
             end if
            end do
            psi_coef_indx_max_start(istate)=idet
           end do


!         Lused=.false.
!         psi_coef=0.D0
!         do istate=1,N_states
!          ss=1.D8
!          do i=1,N_det
!           if (ham_diag(i).le.ss.and..not.lused(i)) then
!            idet=i
!            ss=ham_diag(i)
!           end if
!          end do
!          psi_coef(idet,istate)=1.D0
!          psi_coef_indx_max_start(istate)=idet
!          lused(idet)=.true.
!         end do

!
! start with proper spin functions, i.e. we construct the spin matrix and diagonalize it
! this is still to be done, we do not have a logic of CSFs here
!
! we should construct S=Sz
           
          if (N_states.le.10) then
           write(6,*)  'the initial states '
           write(6,*)  'State No ',(i,i=1,N_states)
           do i=1,n_det
            ss=0.D0
            do istate=1,N_states
             ss+=psi_coef(i,istate)*psi_coef(i,istate)
            end do
            ss=sqrt(ss)
            ss*=1.D0/dble(N_states)
            if (ss.gt.1.D-4) then
             write(6,'(i3,10F8.3)') i,(psi_coef(i,istate),istate=1,N_states)
             call det_extract(det_mu,i,N_int)
             call print_det(det_mu,N_int)
            end if
           end do
           write(6,*)
          end if
      deallocate(det_mu)
         do istate=1,n_states
!         write(6,*) ' normalizing CI vector to 1',istate
          sum=sqrt(u_dot_v(psi_coef(1,istate),psi_coef(1,istate),n_det_FullCAS))
 
          do i=1,n_det_FullCAS
           psi_coef(i,istate)=psi_coef(i,istate)/sum
          end do
!         do jstate=1,istate-1
!          write(6,*) ' overlap',istate,jstate,u_dot_v(psi_coef(1,istate),psi_coef(1,jstate),n_det_FullCAS)
!         end do
          call putv(psi_coef(1,istate),istate,iunit1)
         end do

         do istate=1,n_states
          call getv(psi_coef(1,1),istate,iunit1)
          call hcalc
          call putv(hvect,istate,iunit2)
          bab(istate,istate)=u_dot_v(psi_coef(1,1),hvect,n_det)
          do jstate=1,istate-1
           call getv(qvect,jstate,iunit1)
           bab(istate,jstate)=u_dot_v(qvect,hvect,n_det)
          end do
         end do
!
integer :: mm
         L=N_states
         loop=.true.
         mm=0
real*8 :: E_old,dEav
         E_old=0.D0
!
!     diagonalization of bab ... eigenvalues alam, eigenvectors alpha_dav
!
! this is the iteration loop
!
         do while (loop)
          l+=mm
          mm=0
!         write(6,*) ' Davidson .... l=',l
!
! this small matrix bab has to be diagonalized
!
!         write(6,*) ' the BAB matrix '
          do i=1,l
           do j=i+1,l
            bab(i,j)=bab(j,i)
           end do
          end do

!         do i=1,l
!          write(6,'(20F12.4)') (bab(i,j),j=1,l)
!         end do
          call lapack_diag(alam,alpha_dav,bab,maxdav,l)
          
!     sets up the correction vectors q
!     and perform the convergence test
!
! we need only the N_states lowest eigenvalue
real*8 :: nrm,conv
          conv=0.D0
          mm=0
          do k=1,N_states
           if (l+mm.le.maxdav) then
            qvect=0.D0
            aval=alam(k)
! accumulate dk in qvect
            do i=1,l
             call getv(psi_coef(1,1),i,iunit1)
             call getv(hvect,i,iunit2)
             avec=alpha_dav(i,k)
             do ii=1,n_det
              qvect(ii)+=avec*(hvect(ii)-aval*psi_coef(ii,1))
             end do
            end do
!           write(6,*) ' qvect ',qvect
            nrm=u_dot_v(qvect,qvect,n_det)
            conv=max(conv,nrm)
            nrm=sqrt(nrm)
!           write(6,*) ' norm of dk ',k,nrm,conv
            do ii=1,n_det
             if (abs(aval-ham_diag(ii)).lt.1.D-12) then
              qvect(ii)*=1.D0/(aval-ham_diag(ii)-0.01D0)
             else
              qvect(ii)*=1.D0/(aval-ham_diag(ii))
             end if
            end do
! normalize qvect
            nrm=u_dot_v(qvect,qvect,n_det)
            nrm=1.D0/sqrt(nrm)
            qvect*=nrm
!
! Schmidt orthogonalize wrt the previous l+mm vectors
!
            hvect=0.D0
            do i=1,l+mm
             call getv(psi_coef(1,1),i,iunit1)
real*8 :: qv
             qv=u_dot_v(qvect,psi_coef(1,1),n_det)
             do ii=1,N_det
              hvect(ii)+=qv*psi_coef(ii,1)
             end do
            end do
! test norm of the remaining vector
            qvect-=hvect
            nrm=u_dot_v(qvect,qvect,n_det)
            nrm=sqrt(nrm)
!           write(6,*) ' norm of the correction vector ',k,nrm
            if (nrm.gt.1.D-5) then
             mm+=1
             if (l+mm.gt.maxdav) then
              write(6,*) ' l, mm, maxdav = ',l,mm,maxdav
              write(6,*) ' no convergence '
              loop=.false.
             else
              qvect*=1.D0/nrm
              call putv(qvect,l+mm,iunit1)
              do ii=1,N_det
               psi_coef(ii,1)=qvect(ii)
              end do
              call hcalc
              call putv(hvect,l+mm,iunit2)
! enlarge the BAB matrix
              bab(l+mm,l+mm)=u_dot_v(qvect,hvect,n_det)
!             write(6,*) ' new diagonal element ',l+mm,bab(l+mm,l+mm)
              do i=1,l+mm-1
               call getv(qvect,i,iunit1)
               bab(i,l+mm)=u_dot_v(qvect,hvect,n_det)
               bab(l+mm,i)=bab(i,l+mm)
!              write(6,*) ' new off-diagonal element ',i,l+mm,bab(i,l+mm)
              end do
             end if
            end if
           else
            write(6,*) ' iterative subspace too large, we return '
            loop=.false.
           end if
          end do
          if (conv.lt.thr_david) loop=.false.
real*8 :: Eav
          Eav=0.D0
          do i=1,N_states
           write(6,*) ' energy of state ',i,' is ',alam(i)+ecore
           Eav+=alam(i)
          end do
          Eav*=1.D0/dble(N_states)
          dEav=Eav-E_old
          E_old=Eav
          write(6,9901)  iter,Eav,dEav,conv
   9901 format(' iter = ',i4,' av. energy = ',F15.5,' delta E av. = ',E12.4,' convergence ',E12.4)
          iter+=1
          if (iter.gt.maxiter) then
           write(6,*) ' too many iterations, no convergence, we return '
           loop=.false.
          end if
! close iteration loop
         end do

!     the eigenvectors of bab are back-transformed
!     in the original basis
!
! construct best vectors from the stored intermediates
! as we still have the eigenvectors of BAB
!
         psi_coef=0.D0
         do istate=1,N_states
          ecorr(istate)=alam(istate)
          do i=1,l
           alp=alpha_dav(i,istate)
           call getv(qvect,i,iunit1)
           do ii=1,n_det
            psi_coef(ii,istate)+=qvect(ii)*alp
           end do
          end do
         end do
!
! we return
!
         close(iunit1,status='delete')
         close(iunit2,status='delete')
!
       end subroutine davidson_casdi
!
       subroutine putv(x,ivec,iunit)
         implicit none
         integer :: ivec,iunit,i
         real*8 :: x(*)
!       write(6,*) ' we try to write record no',ivec,' to file ',iunit
         write(iunit,rec=ivec) (x(i),i=1,n_det)
       end subroutine putv
!
       subroutine getv(x,ivec,iunit)
         implicit none
         integer :: ivec,iunit,i
         real*8 :: x(*)
!       write(6,*) ' we try to read record no',ivec,' from file ',iunit
         read(iunit,rec=ivec) (x(i),i=1,n_det)
       end subroutine getv
