! -*- F90 -*-
BEGIN_PROVIDER [ real*8, density, (block_1Dmat_dim) ]
write(6,*) ' providing density '
density=0.D0
END_PROVIDER

BEGIN_PROVIDER [ integer, max_iter ]
      max_iter=120
END_PROVIDER

BEGIN_PROVIDER [ integer, iter_scf ]
      iter_scf=0
END_PROVIDER

BEGIN_PROVIDER [ real*8, thr_scf ]
      thr_scf=1.d-8
END_PROVIDER

BEGIN_PROVIDER [ real*8, thr_p ]
      thr_p=1.d-10
END_PROVIDER

BEGIN_PROVIDER [ real*8, total_energy ]
END_PROVIDER



