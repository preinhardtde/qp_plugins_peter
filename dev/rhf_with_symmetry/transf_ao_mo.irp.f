! -*- F90 -*-
subroutine transf_ao_mo_sym(A_ao,A_mo,orbs)
BEGIN_DOC
! Transform A blockwise from symmetry-adapted AOs to MOs
! upper triangles only
! 
END_DOC
  implicit none
  integer :: ioper,i,j,alpha,beta,addr1D_ij,addr1D_ab
  integer :: blocked_1Dsquare_address,blocked1Daddress,indx
  real*8 :: A_ao(block_1Dmat_dim),A_mo(block_1Dmat_dim)
  real*8 :: cia,cjb,orbs(block_1Dsquare_dim),sum,cja,cib

  addr1D_ij=0
  do ioper=1,noper
   do i=1,norb_sym_cart(ioper)
    do j=1,i
     addr1D_ij+=1
!    addr1D_ij=blocked1Daddress(i,j,ioper)
     sum=0.D0
     addr1D_ab=block_1Doffset(ioper)
     do alpha=1,norb_sym_cart(ioper)
      cia=orbs(blocked_1Dsquare_address(alpha,i,ioper))
      cja=orbs(blocked_1Dsquare_address(alpha,j,ioper))
      do beta=1,alpha
       addr1D_ab+=1
!      addr1D_ab=blocked1Daddress(alpha,beta,ioper)
       cib=orbs(blocked_1Dsquare_address(beta,i,ioper))
       cjb=orbs(blocked_1Dsquare_address(beta,j,ioper))
real*8 :: term
       term=cia*cjb+cja*cib
       if (alpha.eq.beta) then 
        term*=0.5D0
       end if
       sum+=term*A_ao(addr1D_ab)
      end do
     end do
     A_mo(addr1D_ij)=sum
    end do
   end do
  end do
end subroutine transf_ao_mo_sym

