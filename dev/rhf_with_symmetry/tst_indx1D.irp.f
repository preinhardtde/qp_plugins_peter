! -*- F90 -*-
program main
  call header
  call driver
end program main

subroutine header
  write(6,*) 
  write(6,*) ' test for the 1D counters '
  write(6,*) ' P Reinhardt, Paris, 9/2021 ' 
  write(6,*) 
end subroutine header

subroutine driver
       implicit none
       integer :: i,j,ioper,addr1D,indx,blocked1Daddress
       integer :: k,l,joper,blocked_twoint_3index_1Daddress
       integer :: blocked_twoint_1Daddress
       integer :: ii,jj,kk,ll,kstart,lstart

       provide block_1Doffset
       provide blocked_twoint_offset
       provide blocked_twoint_3index_offset
       
       write(6,*)
       write(6,*) ' -----------------------------------------------------'
       write(6,*) 
       write(6,*) ' testing the 2-index counter blocked1Daddress(i,j,ioper)'
       indx=0
       do ioper=1,noper
        do i=1,norb_sym_cart(ioper)
         do j=1,i
          indx+=1
          addr1D=blocked1Daddress(i,j,ioper)
          if (indx.ne.addr1D) then
           write(6,*) ' problem ? ',i,j,ioper,' -> ',indx,addr1D
          end if
          
         end do
        end do
       end do
       write(6,*) 

       write(6,*) ' the 4-index counter blocked_twoint_1Daddress(i,j,ioper,k,l,joper)'
       indx=0
       do ioper=1,noper
       do joper=ioper,noper
        write(6,*) ' block ',name_irreps(ioper),' - ',name_irreps(joper)
        do i=1,norb_sym_cart(ioper)
         ii=block_offset_cart(ioper)+i
         do j=i,norb_sym_cart(ioper)
          jj=block_offset_cart(ioper)+j
          do k=1,norb_sym_cart(joper)
           kk=block_offset_cart(joper)+k
           if (kk.ge.ii) then
           if (kk.eq.ii) then
            lstart=j
           else
            lstart=k
           end if
           do l=lstart,norb_sym_cart(joper)
            ll=block_offset_cart(joper)+l
            indx+=1
            addr1D=blocked_twoint_1Daddress(i,j,ioper,k,l,joper)
            if (indx.ne.addr1D) then
             write(6,*) ' problem ? ',i,j,ioper,k,l,joper,' -> ',indx,addr1D
             write(6,*) ' problem ? ',i,j,ioper,k,l,joper,' -> ',ii,jj,kk,ll
            end if
           end do
            end if
           end do
          end do
         end do
        end do
        end do
           
       write(6,*) 

end subroutine driver



