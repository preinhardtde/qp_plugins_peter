! -*- F90 -*-
      real*8 function blocked_double_contract(A,B)
        implicit none
        BEGIN_DOC
! result=sum_ij A_ij B_ij
        END_DOC
        real*8 :: A(block_1Dmat_dim),B(block_1Dmat_dim)
        integer :: ioper,i,j,addr1D,blocked1Daddress
        blocked_double_contract=0.D0
!$_OMP PARALLEL DO PRIVATE(i,j,ioper,addr1D)      &
!$_OMP DEFAULT(NONE)                        &
!$_OMP SHARED (noper,a,b,blocked_double_contract,norb_sym_cart)
        do ioper=1,noper
         do i=1,norb_sym_cart(ioper)
          do j=1,norb_sym_cart(ioper)
!DIR$ FORCEINLINE
           addr1D=blocked1Daddress(i,j,ioper)
           blocked_double_contract+=A(addr1D)*B(addr1D)
          end do
         end do
        end do
!$_OMP END PARALLEL DO
      end function blocked_double_contract
      
