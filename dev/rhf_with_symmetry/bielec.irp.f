! -*- F90 -*-
  BEGIN_PROVIDER [real*8, twoint_cart, (blocked_twoint_dim)]
  implicit none
  BEGIN_DOC
  ! read and store the bielectronic integrals
  END_DOC
  integer :: i,j,k,l,ioper,joper,addr1D
  real*8 :: hhh
  integer :: blocked_twoint_1Daddress,indx

      twoint_cart=0.D0
      write(6,*) ' reading bielectronic integrals, noper = ',noper
      open(unit=12,file='TWOEL.BLK_6d_simple',status='old',form='formatted')
      do ioper=1,noper
       do joper=ioper,noper
! block ioper - joper
        read(12,*)
        indx=0
101     continue
        read(12,*) i,j,k,l,hhh
        if (i.ne.-1) then
         indx+=1
         addr1D=blocked_twoint_1Daddress(i,j,ioper,k,l,joper)
         twoint_cart(addr1D)=hhh
         go to 101
        end if
       write(6,*) ' read block ',name_irreps(ioper),'  ',name_irreps(joper),' with ',indx,' integrals '
       end do
      end do
      
      close(12)
       
END_PROVIDER
  
BEGIN_PROVIDER [ real*8, xi_old, (block_1Dmat_dim) ]
END_PROVIDER

BEGIN_PROVIDER [ real*8, xi, (block_1Dmat_dim) ]
 implicit none
 BEGIN_DOC
 ! the 2-electron part of the Fock matrix
 END_DOC

 integer                        :: i,j,k,l,ioper,joper,ii,jj,kk,ll,offset_i,offset_j
 integer                        :: addr1D_ij,addr1D_kl,addr2int
 integer                        :: blocked_twoint_1Daddress
 integer                        :: blocked1Daddress,indx
 real*8                         :: sum,hhh,xi_ii(block_1Dmat_dim),xi_ij(block_1Dmat_dim),xi_ij3(block_1Dmat_dim),term

   xi_ii=0.D0
   xi_ij=0.D0
   xi_ij3=0.D0
   xi = 0.d0
   write(6,*) ' constructing 2el part xi '

   real*8 :: numbel,blocked_double_contract
   integer :: addr1D,lstart

   xi=0.D0
   numbel=blocked_double_contract(density,sym_overlap_integrals)
   write(6,*) ' found ',numbel,' electrons in the density matrix '
   if (numbel.lt.1.D-8) then
    return
   end if
real*8 :: eone_b
   eone_b=blocked_double_contract(density,sym_one_e_integrals)
   write(6,*) ' found one-e energy as ',eone_b
 
! read integrals in canonical ordering as 4(ij|kl)-(ik|lj)-(il|kj)
! the integral goes to ij and/or kl 

! (AA|AA) blocks
!
   integer :: addr1D_il,addr1D_ik,addr1D_jl,addr1D_jk,addr1D_kk,addr1D_twoint
   indx=0
   xi=0.D0
   offset_i=0
   do ioper=1,noper
    offset_j=offset_i
    do joper=ioper,noper
     do i=1,norb_sym_cart(ioper)
      ii=i+offset_i
      do j=i,norb_sym_cart(ioper)
       jj=offset_i+j
       addr1D_ij=blocked1Daddress(i,j,ioper)
       do k=1,norb_sym_cart(joper)
        kk=offset_j+k
        if (ii.le.kk) then
         if (ii.eq.kk) then
          lstart=j
         else
          lstart=k
         end if
         do l=lstart,norb_sym_cart(joper)
          ll=offset_j+l
          addr1D_kl=blocked1Daddress(k,l,joper)
          indx+=1
!         addr1D_twoint=blocked_twoint_1Daddress(i,j,ioper,k,l,joper)
!         if (indx.ne.addr1D_twoint) then
!          write(6,*) ' addr1D_twoint, indx =(',i,j,ioper,')-(',k,l,joper,')',addr1D_twoint, indx
!         end if
        
           term=twoint_cart(indx)
! do we have ii=jj ?
           if (ii.eq.jj) then
            xi(addr1D_kl)+=density(addr1D_ij)*term*0.5D0
           else
            xi(addr1D_kl)+=density(addr1D_ij)*term
           end if
           if (ii.ne.kk.or.jj.ne.ll) then
            if (kk.eq.ll) then
             xi(addr1D_ij)+=density(addr1D_kl)*term*0.5D0
            else
             xi(addr1D_ij)+=density(addr1D_kl)*term
            end if
           end if

          end do
         end if
       end do
      end do
     end do
     offset_j+=norb_sym_cart(joper)
    end do
    offset_i+=norb_sym_cart(ioper)
   end do

   xi*=0.5D0

!  numbel=blocked_double_contract(density,sym_overlap_integrals)
!  write(6,*) ' II found ',numbel,' electrons in the density matrix '
!  numbel=blocked_double_contract(density,sym_one_e_integrals)
!  write(6,*) ' one-electron energy is ',numbel
!  numbel=blocked_double_contract(density,xi)
!  write(6,*) ' two-electron energy is ',numbel*0.5D0
!  write(6,*)

!  indx=0
!  do ioper=1,noper
!   write(6,*) ' ioper = ',ioper
!   do i=1,norb_sym_cart(ioper)
!    do j=i,norb_sym_cart(ioper)
!     indx+=1
!     write(6,*) i,j,ioper,indx,xi(indx)
!    end do
!   end do
!  end do

!  stop
   
END_PROVIDER


subroutine print1D_packed(matrix)
  implicit none
  integer :: i,j,ioper,addr1D,blocked1Daddress,indx
  real*8 :: matrix(block_1Dmat_dim)


  write(6,*) ' 1D-packed matrix '
  indx=0
  do ioper=1,noper
   write(6,*) ' IRREP ',name_irreps(ioper)
   do i=1,norb_sym_cart(ioper)
    do j=1,i
     indx+=1
!    addr1D=blocked1Daddress(i,j,ioper)
!    if (indx.ne.addr1D) then
!     write(6,*) ' print_packed : problem ',i,j,ioper,indx,addr1D
!    end if
!    if (abs(matrix(addr1D)).gt.1.D-10) write(6,'(2I4,E20.12)') &
!         i,j,matrix(addr1D)
     if (abs(matrix(indx)).gt.1.D-10) write(6,'(2I4,E20.12)') &
          i,j,matrix(indx)
    end do
   end do
  end do
  write(6,*) ' -------------------------------------- '
  write(6,*)
end subroutine print1D_packed

subroutine reorder(i,j,k,l,ii,jj,kk,ll)
  implicit none
  integer ::  i,j,k,l,ii,jj,kk,ll,idum
  ii=i
  jj=j
  kk=k
  ll=l
  if (ii.gt.jj) then
   idum=ii
   ii=jj
   jj=idum
  end if
  if (kk.gt.ll) then
   idum=kk
   kk=ll
   ll=idum
  end if
  if (ii.gt.kk) then
   idum=kk
   kk=ii
   ii=idum
   idum=jj
   jj=ll
   ll=idum
  end if
  if (kk.eq.ii) then
   if (jj.gt.ll) then
    idum=jj
    jj=ll
    ll=idum
   end if
  end if
end subroutine reorder
