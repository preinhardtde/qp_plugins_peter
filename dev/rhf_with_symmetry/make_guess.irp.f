! -*- F90 -*-
BEGIN_PROVIDER [logical, lowdin ]
      implicit none
      lowdin=.false.
END_PROVIDER

subroutine create_guess
      implicit none
      BEGIN_DOC
!   we orthogonalize the initial symmetry-adapted basis
      END_DOC
      integer :: i,j,k,ioper,alpha,addr1D,indx,info
      integer :: blocked1Daddress,blocked_1Dsquare_address
      real*8 :: ss
      real*8 :: sss,cjb,w
      integer :: a,b
      integer :: ii,indx0
      real*8 :: blocked_double_contract,cia,cib
      integer :: iorb,beta
      real*8, allocatable :: s_tmp(:),outorb(:)
      real*8, allocatable :: large_wrktmp(:,:)
      real*8, allocatable :: wrk(:)

      mo_coef_sym=0.D0
      do ioper=1,noper
       do i=1,norb_sym_cart(ioper)
        addr1D=blocked_1Dsquare_address(i,i,ioper)
        mo_coef_sym(addr1D)=1.D0
       end do
      end do

      if (lowdin) then
!
! we orthogonalize with S^-1/2
!
      write(6,*) ' orthogonalizing with S^(-1/2)'

      allocate (s_tmp(block_1Dmat_dim))

      s_tmp=sym_overlap_integrals

!     write(6,*) ' the overlap matrix '
!     call print1D_packed(sym_overlap_integrals)
      
      allocate (large_wrktmp(ao_num,ao_num))
      large_wrktmp=0.D0
      indx=0
      do ioper=1,noper
       if (norb_sym_cart(ioper).gt.0) then
        call dspev('V','U',norb_sym_cart(ioper) &
            ,s_tmp(1+block_1Doffset(ioper))  &
            ,orben(indx+1),large_wrktmp(1,1+indx),ao_num,wrk,info)
       end if
       indx+=norb_sym_cart(ioper)
      end do
      deallocate (s_tmp)
!
! we have eigenvectors and eigenvalues
!
      do i=1,ao_num
       if (orben(i).gt.1.D-12) then
        orben(i)=1.D0/sqrt(orben(i))
       else
        write(6,*) ' encountered a small overlap: ',i,orben(i)
        stop
       end if
      end do
  
      fock_mo=0.D0
! form S^(-1/2)
      indx=0
      do ioper=1,noper
       do k=1,norb_sym_cart(ioper)
        indx+=1
        ss=orben(indx)
        do i=1,norb_sym_cart(ioper)
         do j=1,i
          fock_mo(blocked1Daddress(i,j,ioper))+=large_wrktmp(i,indx) &
               *large_wrktmp(j,indx)*ss
         end do
        end do
       end do
! transform the orbitals
! the original orbitals are the unit matrix
       do alpha=1,norb_sym_cart(ioper)
        do i=1,norb_sym_cart(ioper)
         addr1D=blocked_1Dsquare_address(alpha,i,ioper)
         mo_coef_sym(addr1D)=fock_mo(blocked1Daddress(alpha,i,ioper))
        end do
       end do
      end do       
      deallocate (large_wrktmp)
      else
       write(6,*) ' Orthogonalizing with Schmidt '

       allocate (wrk(ao_num))
       do ioper=1,noper
!       write(6,*) ' ioper = ',ioper
        do i=2,norb_sym_cart(ioper)
!        write(6,*) ' i = ',i
         wrk=0.D0
         wrk(i)=1.D0
! Schmidt orthogonalization
         do j=1,i-1
!         write(6,*) '       j = ',j
          sss=0.D0
          do b=1,norb_sym_cart(ioper)
           cjb=mo_coef_sym(blocked_1Dsquare_address(b,j,ioper))
! cia=1
           sss+=cjb*sym_overlap_integrals(blocked1Daddress(i,b,ioper))
          end do
          addr1D=blocked_1Dsquare_address(j,i,ioper)
!         write(6,*) '       overlap with i is ',sss
          do a=1,i
           addr1D=blocked_1Dsquare_address(a,j,ioper)
           wrk(a)-=sss*mo_coef_sym(addr1D)
          end do
         end do
! renormalize the new vector
         sss=0.D0
         do a=1,i
          cia=wrk(a)
          do b=1,i
           cib=wrk(b)
           sss+=cia*cib*sym_overlap_integrals(blocked1Daddress(a,b,ioper))
          end do
         end do
!        write(6,*) '       norm of the remainder is ',sqrt(sss)
         sss=1.D0/sqrt(sss)
         do a=1,i
          mo_coef_sym(blocked_1Dsquare_address(a,i,ioper))=wrk(a)*sss
         end do
        end do
       end do
       deallocate(wrk)
      end if

      
      write(6,*) ' the orbitals in symmetry AOs'
      do ioper=1,noper
       write(6,*) ' IRREP ',name_irreps(ioper)
       do i=1,norb_sym_cart(ioper)
        write(6,*) ' Orbital No ',i
        write(6,'(5(i4,F12.4))') (alpha  &
             ,mo_coef_sym(blocked_1Dsquare_address(alpha,i,ioper)) &
             ,alpha=1,norb_sym_cart(ioper))
       end do
      end do
      write(6,*)
! write orbitals in cartesian AOs
      allocate (outorb(ao_num))
      open(unit=12,file='VECTOR_6d.orth',status='unknown',form='formatted')
      indx0=0
      do ioper=1,noper
       do i=1,norb_sym_cart(ioper)
        outorb=0.D0
        indx=indx0
        do alpha=1,norb_sym_cart(ioper)
         indx+=1
         w=mo_coef_sym(blocked_1Dsquare_address(alpha,i,ioper))
         do ii=1,ao_num
          outorb(ii)+=w*ao_cart_to_sym_coef(ii,indx)
         end do
        end do
        write(12,'(4E20.11)') outorb
        write(12,*)
       end do
       indx0+=norb_sym_cart(ioper)
      end do
      close(12)
      deallocate (outorb)
       
      write(6,*) ' testing orthonormality of the generated orbitals '
      call tst_orthonorm(mo_coef_sym)

      density=0.d0

      indx=0
      do ioper=1,noper
       do alpha=1,norb_sym_cart(ioper)
        do beta=1,alpha
         indx+=1
         do iorb=1,nocc(ioper)
          cia=mo_coef_sym(blocked_1Dsquare_address(alpha,iorb,ioper))
          cib=mo_coef_sym(blocked_1Dsquare_address(beta,iorb,ioper))
          density(indx)+=cia*cib
         end do
        end do
       end do
      end do

      density*=2.D0

      write(6,*) ' eone = ',blocked_double_contract(density,sym_one_e_integrals)


end subroutine create_guess

      subroutine tst_orthonorm(orbs)
        implicit none
        real*8 :: orbs(block_1Dsquare_dim)
        real*8, allocatable :: smo(:)
        integer :: i,j,indx,ioper
        real*8 :: smx,sss

! test the orthogonality

      allocate (smo(block_1Dmat_dim))
      smo=0.D0
      call transf_ao_mo_sym(sym_overlap_integrals,smo,orbs)

!     write(6,*) ' testing orthogonality - this should be the unit matrix '
!     call print1D_packed(smo)
      smx=0.D0
      indx=0
      do ioper=1,noper
       do i=1,norb_sym_cart(ioper)
        do j=1,i
         indx+=1
!        addr1D=blocked1Daddress(i,j,ioper)
!        if (indx.ne.addr1D) then
!         write(6,*) ' problem ? ',i,j,ioper,' -> ',indx,addr1D
!        end if
         sss=smo(indx)
         if (i.eq.j) then
          smx+=(1.D0-sss)*(1.D0-sss)
         else
          smx+=sss*sss
         end if
        end do
       end do
      end do
      smx=sqrt(smx)/dble(block_1Dmat_dim)
      write(6,*) ' average deviation from unit matrix ',smx
      deallocate (smo)
      end subroutine tst_orthonorm



