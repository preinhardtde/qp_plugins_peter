! -*- F90 -*-
BEGIN_PROVIDER [ real*8, E_two ]
BEGIN_DOC
! the two-electron energy as contraction of XI with P
END_DOC
     implicit none  
     integer  :: alpha,beta,ioper,addr1D,blocked1Daddress
     real*8 :: blocked_double_contract

     e_two=blocked_double_contract(density,xi)

     e_two*=0.5d0
     write(6,*) ' etwo = ',e_two
END_PROVIDER  

BEGIN_PROVIDER [ real*8, E_one ]
    implicit none
    integer :: alpha,beta,ioper,addr1D,blocked1Daddress
     real*8 :: blocked_double_contract

    e_one=blocked_double_contract(density,sym_one_e_integrals)

END_PROVIDER

BEGIN_PROVIDER [real*8, sym_one_e_integrals, (block_1Dmat_dim)]
     implicit none
     integer :: i,ioper,j,addr1D,blocked1Daddress
     real*8 :: hhh

     sym_one_e_integrals=0.D0
     write(6,*) ' reading one-electron integrals '
     open(unit=12,file='HAMILTO.BLK_6d',status='old',form='formatted')
     do ioper=1,noper
      read(12,*) 
 100  continue
      read(12,*) i,j,hhh
      if ((i.gt.norb_sym_cart(ioper)).or.(j.gt.norb_sym_cart(ioper))) then
       write(6,*) ' indexes beyond limits, i,j,limit = ',i,j,norb_sym_cart(ioper)
       stop 
      end if
      if (i.ne.-1) then
       addr1D=blocked1Daddress(i,j,ioper)
       sym_one_e_integrals(addr1D)=hhh
       go to 100
      end if
     end do
     close(12)

END_PROVIDER

BEGIN_PROVIDER [real*8, sym_overlap_integrals, (block_1Dmat_dim)]
     implicit none
     integer :: i,ioper,j,addr1D,blocked1Daddress
     real*8 :: hhh

     sym_overlap_integrals=0.D0
     write(6,*) ' reading overlap integrals '
     open(unit=12,file='OVERLAP.BLK_6d',status='old',form='formatted')
     do ioper=1,noper
      read(12,*) 
 100  continue
      read(12,*) i,j,hhh
      if ((i.gt.norb_sym_cart(ioper)).or.(j.gt.norb_sym_cart(ioper))) then
       write(6,*) ' indexes beyond limits, i,j,limit = ',i,j,norb_sym_cart(ioper)
       stop 
      end if
      if (i.ne.-1) then
       addr1D=blocked1Daddress(i,j,ioper)
       sym_overlap_integrals(addr1D)=hhh
       go to 100
      end if
     end do
     close(12)

END_PROVIDER
