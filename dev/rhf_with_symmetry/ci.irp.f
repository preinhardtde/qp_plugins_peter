! -*- F90 -*-
integer function blocked_1Dsquare_address(alpha,i,ioper)
  implicit none
  integer :: alpha,i,ioper
  blocked_1Dsquare_address=block_1Dsq_offset(ioper) &
       +(i-1)*norb_sym_cart(ioper)+alpha
end function blocked_1Dsquare_address

BEGIN_PROVIDER [integer, nocc, (noper) ]
         implicit none
         integer :: i,n
         if (elec_alpha_num.ne.elec_beta_num) then
          write(6,*) ' closed-shell program only '
          stop ' no open shells, please '
         end if
         write(6,*) ' please provide the occupation numbers for the ' &
              ,noper,' IRREPs'
         write(6,*) ' order is ',('  ',name_irreps(i),i=1,noper)
         read(5,*) (nocc(i),i=1,noper)
         n=0
         do i=1,noper
          n+=nocc(i)
         end do
         if (n.ne.elec_alpha_num) then
          write(6,*) ' occupation does not sum to the number of electrons '
          write(6,*) ' you gave ',(nocc(i),i=1,noper)
          write(6,*) ' for ',elec_alpha_num+elec_beta_num,' electrons '
          stop ' try again '
         end if
         write(6,*) ' number of occupied orbitals ',nocc
END_PROVIDER

 BEGIN_PROVIDER [integer, block_1Dsquare_dim]
&BEGIN_PROVIDER [integer, block_1Dsq_offset, (noper) ]
      implicit none
      integer :: ioper
      block_1Dsquare_dim=0
      do ioper=1,noper
       block_1Dsq_offset(ioper)=block_1Dsquare_dim
       block_1Dsquare_dim+=norb_sym_cart(ioper)*norb_sym_cart(ioper)
      end do
      write(6,*) ' block_1Dsquare_dim =',block_1Dsquare_dim
END_PROVIDER

BEGIN_PROVIDER [ real*8, mo_coef_sym, (block_1Dsquare_dim) ]
mo_coef_sym=0.D0
END_PROVIDER

 BEGIN_PROVIDER [real*8, fock, (block_1Dmat_dim)]
&BEGIN_PROVIDER [real*8, fock_mo, (block_1Dmat_dim)]
END_PROVIDER


BEGIN_PROVIDER [ real*8, ci, (block_1Dsquare_dim) ]
      implicit none
      integer :: ione,ierr,i,j,alpha,beta
      integer :: blocked_1Dsquare_address,ioper,addr1D,addr1Dsq_alpha
      integer :: addr1Dsq_beta,addr1Dsq_j,indx,info,offset,offset_square
      integer :: blocked1Daddress
      real*8  :: cdum,ss

      write(6,*) ' constructing orbitals, iter No ',iter_scf
      fock=0.D0
      fock_mo=0.D0
      
      if (iter_scf.eq.0) then

       ci=mo_coef_sym
       do ioper=1,noper
!       offset=block_1Doffset(ioper)
!       offset_square=block_1Dsq_offset(ioper)
        do alpha=1,norb_sym_cart(ioper)
         do beta=1,alpha
         addr1D=blocked1Daddress(alpha,beta,ioper)
         ss=0.D0
         do i=1,nocc(ioper)
          addr1Dsq_alpha=blocked_1Dsquare_address(alpha,i,ioper)
          addr1Dsq_beta =blocked_1Dsquare_address(beta ,i,ioper)
          ss+=ci(addr1Dsq_alpha)*ci(addr1Dsq_beta)
         end do
         density(addr1D)=ss
        end do
        end do
       end do
       density*=2.D0
       touch density

!      fock = sym_one_e_integrals

!      do i=1,ao_num
!       fock(i,i)=0.D0
!      end do

       fock = sym_one_e_integrals + xi
      else
       fock = sym_one_e_integrals + (1.D0-lambda_oda)*xi_old &
               + lambda_oda * xi
      end if
!     write(6,*) ' Fock matrix in AO '
!     call print1D_packed(fock)
! transform F onto MOs
      call transf_ao_mo_sym(fock,fock_mo,ci)
!     write(6,*) ' Fock matrix in MOs'
!     call print1D_packed(fock_mo)

!     write(6,*) ' diagonal of the Fock matrix '
!     do ioper=1,noper
!      write(6,*) ' IRREP ',name_irreps(ioper)
!      write(6,'(5(i5F16.8))') (i,fock_mo(blocked1Daddress(i,i,ioper)) &
!           ,i=1,norb_sym_cart(ioper))
!     end do

! diagonalize each symmetry block separately,
! we may use the dspev (=real symmetric packed) routine from lapack
!     SUBROUTINE DSPEV( JOBZ, UPLO, N, AP, W, Z, LDZ, WORK, INFO )
!          On entry, the upper or lower triangle of the symmetric matrix
!          A, packed columnwise in a linear array.  The j-th column of A
!          is stored in the array AP as follows:
!          if UPLO = 'U', AP(i + (j-1)*j/2) = A(i,j)=A(j,i) for 1<=i<=j;
!          if UPLO = 'L', AP(i + (j-1)*(2*n-j)/2) = A(i,j) for j<=i<=n.


real*8, allocatable :: wrk(:),large_wrktmp(:,:)
allocate (wrk(3*ao_num))
allocate (large_wrktmp(ao_num,ao_num))

      large_wrktmp=0.D0
      wrk=0.D0

      indx=0
      do ioper=1,noper
       if (norb_sym_cart(ioper).gt.0) then
        call dspev('V','U',norb_sym_cart(ioper) &
             ,fock_mo(1+block_1Doffset(ioper))  &
             ,orben(indx+1),large_wrktmp(1,1+indx),ao_num,wrk,info)
!       write(6,*) ' IRREP ',name_irreps(ioper)
!       do i=1,norb_sym_cart(ioper)
!       write(6,*) ' eigenvalue: ',i,orben(indx+i)
!       write(6,9911) (large_wrktmp(j,indx+i),j=1,norb_sym_cart(ioper))
 9911 format(' eigenvector: ',20F12.4)
!       write(6,*)
!       end do
       end if
       indx+=norb_sym_cart(ioper)
      end do
deallocate (wrk)
      
      WRITE(6,*) 
      WRITE(6,*) ' ORBITAL ENERGIES OF THE CANONICAL ORBITALS' 
      write(6,*) 
      indx=0
      do ioper=1,noper
       write(6,'(4(i4,e14.5))') (i,orben(indx+i),i=1,norb_sym_cart(ioper))
       indx+=norb_sym_cart(ioper)
      end do
      write(6,*) 
!
! well, it should come down to a simple matrix multiplication ...
!
      indx=0
      do ioper=1,noper
      do alpha=1,norb_sym_cart(ioper)
       do i=1,norb_sym_cart(ioper)
        cdum=0.d0
        do j=1,norb_sym_cart(ioper)
         addr1Dsq_j=blocked_1Dsquare_address(alpha,j,ioper)
! the j components of eigenvector i
         cdum+=large_wrktmp(j,i+indx)*ci(addr1Dsq_j)
        end do
        cwrk(i)=cdum
       end do
       do i=1,norb_sym_cart(ioper)
        addr1Dsq_alpha=blocked_1Dsquare_address(alpha,i,ioper)
        ci(addr1Dsq_alpha)=cwrk(i)
       end do
! close loop over alpha
       end do
       indx+=norb_sym_cart(ioper)
      end do
deallocate (large_wrktmp)

! orthonormality of the new orbitals ?
 
      call tst_orthonorm(ci)


      if (bavard) then 
       write(6,*) ' new occupied orbitals '
       do ioper=1,noper
        write(6,*) ' IRREP ',name_irreps(ioper)
        do i=1,nocc(ioper)
         write(6,*) ' Orbital No ',i
         write(6,'(5(i4,F16.8))') (alpha &
              ,ci(blocked_1Dsquare_address(alpha,i,ioper)) &
              ,alpha=1,norb_sym_cart(ioper))
        end do
       end do
      end if

END_PROVIDER

BEGIN_PROVIDER [ real*8, orben, (ao_num) ]
END_PROVIDER

BEGIN_PROVIDER [ real*8, cwrk, (ao_num) ]
END_PROVIDER

BEGIN_PROVIDER [ real*8, lambda_oda]
END_PROVIDER

BEGIN_PROVIDER [ logical, loda ]
   loda=.true.
END_PROVIDER
