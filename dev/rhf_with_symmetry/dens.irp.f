! -*- F90 -*-
BEGIN_PROVIDER [ real*8, p_next, (block_1Dmat_dim) ]
      implicit none 
      integer :: alpha,beta,iorb,ioper,addr1D,blocked_1Dsquare_address
      integer :: blocked1Daddress,indx
      real*8 :: cia,cib
      
      write(6,*) ' constructing density p_next '
      
      p_next=0.d0
      
      indx=0
      do ioper=1,noper
       do alpha=1,norb_sym_cart(ioper)
        do beta=1,alpha
         indx+=1
         do iorb=1,nocc(ioper)
          cia=ci(blocked_1Dsquare_address(alpha,iorb,ioper))
          cib=ci(blocked_1Dsquare_address(beta,iorb,ioper))
          p_next(indx)+=cia*cib
         end do
        end do
       end do
      end do
      
      p_next*=2.D0

END_PROVIDER
