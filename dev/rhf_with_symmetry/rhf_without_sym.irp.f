! -*- F90 -*-
program main
  call header
  call setup
  call create_guess
  call driver
end program main

subroutine header
  write(6,*) 
  write(6,*) ' a model closed-shell Hartree-Fock program '
  write(6,*) ' making use of molecular symmetry '
  write(6,*) ' P Reinhardt, Paris, 9/2021 ' 
  write(6,*) 
end subroutine header

subroutine setup
      implicit none
      nosym=.true.
end subroutine setup

subroutine driver
       implicit none
       real*8 :: E_old,delta_P
       real*8 :: delta_E,pp
       integer :: i,j,iter       

       write(6,*) 
real*8 :: dmin
       if (nucl_num.gt.1) then
       dmin=1.D12
       do i=1,nucl_num
        do j=i+1,nucl_num
         dmin=min(dmin,nucl_dist(i,j))
        end do
       end do
       write(6,*) ' Minimum distance beween atoms found is ',dmin,' Bohr'
       write(6,*) 
       write(6,*) 
       end if
       
!      write(6,*) ' basis set information '
!      do i=1,ao_num
!       write(6,*) ' ao_l = ',i,ao_l(i)
!      end do

       delta_e=huge(1.d0)
       delta_p=huge(1.d0)
       do while (delta_E.ge.thr_scf.or.delta_P.ge.thr_P)

       write(6,*) 
       write(6,*)  ' ============================================ '
       write(6,*)  '    Iteration No ',iter_scf
       write(6,*)  ' ============================================ '
       write(6,*)  '                Iteration No ',iter_scf
       write(6,*)  ' ============================================ '
       write(6,*) 
       e_old=total_energy

       delta_P=0.d0
       do i=1,block_1Dmat_dim
        pp=p_next(i)-density(i)
        delta_P+=pp*pp
       end do
       delta_P=delta_P/block_1Dmat_dim
       
       total_energy = nuclear_repulsion + e_one + e_two
       delta_e=abs(total_energy-e_old)
       write(6,*) ' nuclear repulsion   is ',nuclear_repulsion
       write(6,*) ' one-electron energy is ',e_one
       write(6,*) ' two-electron energy is ',e_two
       write(6,*) ' total energy is ',total_energy
       write(6,*) ' iter ',iter_scf, 'delta_E:', total_energy - E_old
       write(6,*) ' Delta P :',delta_P
       iter_scf=iter_scf+1

! the optimal damping algorithm of Eric Cancès and Claude Le Bris
       if (iter_scf.gt.2) then
real*8 :: a1,a2,b1,b2,b3
        a1=0.D0
        a2=0.D0
        b1=0.D0
        b2=0.D0
        b3=0.D0
        integer :: alpha,beta,addr1D,blocked1Daddress,ioper
!       do  ioper=1,noper
!        do alpha=1,norb_sym_cart(ioper)
!         do beta=1,norb_sym_cart(ioper)
!          addr1D=blocked1Daddress(i,j,ioper)
!          a1+=sym_one_e_integrals(addr1D)*density(addr1D)
!          a2+=sym_one_e_integrals(addr1D)*p_next(addr1D)
!          b1+=xi_old(addr1D)*density(addr1D)
!          b2+=xi_old(addr1D)*p_next(addr1D)
!          b3+=xi(addr1D)*p_next(addr1D)
!         end do
!        end do
!       end do
real*8 :: blocked_double_contract
        a1=blocked_double_contract(sym_one_e_integrals,density)
        a2=blocked_double_contract(sym_one_e_integrals,p_next)
        b1=blocked_double_contract(xi_old,density)
        b2=blocked_double_contract(xi_old,p_next)
        b3=blocked_double_contract(xi,p_next)


        b1*=0.5D0
        b3*=0.5D0
real*8 :: oda_denom
        oda_denom=b1-b2+b3
        write(6,*) ' Optimal Damping Algorithm of Cances et al.'
        if ((oda_denom).lt.0.D0) then
         lambda_oda=1.D0
        else
         if (abs(oda_denom).lt.1.D-12) then
          lambda_oda=0.5D0
         else
          lambda_oda=-(a2-a1+b2-2.D0*b1)/oda_denom*0.5D0
         end if
        end if
        write(6,*) ' optimal lambda is ',lambda_oda
        if (lambda_oda.lt.0.D0.or.lambda_oda.gt.1.D0) then
         write(6,*) ' lambda outside [0,1], setting lambda to 1 '
         lambda_oda=1.0D0
       end if
       else
        lambda_oda=1.D0
       end if

       density = p_next
       xi_old=xi

       touch density

       if (iter_scf.gt.max_iter) then
        write(6,*) ' max number of iterations ',max_iter,' attained '
        exit
       end if

       end do

       total_energy = nuclear_repulsion + e_one + e_two
       write(6,*) ' nuclear repulsion   is ',nuclear_repulsion
       write(6,*) ' one-electron energy is ',e_one
       write(6,*) ' two-electron energy is ',e_two
       write(6,*) 
       write(6,*) ' total energy is ',total_energy
 
       
       write(6,9904) total_energy,delta_E,delta_P,iter_scf
9904   format(//,70('='),//,' converged solution : E_HF = ',F20.12,/, & 
            ' Delta E = ',E15.5,'  delta_P  =  ',E15.5,/, &
            ' at iteration No ',i4,//)
!!$
!!$         mo_label = "Canonical"
!!$         mo_coef=ci
!!$         call save_mos
!!$         write(6,*) ' saving orbitals,label =  ',mo_label
!!$         write(6,*) ' all done '

end subroutine driver



