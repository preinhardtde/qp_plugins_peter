! -*- F90 -*-
BEGIN_PROVIDER [real*8, oneham_frag, (ao_num,ao_num,nfrag)]
BEGIN_DOC
! one-electron hamiltonians for each fragment, in the basis
! of all AOs
END_DOC
        implicit none
        integer :: i,j,ifrag,iat

        oneham_frag=0.D0

        do iat=1,nucl_num
         ifrag=atom_list_frag(iat)
         do i=1,ao_num
          do j=1,ao_num
           oneham_frag(i,j,ifrag)+=ao_integrals_n_e_per_atom(i,j,iat)*nucl_charge(iat)
          end do
         end do
        end do

        write(6,*) ' oneham_frag 1e-integrals ready '
         
END_PROVIDER
