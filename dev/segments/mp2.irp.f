! -*- F90 -*-
BEGIN_PROVIDER [logical, lmp2]
    lmp2=.false.
    lmp2=.true.
END_PROVIDER

BEGIN_PROVIDER [real*8, emp2_frag, (nfrag,2)]
BEGIN_DOC
! MP2 correlation energies of the individual fragments
END_DOC
         implicit none
         emp2_frag=0.D0
         no_vvvv_integrals = .True.
END_PROVIDER

BEGIN_PROVIDER [real*8, emp2tot]
BEGIN_DOC
! MP2 correlation energy of the whole molecule
END_DOC
         implicit none
         emp2tot=0.D0
         no_vvvv_integrals = .True.
END_PROVIDER

BEGIN_PROVIDER [real*8, DEMP2, (2)]
BEGIN_DOC
! MP2 contribution to the interaction energy 
END_DOC
      implicit none
      integer :: irun,ifrag
      do  irun=1,2
       deMP2(irun)=eMP2tot
       do ifrag=1,nfrag
        deMP2(irun)-= eMP2_frag(ifrag,irun)
       end do
      end do
END_PROVIDER

