! -*- F90 -*-
      subroutine shalf(start_indx,end_indx,orbs)
BEGIN_DOC
! orthogonalize by S^(-1/2) the set of orbitals between start and end
END_DOC
         implicit none
         real*8 :: orbs(ao_num,mo_num),ss
         integer :: alpha,i,j,beta,start_indx,end_indx,norb
         real*8, allocatable :: overlap_shalf(:,:)
         real*8, allocatable :: evals_shalf(:)
         real*8, allocatable :: evecs_shalf(:,:)
         real*8, allocatable :: cwrk_ortho(:,:)
         real*8, allocatable :: orb_tmp(:,:)

         real*8 :: cpu_0,cpu_1

         norb=end_indx-start_indx+1
         write(6,*) ' Shalf ',start_indx,' - ',end_indx,' = ',norb,' orbitals '
!
         allocate(overlap_shalf(norb,norb))
         allocate(evals_shalf(norb))
         allocate(evecs_shalf(norb,norb))
         allocate(cwrk_ortho(norb,norb))
         allocate(orb_tmp(ao_num,norb))

integer :: ii,jj
         do ii=1,norb
          i=start_indx+ii-1
          do alpha=1,ao_num
           orb_tmp(alpha,ii)=orbs(alpha,i)
          end do
         end do

         call transf2sq(ao_overlap,overlap_shalf,norb,orb_tmp)

!        do ii=1,norb
!         i=start_indx+ii-1
!         do jj=1,norb
!          j=start_indx+jj-1
!          ss=0.D0
!          do alpha=1,ao_num
!           do beta=1,ao_num
!            ss+=orbs(alpha,i)*orbs(beta,j)*ao_overlap(alpha,beta)
!           end do
!          end do
!          overlap_shalf(ii,jj)=ss
!         end do
!        end do

         call lapack_diag(evals_shalf,evecs_shalf,overlap_shalf,norb,norb)
if (bavard) then
         write(6,*) start_indx,norb
         write(6,*) ' eigenvalues '
         write(6,'(5(i5,F12.6))') (i,evals_shalf(i),i=1,norb)
end if

         if (evals_shalf(1).lt.1.D-12) then
          write(6,*) ' smallest eigenvalue for SHALF is ',evals_shalf(1)
          write(6,*) ' overlap matrix :'
          do i=1,norb
           do j=1,norb
            write(6,*) ' i,j,s ',i,j,overlap_shalf(i,j)
           end do
          end do
          stop ' Linear dependencies in SHALF '
         end if

         cwrk_ortho=0.D0
! form S^(-1/2)
integer :: k
         do k=1,norb
          ss=1.D0/sqrt(evals_shalf(k))
          do i=1,norb
           do j=1,norb
            cwrk_ortho(i,j)+=evecs_shalf(i,k)*evecs_shalf(j,k)*ss
           end do
          end do
         end do
! transform the orbitals
         do alpha=1,ao_num
          do i=1,norb
           evals_shalf(i)=orbs(alpha,start_indx+i-1)
           orbs(alpha,start_indx+i-1)=0.D0
          end do
          do i=1,norb
           do j=1,norb
            orbs(alpha,start_indx+i-1)+=evals_shalf(j)*cwrk_ortho(i,j)
           end do
          end do
         end do
                                                             
! test orthogonality
!        if (bavard) then
!           write(6,*) ' verifying orthonormality'
!        end if

!        do ii=1,norb
!         i=start_indx+ii-1
!         do alpha=1,ao_num
!          orb_tmp(alpha,ii)=orbs(alpha,i)
!         end do
!        end do

!        call transf2sq(ao_overlap,overlap_shalf,norb,orb_tmp)

!        do ii=1,norb
!         if (abs(overlap_shalf(ii,ii)-1.D0).gt.1.D-5) write(6,*) ' ii, ss ',ii,overlap_shalf(ii,ii)
!         do jj=ii+1,norb
!          if (abs(overlap_shalf(ii,jj)).gt.1.D-5) write(6,*) ' ii, jj, ss ',ii,jj,overlap_shalf(ii,ii)
!         end do
!        end do

!        if (bavard) then
!         write(6,*) ' done '
!        end if
       deallocate(overlap_shalf)
       deallocate(evals_shalf)
       deallocate(evecs_shalf)
       deallocate(cwrk_ortho)
       deallocate(orb_tmp)

      end subroutine shalf

      SUBROUTINE ORTHO(vector)
        implicit none
        real*8 :: vector(ao_num,mo_num)
        write(6,*) ' orthogonalizing orbitals'

!
! we orthogonalize the given set of orbitals
!
        CALL SHALF(1,nocc,vector)
        CALL SFULL(vector,nocc)
        CALL SHALF(nocc+1,mo_num,vector)
!
      END SUBROUTINE ORTHO

      SUBROUTINE ORTHO_frag(vector,n)
        implicit none
        real*8 :: vector(ao_num,mo_num)
        integer :: n
        write(6,*) ' orthogonalizing orbitals, nocc= ',n

!
! we orthogonalize the given set of orbitals
!
        CALL SHALF(1,n,vector)
        CALL SFULL(vector,n)
        CALL SHALF(n+1,mo_num,vector)
!
! verify orthonormality of the ensemble
!
! integer :: i,j,alpha,beta
! real*8 :: ss
!       write(6,*) ' verifying orthonormality for the whole space '
!       do i=1,mo_num
!        do j=i,mo_num
!         ss=0.D0
!         do alpha=1,ao_num
!          do beta=1,ao_num
!           ss+=vector(alpha,i)*vector(beta,j)*ao_overlap(alpha,beta)
!          end do
!         end do
!         if (i.eq.j) then
!          if (abs(ss-1.D0).gt.1.D-5) write(6,*) ' problem for diagonal element ',i,ss
!         else
!          if (abs(ss).gt.1.D-5) write(6,*) ' problem for element ',i,j,ss
!         end if
!        end do
!       end do
!       write(6,*) ' done '
!       write(6,*)
!
      END SUBROUTINE ORTHO_frag

      subroutine sfull(vector,n)
!
! we project the N occupied orbitals from the mo_num-n virtuals
!
        implicit none
        real*8 :: vector(ao_num,mo_num),eps,sss,ccc
        integer :: iter,nov,i,j,k,alpha,beta,n

        real*8, allocatable :: c3_ortho(:)
        real*8, allocatable :: cwrk_ortho(:,:)
        allocate(c3_ortho(n))
        allocate(cwrk_ortho(mo_num,mo_num))

        eps=1.d-10

        call transf_ao_mo(ao_overlap,ao_num,cwrk_ortho,mo_num,vector)
!
! the corrected functions
!
        iter=0
        nov=n+1
! with iterative refinement
100     continue
        do i=nov,mo_num
         do j=1,n
          c3_ortho(j)=cwrk_ortho(i,j)
         end do
         do k=1,ao_num
          ccc=0.d0
          do j=1,n
           ccc=ccc+vector(k,j)*c3_ortho(j)
          end do
          vector(k,i)=vector(k,i)-ccc
         end do
        end do
!
        call transf_ao_mo(ao_overlap,ao_num,cwrk_ortho,mo_num,vector)
!
! calculate new overlaps
        sss=0.d0
        iter=iter+1
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (i,j) SHARED (mo_num,n,cwrk_ortho,sss)
        do i=nov,mo_num
         do j=1,n
          sss=sss+cwrk_ortho(i,j)*cwrk_ortho(i,j)
         end do
        end do
!$XXYXXY END PARALLEL DO
        sss=sqrt(sss)/((mo_num-n)*n)
        write(6,*) ' iteration, sss:',iter,sss
        if (sss.gt.eps.and.iter.le.100) go to 100
!
! renormalize new virtual orbitals
!
        do i=nov,mo_num
         sss=0.d0
         do j=1,ao_num
          ccc=vector(j,i)
          do k=1,ao_num
           sss=sss+ao_overlap(j,k)*ccc*vector(k,i)
          end do
         end do
         sss=sqrt(sss)
         do j=1,ao_num
          vector(j,i)=vector(j,i)/sss
         end do
        end do
        deallocate(c3_ortho)
        deallocate(cwrk_ortho)

      end subroutine sfull

