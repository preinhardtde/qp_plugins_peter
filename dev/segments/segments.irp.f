! -*- F90 -*-
program segments
  implicit none
  BEGIN_DOC
! define fragments
! calculate canonical Hartree-Fock per fragment, without knowledge of 
!     other fragments
! keep virtual orbitals
! calculate monomers in the dimer basis - keep occupied orbitals
! calculte multimer
  END_DOC
         real*8 :: cpu_0,cpu_1,wall_0,wall_1
         call cpu_time(cpu_0)
         call wall_time(wall_0)
  call header
  call hueckel_guess_frag
  call driver
         call cpustamp(cpu_0,wall_0,cpu_0,wall_0,'ALL ')

end program segments

     subroutine header
         implicit none
         write(6,*) 
         write(6,*) ' the triple jump within the QP2 '
         write(6,*) ' P Reinhardt, Paris, May 2020 '
         write(6,*) ' in confinement we trust ' 
         write(6,*) 
     end subroutine header

     subroutine driver
         implicit none
         integer :: i,j,ifrag

         write(6,*) 
         write(6,*) ' S U M M A R Y '
         write(6,*) '==============='
         write(6,*) 
         do i=1,nfrag
          write(6,*) ' Fragment No ',i
          write(6,*) ' number of atoms    ',natom_frag(i)
          write(6,*) ' number of AOs      ',ao_num_frag(i)
          write(6,*) ' starting at AO No  ',ao_start_frag(i)
          write(6,*) ' number of MOs      ',mo_num_frag(i)
          write(6,*) ' starting at MO No  ',mo_start_frag(i)
          write(6,*) ' number of occupied orbitals ',nocc_frag(i)
          write(6,*) ' nuclear repulsion within    ',en_frag(i)
          write(6,*)
         end do
if (bavard) then
         write(6,*)
         write(6,*) '  inverse lists '
         write(6,*) ' AOs    ',(ao_list_frag(i),i=1,ao_num)
         write(6,*) ' nuclei ',(atom_list_frag(i),i=1,nucl_num)
         write(6,*)
end if

         write(6,*) ' orbital attribution :',(frag_orb(i),i=1,nocc)
real*8 :: pop_decomp(6),ss
integer :: alpha,beta,ii
! decompose the fragment populations into MM,MS,MO,SS,SO,OO
         write(6,*)
         write(6,*) ' fragment populations '
         write(6,*) ' frag ',('  ',chtype(ii),'    ',ii=1,6)
         do ifrag=1,nfrag
          pop_decomp=0
          do ii=1,6
           ss=0.D0
           do alpha=1,ao_num
            do beta=1,ao_num
             ss+=density6(alpha,beta,ifrag,ii)*ao_overlap(alpha,beta)
            end do
           end do
           pop_decomp(ii)=ss
          end do
          write(6,'(i4,6F10.4)') ifrag,(pop_decomp(ii),ii=1,6)
         end do
            

! here we have to do (for the moment) the substraction of large numbers
! as we do not have any more the decomposition of the monomer
! contributions
! for a direct calculation of the orbital interaction part
!
integer :: irun
      do irun=1,2
       write(6,*)
       write(6,'(72(1H-))') 
       if (irun.eq.1) then
        write(6,*)
        write(6,*) ' Energy decomposition without counterpoise correction'
        write(6,*) '  (monomers calculated in the monomer basis) '
        write(6,*)
       else
        write(6,*)
        write(6,*) ' Energy decomposition with counterpoise correction'
        write(6,*) '  (monomers calculated in the dimer basis) '
        write(6,*)
       end if
       
       write(6,*) ' total energy          ',etot,' Hartree'
       write(6,*) ' sum of all fragments: ',esss(irun),' Hartree'
       write(6,*) ' interaction:          ',etot-esss,' as simple difference '

       write(6,9623) elstat(irun),elstat(irun)*kcal                  &
                    ,elstat(irun)/nfrag,elstat(irun)*kcal/nfrag      &
                    ,ehl(irun),kcal*ehl(irun)                        &
                    ,ehl(irun)/nfrag,kcal*ehl(irun)/nfrag            &
                    ,dtpauli(irun),kcal*dtpauli(irun)                &
                    ,dtpauli(irun)/nfrag,kcal*dtpauli(irun)/nfrag    &
                    ,dvpauli(irun),kcal*dvpauli(irun)                &
                    ,dvpauli(irun)/nfrag,kcal*dvpauli(irun)/nfrag
       if (lexex) then
        write(6,9632) exmul*dxpauli(irun),exmul*kcal*dxpauli(irun)   &
        ,exmul*dxpauli(irun)/nfrag,exmul/nfrag*kcal*dxpauli(irun)
       end if
       write(6,9633) dxcpauli(irun),kcal*dxcpauli(irun)              &
                    ,dxcpauli(irun)/nfrag,kcal/nfrag*dxcpauli(irun)  &
             ,eoi(irun),kcal*eoi(irun)                               &
             ,eoi(irun)/nfrag,kcal/nfrag*eoi(irun)                   &
             ,dtoi(irun),kcal*dtoi(irun)                             &
             ,dtoi(irun)/nfrag,kcal/nfrag*dtoi(irun)                 &
             ,dvoi(irun),kcal*dvoi(irun)                             &
             ,dvoi(irun)/nfrag,kcal/nfrag*dvoi(irun)
       if (lexex) then
        write(6,9634) exmul*dxoi(irun),exmul*kcal*dxoi(irun)         &
               ,exmul/nfrag*dxoi(irun),exmul/nfrag*kcal*dxoi(irun)
       end if
       write(6,9635) dxcoi(irun),dxcoi(irun)*kcal                    &
              ,dxcoi(irun)/nfrag,dxcoi(irun)/nfrag*kcal              &
            ,esum(irun),kcal*esum(irun)                              &
            ,esum(irun)/nfrag,kcal/nfrag*esum(irun)                  &
            ,eint(irun),kcal*eint(irun)                              &
            ,eint(irun)/nfrag,kcal/nfrag*eint(irun)
       write(6,*) ' numerical error : ',esum(irun)-eint(irun),' a.u. ' &
            ,kcal*(esum(irun)-eint(irun)),' kcal/mol'
       if (lmp2) then
        write(6,9636) demp2(irun),demp2(irun)*kcal                   &
                     ,demp2(irun)/nfrag,demp2(irun)/nfrag*kcal
        if (lccsdt) write(6,9637) deccsdt(irun),kcal*deccsdt(irun)
       end if
      end do

      write(6,9624) ebsse,ebsse*kcal                              &
                   ,ebsse/nfrag,ebsse*kcal/nfrag                  &  
                   ,esbsse,esbsse*kcal                            &
                   ,esbsse/nfrag,esbsse/nfrag*kcal

 9623 FORMAT(/,' The triple decomposition of the interaction energy:',    &
           /,50X,'        total system         per fragment '             &
           /,50X,' a.u.         kcal/mol      a.u.           kcal/mol',/  &
           ,'  1)  electrostatic interaction of monomers ',2(F14.8,F14.4) &
           ,//,'  2)  Heitler-London exchange repulsion     ',2(F14.8     &
           ,F14.4),//,10X,'Delta T (kinetic energy)',10X                  &
           ,2(F14.8,F14.4),/                                              &
           ,10X,'Delta V (electrostatic)',11X,2(F14.8,F14.4)) 
 9632 FORMAT(10X,'Exchange',26X,2(F14.8,F14.4))
 9633 FORMAT(10X,'DFT XC',28X,2(F14.8,F14.4)                                &
           ,//,'  3)  final relaxation or Orb. Interaction  ',2(F14.8,F14.4) &
              ,//,10X,'Delta T (kinetic energy)',10X,2(F14.8,F14.4)         &
            ,/,10X,'Delta V (electrostatic)',11X,2(F14.8,F14.4))
 9634 FORMAT(10X,'Exchange',26X,2(F14.8,F14.4))
 9635 FORMAT(10X,'DFT XC',28X,2(F14.8,F14.4)                                 & 
           ,//,'  the sum of all these:',21X,2(F14.8,F14.4)                  &
           ,//,'  interaction energy (direct difference)',4X,2(F14.8,F14.4))
 9636 FORMAT(//,10X,'MP2 contribution to add',11X,2(F14.8,F14.4),/)
 9637 format(/,'    CCSD(T) contribution to add',13X,2(F14.8,F14.4),/) 
 9624 FORMAT(/,'  BSSE correction',27X,2(F14.8,F14.4)                        &
            ,/,'  interaction without BSSE correction',7X,2(F14.8,F14.4),/)

     end subroutine driver

