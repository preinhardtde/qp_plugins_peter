! -*- F90 -*-
BEGIN_PROVIDER [integer, nfrag]
BEGIN_DOC
! number of fragments, read from file
END_DOC
     implicit none 
     open (unit=12,file='FRAGMENTS',status='old',form='formatted')
     read(12,*) nfrag
     close (12)
     write(6,*) ' Defined ',nfrag,' fragments '
END_PROVIDER

BEGIN_PROVIDER [integer, natom_frag, (nfrag)]
&BEGIN_PROVIDER [integer, charge_frag, (nfrag)]
BEGIN_DOC
! how many atoms per fragment ? read from file
END_DOC
     implicit none 
     integer :: i
     charge_frag=0
     open (unit=12,file='FRAGMENTS',status='old',form='formatted')
     read(12,*) i
     read(12,*) (natom_frag(i),i=1,nfrag)
     read(12,*,err=300,end=300) (charge_frag(i),i=1,nfrag)
     go to 400
 300 continue
     charge_frag=0
 400 continue
     close (12)
     write(6,*) ' Fragment  #atoms   charge '
     do i=1,nfrag
      write(6,9901) i,natom_frag(i),charge_frag(i)
 9901 format(i3,10X,i3,6X,i3)
     end do
END_PROVIDER

BEGIN_PROVIDER [integer, ao_list_frag, (ao_num)]
BEGIN_DOC
! the inverse list : to which fragment belongs ao i ?
END_DOC
     implicit none 
     integer :: i,ifrag,sumfrag

     ifrag=1
     sumfrag=ao_num_frag(1)
     do i=1,ao_num
      if (i.gt.sumfrag) then
       ifrag+=1
       sumfrag+=ao_num_frag(ifrag)
      end if
      ao_list_frag(i)=ifrag
     end do

END_PROVIDER

BEGIN_PROVIDER [integer, atom_list_frag, (nucl_num)]
BEGIN_DOC
! the inverse list : to which fragment belongs atom i ?
END_DOC
     implicit none 
     integer :: i,ifrag,sumfrag

     ifrag=1
     sumfrag=natom_frag(1)
     do i=1,nucl_num
      if (i.gt.sumfrag) then
       ifrag+=1
       sumfrag+=natom_frag(ifrag)
      end if
      atom_list_frag(i)=ifrag
     end do
END_PROVIDER
      

 BEGIN_PROVIDER [integer, ao_num_frag, (nfrag)]
&BEGIN_PROVIDER [integer, ao_start_frag, (nfrag)]
BEGIN_DOC
! how many AOs per fragment ? 
END_DOC
     implicit none 
     integer :: ifrag,iat,sumfrag,i
     do ifrag=1,nfrag
      ao_num_frag(ifrag)=0
     end do

     do iat=1,nucl_num
      ifrag=atom_list_frag(iat)
      ao_num_frag(ifrag)+=nucl_n_aos(iat)
     end do

     ao_start_frag(1)=1
     sumfrag=1
     do ifrag=2,nfrag
      sumfrag+=ao_num_frag(ifrag-1)
      ao_start_frag(ifrag)=sumfrag
     end do
END_PROVIDER

BEGIN_PROVIDER [integer, mo_num_frag, (nfrag)]
&BEGIN_PROVIDER [integer, mo_start_frag, (nfrag)]
BEGIN_DOC
! how many MOs per fragment ? 
END_DOC
     implicit none 
     integer :: ifrag,iat,sumfrag,i
     do ifrag=1,nfrag
      mo_num_frag(ifrag)=0 
     end do

    if (ao_cartesian) then
     do ifrag=1,nfrag
      mo_num_frag(ifrag)=ao_num_frag(ifrag)
     end do
    else
     do ifrag=1,nfrag
real*8 :: frac_mo_ao
      frac_mo_ao=0.D0
      do i=1,ao_num
       if (ao_list_frag(i).eq.ifrag) then
        frac_mo_ao+=dble(2*ao_l(i)+1)/dble((3+ao_l(i))*ao_l(i)/2+1)
       end if
      end do
      mo_num_frag(ifrag)=nint(frac_mo_ao)
     end do
    end if

     mo_start_frag(1)=1
     sumfrag=1
     do ifrag=2,nfrag
      sumfrag+=mo_num_frag(ifrag-1)
      mo_start_frag(ifrag)=sumfrag
     end do
END_PROVIDER

 BEGIN_PROVIDER [real*8,EN ]     
&BEGIN_PROVIDER [real*8, EN_frag,    (NFRAG)]
BEGIN_DOC
!
! nuclear repulsion, inside fragments and between all atoms
!
END_DOC
     implicit none
     integer :: i,j,ifrag
     real*8 :: chai
     en=nuclear_repulsion

     EN_frag=0.D0

     do i=1,nucl_num
      ifrag=atom_list_frag(i)
      chai=nucl_charge(i)
      do j=i+1,nucl_num
       if (ifrag.eq.atom_list_frag(j)) then
        EN_frag(ifrag)+=chai*nucl_charge(j)/nucl_dist(i,j)
       end if
      end do
     end do
END_PROVIDER

 BEGIN_PROVIDER [integer, nocc_frag, (nfrag)]
&BEGIN_PROVIDER [integer, nocc]
BEGIN_DOC
!
! number of occupied orbitals for each fragment
!
END_DOC
     implicit none
     integer :: ifrag,iat

     nocc=0
     nocc_frag=-charge_frag
     do iat=1,nucl_num
      nocc_frag(atom_list_frag(iat))+=nint(nucl_charge(iat))
     end do
     do ifrag=1,nfrag
      if (mod(nocc_frag(ifrag),2).eq.1) then
       write(6,*) ' Fragment No ',ifrag,' has an odd number of electrons '
       stop ' no open shells, please '
      end if
      nocc_frag(ifrag)=nocc_frag(ifrag)/2
      nocc+=nocc_frag(ifrag)
     end do
END_PROVIDER

