! -*- F90 -*-
subroutine hueckel_guess_frag
  implicit none
  BEGIN_DOC
! Build the MOs using the extended Huckel model
  END_DOC
  integer                        :: i,j,ifrag
  double precision               :: accu
  double precision               :: c
  character*(64)                 :: label
  double precision, allocatable  :: A(:,:)
  double precision, allocatable  :: evec_hueckel(:,:),eval_hueckel(:)
  logical :: hueckel_or_not
  integer :: iat
  label    = "Guess"
  mo_label = "Guess"
  c = 0.5d0 * 1.75d0

  hueckel_or_not=.false.

  allocate (A(ao_num, ao_num))
  allocate (evec_hueckel(mo_num, mo_num))
  allocate (eval_hueckel(mo_num))

! we have mo_num molecular orbitals in total
  orb_work=0.D0
  orb_work=ao_cart_to_sphe_coef

  if (hueckel_or_not) then

! we should parallelize, 
   write(6,*) ' Constructing Hueckel guess '

   A = 0.d0
   do j=1,ao_num
    ifrag=ao_list_frag(j)
    do i=1,ao_num
! if i and j belong to the same fragment, we take
     if (ao_list_frag(i).eq.ifrag) then
! we cannot use the one_e_integrals, as all nuclei are included
! we need the nuclei of the fragment only
!    A(i,j) = c * ao_overlap(i,j) * (ao_one_e_integrals_diag(i) + & 
!      ao_one_e_integrals_diag(j))
      A(i,j) = ao_kinetic_integrals(j,j)+ao_kinetic_integrals(j,j)
      do iat=1,nucl_num
       if (atom_list_frag(iat).eq.ifrag) then
        A(i,j) += nucl_charge(iat)*( ao_integrals_n_e_per_atom(i,i,iat) +  &
                      ao_integrals_n_e_per_atom(j,j,iat)) 
       end if 
      end do
      A(i,j) *= c * ao_overlap(i,j) 
     end if
    end do
!  A(j,j) = ao_one_e_integrals_diag(j) + ao_two_e_integral_alpha(j,j)
!  A(j,j) = ao_two_e_integral_alpha(j,j) + ao_kinetic_integrals(j,j)
! CCPR no bielectronic term yet
    A(j,j) =                                ao_kinetic_integrals(j,j)
    do iat=1,nucl_num
     if (atom_list_frag(iat).eq.ifrag) then
      A(j,j) += nucl_charge(iat)*ao_integrals_n_e_per_atom(j,j,iat)
     end if 
    end do
   end do
  else
   write(6,*) ' Constructing CoreHam guess '
   A = 0.d0
   do j=1,ao_num
    ifrag=ao_list_frag(j)
    do i=1,ao_num
! if i and j belong to the same fragment, we take
     if (ao_list_frag(i).eq.ifrag) then
! kinetic energy
      A(i,j) = ao_kinetic_integrals(i,j)
! nuclear potential
      do iat=1,nucl_num
       if (atom_list_frag(iat).eq.ifrag) then
        A(i,j) += nucl_charge(iat)*ao_integrals_n_e_per_atom(i,j,iat)
       end if 
      end do
     end if
    end do
   end do
  end if

! A is in blocks, and so is the matrix of orbitals. 
! we can thus store all in one matrix, and multiply for the AO -> MO transformation.
! this makes multiplying many blocks in one operation
! write(6,*) ' Hueckel matrix in AOs '
! do i=1,ao_num
!  do j=i,ao_num
!   if (abs(a(i,j)).gt.1.D-10) write(6,*) ' Hueckel AO ',i,j,A(i,j)
!  end do
! end do

! work block by block            
integer :: istart,iend
real*8 :: cpu_0,cpu_1,wall_0,wall_1
call wall_time(wall_0)
call cpu_time(cpu_0)
  do ifrag=1,nfrag
   istart=mo_start_frag(ifrag)
   iend  =mo_start_frag(ifrag)+mo_num_frag(ifrag)-1
! orthogonalize
   call shalf(istart,iend,orb_work)
  end do
  call cpustamp(cpu_0,wall_0,cpu_1,wall_1,'ORTH')

! transform the Fock-matrix blocks to the orthogonalized AOs
   call transf_ao_mo(A,ao_num,fock_mo,mo_num,orb_work)
!  do i=1,mo_num
!   do j=1,mo_num
!    if (abs(fock_mo(i,j)).gt.1.D-12) write(6,*) ' Hueckel, i , j , fock_mo ',i,j,fock_mo(i,j)
!   end do
!  end do

   A=fock_mo
   fock_mo=0.D0

integer :: imo,jmo
! diagonalize block by block
   write(6,*) ' Diagonalization of the Hueckel matrix for each fragment '
   do ifrag=1,nfrag
    write(6,*) ' fragment No ',ifrag
    integer :: offset
    offset=mo_start_frag(ifrag)-1
    do i=1,mo_num_frag(ifrag)
     imo=i+offset
     do j=1,mo_num_frag(ifrag)
      jmo=j+offset
      fock_mo(i,j)=A(imo,jmo)
     end do
    end do
    call lapack_diag(eval_hueckel,evec_hueckel,fock_mo,mo_num,mo_num_frag(ifrag))
if (bavard) then
    write(6,'(4(i4,F15.5))') (i,eval_hueckel(i),i=1,mo_num_frag(ifrag))
end if
! update the orbitals

! as well block per block
    do alpha=1,ao_num
     do i=1,mo_num_frag(ifrag)
      eval_hueckel(i)=orb_work(alpha,i+offset)
      orb_work(alpha,i+offset)=0.D0
     end do
     do i=1,mo_num_frag(ifrag)
      do j=1,mo_num_frag(ifrag)
       orb_work(alpha,i+offset)+=evec_hueckel(j,i)*eval_hueckel(j)
      end do
     end do
    end do
!   istart=offset+1
!   iend=offset+mo_num_frag(ifrag)
!   call shalf(istart,iend,orb_work)
!   write(6,*)
   end do

integer :: alpha
!  write(6,*) 
!  write(6,*) ' Starting orbitals '
!  do ifrag=1,nfrag
!   write(6,*) ' Fragment No ',ifrag
!   do i=1,mo_num_frag(ifrag)
!    write(6,*) '   Orbital No ',i
!    write(6,'(5(i4,F12.6))') (alpha,orb_work(alpha+ao_start_frag(ifrag)-1 &
!          ,i+mo_start_frag(ifrag)-1),alpha=1,ao_num_frag(ifrag))
!   end do
!   write(6,*) 
!  end do

 write(6,*) ' filling <density_frag> from the Hueckel orbitals '
integer :: beta,indx
   density_frag=0.D0
   do alpha=1,ao_num
    do beta=1,ao_num
     do ifrag=1,nfrag
      indx=mo_start_frag(ifrag)
      do i=1,nocc_frag(ifrag)
       density_frag(alpha,beta,ifrag)+=orb_work(alpha,indx)*orb_work(beta,indx)
       indx+=1
      end do
     end do
    end do
   end do
   density_frag*=2.D0
!  write(6,*) ' Hueckel density matrix '
!  do alpha=1,ao_num
!   ifrag=ao_list_frag(alpha)
!   do beta=1,ao_num
!    if (abs(density_frag(alpha,beta,ifrag)).gt.1.D-7) write(6,*) alpha,beta,density_frag(alpha,beta,ifrag)
!   end do
!  end do
! number of electrons per fragment
   pop=0.D0
   do alpha=1,ao_num
    ifrag=ao_list_frag(alpha)
    do beta=1,ao_num
     if (ifrag.eq.ao_list_frag(beta)) then
      pop(ifrag)+=density_frag(alpha,beta,ifrag)*ao_overlap(alpha,beta)
     end if
    end do
   end do


   write(6,9901) (pop(ifrag),ifrag=1,nfrag) 
 9901 format(' Population of fragments ',20F8.2)
     
   mo_label='Guess'
   mo_coef=orb_work
   call save_mos
   
   deallocate(A)
   deallocate (evec_hueckel)
   deallocate (eval_hueckel)

end subroutine huckel_guess_frag

BEGIN_PROVIDER [real*8, pop, (nfrag)]
END_PROVIDER
