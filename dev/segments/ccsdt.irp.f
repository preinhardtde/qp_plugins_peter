! -*- F90 -*-
BEGIN_PROVIDER [real*8, eccsdt_frag, (nfrag,2)]
BEGIN_DOC
! CCSD(T) correlation energies of the individual fragments
END_DOC
       implicit none
       eccsdt_frag=0.D0
END_PROVIDER

BEGIN_PROVIDER [real*8, eccsdtot]
BEGIN_DOC
! CCSD(T) correlation energy of the whole molecule
END_DOC
       implicit none
       eccsdtot=0.D0
END_PROVIDER

BEGIN_PROVIDER [real*8, DECCSDT, (2)]
BEGIN_DOC
! CCSD(T) contribution to the interaction energy 
END_DOC
      implicit none
      integer :: irun,ifrag
      do  irun=1,2
       deccsdt(irun)=eccsdtot
       do ifrag=1,nfrag
        deccsdt(irun)+=eccsdt_frag(ifrag,irun)
       end do
      end do
END_PROVIDER


