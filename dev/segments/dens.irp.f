! -*- F90 -*-
BEGIN_PROVIDER [ real*8, p_next, (ao_num,ao_num) ]
END_PROVIDER

 subroutine fill_p_next_1st
      implicit none
      integer :: ialph,ibeta,iorb,ifrag,offset
      real*8 :: cia

      write(6,*) ' constructing density p_next for the first step'

      p_next=0.d0

      do ifrag=1,nfrag
       offset=mo_start_frag(ifrag)-1
       do iorb=1,nocc_frag(ifrag)
        do ialph=1,ao_num
         cia=orb_work(ialph,iorb+offset)
         do ibeta=1,ao_num
          p_next(ialph,ibeta)+=cia*orb_work(ibeta,iorb+offset)
         end do
        end do
       end do
      end do

      p_next*=2.D0
 end subroutine fill_p_next_1st
 
 subroutine fill_p_next_3rd
      implicit none
      integer :: ialph,ibeta,iorb
      real*8 :: cia

      write(6,*) ' constructing density p_next for the third step'

      p_next=0.d0

      do iorb=1,nocc
       do ialph=1,ao_num
        cia=orb_work(ialph,iorb)
        do ibeta=1,ao_num
         p_next(ialph,ibeta)+=cia*orb_work(ibeta,iorb)
        end do
       end do
      end do

      p_next*=2.D0
 end subroutine fill_p_next_3rd
 

 subroutine fill_p_next_2nd
      implicit none
      integer :: alpha,beta,i,ifrag
      real*8 :: cia

      write(6,*) ' constructing density p_next_frag for the second step'

      p_next_frag=0.D0
      do ifrag=1,nfrag
       do alpha=1,ao_num
        do beta=1,ao_num
         do i=1,nocc_frag(ifrag)
          p_next_frag(alpha,beta,ifrag)+=orb_work_frag(alpha,i,ifrag)*orb_work_frag(beta,i,ifrag)
         end do
        end do
       end do
      end do
      p_next_frag*=2.D0

 end subroutine fill_p_next_2nd
 
 subroutine fill_p_next
      implicit none
      integer :: ialph,ibeta,iorb
      real*8 :: cia

      write(6,*) ' constructing density p_next '

      p_next=0.d0

      do iorb=1,nocc
       do ialph=1,ao_num
        cia=orb_work(ialph,iorb)
        do ibeta=1,ao_num
         p_next(ialph,ibeta)+=cia*orb_work(ibeta,iorb)
        end do
       end do
      end do

      p_next*=2.D0
 end subroutine fill_p_next
 

BEGIN_PROVIDER [ real*8, p_next_frag, (ao_num,ao_num,nfrag) ]
      implicit none
      integer :: ialph,ibeta,iorb,ifrag
      real*8 :: cia

      write(6,*) ' constructing density p_next_frag '

      p_next_frag=0.d0

      do ifrag=1,nfrag
       do iorb=1,nocc_frag(ifrag)
        do ialph=1,ao_num
         cia=orb_work_frag(ialph,iorb,ifrag)
         do ibeta=1,ao_num
          p_next_frag(ialph,ibeta,ifrag)+=cia*orb_work_frag(ibeta,iorb,ifrag)
         end do
        end do
       end do
      end do

      p_next_frag*=2.D0

END_PROVIDER

BEGIN_PROVIDER [ real*8, dens_old_frag, (ao_num,ao_num,nfrag) ]
END_PROVIDER

BEGIN_PROVIDER [ real*8, dens_old, (ao_num,ao_num) ]
END_PROVIDER
