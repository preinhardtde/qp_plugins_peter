! -*- F90 -*-
BEGIN_PROVIDER [logical, lexex]
    lexex=.true.
END_PROVIDER

BEGIN_PROVIDER [logical, l_Davidson]
    l_Davidson=.false.
    l_Davidson=.true.
END_PROVIDER

BEGIN_PROVIDER [real*8, exmul ]
BEGIN_DOC
! weight of the exact Hartree-Fock exchange in the energy expression
END_DOC
   exmul=1.D0
END_PROVIDER

BEGIN_PROVIDER [logical, lccsdt]
    lccsdt=.false.
END_PROVIDER

     subroutine cpustamp(cpu_in,wall_in,cpu_out,wall_out,label)
BEGIN_DOC
!
! CPU and wall time since incoming cpu and wall 
! returns cpu and wall of the moment
!
END_DOC
         implicit none
         real*8 :: cpu_in,wall_in,cpu_out,wall_out,cpu_tmp,wall_tmp
         character*4 :: label
 
         call cpu_time(cpu_tmp)
         call wall_time(wall_tmp)
         write(6,7702) label,cpu_tmp-cpu_in,wall_tmp-wall_in,(cpu_tmp-cpu_in)/(wall_tmp-wall_in+1.D-11)
  7702 format(A4,' --- CPU and Wall time:',2F14.6,' ratio:',F10.5)
         cpu_out=cpu_tmp
         wall_out=wall_tmp
     end subroutine cpustamp
