! -*- F90 -*-
BEGIN_PROVIDER [real*8, pop_frag, (nfrag) ]
END_PROVIDER 

 BEGIN_PROVIDER [integer, frag_orb, (nocc) ]
&BEGIN_PROVIDER [integer, attr_orb, (nocc) ]
&BEGIN_PROVIDER [real*8, orb_decomp_m, (ao_num,nocc)]
&BEGIN_PROVIDER [real*8, orb_decomp_s, (ao_num,nocc)]
&BEGIN_PROVIDER [real*8, orb_decomp_o, (ao_num,nocc)]
BEGIN_DOC
!
! decomposition of every occupied orbital into M + S + O
! |phi> = |phi_M><phi_M|phi> + |phi_S><phi_S|phi> + |phi_O><phi_O|phi> 
!
!
! by population analysis we attribute to each occupied orbital 
! the monomer where it is located
!
END_DOC
         implicit none
         integer :: i,ifrag,alpha,beta,j
         real*8 :: frag_max

         orb_decomp_m=0.d0
         orb_decomp_s=0.d0
         orb_decomp_o=0.d0

         write(6,*) ' distribution of orbitals on fragments '
         write(6,*) '  (Mulliken net populations) '
         do i=1,nocc
          write(6,*) ' Orbital No ',i
!         write(6,'(5(I5,F15.6))') (alpha,orb_save(alpha,i),alpha=1,ao_num)
          pop_frag=0.D0
          do alpha=1,ao_num
           ifrag=ao_list_frag(alpha)
           do beta=1,ao_num
            if (ifrag.eq.ao_list_frag(beta)) then
             pop_frag(ifrag)+=orb_save(alpha,i)*orb_save(beta,i)*ao_overlap(alpha,beta)
            end if
           end do
          end do
          frag_max=-2.D0
          do ifrag=1,nfrag
           if (pop_frag(ifrag).gt.frag_max) then
            frag_orb(i)=ifrag
            frag_max=pop_frag(ifrag)
           end if
          end do
          write(6,'(15(I3,F15.5))') (ifrag,pop_frag(ifrag),ifrag=1,nfrag)
          write(6,*) ' Orbital attributed to fragment No ',frag_orb(i)
! look for the maximum overlap with the ocupied orbitals 
real*8 :: ovmx,ovl
          ovmx=0.D0
          do j=1,nocc
! calculate overlap
           ovl=0.D0
           do alpha=1,ao_num
            do beta=1,ao_num
             ovl+=orb_save(alpha,i)*orb_occ(beta,j,2)*ao_overlap(alpha,beta)
            end do
           end do
!          write(6,*) ' overlap ',i,j,ovl
           if (abs(ovl).gt.abs(ovmx)) then
            ovmx=ovl
            attr_orb(i)=j
           end if
          end do
          if (ovmx.lt.0.D0) then
           ovmx*=-1.D0
           do alpha=1,ao_num
            orb_save(alpha,i)*=-1.D0
           end do
          end if
          write(6,*) '   Overlap with orbital ',attr_orb(i),' is ',ovmx
          do alpha=1,ao_num
           orb_decomp_m(alpha,i)=ovmx*orb_occ(alpha,attr_orb(i),2)
           if (ao_list_frag(alpha).eq.frag_orb(i)) then
            orb_decomp_s(alpha,i)=orb_work(alpha,i)-orb_decomp_m(alpha,i)
           else
            orb_decomp_o(alpha,i)=orb_work(alpha,i)-orb_decomp_m(alpha,i)
           end if
          end do
         end do
END_PROVIDER 

 BEGIN_PROVIDER [CHARACTER*4, CHTYPE,(6)]
&BEGIN_PROVIDER [CHARACTER*4, CRTYPE,(6)]
      CHTYPE(1)=' M-M'
      chtype(2)=' M-S'
      chtype(3)=' M-O'
      chtype(4)=' S-S'
      chtype(5)=' S-O'
      chtype(6)=' O-O'
      crtype(1)=' M-M'
      crtype(2)=' M-P'
      crtype(3)=' P-P'
      crtype(4)=' M-M'
      crtype(5)=' M-P'
      crtype(6)=' P-P'
END_PROVIDER

BEGIN_PROVIDER [real*8, density6, (ao_num,ao_num,nfrag,6)]
BEGIN_DOC
! decomposition of the total density in 6 parts: MM, MS, MO, SS, SO, OO
END_DOC
     implicit none 
     integer :: alpha,beta,ifrag,i
     density6=0.D0
     do i=1,nocc    
      ifrag=frag_orb(i)
      do alpha=1,ao_num
       do beta=1,ao_num
        density6(alpha,beta,ifrag,1)+=orb_decomp_m(alpha,i)*orb_decomp_m(beta,i)
        density6(alpha,beta,ifrag,2)+=orb_decomp_m(alpha,i)*orb_decomp_s(beta,i) &
                                 +orb_decomp_s(alpha,i)*orb_decomp_m(beta,i)
        density6(alpha,beta,ifrag,3)+=orb_decomp_m(alpha,i)*orb_decomp_o(beta,i) &
                                    +orb_decomp_o(alpha,i)*orb_decomp_m(beta,i)
        density6(alpha,beta,ifrag,4)+=orb_decomp_s(alpha,i)*orb_decomp_s(beta,i)
        density6(alpha,beta,ifrag,5)+=orb_decomp_s(alpha,i)*orb_decomp_o(beta,i) &
                                    +orb_decomp_o(alpha,i)*orb_decomp_s(beta,i)
        density6(alpha,beta,ifrag,6)+=orb_decomp_o(alpha,i)*orb_decomp_o(beta,i)
       end do
      end do
     end do
     density6*=2.D0
END_PROVIDER

 BEGIN_PROVIDER [real*8, Eint, (2) ]
&BEGIN_PROVIDER [real*8, EHL, (2)]
&BEGIN_PROVIDER [real*8, EOI, (2)]
&BEGIN_PROVIDER [real*8, ESUM, (2)]
&BEGIN_PROVIDER [real*8, ESSS, (2)]
&BEGIN_PROVIDER [real*8, EBSSE]
&BEGIN_PROVIDER [real*8,ESBsse]  
BEGIN_DOC
! different terms to the interaction energy
! direct difference
! BSSE, Heitler-London, Orbital Interaction
END_DOC
          implicit none
          integer :: ifrag,irun

          ebsse=0.D0
          do ifrag=1,nfrag
           EBSSE+=Etot_frag(ifrag,1)-etot_frag(ifrag,2)
          end do

          do irun=1,2
           esss(irun)=0.d0
           eint(irun)=etot
           do ifrag=1,nfrag
            eint(irun)=eint(irun)-etot_frag(ifrag,irun)
            esss(irun)=esss(irun)+etot_frag(ifrag,irun)
           end do
          end do

          do irun=1,2
           ehl(irun)=dtpauli(irun)+dvpauli(irun)+dxcpauli(irun)
           eoi(irun)=dtoi(irun)+dvoi(irun)+dxcoi(irun)
           if (lexex) then
            ehl(irun)=ehl(irun)+dxpauli(irun)*exmul
            eoi(irun)=eoi(irun)+dxoi(irun)*exmul
           end if
          end do

          do irun=1,2
           esum(irun)=elstat(irun)+ehl(irun)+eoi(irun)
          end do

          esbsse=esum(2)-ebsse
END_PROVIDER

 BEGIN_PROVIDER [real*8, DVOI,     (2)] 
&BEGIN_PROVIDER [real*8, DTOI,     (2)]  
&BEGIN_PROVIDER [real*8, DXOI,     (2)]  
&BEGIN_PROVIDER [real*8, DXCOI,    (2)] 
BEGIN_DOC
! the contributions to orbital interactions
END_DOC
        implicit none
        integer :: irun,ifrag

        do irun=1,2
         dvoi(irun) = en+ecoul+eone-ekin-dvpauli(irun)-elstat(irun)
         dtoi(irun) = ekin           -dtpauli(irun)
         dxoi(irun) = eexch          -dxpauli(irun)
         dxcoi(irun)= edftxc         -dxcpauli(irun)
         do ifrag=1,nfrag
          dtoi(irun) -=ekin_frag(ifrag,irun)
          dvoi(irun) += -ecoul_frag(ifrag,irun)-eone_frag(ifrag,irun)   &
               +ekin_frag(ifrag,irun)-en_frag(ifrag)
          dxoi(irun) -=  eexch_frag(ifrag,irun)
          dxcoi(irun)-= edfxc_frag(ifrag,irun)
         end do
        end do

END_PROVIDER
