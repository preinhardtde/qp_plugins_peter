! -*- F90 -*-
BEGIN_PROVIDER [ integer, max_iter ]
    max_iter=75 
END_PROVIDER

BEGIN_PROVIDER [ integer, iter_scf ]
      iter_scf=0
END_PROVIDER

BEGIN_PROVIDER [ real*8, thr_scf ]
      thr_scf=1.d-7
END_PROVIDER

BEGIN_PROVIDER [ real*8, thr_p ]
      thr_p=1.d-10
END_PROVIDER

BEGIN_PROVIDER [ real*8, lambda_oda ]
END_PROVIDER

BEGIN_PROVIDER [ real*8, lambda_oda_frag, (nfrag) ]
END_PROVIDER

BEGIN_PROVIDER [real*8, levelshift ]
   levelshift=0.D0
END_PROVIDER

BEGIN_PROVIDER [real*8, ci_scaling ]
   ci_scaling=1.00D0
END_PROVIDER

 BEGIN_PROVIDER [integer, maxdav]
&BEGIN_PROVIDER [real*8, thrdav]
   maxdav=25
   thrdav=1.D-8
END_PROVIDER


