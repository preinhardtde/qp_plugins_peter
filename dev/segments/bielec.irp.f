! -*- F90 -*-
BEGIN_PROVIDER [ real*8, xi_old, (ao_num, ao_num) ]
       xi_old=0.D0
END_PROVIDER

BEGIN_PROVIDER [ real*8, xi_old_frag, (ao_num, ao_num, nfrag) ]
       xi_old_frag=0.D0
END_PROVIDER

 BEGIN_PROVIDER [ real*8, xi_coul_frag, (ao_num, ao_num, nfrag) ]
&BEGIN_PROVIDER [ real*8, xi_exch_frag, (ao_num, ao_num, nfrag) ]
 use map_module
 implicit none
 BEGIN_DOC
 ! the 2-electron part of the Fock matrix per fragment
 ! we can use this for the 1st and the 2nd step
 END_DOC

 integer                        :: i,j,k,l,k1,r,s
 integer                        :: i0,j0,k0,l0,ifrag
 integer*8                      :: p,q
 double precision               :: integral, c0, c1, c2
 double precision               :: ao_two_e_integral, local_threshold
 double precision, allocatable  :: ao_two_e_integral_coul_frag(:,:,:)
 double precision, allocatable  :: ao_two_e_integral_exch_frag(:,:,:)

   xi_coul_frag = 0.d0
   xi_exch_frag = 0.d0
   write(6,*) ' constructing 2el part xi for all fragments'
   PROVIDE ao_two_e_integrals_in_map

   integer(omp_lock_kind) :: lck(ao_num)
   integer(map_size_kind)     :: i8
   integer                        :: ii(8), jj(8), kk(8), ll(8), k2
   integer(cache_map_size_kind)   :: n_elements_max, n_elements
   integer(key_kind), allocatable :: keys(:)
   double precision, allocatable  :: values(:)

   !$OMP PARALLEL DEFAULT(NONE)           &
   !$OMP PRIVATE(i,j,l,k1,k,integral,ii,jj,kk,ll,     &
   !$OMP  i8,keys,values,n_elements_max, &
   !$OMP  n_elements,ao_two_e_integral_exch_frag ,    &
   !$OMP  ao_two_e_integral_coul_frag,ifrag)  &
   !$OMP SHARED(nfrag,ao_num,density_frag,    &
   !$OMP  ao_integrals_map, xi_coul_frag, xi_exch_frag )

   call get_cache_map_n_elements_max(ao_integrals_map,n_elements_max)
   allocate(keys(n_elements_max), values(n_elements_max))
   allocate(ao_two_e_integral_exch_frag(ao_num,ao_num,nfrag))
   allocate(ao_two_e_integral_coul_frag(ao_num,ao_num,nfrag))
   ao_two_e_integral_coul_frag = 0.d0
   ao_two_e_integral_exch_frag = 0.d0

   !$OMP DO SCHEDULE(static,1)
   do i8=0_8,ao_integrals_map%map_size
     n_elements = n_elements_max
     call get_cache_map(ao_integrals_map,i8,keys,values,n_elements)
     do k1=1,n_elements
       call two_e_integrals_index_reverse(kk,ii,ll,jj,keys(k1))

       do k2=1,8
         if (kk(k2)==0) then
           cycle
         endif
         i = ii(k2)
         j = jj(k2)
         k = kk(k2)
         l = ll(k2)
         do ifrag=1,nfrag
          integral = density_frag(k,l,ifrag) * values(k1)
          ao_two_e_integral_coul_frag(i,j,ifrag) += integral
          integral = values(k1)
          ao_two_e_integral_exch_frag(l,j,ifrag) -= 0.5D0*density_frag(k,i,ifrag) * integral
         end do
       enddo
     enddo
   enddo
   !$OMP END DO NOWAIT
   !$OMP CRITICAL
   xi_coul_frag += ao_two_e_integral_coul_frag
   xi_exch_frag += ao_two_e_integral_exch_frag
   !$OMP END CRITICAL
   deallocate(keys,values,ao_two_e_integral_exch_frag,ao_two_e_integral_coul_frag)
   !$OMP END PARALLEL

END_PROVIDER

 BEGIN_PROVIDER [ real*8, xi_coul3, (ao_num, ao_num) ]
&BEGIN_PROVIDER [ real*8, xi_exch3, (ao_num, ao_num) ]
 use map_module
 implicit none
 BEGIN_DOC
 ! the 2-electron part of the Fock matrix for the whole system
 ! we use this for 3rd step
 END_DOC

 integer                        :: i,j,k,l,k1,r,s
 integer                        :: i0,j0,k0,l0,ifrag
 integer*8                      :: p,q
 double precision               :: integral, c0, c1, c2
 double precision               :: ao_two_e_integral, local_threshold
 double precision, allocatable  :: ao_two_e_integral_coul(:,:)
 double precision, allocatable  :: ao_two_e_integral_exch(:,:)

   xi_coul3 = 0.d0
   xi_exch3 = 0.d0
   write(6,*) ' constructing 2el part xi for the whole molecule'
   PROVIDE ao_two_e_integrals_in_map

   integer(omp_lock_kind) :: lck(ao_num)
   integer(map_size_kind)     :: i8
   integer                        :: ii(8), jj(8), kk(8), ll(8), k2
   integer(cache_map_size_kind)   :: n_elements_max, n_elements
   integer(key_kind), allocatable :: keys(:)
   double precision, allocatable  :: values(:)

   !$OMP PARALLEL DEFAULT(NONE)           &
   !$OMP PRIVATE(i,j,l,k1,k,integral,ii,jj,kk,ll,     &
   !$OMP  i8,keys,values,n_elements_max, &
   !$OMP  n_elements,ao_two_e_integral_exch ,    &
   !$OMP  ao_two_e_integral_coul)  &
   !$OMP SHARED(ao_num,density3,    &
   !$OMP  ao_integrals_map, xi_coul3, xi_exch3 )

   call get_cache_map_n_elements_max(ao_integrals_map,n_elements_max)
   allocate(keys(n_elements_max), values(n_elements_max))
   allocate(ao_two_e_integral_exch(ao_num,ao_num))
   allocate(ao_two_e_integral_coul(ao_num,ao_num))
   ao_two_e_integral_coul = 0.d0
   ao_two_e_integral_exch = 0.d0

   !$OMP DO SCHEDULE(static,1)
   do i8=0_8,ao_integrals_map%map_size
     n_elements = n_elements_max
     call get_cache_map(ao_integrals_map,i8,keys,values,n_elements)
     do k1=1,n_elements
       call two_e_integrals_index_reverse(kk,ii,ll,jj,keys(k1))

       do k2=1,8
         if (kk(k2)==0) then
           cycle
         endif
         i = ii(k2)
         j = jj(k2)
         k = kk(k2)
         l = ll(k2)
          integral = density3(k,l) * values(k1)
          ao_two_e_integral_coul(i,j) += integral
          integral = values(k1)
          ao_two_e_integral_exch(l,j) -= 0.5D0*density3(k,i) * integral
       enddo
     enddo
   enddo
   !$OMP END DO NOWAIT
   !$OMP CRITICAL
   xi_coul3 += ao_two_e_integral_coul
   xi_exch3 += ao_two_e_integral_exch
   !$OMP END CRITICAL
   deallocate(keys,values,ao_two_e_integral_exch,ao_two_e_integral_coul)
   !$OMP END PARALLEL

END_PROVIDER

