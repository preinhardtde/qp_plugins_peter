! -*- F90 -*-
      subroutine davidson_general(vector,diagonal,eval_dav,ndim)
BEGIN_DOC
!
! generalized Davidson routine
! we need the diagonal of the matrix H, one starting vector x0
! the dimension of the problem, and the name of the function 
! to perform H.x
! we give backthe best vector and the eigenvalue.
END_DOC
         implicit none
         integer :: ndim
         real*8 :: diagonal(ndim),vector(ndim),eval_dav,u_dot_v

         real*8 :: thr_david,avec,zero,sumq,alp,aval
         real*8, allocatable :: hvect_dav(:)
         real*8, allocatable :: qvect_dav(:)

         allocate(hvect_dav(ndim))
         allocate(qvect_dav(ndim))
!
!     diagonalization subroutine for large symmetric matrices
!     E. R. Davidson, J. Comp. Phys 17 (185) 87
!
! structure from the casdi program of D. Maynau, completely rewritten
!
         write(6,*) ' entering the Davidson procedure, ndim =  ' , ndim

!        write(6,*) ' diagonal of the matrix '
!        write(6,'(5(I4,F15.5))') (i,diagonal(i),i=1,ndim)
!        write(6,*) ' starting vector '
!        write(6,'(5(I4,F15.5))') (i,vector(i),i=1,ndim)
 
10       format (3x,'iter=',i3,'  eigenvalue=',f17.12,', convergence =',d10.2)

         zero=0.d0
         thr_david=thrdav
!
! we open 2 direct-access files
!
         iunit1=76
         iunit2=87
         open(unit=iunit1,file='/dev/shm/VECTOR.TMP',status='unknown',access='direct', &
              recl=ndim*8)
         open(unit=iunit2,file='/dev/shm/HVECT.TMP',status='unknown',access='direct' &
              ,recl=ndim*8)
!
         iter=1

         call vnorm(vector,ndim,1)
!        write(6,*) ' starting vector '
!        write(6,'(5(I4,F15.5))') (i,vector(i),i=1,ndim)
         hvect_dav=0.d0
!
         call hx_sci(vector,hvect_dav,ndim)
!        write(6,*) ' result of H.x '
!        write(6,'(5(I4,F15.5))') (i,hvect_dav(i),i=1,ndim)
         call putv_dav(vector,1,iunit1,ndim)
         call putv_dav(hvect_dav,1,iunit2,ndim)
         babl=u_dot_v(vector,hvect_dav,ndim)

         bab_dav(1,1)=babl
!
integer :: ll,m,iunit1,iunit2,j,l,i,iq,iter
real*8 :: sum,babl,diffi
logical :: loop
         m=1
         loop=.true.
!
!     diagonalization of bab ... eigenvalues alam, eigenvectors alpha_dav
!
! this is the iteration loop
!
         do while (loop)
!         write(6,*) ' Davidson .... m=',m
!
! this small matrix bab has to be diagonalized
!
!         write(6,*) ' the BAB matrix '
          do i=1,m
           do j=i+1,m
            bab_dav(i,j)=bab_dav(j,i)
           end do
          end do

          call lapack_diag(alam_dav,alpha_dav,bab_dav,maxdav,m)

!     sets up the correction vector q
!     and performs the convergence test
!
! we need only the lowest eigenvalue
          call getv_dav(vector,1,iunit1,ndim)
          call getv_dav(hvect_dav,1,iunit2,ndim)
          aval=alam_dav(1)
          avec=alpha_dav(1,1)
          do i=1,ndim 
           diffi=hvect_dav(i)-aval*vector(i)
           qvect_dav(i)=avec*diffi
          end do
          do l=2,m
           call getv_dav(vector,l,iunit1,ndim)
           call getv_dav(hvect_dav,l,iunit2,ndim)
! ... and the corresponding eigenvector
           avec=alpha_dav(l,1)
           do i=1,ndim
            diffi=hvect_dav(i)-aval*vector(i)
            qvect_dav(i)=qvect_dav(i)+avec*diffi
           end do
          end do
!
! convergence?
          sumq=u_dot_v(qvect_dav,qvect_dav,ndim)
          sumq=sqrt(sumq/ndim)

          write (6,10) iter,alam_dav(1),sumq
          if (sumq.lt.thr_david) exit
!         if (iter.eq.1) then
!  continue
!         else
!          if (iter.eq.2) then
!  continue
!          else
            if (iter.gt.2) then
             if (sumq.lt.thr_david) then
              loop=.false.
              write(6,*) ' Davidson procedure converged '
             end if
            end if
!          end if
                              
           if (iter.eq.maxdav) then
!
!     if m overflows, we return 
!
           loop=.false.
           write(6,*) ' WARNING  WARNING  WARNING  WARNING  WARNING  WARNING '
           write(6,*) '    NO CONVERGENCE within ',maxdav,' iterations'
           write(6,*) ' WARNING  WARNING  WARNING  WARNING  WARNING  WARNING '
          else

           iter=iter+1
!
!     computes the vector b(m+1)  to enlarge the b subspace
!
!     q contains (h-e_0)b
!
           iq=0
           do i=1,ndim
            iq=iq+1
            if (abs(qvect_dav(iq)).le.1.D-22) then
             qvect_dav(iq)=zero
            else
             if(dabs(alam_dav(1)-diagonal(i)).lt.1.d-15) then
              write(6,*)'quasi degenerescence',alam_dav(1),diagonal(i)
              qvect_dav(iq)=qvect_dav(iq)/(alam_dav(1)-diagonal(i)-0.01)
             else
              qvect_dav(iq)=qvect_dav(iq)/(alam_dav(1)-diagonal(i))
             end if
            end if
           end do
!
! --   orthogonalisation de b(m+1) aux b(1)...b(m)
!
!      write(6,*) ' the new vector q'
!      write(6,'(5(i4,e12.5))') (i,qvect(i),i=1,ndim)
           do l=1,m
            call getv_dav(vector,l,iunit1,ndim)
            sum=u_dot_v(vector,qvect_dav,ndim)
            do i=1,ndim
             qvect_dav(i)=qvect_dav(i)-sum*vector(i)
            end do
           end do

           sum=u_dot_v(qvect_dav,qvect_dav,ndim)
           sum=1.d0/sqrt(sum)
           do i=1,ndim
            vector(i)=sum*qvect_dav(i)
           end do
!
           m=m+1
           call putv_dav(vector,m,iunit1,ndim)
           do i=1,ndim
            hvect_dav(i)=0.d0
           end do
           call hx_sci(vector,hvect_dav,ndim)

           call putv_dav(hvect_dav,m,iunit2,ndim)
           do l=1,m
            sum=zero
            call getv_dav(vector,l,iunit1,ndim)
            bab_dav(m,l)=u_dot_v(vector,hvect_dav,ndim)
           end do
          end if
!
!     back to the diagonalization of bab
!
! close loop do while ...
         end do
!
!     the eigenvectors of bab are back-transformed
!     in the original basis
!
! construct best vector from the stored intermediates
!
         write(6,*) ' constructing best vector '
         do i=1,ndim
          vector(i)=zero
         end do
         do ll=1,m
          alp=alpha_dav(ll,1)
          call getv_dav(hvect_dav,ll,iunit1,ndim)
          do i=1,ndim
           vector(i)=vector(i)+hvect_dav(i)*alp
          end do
         end do
         call hx_sci(vector,hvect_dav,ndim)
         eval_dav=u_dot_v(vector,hvect_dav,ndim)
!
! we return
!
         close(iunit1,status='delete')
         close(iunit2,status='delete')
         deallocate(hvect_dav)
         deallocate(qvect_dav)
!
      end subroutine davidson_general
!
       subroutine putv_dav(x,ivec,iunit,n)
         implicit none
         integer :: ivec,iunit,i,n
         real*8 :: x(*)
         write(iunit,rec=ivec) (x(i),i=1,n)
       end subroutine putv_dav
!
       subroutine getv_dav(x,ivec,iunit,n)
         implicit none
         integer :: ivec,iunit,i,n
         real*8 :: x(n)
         read(iunit,rec=ivec) (x(i),i=1,n)
       end subroutine getv_dav


 BEGIN_PROVIDER[real*8, bab_dav,   (maxdav, maxdav)]
&BEGIN_PROVIDER[real*8, alpha_dav, (maxdav, maxdav)]
&BEGIN_PROVIDER[real*8, alam_dav,  (maxdav)]
END_PROVIDER

     subroutine vnorm(vect,ndim,howto)
       implicit none
       integer :: i,howto,idet,ndim
       real*8 :: sum,xmlt,xnrm,u_dot_v,vect(ndim)
BEGIN_DOC
!
! three types of norms for the ci vector vect
! howto=1: normalize to one
!      =2: intermediate norm, i.e. c(1)=1
!      =3: complementary norm, all but c(1) normed to one
!
END_DOC

       if (howto.eq.1) then
        write(6,*) ' normalizing CI vector to 1'
        write(6,*) ' vect(1)  = ',vect(1)
        sum=sqrt(u_dot_v(vect,vect,ndim))
        do i=1,ndim
         vect(i)=vect(i)/sum
        end do
       else
        if (howto.eq.2) then
         xmlt=1.d0/vect(1)
         do i=1,ndim
          vect(i)=vect(i)*xmlt
         end do
        else
         if (howto.eq.3) then
          xnrm=u_dot_v(vect(1),vect(1),ndim) &
            -vect(1)*vect(1)
          if (xnrm.gt.1.d-16) then
           xnrm=1.d0/sqrt(xnrm)
           do idet=1,ndim
            vect(idet)=xnrm*vect(idet)
           end do
           write(6,*) ' K0 = ',vect(1)
           write(6,*)
          else
           vect(1)=1.d10
           write(6,*) ' no complement available '
           write(6,*) ' setting k0 to 10000 '
           write(6,*)
          end if
         else
          write(6,*) ' call to VNORM should have 1, 2 or 3.'
          stop       ' call to VNORM should have 1, 2 or 3'
         end if
        end if
       end if
      end subroutine vnorm
