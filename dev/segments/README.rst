========
segments
========

The triple jump for intermolecular interactions, for closed shells.

The molecular system is defined as a whole via qp_create_ezfio. 
Fragments are defined via a file FRAGMENTS in your working directory,
as a first line stating how many fragments, and then in a second 
line for each fragment how many atoms from the xyz file, in the order 
given by the .xyz file. Fragments must be closed-shell entities.
Charges may be given for each fragment in a third line, default is zero.

example: Na+ ... H2O, 4 atoms
The file FRAGMENTS should contain 3 lines
2
1 3
1 0

example: 3 water molecules
The file FRAGMENTS should contain 2 lines
3
3 3 3 

Bielectronic integrals are read only once in each iteration,
however needed for every monomer. Fock matrices are constructed 
and diagonalized for all monomers in parallel.
 - canonical SCF in the monomer basis for each monomer
 - Singles-CI SCF in the multimer basis for each monomer
 - Singles CI SCF for the complete multimer

This allows for the energy decomposition as electrostatic, Pauli repulsion
and orbital interaction, with or without BSSE.

Correlation contributions via MP2 are operational: 
 1st step) canonical orbitals are available for every monomer, in the monomer basis
 2nd step) Fock matrix is diagonalized for each monomer -> formula for canonical 
       orbitals to be used
 3rd step) Fock matrix in localized orbitals is diagonalized -> canonical orbitals
        for the MP2 calculation.

We do not have a Coupled-Cluster code, so no contributions from that side.
But we may experiment with an ACPF module.

DFT and RSHDFT, together with a correlation term will be included, in MP2, 
with extraction of the dispersion contribution.
