! -*- F90 -*-
      subroutine hx_sci(vector,hvect,ndim)
          implicit none
          integer :: indxi,indxj,i,a,ifrag,iifrag,nocc_sci,ndim,j,b
          real*8 :: vector(ndim),hvect(ndim),hhh
          logical :: lij,lab
BEGIN_DOC
!
! perform <Psi_ia|H|Psi_jb> = F_ab delta_ij - Fij delta_ab
!   F is in Fock_MO, of dimension mo_num x mo_num
!
END_DOC
! from what situation do we come, do we have ndim=1+nocc(mo_num-nocc)
! or ndim=1+nocc_frag(ifrag)(mo_num-nocc_frag(ifrag)) for one ifrag ?

!   real*8 :: cpu_1,cpu_2,wall_1,wall_2
!         call cpu_time(cpu_1)
!         call wall_time(wall_1)
          nocc_sci=0
          if (ndim.eq.1+nocc*(mo_num-nocc)) then
            nocc_sci=nocc
            write(6,*) ' SCI for the complete system '
          else
           do iifrag=1,nfrag
            if (ndim.eq.1+nocc_frag(iifrag)*(mo_num-nocc_frag(iifrag))) then
             nocc_sci=nocc_frag(iifrag)
!            write(6,*) ' SCI for monomer ',iifrag
             if (nocc_sci.ne.0.and.nocc_sci.ne.nocc_frag(iifrag)) then
              write(6,*) ' found 2 different possibilities for nocc_sci '
              write(6,*) ' nocc_sci = nocc*(mo_num-nocc) with nocc = ',nocc_sci,' or ',nocc_frag(iifrag)
              stop ' problem to determine nocc_sci '
             end if
            end if
           end do
          end if

!        write(6,*) ' starting vector '
!        write(6,'(5(I4,F15.5))') (i,vector(i),i=1,ndim)


          hvect=0.D0
!XXYXXY PARALLEL DO &
!XXYXXY DEFAULT (none) &
!XXYXXY PRIVATE (i,a,indxi) &
!XXYXXY SHARED (hvect,fock_mo,vector,nocc_sci,mo_num)
         do i=1,nocc_sci
          do a=nocc_sci+1,mo_num
           indxi=1+(i-1)*(mo_num-nocc_sci)+a-nocc_sci
           hvect(1)+=vector(indxi)*Fock_mo(i,a)*sqrt(2.D0)
           hvect(indxi)+=vector(1)*Fock_mo(i,a)*sqrt(2.D0)
          end do
         end do
!XXYXXY END PARALLEL DO
!        write(6,*) ' hvect  '
!        write(6,'(5(I4,F15.5))') (i,hvect(i),i=1,ndim)

real*8 :: ss
!         indxi=1
!$XXYXXY PARALLEL DO &
!$XXYXXY DEFAULT (none) &
!$XXYXXY PRIVATE (i,a,indxi,j,b,indxj,ss,lij,lab,hhh) &
!$XXYXXY SHARED (hvect,levelshift,fock_mo,vector,nocc_sci,mo_num)
          do i=1,nocc_sci
           do a=nocc_sci+1,mo_num
            indxi=1+(i-1)*(mo_num-nocc_sci)+a-nocc_sci
!           indxi+=1
!           indxj=1
            ss=0.D0
            do j=1,nocc_sci
             lij=i.eq.j
             do b=nocc_sci+1,mo_num
              indxj=1+(j-1)*(mo_num-nocc_sci)+b-nocc_sci
!             indxj+=1
              lab=a.eq.b
              hhh=0.D0
              if (lab) then
               hhh-=Fock_mo(i,j)
              end if
              if (lij) then
               hhh+=Fock_mo(a,b)
              end if
! we run through the whole matrix
              ss+=vector(indxj)*hhh
             end  do
            end do
            hvect(indxi)+=ss
           end  do
          end do
!$XXYXXY END PARALLEL DO
!         call cpu_time(cpu_2)
!         call wall_time(wall_2)
!        write(6,7702) cpu_2-cpu_1,wall_2-wall_1,(cpu_2-cpu_1)/(wall_2-wall_1+1.D-11)
!7702 format(' hx: CPU and Wall time:',2F14.6,' ratio:',F10.5)
!
! we add the levelshift on the diagonal
         do i=2,ndim
          hvect(i)+=vector(i)*levelshift
         end do


      end subroutine hx_sci


