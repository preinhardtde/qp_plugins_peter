! -*- F90 -*-
BEGIN_PROVIDER [real*8, fock_mo, (mo_num,mo_num)]
BEGIN_DOC
!  for each fragment we need a Fock matrix in AOs, but only one in MOs
END_DOC
END_PROVIDER

 BEGIN_PROVIDER [real*8, density_frag, (ao_num,ao_num,nfrag)]
BEGIN_DOC
!  for each fragment we need a density matrix in AOs
END_DOC
END_PROVIDER

BEGIN_PROVIDER [real*8, density, (ao_num,ao_num)]
END_PROVIDER

BEGIN_PROVIDER [real*8, density3, (ao_num,ao_num)]
END_PROVIDER

BEGIN_PROVIDER [real*8, orben, (mo_num)]
END_PROVIDER

BEGIN_PROVIDER [real*8, fock_ao, (ao_num,ao_num)]
END_PROVIDER

BEGIN_PROVIDER [real*8, fock_frag, (ao_num,ao_num,nfrag)]
END_PROVIDER
