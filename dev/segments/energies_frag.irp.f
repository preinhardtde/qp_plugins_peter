! -*- F90 -*-
 BEGIN_PROVIDER [real*8, ETOT_frag,  (NFRAG,2)]
&BEGIN_PROVIDER [real*8, EKIN_frag,  (NFRAG,2)]
&BEGIN_PROVIDER [real*8, ECOUL_frag, (NFRAG,2)]
&BEGIN_PROVIDER [real*8, EONE_frag,  (NFRAG,2)]
&BEGIN_PROVIDER [real*8, EEXCH_frag, (NFRAG,2)]
&BEGIN_PROVIDER [real*8, EDFXC_frag, (NFRAG,2)]
&BEGIN_PROVIDER [real*8, ELSTAT, (2)]
&BEGIN_PROVIDER [real*8, DVPAULI,  (2)]
&BEGIN_PROVIDER [real*8, DTPAULI,  (2)]
&BEGIN_PROVIDER [real*8, DXPAULI,  (2)]
&BEGIN_PROVIDER [real*8, DXCPAULI, (2)]
&BEGIN_PROVIDER [real*8, orb_occ, (ao_num,nocc,2)]
&BEGIN_PROVIDER [real*8, orb_virt1, (ao_num,mo_num)]
&BEGIN_PROVIDER [real*8,Etot ]   
&BEGIN_PROVIDER [real*8,ECOUL ]  
&BEGIN_PROVIDER [real*8,EONE ]   
&BEGIN_PROVIDER [real*8,EKIN ]   
&BEGIN_PROVIDER [real*8,EEXCH ]  
&BEGIN_PROVIDER [real*8,EDFTXC ] 
&BEGIN_PROVIDER [real*8, orb_save, (ao_num, mo_num) ]  
BEGIN_DOC
! energy terms for each fragment
! the first is for separated fragments
! in this provider we put the first two SCF steps : 
!  - canonical Hartree-Fock for the monomers in 
!    their respective basis sets
!  - Singles-CI SCF from the previous orbitals in the 
!    multimer basis
!
! in the same instance we provide the orbitals of the first two steps
!
! we have as well the electrostatic terms between monomers available
!
END_DOC
         implicit none
         double precision, allocatable  :: evec_fock(:,:),eval_fock(:)
         double precision, allocatable  :: evec_SCI(:,:),eval_SCI(:)
         double precision, allocatable  :: bestvec_SCI(:),SCI_mat(:,:)
         double precision, allocatable  :: diag_SCI(:)
         double precision, allocatable :: integrals_array(:,:)
         double precision, allocatable  :: orb_tmp(:)

         real*8 :: Eold_frag(NFRAG)
         real*8 :: cpu_2,cpu_1,cpu_0,wall_2,wall_1,wall_0
         real*8 :: qi,qj
         real*8 :: delta_e,delta_p
         real*8 :: pp
         real*8 :: a1,a2,b1,b2,b3
         real*8 :: oda_denom
         real*8 :: ei,ej,ea,eb,denom,int_iajb,int_ibja
         real*8 :: ss
         real*8 :: bestval_SCI
         real*8 :: c0,cia
         real*8 :: Eold
         real*8 :: e_opt

         integer :: i,j,alpha,beta,ifrag,offset
         integer :: iat,jat,jfrag
         integer :: ia,ib,imo,jmo
         integer :: norbs
         integer :: aa,bb,ii,jj,offset_occ,offset_virt
         integer :: indx2
         integer :: nMonoMax
         integer :: nMonoEx
         integer :: indx,indxi,indxj
         integer :: a,b
         logical :: lij,lab
         
         call wall_time(wall_0)
         call cpu_time(cpu_0)
 
         if (lmp2)  allocate(integrals_array(mo_num,mo_num))
!
! electrostatic interaction 
          elstat=0.D0
! nuclear
          do iat=1,nucl_num
           ifrag=atom_list_frag(iat)
           qi=nucl_charge(iat)
           do jat=iat+1,nucl_num
            if(ifrag.ne.atom_list_frag(jat)) then
             qj=nucl_charge(jat)
             elstat+=qi*qj/nucl_dist(iat,jat)
            end if
           end do
          end do

         allocate (evec_fock(mo_num, mo_num))
         allocate (eval_fock(mo_num))
!
         ETOT_frag = 0.D0
         EKIN_frag = 0.D0
         ECOUL_frag = 0.D0
         EONE_frag = 0.D0
         EEXCH_frag = 0.D0
         EDFXC_frag = 0.D0
         eold_frag=0.D0

! here we are with our guess orbitals and the corresponding density

         do ifrag=1,nfrag
          offset = ao_start_frag(ifrag)-1
          do alpha=1,ao_num_frag(ifrag)
           ia=alpha+offset
           do beta=1,ao_num_frag(ifrag)
            ib=beta+offset
! one-electron terms
            Ekin_frag(ifrag,1)+=density_frag(ia,ib,ifrag)*ao_kinetic_integrals(ia,ib)
            Eone_frag(ifrag,1)+=density_frag(ia,ib,ifrag)*(ao_kinetic_integrals(ia,ib) &
                 +oneham_frag(ia,ib,ifrag))
! one-electron terms
            Ecoul_frag(ifrag,1)+=density_frag(ia,ib,ifrag)*xi_coul_frag(ia,ib,ifrag)
            Eexch_frag(ifrag,1)+=density_frag(ia,ib,ifrag)*xi_exch_frag(ia,ib,ifrag)
           end do
          end do
          Ecoul_frag(ifrag,1)*=0.5D0
          Eexch_frag(ifrag,1)*=0.5D0
          write(6,*) ' Fragment No ',ifrag
          write(6,9901) Ekin_frag(ifrag,1),Eone_frag(ifrag,1)    &
                       ,Ecoul_frag(ifrag,1),Eexch_frag(ifrag,1),EN_frag(ifrag)
!9901 format('          Ekin = ',F15.5,/ &
!           ,'          Eone = ',F15.5,/ &
!           ,'         ECoul = ',F15.5,/ &
!           ,'         Eexch = ',F15.5,/ &
!           ,'       nuclear = ',F15.5)
          etot_frag(ifrag,1)= en_frag(ifrag) + eone_frag(ifrag,1)      &
               + ecoul_frag(ifrag,1) + eexch_frag(ifrag,1)
          write(6,*) ' Etot_frag = ',etot_frag(ifrag,1)
         end do
!
! canonical SCF in the monomer basis set
!
         call cpustamp(cpu_0,wall_0,cpu_1,wall_1,'pre1')
         delta_e=huge(1.d0)
         delta_p=huge(1.d0)
         iter_scf=0
         p_next=0.D0
         lambda_oda=1.D0
         xi_old_frag=0.D0
         do while (delta_E.ge.thr_scf.or.delta_P.ge.thr_P)
          
          write(6,*) 
          write(6,*)  ' ============================================ '
          write(6,*)  '    1st step Iteration No ',iter_scf
          write(6,*)  ' ============================================ '

          
          do ifrag=1,nfrag
           eold_frag(ifrag)=etot_frag(ifrag,1)
           write(6,*) ' fragment No ',ifrag,' has energy ',etot_frag(ifrag,1)
          end do

          fock_ao=0.D0
!$XXYXXY PARALLEL DO &
!$XXYXXY DEFAULT (none) &
!$XXYXXY PRIVATE (alpha,beta,ifrag) &
!$XXYXXY SHARED (fock_ao,ao_num,ao_list_frag,ao_kinetic_integrals,oneham_frag,    &
!$XXYXXY         lambda_oda,xi_coul_frag,xi_exch_frag,xi_old_frag)
          do alpha=1,ao_num
           ifrag=ao_list_frag(alpha)
           do beta=1,ao_num
            if (ifrag.eq.ao_list_frag(beta)) then
             fock_ao(alpha,beta)=ao_kinetic_integrals(alpha,beta)
             fock_ao(alpha,beta)+=oneham_frag(alpha,beta,ifrag)
             fock_ao(alpha,beta)+=lambda_oda*xi_coul_frag(alpha,beta,ifrag)
             fock_ao(alpha,beta)+=lambda_oda*xi_exch_frag(alpha,beta,ifrag)
             fock_ao(alpha,beta)+=(1.D0-lambda_oda)*xi_old_frag(alpha,beta,ifrag)
            end if
           end do
          end do
!$XXYXXY END PARALLEL DO

          call transf_ao_mo(fock_ao,ao_num,fock_mo,mo_num,orb_work)
          
! we save the diagonal elements for an eventual MP2
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (i) SHARED (orben,fock_mo,mo_num)
           do i=1,mo_num
            orben(i)=fock_mo(i,i)
           end do
           write(6,'(5(i4,F12.5))') (i,orben(i),i=1,mo_num)
!$XXYXXY END PARALLEL DO

! prepare diagonalization
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (i,j) SHARED (fock_ao,fock_mo,mo_num)
          do i=1,mo_num
           do j=1,mo_num
            fock_ao(i,j)=fock_mo(i,j)
           end do
          end do
!$XXYXXY END PARALLEL DO
          
! diagonalize block by block
          write(6,*) ' Fock matrix in MOs '
!$XXYXXY PARALLEL DO &
!$XXYXXY DEFAULT (none) &
!$XXYXXY PRIVATE (i,j,fock_mo,eval_fock,evec_fock,offset,imo,jmo,ifrag,norbs,alpha) &
!$XXYXXY SHARED (nfrag,fock_ao,mo_start_frag,mo_num_frag,ao_num,mo_num,orb_work)
          do ifrag=1,nfrag
           eval_fock=0.D0
           evec_fock=0.D0
           fock_mo=0.D0
           offset=mo_start_frag(ifrag)-1
           write(6,*) ' fragment No ',ifrag
           do i=1,mo_num_frag(ifrag)
            imo=i+offset
            do j=1,mo_num_frag(ifrag)
             jmo=j+offset
             fock_mo(i,j)=fock_ao(imo,jmo)
            end do
           end do
           norbs=mo_num_frag(ifrag)
           call lapack_diag(eval_fock,evec_fock,fock_mo,mo_num,norbs)
!          write(6,'(5(i4,F15.5))') (i,eval_fock(i),i=1,mo_num_frag(ifrag))
! update the orbitals
           
! as well block per block
           do alpha=1,ao_num
            do i=1,mo_num_frag(ifrag)
             imo=i+offset
             eval_fock(i)=orb_work(alpha,imo)
             orb_work(alpha,imo)=0.D0
            end do
            do i=1,mo_num_frag(ifrag)
             imo=i+offset
             do j=1,mo_num_frag(ifrag)
              orb_work(alpha,imo)+=evec_fock(j,i)*eval_fock(j)
             end do
            end do
           end do
          end do
!$XXYXXY END PARALLEL DO

          call fill_p_next_1st

          delta_P=0.d0
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (i,j,pp,ifrag)  &
!$XXYXXY SHARED (ao_list_frag,ao_num,p_next,density_frag,delta_p)
          do i=1,ao_num
           ifrag=ao_list_frag(i)
           do j=1,ao_num
            pp=p_next(i,j)-density_frag(i,j,ifrag)
            delta_P=delta_P+pp*pp
           end do
          end do
!$XXYXXY END PARALLEL DO
          delta_P=delta_P/(ao_num*ao_num)

          dens_old_frag=density_frag

! redistribute p_next onto density_frag
          density_frag=0.D0
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (alpha,ifrag,beta)  &
!$XXYXXY SHARED (ao_list_frag,ao_num,p_next,density_frag)
          do alpha=1,ao_num
           ifrag=ao_list_frag(alpha)
           do beta=1,ao_num
            if (ifrag.eq.ao_list_frag(beta)) density_frag(alpha,beta,ifrag)=p_next(alpha,beta)
           end do
          end do
!$XXYXXY END PARALLEL DO

          xi_old_frag=xi_coul_frag+xi_exch_frag

          touch density_frag

          delta_e=0.D0
          do ifrag=1,nfrag
           Ekin_frag(ifrag,1)=0.D0
           Eone_frag(ifrag,1)=0.D0
           Ecoul_frag(ifrag,1)=0.D0
           Eexch_frag(ifrag,1)=0.D0
          end do

!$XXYXXY PARALLEL DO &
!$XXYXXY DEFAULT (none) &
!$XXYXXY PRIVATE (ifrag,offset,alpha,beta,ia,ib) &
!$XXYXXY SHARED (nfrag,ao_start_frag,ao_num_frag,Ekin_frag,Eone_frag,density_frag, &
!$XXYXXY ao_kinetic_integrals,oneham_frag,xi_coul_frag,xi_exch_frag,Ecoul_frag,Eexch_frag)
          do ifrag=1,nfrag
           offset=ao_start_frag(ifrag)-1
           do alpha=1,ao_num_frag(ifrag)
            ia=alpha+offset
            do beta=1,ao_num_frag(ifrag)
             ib=beta+offset
! one-electron terms
             Ekin_frag(ifrag,1)+=density_frag(ia,ib,ifrag)*ao_kinetic_integrals(ia,ib)
             Eone_frag(ifrag,1)+=density_frag(ia,ib,ifrag)*(ao_kinetic_integrals(ia,ib) &
                  +oneham_frag(ia,ib,ifrag))
! one-electron terms
             Ecoul_frag(ifrag,1)+=density_frag(ia,ib,ifrag)*xi_coul_frag(ia,ib,ifrag)
             Eexch_frag(ifrag,1)+=density_frag(ia,ib,ifrag)*xi_exch_frag(ia,ib,ifrag)
            end do
           end do
           Ecoul_frag(ifrag,1)*=0.5D0
           Eexch_frag(ifrag,1)*=0.5D0
          end do
!$XXYXXY END PARALLEL DO


          do ifrag=1,nfrag
           write(6,*) ' Fragment No ',ifrag
           write(6,9901) Ekin_frag(ifrag,1),Eone_frag(ifrag,1)    &
                       ,Ecoul_frag(ifrag,1),Eexch_frag(ifrag,1),EN_frag(ifrag)

           etot_frag(ifrag,1)= en_frag(ifrag) + eone_frag(ifrag,1) &
                + ecoul_frag(ifrag,1) + eexch_frag(ifrag,1)
           write(6,*) ' Etot_frag = ',etot_frag(ifrag,1)
           delta_e+=abs(etot_frag(ifrag,1)-eold_frag(ifrag))
          end do
          delta_e*=1.D0/dble(nfrag)
          write(6,*) ' delta_E:', delta_e
          write(6,*) ' Delta P :',delta_P
          iter_scf=iter_scf+1

! the optimal damping algorithm of Eric Cancès and Claude Le Bris
          if (iter_scf.gt.2) then
           a1=0.D0
           a2=0.D0
           b1=0.D0
           b2=0.D0
           b3=0.D0
! parallelizing with XXYXXY goes wrong
           do alpha=1,ao_num
            ifrag=ao_list_frag(alpha)
            do beta=1,ao_num
             if (ifrag.eq.ao_list_frag(beta)) then
              a1+=(ao_kinetic_integrals(alpha,beta)+oneham_frag(alpha,beta,ifrag))*dens_old_frag(alpha,beta,ifrag)
              a2+=(ao_kinetic_integrals(alpha,beta)+oneham_frag(alpha,beta,ifrag))*density_frag(alpha,beta,ifrag)
              b1+=xi_old_frag(alpha,beta,ifrag)*dens_old_frag(alpha,beta,ifrag)
              b2+=xi_old_frag(alpha,beta,ifrag)*density_frag(alpha,beta,ifrag)
              b3+=(xi_coul_frag(alpha,beta,ifrag)+xi_exch_frag(alpha,beta,ifrag)) &
                   *density_frag(alpha,beta,ifrag)
             end if
            end do
           end do
           b1*=0.5D0
           b3*=0.5D0
           oda_denom=b1-b2+b3
           write(6,*) ' Optimal Damping Algorithm of Cances et al.'
           if ((oda_denom).lt.0.D0) then
            lambda_oda=1.D0
           else
            if (abs(oda_denom).lt.1.D-12) then
             lambda_oda=0.5D0
            else
             lambda_oda=-(a2-a1+b2-2.D0*b1)/oda_denom*0.5D0
            end if
           end if
           write(6,*) ' optimal lambda is ',lambda_oda
           if (lambda_oda.lt.0.D0.or.lambda_oda.gt.1.D0) then
            write(6,*) ' lambda outside [0,1], setting lambda to 1 '
            lambda_oda=1.0D0
           end if
          else
           lambda_oda=1.D0
          end if

          if (iter_scf.gt.max_iter) then
           write(6,*) ' max number of iterations ',max_iter,' attained '
           exit
          end if
! close loop for the first SCF

          call cpustamp(cpu_1,wall_1,cpu_1,wall_1,'IT 1')
          
         end do

         write(6,*) ' =========================================================== '
         write(6,*) '  Step 1 : Convergence after ',iter_scf,' iterations '
         do ifrag=1,nfrag
          write(6,*) '  Fragment ',ifrag,': total energy = ',etot_frag(ifrag,1)
          etot_frag(ifrag,2)=etot_frag(ifrag,1)
         end do
         call cpustamp(cpu_0,wall_0,cpu_0,wall_0,'1rst')
         write(6,*) ' =========================================================== '

!
! we have the converged density_frag and xi available 
! for calculating electrostatic interactions between fragments
!
! e-n and e-e together
          write(6,*) ' Calculating electrostatics '
!$XXYXXY PARALLEL DO &
!$XXYXXY DEFAULT (none) &
!$XXYXXY PRIVATE (ifrag,jfrag,alpha,beta,ss) &
!$XXYXXY SHARED (nfrag,ao_num,density_frag,elstat,oneham_frag,xi_coul_frag)
           do ifrag=1,nfrag
            do jfrag=ifrag+1,nfrag
             ss=0.D0
             do alpha=1,ao_num
              do beta=1,ao_num
! e-n
! we need the integrals of n on ifrag and electrons on jfrag
               ss+=density_frag(alpha,beta,ifrag) &
                 *oneham_frag(alpha,beta,jfrag)
               ss+=density_frag(alpha,beta,jfrag) &
                 *oneham_frag(alpha,beta,ifrag)
! e-e
               ss+=density_frag(alpha,beta,ifrag) &
                 *xi_coul_frag(alpha,beta,jfrag)
              end do
             end do
             elstat(1)+=ss
            end do
           end do
!$XXYXXY END PARALLEL DO
          write(6,*) ' Elstat(1) = ',elstat(1)
          call cpustamp(cpu_0,wall_0,cpu_2,wall_2,'EST1')

! save the orbitals for the next step, the Singles CI in the multimer basis sets
integer :: indx_occ,indx_virt
         indx_occ=0
         indx_virt=nocc
         offset=0
         orb_virt1=0.D0
         do ifrag=1,nfrag
! pick out the occupied orbitals of each monomer, and place them in orb_occ
! pick out the virtual orbitals of each monomer, and place them in orb_virt1
          do i=1,nocc_frag(ifrag)
           indx_occ+=1
           do alpha=1,ao_num
            orb_occ(alpha,indx_occ,1)=orb_work(alpha,mo_start_frag(ifrag)-1+i)
           end do
          end do
          do i=1+nocc_frag(ifrag),mo_num_frag(ifrag)
           indx_virt+=1
           do alpha=1,ao_num
            orb_virt1(alpha,indx_virt)=orb_work(alpha,offset+i)
           end do
          end do
          offset+=mo_num_frag(ifrag)
         end do
           
         mo_coef=orb_work
         mo_label='Localized'
         call save_mos

         write(6,*)
         write(6,*) ' Calculating the Pauli interaction terms' 
         write(6,*)
! for the Heitler-London or Pauli term we orthogonalize the occupied orbitals
! and recalculate kinetic energy, potential energy and exchange 
         do i=1,nocc
          do alpha=1,ao_num
           orb_work_frag(alpha,i,1)=orb_occ(alpha,i,1)
          end do
         end do
         call shalf(1,nocc,orb_work_frag(1,1,1))
! in the first fragment we accumulate the difference density between 
! the converged monomer densities and the density from orthogonalized 
! monomer orbitals
! the difference density will serve for the electrostatic terms
! however, for the e-e terms we will need the cross-terms density*xi
! e-e = rho_d(1)rho_d(2) - rho_m(1)rho_m(2) 
!     = (rho_d(1)-rho_m(1))(rho_d(2)-rho_m(2))
! set R=rho_d, r=rho_m, thus 
! RR-rr = (R-r)(R-r) + r(R-r) + (R-r)r = (R-r)(R-r) + 2(R-r)r
! 
         do ifrag=2,nfrag
          do alpha=1,ao_num
           do beta=1,ao_num
            density_frag(alpha,beta,1)+= density_frag(alpha,beta,ifrag)
           end do
          end do
         end do
         do i=1,nocc
          do alpha=1,ao_num
           do beta=1,ao_num
            density_frag(alpha,beta,1)-=orb_work_frag(alpha,i,1)   &
               *orb_work_frag(beta,i,1)*2.D0
           end do
          end do
         end do
         do alpha=1,ao_num
          do beta=1,ao_num
           density_frag(alpha,beta,1)*= -1.D0
          end do
         end do
! we have now the difference density
! before recalculating xi_coul_frag and xi_exch_frag

         dtpauli(1)=0.D0
         do alpha=1,ao_num
          do beta=1,ao_num
           dtpauli(1)+=density_frag(alpha,beta,1)*ao_kinetic_integrals(alpha,beta)
          end do
         end do
         dvpauli(1)=0.D0
         dxpauli(1)=0.D0
! e-n
         dvpauli(1)=0.D0
         do ifrag=1,nfrag
          do alpha=1,ao_num
           do beta=1,ao_num
            dvpauli(1)+=density_frag(alpha,beta,1)*oneham_frag(alpha,beta,ifrag)
           end do
          end do
         end do
! 2 (R-r)r
         do ifrag=1,nfrag
          do alpha=1,ao_num
           do beta=1,ao_num
            dvpauli(1)+=density_frag(alpha,beta,1)*xi_coul_frag(alpha,beta,ifrag)
            dxpauli(1)+=density_frag(alpha,beta,1)*xi_exch_frag(alpha,beta,ifrag)
           end do
          end do
         end do
         touch density_frag
! (R-r)(R-r)
         do alpha=1,ao_num
          do beta=1,ao_num
           dvpauli(1)+=density_frag(alpha,beta,1)*xi_coul_frag(alpha,beta,1)
           dxpauli(1)+=density_frag(alpha,beta,1)*xi_exch_frag(alpha,beta,1)
          end do
         end do
!
! calculate RR-rr directly
!
         write(6,*) ' calculated dxpauli(1) = ',dxpauli(1)
         write(6,*) ' calculate RR-rr for the exchange '
         do alpha=1,ao_num
          do beta=1,ao_num
           density_frag(alpha,beta,1)=0.D0
          end do
         end do
          
         do i=1,nocc
          do alpha=1,ao_num
           do beta=1,ao_num
            density_frag(alpha,beta,1)+=orb_work_frag(alpha,i,1)   &
               *orb_work_frag(beta,i,1)
           end do
          end do
         end do
         do alpha=1,ao_num
          do beta=1,ao_num
           density_frag(alpha,beta,1)*=2.D0
          end do
         end do
         touch density_frag
         dxpauli(1)=0.D0
         do ifrag=1,nfrag
          dxpauli(1)-=Eexch_frag(ifrag,1)
         end do
         do alpha=1,ao_num
          do beta=1,ao_num
           dxpauli(1)+=0.50*density_frag(alpha,beta,1)*xi_exch_frag(alpha,beta,1)
          end do
         end do
         write(6,*) ' recalculated dxpauli(1) = ',dxpauli(1)

         call  cpustamp(cpu_0,wall_0,cpu_1,wall_1,'XV 1')
 
         write(6,*)
         write(6,*) ' ... done ' 
         write(6,*)
!
! do we need to calculate MP2 ?
         if (lmp2) then
 write(6,*) '------------------------------------------ '
 write(6,*) ' MP2 '
          mo_coef=orb_work
          no_vvvv_integrals = .true.
! we have to set mo_class for generating bitmaps
          indx=0
          do ifrag=1,nfrag
           do i=1,nocc_frag(ifrag)
            indx+=1
            mo_class(indx)='Core'
           end do
           do i=1+nocc_frag(ifrag),mo_num_frag(ifrag)
            indx+=1
            mo_class(indx)='Virtual'
           end do
          end do
          emp2_frag=0.D0

   double precision, external :: mo_two_e_integral
   real*8 :: Emp2_frag_local
    
          do ifrag=1,nfrag
           offset=mo_start_frag(ifrag)-1
!$XXYXXY PARALLEL &
!$XXYXXY DEFAULT (none) &
!$XXYXXY PRIVATE (ii,jj,i,j,a,b,emp2_frag_local,aa,bb,int_iajb,int_ibja,denom) &
!$XXYXXY SHARED (mo_num,ifrag,offset,emp2_frag,nocc_frag,integrals_array   &
!$XXYXXY    ,mo_num_frag,orben)
           emp2_frag_local=0.D0
!$XXYXXY DO
           do ii=1,nocc_frag(ifrag)
            i=ii+offset
            do jj=1,nocc_frag(ifrag)
             j=jj+offset
             call get_mo_two_e_integrals_ij(i,j,mo_num,integrals_array,mo_integrals_map)
             do a=1,mo_num
              do b=1,mo_num
               integrals_array(a,b) = mo_two_e_integral(i,j,a,b)
              end do
             end do
             do aa=nocc_frag(ifrag)+1,mo_num_frag(ifrag)
              a=aa+offset
              do bb=nocc_frag(ifrag)+1,mo_num_frag(ifrag)
               b=bb+offset
               int_iajb=integrals_array(a,b)
               int_ibja=integrals_array(b,a)
               denom=orben(a)+orben(b)-orben(i)-orben(j)
               emp2_frag_local-=int_iajb*(2.D0*int_iajb-int_ibja)/denom
!              write(6,*) i,j,a,b,int_iajb,int_ibja,denom,emp2_frag(ifrag,1)
              end do
             end do
            end do
           end do
!$XXYXXY END DO
!$XXYXXY CRITICAL
           emp2_frag(ifrag,1) = emp2_frag(ifrag,1) + emp2_frag_local
!$XXYXXY END CRITICAL
!$XXYXXY END PARALLEL
           write(6,*) ' Fragment No ',ifrag,': EMP2 = ',emp2_frag(ifrag,1)
          end do

          call cpustamp(cpu_1,wall_1,cpu_1,wall_1,'MP2 ')
 write(6,*) '------------------------------------------ '
         end if
         

! the second SCF step
! fill the starting orbitals - we take the occupied orbitals of each fragment as first ones
! and the remaining ones as virtual. Orbitals are not orthogonal. We need the hierarchial 
! orthogonalization

         indx=nocc
         indx2=nocc
         offset=0
         do ifrag=1,nfrag
! take the occupied orbitals of orb_work
          do i=1,nocc_frag(ifrag)
           do alpha=1,ao_num
            orb_work_frag(alpha,i,ifrag)=orb_work(alpha,offset+i)
           end do
          end do
!         take all other orbitals of orb_work
          indx=nocc_frag(ifrag)
          do i=1,mo_num
           if (i.le.offset.or.i.gt.offset+nocc_frag(ifrag)) then
            indx+=1
            do alpha=1,ao_num
             orb_work_frag(alpha,indx,ifrag)=orb_work(alpha,i)
            end do
           end if
          end do
          offset+=mo_num_frag(ifrag)
         end do
!
! orthogonalize the orbital spaces
         do ifrag=1,nfrag
          do alpha=1,ao_num
           do i=1,mo_num
            orb_work(alpha,i)=orb_work_frag(alpha,i,ifrag)
           end do
          end do
          call ortho_frag(orb_work,nocc_frag(ifrag))
          do alpha=1,ao_num
           do i=1,mo_num
            orb_work_frag(alpha,i,ifrag)=orb_work(alpha,i)
           end do
          end do
         end do

! fill density_frag
         density_frag=0.D0
!$XXYXXY PARALLEL DO &
!$XXYXXY DEFAULT (none) &
!$XXYXXY PRIVATE (ifrag,i,alpha,beta) &
!$XXYXXY SHARED (density_frag,nfrag,ao_num,nocc_frag,orb_work_frag)
         do ifrag=1,nfrag
          do alpha=1,ao_num
           do beta=1,ao_num
            do i=1,nocc_frag(ifrag)
             density_frag(alpha,beta,ifrag)+=orb_work_frag(alpha,i,ifrag)*orb_work_frag(beta,i,ifrag)
            end do
           end do
          end do
         end do
!$XXYXXY END PARALLEL DO
         density_frag*=2.D0

         touch density_frag

         do ifrag=1,nfrag
! calculate number of electrons
          ss=0.D0
          do alpha=1,ao_num
           do beta=1,ao_num
            ss+=density_frag(alpha,beta,ifrag)*ao_overlap(alpha,beta)
           end do
          end do
          write(6,*) ' number of electrons in fragment ',ifrag,': ',ss
         end do
! we recalculate energy contributions
!$XXYXXY PARALLEL DO &
!$XXYXXY DEFAULT (none) &
!$XXYXXY PRIVATE (ifrag,alpha,beta) &
!$XXYXXY SHARED (nfrag,ao_num,density_frag,Ekin_frag,Eone_frag,Ecoul_frag,Eexch_frag, &
!$XXYXXY ao_kinetic_integrals,oneham_frag,xi_coul_frag,xi_exch_frag)
         do ifrag=1,nfrag
          write(6,*) ' Fragment No ',ifrag
          do alpha=1,ao_num
           do beta=1,ao_num
! one-electron terms
            Ekin_frag(ifrag,2)+=density_frag(alpha,beta,ifrag)*ao_kinetic_integrals(alpha,beta)
            Eone_frag(ifrag,2)+=density_frag(alpha,beta,ifrag)*(ao_kinetic_integrals(alpha,beta) &
                 +oneham_frag(alpha,beta,ifrag))
! one-electron terms
            Ecoul_frag(ifrag,2)+=density_frag(alpha,beta,ifrag)*xi_coul_frag(alpha,beta,ifrag)
            Eexch_frag(ifrag,2)+=density_frag(alpha,beta,ifrag)*xi_exch_frag(alpha,beta,ifrag)
           end do
          end do
          Ecoul_frag(ifrag,2)*=0.5D0
          Eexch_frag(ifrag,2)*=0.5D0
         end do
!$XXYXXY END PARALLEL DO
         do ifrag=1,nfrag
          write(6,9901) Ekin_frag(ifrag,2),Eone_frag(ifrag,2)    &
                      ,Ecoul_frag(ifrag,2),Eexch_frag(ifrag,2),EN_frag(ifrag)

          etot_frag(ifrag,2)= en_frag(ifrag) + eone_frag(ifrag,2) &
               + ecoul_frag(ifrag,2) + eexch_frag(ifrag,2)
          write(6,*) ' Etot_frag = ',etot_frag(ifrag,2)
         end do
!
! Singles-CI SCF in the multimer basis set
!
         nMonoMax=nocc*mo_num+1
         if (l_davidson) then
          allocate (diag_SCI(nMonoMax))
         else
          allocate (evec_SCI(nMonoMax, nMonoMax))
          allocate (SCI_mat(nMonoMax, nMonoMax))
          allocate (eval_SCI(nMonoMax))
         end if
         allocate (bestvec_SCI(nMonoMax))

         delta_e=huge(1.d0)
         delta_p=huge(1.d0)
         iter_scf=0
         p_next_frag=0.D0
         lambda_oda_frag=1.D0
         call cpustamp(cpu_1,wall_1,cpu_0,wall_0,'pre2')
         cpu_1=cpu_0
         wall_1=wall_0

!        xi_old_frag=0.D0
         do while (delta_E.ge.thr_scf.or.delta_P.ge.thr_P)
          
          write(6,*) 
          write(6,*)  ' ============================================ '
          write(6,*)  '    2nd step Iteration No ',iter_scf
          write(6,*)  ' ============================================ '
          
          
          do ifrag=1,nfrag
           eold_frag(ifrag)=etot_frag(ifrag,2)
           write(6,*) ' fragment No ',ifrag,' has energy ',etot_frag(ifrag,2)
          end do
          
! construct Fock matrices, and perform Singles CI, one monomer after the other
! density_frag, xi and orbitals are coherent
          do ifrag=1,nfrag
           write(6,*) ' fragment No ',ifrag,': Singles-CI step '
           orb_work=0.D0
           fock_ao=0.D0
!$XXYXXY PARALLEL DO &
!$XXYXXY DEFAULT (none) &
!$XXYXXY PRIVATE (alpha,beta) &
!$XXYXXY SHARED (fock_ao,ao_num,ao_list_frag,ao_kinetic_integrals,oneham_frag,    &
!$XXYXXY         ifrag,lambda_oda_frag,xi_coul_frag,xi_exch_frag,xi_old_frag)
           do alpha=1,ao_num
            do beta=1,ao_num
             fock_ao(alpha,beta)=ao_kinetic_integrals(alpha,beta)
             fock_ao(alpha,beta)+=oneham_frag(alpha,beta,ifrag)
             fock_ao(alpha,beta)+=lambda_oda_frag(ifrag)*xi_coul_frag(alpha,beta,ifrag)
             fock_ao(alpha,beta)+=lambda_oda_frag(ifrag)*xi_exch_frag(alpha,beta,ifrag)
             fock_ao(alpha,beta)+=(1.D0-lambda_oda_frag(ifrag))*xi_old_frag(alpha,beta,ifrag)
            end do
           end do
!$XXYXXY END PARALLEL DO
! we work in a local copy of the orbitals
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (i,alpha) SHARED (ifrag,ao_num,mo_num,orb_work,orb_work_frag)
           do i=1,mo_num
            do alpha=1,ao_num
             orb_work(alpha,i)=orb_work_frag(alpha,i,ifrag)
            end do
           end do
!$XXYXXY END PARALLEL DO

           call transf_ao_mo(fock_ao,ao_num,fock_mo,mo_num,orb_work)
           
           write(6,*) ' diagonal of the Fock matrix in MOs'
           do i=1,mo_num
            orben(i)=fock_mo(i,i)
           end do
           write(6,'(5(i4,f15.5))') (i,orben(i),i=1,mo_num)

           nMonoEx=1+nocc_frag(ifrag)*(mo_num-nocc_frag(ifrag))
           if (l_Davidson) then
            bestvec_SCI=0.D0
            bestvec_SCI(1)=1.D0
            diag_SCI(1)=0.D0
            indx=1
            do i=1,nocc
             do a=nocc+1,mo_num
              indx+=1
              diag_SCI(indx)=orben(a)-orben(i)+levelshift
             end do
            end do
            call davidson_general(bestvec_SCI,diag_SCI,bestval_SCI,nMonoEx)
           else
! construct CI matrix and diagonalize it
            SCI_mat=0.D0
            eval_SCI=0.D0
            evec_SCI=0.D0
!          indx=1
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (i,a,indx) SHARED (ifrag,nocc_frag,mo_num,Fock_mo,SCI_mat)
            do i=1,nocc_frag(ifrag)
             do a=nocc_frag(ifrag)+1,mo_num
              indx=1-nocc_frag(ifrag)+a+(i-1)*(mo_num-nocc_frag(ifrag))
              SCI_mat(1,indx)=Fock_mo(i,a)*sqrt(2.D0)
              SCI_mat(indx,1)=Fock_mo(i,a)*sqrt(2.D0)
             end do
            end do
!$XXYXXY END PARALLEL DO
            
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (i,a,j,b,indxi,indxj,lij,lab) & 
!$XXYXXY SHARED (ifrag,nocc_frag,mo_num,Fock_mo,SCI_mat)
            do i=1,nocc_frag(ifrag)
             do a=nocc_frag(ifrag)+1,mo_num
              indxi=1-nocc_frag(ifrag)+a+(i-1)*(mo_num-nocc_frag(ifrag))
              do j=1,nocc_frag(ifrag)
               lij=i.eq.j
               do b=nocc_frag(ifrag)+1,mo_num
                indxj=1-nocc_frag(ifrag)+b+(j-1)*(mo_num-nocc_frag(ifrag))
                lab=a.eq.b
                if (lij) then 
                 SCI_mat(indxi,indxj)+=Fock_mo(a,b)
                end if
                if (lab) then
                 SCI_mat(indxi,indxj)-=Fock_mo(i,j)
                end if
               end do
              end do
             end do
            end do
!$XXYXXY END PARALLEL DO

            if (bavard) then
             write(6,*) ' diagonal of the SCI matrix '
             write(6,'(5(i4,f15.5))') (i,SCI_mat(i,i),i=1,nMonoEx)
            end if
            call lapack_diag(eval_SCI,evec_SCI,SCI_mat,nMonoMax,nMonoEx)
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (i) SHARED (nMonoEx,bestvec_SCI,evec_SCI)
            do i=1,nMonoEx
             bestvec_SCI(i)=evec_SCI(i,1)
            end do
!$XXYXXY END PARALLEL DO
            bestval_SCI=eval_SCI(1)
           end if
           
           write(6,*) ' Energy lowering, weight of the reference ',bestval_SCI,bestvec_SCI(1)
! intermediate normalization for the eigenvector
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (i) SHARED (ci_scaling,nMonoEx,bestvec_SCI,evec_SCI)
           do i=2,nMonoEx
            bestvec_SCI(i)*=1.D0/bestvec_SCI(1)*ci_scaling
           end do
!$XXYXXY END PARALLEL DO
           bestvec_SCI(1)=1.D0
           
! in order to parallelize we need an index function ...
           
           ss=0.D0
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (i,a,indx) &
!$XXYXXY SHARED (ss,nocc_frag,ifrag,mo_num,bestvec_SCI)
           do i=1,nocc_frag(ifrag)
            do a=1+nocc_frag(ifrag),mo_num
             indx=a-nocc_frag(ifrag)+(mo_num-nocc_frag(ifrag))*(i-1)+1
             if (abs(bestvec_SCI(indx)).gt.1.D0) bestvec_SCI(indx)= &
                  bestvec_SCI(indx)/abs(bestvec_SCI(indx))*0.5D0
             ss+=bestvec_SCI(indx)*bestvec_SCI(indx)
            end do
           end do
!$XXYXXY END PARALLEL DO
           ss=sqrt(ss)/nMonoEx
           do i=1,nocc_frag(ifrag)
            do a=1+nocc_frag(ifrag),mo_num
             indx=a-nocc_frag(ifrag)+(mo_num-nocc_frag(ifrag))*(i-1)+1
             if (abs(bestvec_SCI(indx)).gt.1.D-4) write(6,*) ' excitation ' &
                  ,i,' - ',a,' weight ',bestvec_SCI(indx)
            end do
           end do
           
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (i) &
!$XXYXXY SHARED (nMonoEx,ci_scaling,bestvec_SCI)
           do i=2,nMonoEx
            bestvec_SCI(i)*=1.D0/bestvec_SCI(1)*ci_scaling
           end do
!$XXYXXY END PARALLEL DO
           write(6,*) ' average excitation = ',ss,' scaling factor = ',ci_scaling
           
! update the orbitals
!$XXYXXY PARALLEL DO &
!$XXYXXY DEFAULT (none) &
!$XXYXXY PRIVATE (i,a,indx,cia,alpha) &
!$XXYXXY SHARED (ifrag,nocc_frag,bestvec_SCI,mo_num,ao_num,orb_work,orb_work_frag)
           do i=1,nocc_frag(ifrag)
            do a=nocc_frag(ifrag)+1,mo_num
             indx=a-nocc_frag(ifrag)+(mo_num-nocc_frag(ifrag))*(i-1)+1
             cia=bestvec_SCI(indx)
             do alpha=1,ao_num
              orb_work(alpha,i)+=cia*orb_work_frag(alpha,a,ifrag)
              orb_work(alpha,a)-=cia*orb_work_frag(alpha,i,ifrag)
             end do
            end do
           end do
!$XXYXXY END PARALLEL DO
! orthogonalize the orbitals
           call ortho_frag(orb_work,nocc_frag(ifrag))
! put orbitals back to orb_work_frag
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (i,alpha) &
!$XXYXXY SHARED (ao_num,mo_num,orb_work_frag,ifrag,orb_work)
           do alpha=1,ao_num
            do i=1,mo_num
             orb_work_frag(alpha,i,ifrag)=orb_work(alpha,i)
            end do
           end do
!$XXYXXY END PARALLEL DO
           orb_work=0.D0
! close loop over ifrag
          end do

! fill P_next
          call fill_p_next_2nd

          delta_P=0.d0
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (i,j,ifrag,pp) &
!$XXYXXY SHARED (ao_num,p_next_frag,density_frag,delta_p)
          do ifrag=1,nfrag
           do i=1,ao_num
            do j=1,ao_num
             pp=p_next_frag(i,j,ifrag)-density_frag(i,j,ifrag)
             delta_P=delta_P+pp*pp
            end do
           end do
          end do
!$XXYXXY END PARALLEL DO
          delta_P=delta_P/(ao_num*ao_num*nfrag)

! here we should give xi to xi_old and recalculate xi from p_next

          xi_old_frag=xi_coul_frag+xi_exch_frag
          dens_old_frag=density_frag
          density_frag=p_next_frag
! touch for invalidating xi
          touch density_frag 

          do ifrag=1,nfrag
           Ekin_frag(ifrag,2)=0.D0
           Eone_frag(ifrag,2)=0.D0
           Ecoul_frag(ifrag,2)=0.D0
           Eexch_frag(ifrag,2)=0.D0
          end do

!$XXYXXY PARALLEL DO &
!$XXYXXY DEFAULT (none) &
!$XXYXXY PRIVATE (ifrag,alpha,beta) &
!$XXYXXY SHARED (nfrag,ao_start_frag,ao_num,density_frag,Ekin_frag,Eone_frag,Ecoul_frag,Eexch_frag &
!$XXYXXY    ,ao_kinetic_integrals,oneham_frag,xi_coul_frag,xi_exch_frag)
          do ifrag=1,nfrag
           do alpha=1,ao_num
            do beta=1,ao_num
! one-electron terms
             Ekin_frag(ifrag,2)+=density_frag(alpha,beta,ifrag)*ao_kinetic_integrals(alpha,beta)
             Eone_frag(ifrag,2)+=density_frag(alpha,beta,ifrag)*(ao_kinetic_integrals(alpha,beta) &
                  +oneham_frag(alpha,beta,ifrag))
! one-electron terms
             Ecoul_frag(ifrag,2)+=density_frag(alpha,beta,ifrag)*xi_coul_frag(alpha,beta,ifrag)
             Eexch_frag(ifrag,2)+=density_frag(alpha,beta,ifrag)*xi_exch_frag(alpha,beta,ifrag)
            end do
           end do
           Ecoul_frag(ifrag,2)*=0.5D0
           Eexch_frag(ifrag,2)*=0.5D0
          end do
!$XXYXXY END PARALLEL DO

          delta_e=0.D0
          do ifrag=1,nfrag
           write(6,*) ' Fragment No ',ifrag
           write(6,9901) Ekin_frag(ifrag,2),Eone_frag(ifrag,2)    &
                       ,Ecoul_frag(ifrag,2),Eexch_frag(ifrag,2),EN_frag(ifrag)

           etot_frag(ifrag,2)= en_frag(ifrag) + eone_frag(ifrag,2) &
                + ecoul_frag(ifrag,2) + eexch_frag(ifrag,2)
           write(6,*) ' Etot_frag = ',etot_frag(ifrag,2)
           write(6,*) ' real energy lowering : ',etot_frag(ifrag,2)-eold_frag(ifrag)
           delta_e+=abs(etot_frag(ifrag,2)-eold_frag(ifrag))
          end do
          delta_e*=1.D0/dble(nfrag)
          write(6,*) ' delta_E:', delta_e
          write(6,*) ' Delta P :',delta_P
          iter_scf=iter_scf+1
          
! the optimal damping algorithm of Eric Cancès and Claude Le Bris
          if (iter_scf.gt.0) then
           write(6,*) ' Optimal Damping Algorithm of Cances et al.'
           do ifrag=1,nfrag
            write(6,*) '   Fragment No ',ifrag
            a1=0.D0
            a2=0.D0
            b1=0.D0
            b2=0.D0
            b3=0.D0
! this we do not parallelize
            do alpha=1,ao_num
             do beta=1,ao_num
              a1+=(ao_kinetic_integrals(alpha,beta)+oneham_frag(alpha,beta,ifrag)) &
                   *dens_old_frag(alpha,beta,ifrag)
              a2+=(ao_kinetic_integrals(alpha,beta)+oneham_frag(alpha,beta,ifrag)) &
                   *p_next_frag(alpha,beta,ifrag)
              b1+=xi_old_frag(alpha,beta,ifrag)*dens_old_frag(alpha,beta,ifrag)
              b2+=xi_old_frag(alpha,beta,ifrag)*p_next_frag(alpha,beta,ifrag)
              b3+=(xi_coul_frag(alpha,beta,ifrag)+xi_exch_frag(alpha,beta,ifrag)) &
                   *p_next_frag(alpha,beta,ifrag)
             end do
            end do
            b1*=0.5D0
            b3*=0.5D0
            oda_denom=b1-b2+b3
            if ((oda_denom).lt.0.D0) then
             write(6,*) ' negative denominator, setting lambda to 1.0'
             lambda_oda_frag(ifrag)=1.0D0
            else
             if (abs(oda_denom).lt.1.D-12) then
              write(6,*) ' very small denominator, setting lambda to 0.5'
              lambda_oda_frag(ifrag)=0.5D0
             else
              lambda_oda_frag(ifrag)=-(a2-a1+b2-2.D0*b1)/oda_denom*0.5D0
             end if
            end if
            write(6,*) ' optimal lambda is ',lambda_oda_frag(ifrag)
            if (lambda_oda_frag(ifrag).lt.0.D0.or.lambda_oda_frag(ifrag).gt.1.D0) then
             write(6,*) ' lambda outside [0,1], setting lambda to 1 '
             lambda_oda_frag(ifrag)=1.0D0
            end if
            e_opt=a1+b1+En_frag(ifrag)+lambda_oda_frag(ifrag)* &
                   (a2-a1+b2-2.D0*b1 +lambda_oda_frag(ifrag)* &
                   (b1-b2+b3))
            if (e_opt.gt.eold_frag(ifrag)) lambda_oda_frag(ifrag)=-lambda_oda_frag(ifrag)
            write(6,*) ' best energy and lowering :',e_opt,e_opt-eold_frag(ifrag)
           end do
          else
           lambda_oda_frag=1.D0
          end if

          if (iter_scf.gt.max_iter) then
           write(6,*) ' max number of iterations ',max_iter,' attained '
           exit
          end if
! close loop for the second SCF
          call cpustamp(cpu_1,wall_1,cpu_1,wall_1,'IT 2')
         end do

write(6,*) ' =========================================================== '
write(6,*) '  Step 2 : Convergence after ',iter_scf,' iterations '
         do ifrag=1,nfrag
          write(6,*) '  Fragment ',ifrag,': total energy = ',etot_frag(ifrag,2)
         end do
         call cpustamp(cpu_0,wall_0,cpu_0,wall_0,'2nd ')
write(6,*) ' =========================================================== '
         if (l_davidson) then
          deallocate (diag_SCI)
         else
          deallocate (evec_SCI)
          deallocate (eval_SCI)
          deallocate (SCI_mat)
         end if
         deallocate (bestvec_SCI)

! save the orbitals
         indx_occ=0
         do ifrag=1,nfrag
! pick out the occupied orbitals of each monomer, and place them in orb_occ
! we do not care about the virtual orbitals
          do i=1,nocc_frag(ifrag)
           indx_occ+=1
           do alpha=1,ao_num
            orb_occ(alpha,indx_occ,2)=orb_work_frag(alpha,i,ifrag)
           end do
          end do
         end do

         if (lmp2) then
          do ifrag=1,nfrag
           emp2_frag(ifrag,2)=0.D0

          call clear_mo_map
           do i=1,mo_num
            if (i.le.nocc_frag(ifrag)) then
             mo_class(i)='Core'
            else
             mo_class(i)='Virtual'
            end if
           end do
           touch mo_class
 write(6,*) ' touched mo_class'

           do i=1,mo_num
            do alpha=1,ao_num
             orb_work(alpha,i)=orb_work_frag(alpha,i,ifrag)
            end do
           end do
! construct again the Fock matrix and diagonalize it
! allows to use the formula for MP2 in canonical orbitals
           fock_ao=0.D0
!$XXYXXY  PARALLEL DO &
!$XXYXXY  DEFAULT (none) &
!$XXYXXY  PRIVATE (alpha,beta) &
!$XXYXXY  SHARED (fock_ao,ao_num,ao_kinetic_integrals,oneham_frag,    &
!$XXYXXY          ifrag,xi_coul_frag,xi_exch_frag)
           do alpha=1,ao_num
            do beta=1,ao_num
             fock_ao(alpha,beta)=ao_kinetic_integrals(alpha,beta)
             fock_ao(alpha,beta)+=oneham_frag(alpha,beta,ifrag)
             fock_ao(alpha,beta)+=xi_coul_frag(alpha,beta,ifrag)
             fock_ao(alpha,beta)+=xi_exch_frag(alpha,beta,ifrag)
            end do
           end do
!$XXYXXY END PARALLEL DO
           call transf_ao_mo(fock_ao,ao_num,fock_mo,mo_num,orb_work)
           call lapack_diag(orben,evec_fock,fock_mo,mo_num,mo_num)

           mo_coef=0.D0
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (alpha,i,j,ss) &
!$XXYXXY SHARED (ao_num,mo_num,evec_fock,orb_work,mo_coef) 
           do alpha=1,ao_num
            do i=1,mo_num
             ss=0.D0
             do j=1,mo_num
              ss+=evec_fock(j,i)*orb_work(alpha,j)
             end do
             mo_coef(alpha,i)=ss
            end do
           end do
!$XXYXXY END PARALLEL DO

           touch mo_coef
 write(6,*) ' touched mo_coef'

           emp2_frag(ifrag,2)=0.D0
           do i=1,nocc_frag(ifrag)
            do j=1,nocc_frag(ifrag)
!            write(6,*) ' i, j',i,j
             call get_mo_two_e_integrals_ij(i,j,mo_num,integrals_array,mo_integrals_map)
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (a,b,int_iajb,int_ibja,denom)  &
!$XXYXXY SHARED (nocc_frag,ifrag,mo_num,i,j,integrals_array,orben,emp2_frag)
             do a=nocc_frag(ifrag)+1,mo_num
              do b=nocc_frag(ifrag)+1,mo_num
               int_iajb=integrals_array(a,b)
               int_ibja=integrals_array(b,a)
               denom=orben(a)+orben(b)-orben(i)-orben(j)
               emp2_frag(ifrag,2)-=int_iajb*(2.D0*int_iajb-int_ibja)/denom
!              write(6,*) i,j,a,b,int_iajb,int_ibja,denom,emp2_frag(ifrag,2)
              end do
             end do
!$XXYXXY END PARALLEL DO
            end do
           end do
           write(6,*) ' Fragment No ',ifrag,': EMP2 = ',emp2_frag(ifrag,2)
          end do
         call cpustamp(cpu_0,wall_0,cpu_0,wall_0,'MP2 ')
 write(6,*) '------------------------------------------ '
         end if
!
! we have the converged density_frag and xi available 
! for calculating electrostatic interacions between fragments
!
! e-n and e-e together
!$XXYXXY  PARALLEL DO &
!$XXYXXY  DEFAULT (none) &
!$XXYXXY  PRIVATE (ifrag,jfrag,alpha,beta,ss) &
!$XXYXXY  SHARED (nfrag,ao_num,density_frag,elstat,oneham_frag,xi_coul_frag)
           do ifrag=1,nfrag
            do jfrag=ifrag+1,nfrag
             ss=0.D0
             do alpha=1,ao_num
              do beta=1,ao_num
! e-n
! we need the integrals of n on ifrag and electrons on jfrag
               ss+=density_frag(alpha,beta,ifrag) &
                 *oneham_frag(alpha,beta,jfrag)
               ss+=density_frag(alpha,beta,jfrag) &
                 *oneham_frag(alpha,beta,ifrag)
! e-e
               ss+=density_frag(alpha,beta,ifrag) &
                 *xi_coul_frag(alpha,beta,jfrag)
              end do
             end do
             elstat(2)+=ss
            end do
           end do
!$XXYXXY  END PARALLEL DO

! the same as before for the 2nd set of occupied orbitals
! for the Heitler-London or Pauli term we orthogonalize the occupied orbitals
! and recalculate kinetic energy, potential energy and exchange 
         write(6,*)
         write(6,*) ' Calculating the Pauli interaction terms' 
         write(6,*)
         do i=1,nocc
          do alpha=1,ao_num
           orb_work_frag(alpha,i,1)=orb_occ(alpha,i,2)
          end do
         end do
         call shalf(1,nocc,orb_work_frag(1,1,1))
! in the first fragment we accumulate the difference density between 
! the converged monomer densities and the density from orthogonalized 
! monomer orbitals
! the difference density will serve for the electrostatic terms
! however, for the e-e terms we will need the cross-terms density*xi
! e-e = rho_d(1)rho_d(2) - rho_m(1)rho_m(2) 
!     = (rho_d(1)-rho_m(1))(rho_d(2)-rho_m(2))
! set R=rho_d, r=rho_m, thus 
! RR-rr = (R-r)(R-r) + r(R-r) + (R-r)r = (R-r)(R-r) + 2(R-r)r
! 
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (ifrag,alpha,beta) &
!$XXYXXY SHARED (nfrag,ao_num,density_frag)
         do ifrag=2,nfrag
          do alpha=1,ao_num
           do beta=1,ao_num
            density_frag(alpha,beta,1)+= density_frag(alpha,beta,ifrag)
           end do
          end do
         end do
!$XXYXXY END PARALLEL DO
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (i,alpha,beta) &
!$XXYXXY SHARED (nocc,ao_num,orb_work_frag,density_frag)
         do i=1,nocc
          do alpha=1,ao_num
           do beta=1,ao_num
            density_frag(alpha,beta,1)-=orb_work_frag(alpha,i,1)   &
               *orb_work_frag(beta,i,1)*2.D0
           end do
          end do
         end do
!$XXYXXY END PARALLEL DO
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (alpha,beta) &
!$XXYXXY SHARED (ao_num,density_frag)
         do alpha=1,ao_num
          do beta=1,ao_num
           density_frag(alpha,beta,1)*= -1.D0
          end do
         end do
!$XXYXXY END PARALLEL DO
! we have now the difference density
! before recalculating xi_coul_frag and xi_exch_graf

         dtpauli(2)=0.D0
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (alpha,beta) &
!$XXYXXY SHARED (ao_num,density_frag,dtpauli,ao_kinetic_integrals)
         do alpha=1,ao_num
          do beta=1,ao_num
           dtpauli(2)+=density_frag(alpha,beta,1)*ao_kinetic_integrals(alpha,beta)
          end do
         end do
!$XXYXXY END PARALLEL DO
         dvpauli(2)=0.D0
         dxpauli(2)=0.D0
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (ifrag,alpha,beta) &
!$XXYXXY SHARED (ao_num,nfrag,density_frag,dvpauli,xi_coul_frag,oneham_frag,dxpauli,xi_exch_frag)
         do ifrag=1,nfrag
          do alpha=1,ao_num
           do beta=1,ao_num
            dvpauli(2)+=density_frag(alpha,beta,1)*xi_coul_frag(alpha,beta,ifrag)
            dvpauli(2)+=density_frag(alpha,beta,1)*oneham_frag(alpha,beta,ifrag)
            dxpauli(2)+=density_frag(alpha,beta,1)*xi_exch_frag(alpha,beta,ifrag)
           end do
          end do
         end do
!$XXYXXY END PARALLEL DO
         touch density_frag
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (alpha,beta) &
!$XXYXXY SHARED (ao_num,density_frag,dvpauli,xi_coul_frag,dxpauli,xi_exch_frag)
         do alpha=1,ao_num
          do beta=1,ao_num
           dvpauli(2)+=density_frag(alpha,beta,1)*xi_coul_frag(alpha,beta,1)
           dxpauli(2)+=density_frag(alpha,beta,1)*xi_exch_frag(alpha,beta,1)
          end do
         end do
!$XXYXXY END PARALLEL DO
         write(6,*)
         write(6,*) ' ... done ' 
         write(6,*)

         write(6,*) ' 1 : dtpauli, dvpauli, dxpauli = ', dtpauli(1), dvpauli(1), dxpauli(1) 
         write(6,*) ' 2 : dtpauli, dvpauli, dxpauli = ', dtpauli(2), dvpauli(2), dxpauli(2) 

         Etot=0.D0
         ECOUL =0.D0
         EONE =0.D0
         EKIN =0.D0
         EEXCH =0.D0
         EDFTXC =0.D0

         call cpustamp(cpu_0,wall_0,cpu_0,wall_0,'XV 3')

! get the orbitals of the previous steps
         orb_work=0.D0
! occupied ones
         do i=1,nocc
          do alpha=1,ao_num
           orb_work(alpha,i)=orb_occ(alpha,i,2)
          end do
         end do
! virtual ones
         do i=nocc+1,mo_num
          do alpha=1,ao_num
           orb_work(alpha,i)=orb_virt1(alpha,i)
          end do
         end do

         write(6,*) ' orbitals '
         do i=1,mo_num
          write(6,*) ' Orbital No ',i
          write(6,'(5(I4,F16.6))') (alpha,orb_work(alpha,i),alpha=1,ao_num)
         end do
! orthogonalize
         call ortho(orb_work)
! fill density3

         density3=0.D0
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (alpha,beta,i) &
!$XXYXXY SHARED (nocc,ao_num,density3,orb_work)
         do alpha=1,ao_num
          do beta=1,ao_num
           do i=1,nocc
            density3(alpha,beta)+=orb_work(alpha,i)*orb_work(beta,i)
           end do
          end do
         end do
!$XXYXXY END PARALLEL DO
         density3*=2.D0

         touch density3
! calculate total energy
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (alpha,beta) &
!$XXYXXY SHARED (ao_num,density3,ao_kinetic_integrals &
!$XXYXXY ,ao_one_e_integrals,xi_coul3,xi_exch3,Ekin,Eone,Ecoul,Eexch)
         do alpha=1,ao_num
          do beta=1,ao_num
! one-electron terms
           Ekin+=density3(alpha,beta)*ao_kinetic_integrals(alpha,beta)
           Eone+=density3(alpha,beta)*ao_one_e_integrals(alpha,beta)
! two-electron terms
           Ecoul+=density3(alpha,beta)*xi_coul3(alpha,beta)
           Eexch+=density3(alpha,beta)*xi_exch3(alpha,beta)
          end do
         end do
!$XXYXXY END PARALLEL DO
         Ecoul*=0.5D0
         Eexch*=0.5D0
         etot= nuclear_repulsion + eone + ecoul + eexch
         write(6,9901) Ekin,Eone,Ecoul,Eexch,nuclear_repulsion
  9901 format(' Ekin=',F11.5,' Eone=',F11.5,' ECoul=',F11.5,' Eexch = ',F11.5,' nuclear= ',F11.5)
! 9901 format('          Ekin = ',F15.5,/ &
!            ,'          Eone = ',F15.5,/ &
!            ,'         ECoul = ',F15.5,/ &
!            ,'         Eexch = ',F15.5,/ &
!            ,'       nuclear = ',F15.5)
         write(6,*) ' Etot = ',etot
         write(6,*)
          nMonoEx=1+nocc*(mo_num-nocc)
         if (l_Davidson) then
          allocate (diag_SCI(nMonoEx))
         else
          allocate (SCI_mat(nMonoEx,nMonoEx))
          allocate (evec_SCI(nMonoEx,nMonoEx))
          allocate (eval_SCI(nMonoEx))
         end if
         allocate (bestvec_SCI(nMonoEx))
         allocate (orb_tmp(mo_num))

         call cpustamp(cpu_0,wall_0,cpu_0,wall_0,'pre3')
         cpu_1=cpu_0
         wall_1=wall_0

         delta_e=huge(1.d0)
         delta_p=huge(1.d0)
         iter_scf=0
         p_next=0.D0
         lambda_oda=1.D0
         xi_old=0.D0
         cpu_1=cpu_0
         wall_1=wall_0
         do while (delta_E.ge.thr_scf.or.delta_P.ge.thr_P)

          write(6,*)
          write(6,*)  ' ============================================ '
          write(6,*)  '    3rd step Iteration No ',iter_scf
          write(6,*)  ' ============================================ '

          eold=etot
          write(6,*) ' energy is ',etot

! construct Fock matrix, and perform Singles CI, for the whole system
! density3, xi3 and orbitals are coherent
          write(6,*) ' Singles-CI step '
          fock_ao=0.D0
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (alpha,beta) &
!$XXYXXY SHARED (ao_num,fock_ao,ao_one_e_integrals,lambda_oda,xi_coul3,xi_exch3,xi_old)
          do alpha=1,ao_num
           do beta=1,ao_num
            fock_ao(alpha,beta)=ao_one_e_integrals(alpha,beta)
            fock_ao(alpha,beta)+=lambda_oda*xi_coul3(alpha,beta)
            fock_ao(alpha,beta)+=lambda_oda*xi_exch3(alpha,beta)
            fock_ao(alpha,beta)+=(1.D0-lambda_oda)*xi_old(alpha,beta)
           end do
          end do
!$XXYXXY END PARALLEL DO

          call transf_ao_mo(fock_ao,ao_num,fock_mo,mo_num,orb_work)

          write(6,*) ' diagonal of the Fock matrix in MOs'
          do i=1,mo_num
           orben(i)=fock_mo(i,i)
          end do
          write(6,'(5(i4,f15.5))') (i,orben(i),i=1,mo_num)

          if (l_DAVIDSON) then
           bestvec_SCI=0.D0
           bestvec_SCI(1)=1.D0
           diag_SCI(1)=0.D0
           indx=1
           do i=1,nocc
            do a=nocc+1,mo_num
             indx+=1
             diag_SCI(indx)=orben(a)-orben(i)
            end do
           end do
!   write(6,*) ' diagonal of the SCI matrix '
!   write(6,'(5(i4,F15.5))') (indx,diag_SCI(indx),indx=1,nMonoEx)
             call davidson_general(bestvec_SCI(1),diag_SCI,bestval_SCI,nMonoEx)
          else
! construct CI matrix and diagonalize it
           SCI_mat=0.D0
           eval_SCI=0.D0
           evec_SCI=0.D0
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (i,a,indx) &
!$XXYXXY SHARED (mo_num,nocc,fock_mo,SCI_mat)
           do i=1,nocc
            do a=nocc+1,mo_num
             indx=1+(i-1)*(mo_num-nocc)+a-nocc
             SCI_mat(1,indx)=Fock_mo(i,a)*sqrt(2.D0)
             SCI_mat(indx,1)=Fock_mo(i,a)*sqrt(2.D0)
            end do
           end do
!$XXYXXY END PARALLEL DO
           
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (i,a,indxi,j,b,indxj,lij,lab) &
!$XXYXXY SHARED (mo_num,nocc,fock_mo,SCI_mat)
           do i=1,nocc
            do a=nocc+1,mo_num
             do j=1,nocc
              do b=nocc+1,mo_num
               indxi=1+(i-1)*(mo_num-nocc)+a-nocc
               indxj=1+(j-1)*(mo_num-nocc)+b-nocc
               lij=i.eq.j
               lab=a.eq.b
               if (lij) then
                SCI_mat(indxi,indxj)+=Fock_mo(a,b)
               end if
               if (lab) then
                SCI_mat(indxi,indxj)-=Fock_mo(i,j)
               end if
              end do
             end do
            end do
           end do
!$XXYXXY END PARALLEL DO
!           write(6,*) ' diagonal of the SCI matrix '
!           write(6,'(5(i4,f15.5))') (i,SCI_mat(i,i),i=1,nMonoEx)
           call lapack_diag(eval_SCI,evec_SCI,SCI_mat,nMonoEx,nMonoEx)
           do i=1,nMonoEx
            bestvec_SCI(i)=evec_SCI(i,1)
           end do
           bestval_SCI=eval_SCI(1)
          end if     
          write(6,*) ' Energy lowering, weight of the reference ',     &
              bestval_SCI,bestvec_SCI(1)
! intermediate normalization for the eigenvector
          do i=2,nMonoEx
           bestvec_SCI(i)*=1.D0/bestvec_SCI(1)*ci_scaling
          end do
          bestvec_SCI(1)=1.D0

          indx=1
          ss=0.D0
          do i=1,nocc
           do a=1+nocc,mo_num
            indx+=1
            if (abs(bestvec_SCI(indx)).gt.1.D0) bestvec_SCI(indx)= &
                 bestvec_SCI(indx)/abs(bestvec_SCI(indx))*0.5D0
            if (abs(bestvec_SCI(indx)).gt.1.D-4) write(6,*) ' excitation ' &
               ,i,' - ',a,' weight ',bestvec_SCI(indx)
            ss+=bestvec_SCI(indx)*bestvec_SCI(indx)
           end do
          end do
          ss=sqrt(ss)/nMonoEx
          write(6,*) ' average excitation = ',ss,' scaling factor = ',ci_scaling

! update the orbitals,
          do alpha=1,ao_num
           do i=1,mo_num
            orb_tmp(i)=orb_work(alpha,i)
           end do
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (i,a,indx,cia) &
!$XXYXXY SHARED (nocc,mo_num,bestvec_SCI,alpha,orb_tmp,orb_work)
           do i=1,nocc
            do a=nocc+1,mo_num
             indx=1+(i-1)*(mo_num-nocc)+a-nocc
             cia=bestvec_SCI(indx)
             orb_tmp(i)+=cia*orb_work(alpha,a)
             orb_tmp(a)-=cia*orb_work(alpha,i)
            end do
           end do
!$XXYXXY END PARALLEL DO
           do i=1,mo_num
            orb_work(alpha,i)=orb_tmp(i)
           end do
          end do
! orthogonalize the orbitals
          call ortho(orb_work)

! fill P_next
          call fill_p_next_3rd

          delta_P=0.d0
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (i,j,pp) &
!$XXYXXY SHARED (ao_num,delta_p,p_next,density3)
          do i=1,ao_num
           do j=1,ao_num
            pp=p_next(i,j)-density3(i,j)
            delta_P=delta_P+pp*pp
           end do
          end do
!$XXYXXY END PARALLEL DO
          delta_P=delta_P/(ao_num*ao_num)

! here we should give xi to xi_old and recalculate xi from p_next

          xi_old=xi_coul3+xi_exch3
          dens_old=density3
          density3=p_next
! touch for invalidating xi
          touch density3

          delta_e=0.D0
          Ekin =0.D0
          Eone =0.D0
          Ecoul=0.D0
          Eexch=0.D0
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (alpha,beta) &
!$XXYXXY SHARED (ao_num,Ekin,Eone,density3,ao_kinetic_integrals &
!$XXYXXY ,ao_one_e_integrals,Ecoul,Eexch,xi_coul3,xi_exch3)
          do alpha=1,ao_num
           do beta=1,ao_num
! one-electron terms
            Ekin+=density3(alpha,beta)*ao_kinetic_integrals(alpha,beta)
            Eone+=density3(alpha,beta)*ao_one_e_integrals(alpha,beta)
! one-electron terms
            Ecoul+=density3(alpha,beta)*xi_coul3(alpha,beta)
            Eexch+=density3(alpha,beta)*xi_exch3(alpha,beta)
           end do
          end do
!$XXYXXY END PARALLEL DO
          Ecoul*=0.5D0
          Eexch*=0.5D0
          write(6,9901) Ekin,Eone,Ecoul,Eexch,nuclear_repulsion

          etot= nuclear_repulsion + eone + ecoul + eexch
          write(6,*) ' Etot = ',etot
          write(6,*) ' real energy lowering : ',etot-eold
          delta_e+=etot-eold
          write(6,*) ' delta_E:', delta_e
          write(6,*) ' Delta P :',delta_P
          iter_scf=iter_scf+1

! the optimal damping algorithm of Eric Cancès and Claude Le Bris
          if (iter_scf.gt.0) then
           write(6,*) ' Optimal Damping Algorithm of Cances et al.'
           a1=0.D0
           a2=0.D0
           b1=0.D0
           b2=0.D0
           b3=0.D0
           do alpha=1,ao_num
            do beta=1,ao_num
             a1+=ao_one_e_integrals(alpha,beta)*dens_old(alpha,beta)
             a2+=ao_one_e_integrals(alpha,beta)*p_next(alpha,beta)
             b1+=xi_old(alpha,beta)*dens_old(alpha,beta)
             b2+=xi_old(alpha,beta)*p_next(alpha,beta)
             b3+=(xi_coul3(alpha,beta)+xi_exch3(alpha,beta)) &
                  *p_next(alpha,beta)
            end do
           end do
           b1*=0.5D0
           b3*=0.5D0
           oda_denom=b1-b2+b3
           if ((oda_denom).lt.0.D0) then
            write(6,*) ' negative denominator, setting lambda to 1.0'
            lambda_oda=1.0D0
           else
            if (abs(oda_denom).lt.1.D-12) then
             write(6,*) ' very small denominator, setting lambda to 0.5'
             lambda_oda=0.5D0
            else
             lambda_oda=-(a2-a1+b2-2.D0*b1)/oda_denom*0.5D0
            end if
           end if
           write(6,*) ' optimal lambda is ',lambda_oda
           if (lambda_oda.lt.0.D0.or.lambda_oda.gt.1.D0) then
            write(6,*) ' lambda outside [0,1], setting lambda to 1 '
            lambda_oda=1.0D0
           end if
           e_opt=a1+b1+nuclear_repulsion+lambda_oda* &
                  (a2-a1+b2-2.D0*b1 +lambda_oda*(b1-b2+b3))
           write(6,*) ' best energy and lowering :',e_opt,e_opt-eold
          else
           lambda_oda=1.D0
          end if

          if (iter_scf.gt.max_iter) then
           write(6,*) ' max number of iterations ',max_iter,' attained '
           exit
          end if
! close loop for the third SCF
         call cpustamp(cpu_1,wall_1,cpu_1,wall_1,'IT 3')

         end do

         write(6,*) ' =========================================================== '
         write(6,*) '  Step 3 : Convergence after ',iter_scf,' iterations '
         write(6,*) '   total energy = ',etot
         call cpustamp(cpu_0,wall_0,cpu_0,wall_0,'3rd ')
 
         write(6,*) ' =========================================================== '

         if (l_Davidson) then
          deallocate (diag_SCI)
         else
          deallocate (evec_SCI)
          deallocate (eval_SCI)
          deallocate (SCI_mat)
         end if
         deallocate (bestvec_SCI)

! save the orbitals
         orb_save=orb_work
         mo_coef=orb_save
         mo_label='Localized'
         call save_mos
         write(6,*) ' saved orbitals '

         if (lmp2) then
          write(6,*) ' MP2 for the whole system '
          call cpu_time(cpu_1)
          call wall_time(wall_1)

! construct again the Fock matrix and diagonalize it
           fock_ao=0.D0
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (alpha,beta) &
!$XXYXXY SHARED (ao_num,fock_ao,ao_one_e_integrals,xi_coul3,xi_exch3)
           do alpha=1,ao_num
            do beta=1,ao_num
             fock_ao(alpha,beta)=ao_one_e_integrals(alpha,beta)
             fock_ao(alpha,beta)+=xi_coul3(alpha,beta)
             fock_ao(alpha,beta)+=xi_exch3(alpha,beta)
            end do
           end do
!$XXYXXY END PARALLEL DO
           call transf_ao_mo(fock_ao,ao_num,fock_mo,mo_num,orb_save)
           call lapack_diag(orben,evec_fock,fock_mo,mo_num,mo_num)
!          write(6,*) ' orben : ',orben

           mo_coef=0.D0
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (alpha,i,j) &
!$XXYXXY SHARED (ao_num,mo_num,mo_coef,evec_fock,orb_save)
           do alpha=1,ao_num
            do i=1,mo_num
             do j=1,mo_num
              mo_coef(alpha,i)+=evec_fock(j,i)*orb_save(alpha,j)
             end do
            end do
           end do
!$XXYXXY END PARALLEL DO

          do i=1,nocc
           mo_class(i)='Core'
          end do
          do i=1+nocc,mo_num
           mo_class(i)='Virtual'
          end do

          call clear_mo_map
          touch mo_class
          touch mo_coef 
 
write(6,*) ' soft touched mo_coef and mo_class'

          emp2tot=0.D0
          do i=1,nocc
           do j=1,nocc
!           write(6,*) ' i, j',i,j
            call get_mo_two_e_integrals_ij(i,j,mo_num,integrals_array,mo_integrals_map)
!$XXYXXY PARALLEL DO DEFAULT (none) PRIVATE (a,b,int_iajb,int_ibja,denom) &
!$XXYXXY SHARED (mo_num,i,j,nocc,integrals_array,orben,emp2tot)
            do a=nocc+1,mo_num
             do b=nocc+1,mo_num
              int_iajb=integrals_array(a,b)
              int_ibja=integrals_array(b,a)
              denom=orben(a)+orben(b)-orben(i)-orben(j)
              emp2tot-=int_iajb*(2.D0*int_iajb-int_ibja)/denom
!             write(6,*) i,j,a,b,int_iajb,int_ibja,denom,emp2tot
             end do
            end do
!$XXYXXY END PARALLEL DO
           end do
          end do
          call cpustamp(cpu_0,wall_0,cpu_0,wall_0,'MP2 ')
          write(6,*) ' Complete system : EMP2 = ',emp2tot
          write(6,*) '------------------------------------------ '
          deallocate(integrals_array)
         end if
         deallocate (orb_tmp)
         deallocate (evec_fock)
         deallocate (eval_fock)
END_PROVIDER
