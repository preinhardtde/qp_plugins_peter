# qp_plugins_peter

my plugins for the only real quantum package
 - mp3 (an exercise on determinants in the quantum package)
 - pertinfini (summation of MPn and ENn series to any order, 
     with Shanks transformation and Richardson summation)
 - model_scf (a basic scf program, more as exercise than for production)
 - model_dft (a basic DFT program, interfaced to the library libxc of 
       common functionals, and as well the range-separated ones of the 
       quantum package)
 - complete_fci (a Full CI program based on the formula-tape approach
       of Knowles and Handy, prepares density-matrix and integral files 
       for the CASSCF orbital optimization process)
 - wdens (extracts density-matrices and integrals for a CAS orbital
       optimization from a CIPSI calculation)
 - casscf_peter (1st and 2nd-order orbital optimization step for a CAS)
 - blowup (projection of orbitals from one basis set to another, useful 
       for transporting information from one ezfio to another)
 - localize (localization of orbitals, Boys or Pipek-Mezey, orbital 
       analysis, create a guess from atomic wfs, rotate atomic orbitals
       of eigenfunctions of l and lz)
 - nci (plot orbitals, calculate Berlin function, NCI, Kato's condition 
       on grids)
 - segments (triple-jump Hartree-Fock for segmented systems, including 
       MP2)

Peter Reinhardt, LCT, Sorbonne University, Paris;
Peter.Reinhardt @ upmc.fr
