! -*- F90 -*-
subroutine frobenius_product(orbs1,n_orbs1,orbs2,n_orbs2)
BEGIN_DOC
! calculates the Frobenius product between two orbitals sets
END_DOC
      implicit none
      integer :: n_orbs1,n_orbs2
      real*8 :: orbs1(ao_num,n_orbs1)
      real*8 :: orbs2(ao_num,n_orbs2)
      integer :: i,j,alpha,beta
      real*8 :: sss,frob,overlap_2MOs

      write(6,*) ' Frobenius, n_orbs1, n_orbs2 = ',n_orbs1, n_orbs2

      frob=0.D0
      do i=1,n_orbs1
       do j=1,n_orbs2
        sss=overlap_2MOs(orbs1(1,i),orbs2(1,j))
!       sss=0.D0
!       do alpha=1,ao_num
!        do beta=1,ao_num
!         sss+=orbs1(alpha,i)*orbs2(beta,j)*ao_overlap(alpha,beta)
!        end do
!       end do
        frob+=sss*sss
!       if (abs(sss).gt.1.D-4) write(6,*) ' Frobenius, i, j, sss = ',i, j, sss
       end do
      end do
      frob*=1.D0/sqrt(dble(n_orbs1*n_orbs2))
      write(6,*) ' Frobenius product is ',frob
end subroutine frobenius_product
