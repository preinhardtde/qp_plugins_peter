! -*- F90 -*-
subroutine transf_ao_mo(A_ao,LDA_ao,A_mo,LDA_mo,orbs)
BEGIN_DOC
! Transform A from the |AO| basis to orbs
!
! $C^\dagger.A_{ao}.C$
END_DOC
  implicit none
  integer, intent(in)            :: LDA_ao,LDA_mo
  double precision, intent(in)   :: A_ao(LDA_ao,ao_num)
  double precision, intent(out)  :: A_mo(LDA_mo,mo_num)
  real*8 :: orbs(ao_num,mo_num)
  double precision, allocatable  :: T(:,:)

  allocate ( T(ao_num,mo_num) )
  !DIR$ ATTRIBUTES ALIGN : $IRP_ALIGN :: T

  call dgemm('N','N', ao_num, mo_num, ao_num,                    &
      1.d0, A_ao,LDA_ao,                                             &
      orbs, size(orbs,1),                                      &
      0.d0, T, size(T,1))

  call dgemm('T','N', mo_num, mo_num, ao_num,                &
      1.d0, orbs,size(orbs,1),                                 &
      T, ao_num,                                                     &
      0.d0, A_mo, size(A_mo,1))

  deallocate(T)
end subroutine transf_ao_mo

subroutine transf2sq(A_ao,A_mo,norbs,orbs)
BEGIN_DOC
! Transform A from the |AO| basis to orbs
! A is a square matrix in AOs and MOs
!
! $C^\dagger.A_{ao}.C$
END_DOC
  implicit none
  integer, intent(in)            :: norbs
  double precision, intent(in)   :: A_ao(ao_num,ao_num)
  double precision, intent(out)  :: A_mo(norbs,norbs)
  real*8 :: orbs(ao_num,norbs)
  double precision, allocatable  :: T(:,:)

  allocate ( T(ao_num,norbs) )
  !DIR$ ATTRIBUTES ALIGN : $IRP_ALIGN :: T

  call dgemm('N','N', ao_num, norbs, ao_num,                    &
      1.d0, A_ao,ao_num,                                             &
      orbs, size(orbs,1),                                      &
      0.d0, T, size(T,1))

  call dgemm('T','N', norbs, norbs, ao_num,                &
      1.d0, orbs,size(orbs,1),                                 &
      T, ao_num,                                                     &
      0.d0, A_mo, size(A_mo,1))

  deallocate(T)
end subroutine transf2sq


