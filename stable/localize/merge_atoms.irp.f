! -*- F90 -*-
program correct_l
  implicit none
  BEGIN_DOC
! merge orbitals for each atom type in order to create a 
! set of guess orbitals
  END_DOC
real*8 :: cpu0,wall0
 read_wf = .True.
 touch read_wf

          call cpu_time(cpu0)
          call wall_time(wall0)
         
          call header
          call driver
          call cpustamp(cpu0,wall0,cpu0,wall0,'M AT')
end program correct_l 

     subroutine header
       write(6,*)
       write(6,*) ' create a set of guess orbitals from stored atomic'
       write(6,*) ' orbitals                '
       write(6,*) '  P Reinhardt, Paris, July 2020 '
       write(6,*) '     in confinement we trust '
       write(6,*)
     end subroutine header

     subroutine driver
       implicit none
       integer :: iat

       n_core_orb=ncore_tot
       n_act_orb=nact_tot
       n_virt_orb=nvirt_tot
       write(6,*) 
       write(6,*) ' found in total '
       write(6,*) n_core_orb,' core orbitals '
       write(6,*) n_act_orb ,' active orbitals '
       write(6,*) n_virt_orb,' virtual orbitals '
       write(6,*) 
  

       call orthogonalize_CAS(mo_merged)
       
       mo_coef=mo_merged
!
! hierarachial orthogonalization: core, act, virt

       WRITE(6,*)
       mo_label='Guess'
       call save_mos
        
     end subroutine driver

