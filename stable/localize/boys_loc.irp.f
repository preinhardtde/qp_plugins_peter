! -*- F90 -*-
program boys_localization
  implicit none
  BEGIN_DOC
! determination of Kato s condition around each atom
! we read a wavefunction, create density and grad density
! on a grid of points
  END_DOC
real*8 :: cpu0,wall0
 read_wf = .True.
 touch read_wf

          call cpu_time(cpu0)
          call wall_time(wall0)
         
          call header
          call driver
          call cpustamp(cpu0,wall0,cpu0,wall0,'KATO')
end program boys_localization

     subroutine header
       write(6,*)
       write(6,*) ' Boys localization of occupied orbitals '
       write(6,*) '   inspired by the lpmb tool of the Ortho series '
       write(6,*) '  P Reinhardt, Paris, may 2020 '
       write(6,*) ' in confinement we trust '
       write(6,*)
     end subroutine header

     subroutine driver
       implicit none
       integer :: iat,ipoint,i,j,k,igrp

       call eval_boys
       do igrp=1,ngroup
        call boys(igrp)
       end do
       call project_group0
       touch mo_coef
       
       call eval_boys
       mo_label = "Localized"
!      call save_mos
       
     end subroutine driver
       
     subroutine eval_boys
       implicit none
       integer :: i,j,k,iat
       real*8 :: d

       WRITE(6,9400)
9400   FORMAT(//,7X,'No',11X,'orb.-en.',14X,'occ.   group   type')
       DO I=1,mo_num
        IF (IOTYP(I).EQ.5) THEN
         WRITE(6,9401) I,ORBEN(I),IOCC(I),NUMGRP(I),COTYP(IOTYP(I))
        ELSE
         WRITE(6,9401) I,ORBEN(I),IOCC(I),NUMGRP(I),COTYP(IOTYP(I)) &
              ,(IATYP(J,I),J=1,IOTYP(I))
        END IF
       END DO
9401   FORMAT(5x,I5,5x,F16.8,10X,2I5,5X,A4,5I3)
       
       WRITE(6,*)
       WRITE(6,*)
       WRITE(6,9909)
9909   FORMAT(/,'  Barycenters of the orbitals ' &
            ,//,5x,'No',10x,'x',15x,'y',15x,'z',10x,'SQRT(<rr>)',/)
       DO I=1,mo_num
        WRITE(6,9902) I,(baryz_orb(I,K),K=1,3),sqrt(radii_orb(i))
9902    FORMAT(I6,4F16.8)
       END DO
       WRITE(6,*)
       WRITE(6,*) ' Pipek-Mezey measure is ',PM_measure
       WRITE(6,*) ' Boys  measure is       ',Boys_measure

     end subroutine eval_boys

