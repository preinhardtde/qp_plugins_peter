! -*- F90 -*-
     subroutine project_group0
       implicit none
BEGIN_DOC
! we subtract from the atomic orbitals the localized ones       
! the AOs are normalized, so we look at the largests remaining norms
END_DOC

         real*8, allocatable :: remaining_length(:)
         real*8, allocatable :: orb_tmp(:,:)
         real*8, allocatable :: vec_tmp(:)
         real*8, allocatable :: smo(:,:)
         real*8, allocatable :: evec(:,:)
         real*8, allocatable :: eval(:)
         integer :: pick,i,j,alpha,beta,indx
         real*8 :: ov_min,ov_max,ss
         allocate (remaining_length(ao_num))
         allocate (vec_tmp(ao_num))
         allocate (orb_tmp(ao_num,mo_num))
         allocate (smo(mo_num,mo_num))
         allocate (evec(mo_num,mo_num))
         allocate (eval(mo_num))

         if (ao_cartesian) then
          orb_tmp=0.D0
          do i=1,ao_num
           orb_tmp(i,i)=1.D0
          end do
         else
          orb_tmp=ao_cart_to_sphe_coef
          call shalf(1,mo_num,orb_tmp)
         end if
! project on the space complementary to the localized orbitals
integer :: norb
         norb=0
         do i=1,mo_num
          if (numgrp(i).eq.0) then
           norb+=1
          end if
         end do

         write(6,*) ' Remaining space consists of ',norb,' orbitals '

         
         do i=1,mo_num
          vec_tmp=0.D0
          do j=1,mo_num
           if (numgrp(j).ne.0) then
            ss=0.D0
            do alpha=1,ao_num
             do beta=1,ao_num
              ss+=orb_tmp(alpha,i)*mo_coef(beta,j)*ao_overlap(alpha,beta)
             end do
            end do
            do alpha=1,ao_num
             vec_tmp(alpha)+=ss*mo_coef(alpha,j)
            end do
           end if
          end do
          do alpha=1,ao_num
           orb_tmp(alpha,i)-=vec_tmp(alpha)
          end do
         end do
! diagonalize the resulting overlap matrix
         call transf2sq(ao_overlap,smo,mo_num,orb_tmp)
         call lapack_diag(eval,evec,smo,mo_num,mo_num)

         indx=mo_num+1
         do i=1,mo_num
          if (numgrp(i).eq.0) then
           indx-=1
           do alpha=1,ao_num
            ss=0.D0
            do j=1,mo_num
             ss+=orb_tmp(alpha,j)*evec(j,indx)
            end do
            mo_coef(alpha,i)=ss
           end do
          end if
         end do




! calculate the norm of the remaining vector
!         ss=0.D0
!         do alpha=1,ao_num
!          do beta=1,ao_num
!           ss+=orb_tmp(alpha,i)*orb_tmp(beta,i)*ao_overlap(alpha,beta)
!          end do
!         end do
!         remaining_length(i)=ss
!        end do
!        
!        write(6,*)
!        
!        do i=1,mo_num
!         if (numgrp(i).eq.0) then
!          pick=0
!          ov_max=0.D0
!          do j=1,mo_num
!           if (remaining_length(j).gt.ov_max) then
!            pick=j
!            ov_max=remaining_length(j)
!           end if
!          end do
!          write(6,*) ' picking AO No ',pick,' with remaining length ',remaining_length(pick) 
!          do alpha=1,ao_num
!           mo_coef(alpha,i)=orb_tmp(alpha,pick)
!          end do
!          remaining_length(pick)=-1.D0
!         end if
!        end do
! extract the projected orbitals
!        orb_tmp=0.D0
!        indx=0
!        do i=1,mo_num
!         if (numgrp(i).eq.0) then
!          indx+=1
!          do alpha=1,ao_num
!           orb_tmp(alpha,indx)=mo_coef(alpha,i)
!          end do
!         end if
!        end do
! orthogonalize with S^-1/2
!        call shalf(1,indx,orb_tmp)
! put the orbitals back
!        indx=0
!        do i=1,mo_num
!         if (numgrp(i).eq.0) then
!          indx+=1
!          do alpha=1,ao_num
!           mo_coef(alpha,i)=orb_tmp(alpha,indx)
!          end do
!         end if
!        end do

         deallocate (remaining_length)
         deallocate (vec_tmp)
         deallocate (orb_tmp)
         deallocate (smo)
         deallocate (evec)
         deallocate (eval)
       
       end subroutine project_group0
