! -*- F90 -*-
  BEGIN_PROVIDER [integer, ngroup]
 &BEGIN_PROVIDER [integer, numgrp, (mo_num) ]
         implicit none
         integer, allocatable :: ingrp(:)
         logical, allocatable :: lgrp(:)
         integer :: igrp,ngrp2,i
         allocate (ingrp(mo_num))
         allocate (lgrp(mo_num))
         lgrp=.false.
         numgrp=0
         write(6,*) ' please give the number of orbital groups to be defined '
         write(6,*) ' (if 0, I will try to find out) '
         read(5,*) ngroup
         if (ngroup.eq.0) then
! we group orbitals of the same kind
! did someone set mo_classes ?
          if (n_act_orb.ne.mo_num) then
           write(6,*) ' core, inact, act, virt, del =',n_core_orb  &
                ,n_inact_orb,n_act_orb,n_virt_orb,n_del_orb
           if (n_core_orb.ne.0) then
            ngroup+=1
            do i=1,n_core_orb
             numgrp(list_core(i))=ngroup
            end do
           end if
           if (n_inact_orb.ne.0) then
            ngroup+=1
            do i=1,n_inact_orb
             numgrp(list_inact(i))=ngroup
            end do
           end if
           if (n_act_orb.ne.0) then
            ngroup+=1
            do i=1,n_act_orb
             numgrp(list_act(i))=ngroup
            end do
           end if
!          if (n_virt_orb.ne.0) then
!           ngroup+=1
!           do i=1,n_virt_orb
!            numgrp(list_virt(i))=ngroup
!           end do
!          end if
          else
! ok, we use alpha and beta electrons to define groups
           integer :: nocc2,nocc1,nocc0
           nocc2=min(elec_alpha_num,elec_beta_num)
           nocc1=abs(elec_alpha_num-elec_beta_num)
           nocc0=mo_num-nocc2-nocc1
           write(6,*) ' groups ',nocc2,nocc1,nocc0
           if (nocc2.ne.0) then
            ngroup+=1
            do i=1,nocc2
             numgrp(i)=ngroup
            end do
           end if
! singly occupied orbitals
           if (nocc1.ne.0) then
            ngroup+=1
            do i=1,nocc1
             numgrp(i+nocc2)=ngroup
            end do
           end if
! virtuals
!          if (nocc0.ne.0) then
!           ngroup+=1
!           do i=1,nocc0
!            numgrp(nocc1+nocc2+i)=ngroup
!           end do
!          end if
          end if
         else 
          write(6,*) '  for each group, please give the number '    &
               ,'and indices of orbitals '
          do igrp=1,ngroup
           write(6,*) ' please give the number of orbitals in group',igrp
           read(5,*) ngrp2
           write(6,*) ' please give the indices of the ',ngrp2,' orbitals '
           read(5,*) (ingrp(i),i=1,ngrp2)
           do i=1,ngrp2
            if (.not.lgrp(ingrp(i))) then
             numgrp(ingrp(i))=igrp
             lgrp(ingrp(i))=.true.
            else
             write(6,*) ' orbital No ',ingrp(i),' already in use '
             stop ' wrong input, please try again '
            end if
           end do
          end do
         end if
         deallocate (ingrp)
         deallocate (lgrp)


END_PROVIDER

BEGIN_PROVIDER [character*4, cotyp, (5)]
       implicit none
       COTYP(1)='Atom'
       COTYP(2)='Bond'
       COTYP(3)='Tric'
       COTYP(4)='Quad'
       COTYP(5)='Dloc'         
END_PROVIDER

BEGIN_PROVIDER [integer, iocc, (mo_num) ]
BEGIN_DOC
! we diagonalize the density matrix in the MO basis
END_DOC
      iocc=-1
END_PROVIDER
