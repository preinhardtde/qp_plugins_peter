! -*- F90 -*-
program analyze_orbs
  implicit none
  BEGIN_DOC
! determination of Kato s condition around each atom
! we read a wavefunction, create density and grad density
! on a grid of points
  END_DOC
real*8 :: cpu0,wall0
 read_wf = .True.
 touch read_wf

          call cpu_time(cpu0)
          call wall_time(wall0)
         
          call header
          call driver
          call cpustamp(cpu0,wall0,cpu0,wall0,'ORBS')
end program analyze_orbs 

     subroutine header
       write(6,*)
       write(6,*) ' just running the analysis without any  '
       write(6,*) ' optimization of the orbitals                    '
       write(6,*) '  P Reinhardt, Paris, June 2020 '
       write(6,*) ' in confinement we trust '
       write(6,*)
     end subroutine header

     subroutine driver
       implicit none
       integer :: i,j,k,iat,istate
       real*8 :: d

       call print_baryz
       call print_Mulliken
       WRITE(6,*)

     end subroutine driver

