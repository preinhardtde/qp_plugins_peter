! -*- F90 -*-
      SUBROUTINE BOYS(igrp)
        implicit none
        integer :: igrp,iter,norb,i
        
        real*8 :: d
        LOGICAL LSWEEP
!
! Boys localization, i.e. the occupied orbitals are localized,
! and as virtual orbitals the atomic orbitals are taken, and
! orthogonalized to the occupied and among themselves
!
! we follow the recipe of Ahlrichs from TURBOMOLE:
! condition for minimization of \sum_i<j) <i|r|j>**2
! is <i|r|j>*(<i|r|i>-<j|r|j> = 0 for i.ne.j
! so we set up the matrix of conditions
!
      WRITE(6,*)
      WRITE(6,*) ' BOYS LOCALIZATION, group No ',igrp,' : '
      WRITE(6,*)

      norb=0
      do i=1,mo_num
       if (numgrp(i).eq.igrp) then
        norb+=1
       end if
      end do
!
      IF (norb.le.1) THEN
       WRITE(6,*) ' FOR zero or one orbital THERE IS NOTHING TO OPTIMIZE '
       WRITE(6,*) ' in group ',igrp,' there are ',norb,' orbitals '
       WRITE(6,*) 
      else   

      ITER=0
      lsweep=.true.
      do while (lsweep)
       ITER=ITER+1
!
! eigentlich sollte eine Boys Lokalisierung nur eine andere Art eines
! SWEEP sein ....
!
       CALL SWEEPB(igrp,LSWEEP)
       if (iter.gt.100) then
        write(6,*) ' no convergence '
        exit
       end if
      end do
!
      if (iter.gt.100) then
      WRITE(6,*) ' BOYS PROCEDURE did not converge within ',ITER-1,' SWEEPS '
      else
      WRITE(6,*) ' BOYS PROCEDURE TOOK ',ITER,' SWEEPS TO CONVERGE'
      end if
      WRITE(6,*)

      end if

    END SUBROUTINE BOYS

BEGIN_PROVIDER [real*8, BOYS_MEASURE, (ngroup)]
        implicit none
        integer :: i,igrp,norb

        boys_measure=0.D0
       WRITE(6,9771) Nucl_num,(baryz_nuc(i),i=1,3)
 9771  FORMAT(' barycenter of the molecule (',I2,' atoms) : ',3F10.4)

        do igrp=1,ngroup

        write(6,*) ' Boys measure for orbital group ',igrp
!
! print the list of orbital centers
!
      WRITE(6,9909)
 9909 FORMAT(/,'  barycenters of the orbitals '      &
           ,//,5x,'No',10x,'x',15x,'y',15x,'z',/)

      norb=0
      DO I=1,mo_num
       if (numgrp(i).eq.igrp) then
        norb+=1
        WRITE(6,9902) I,(BARYZ_orb(I,K),K=1,3)
       end if
 9902  FORMAT(I6,3F16.8)
      END DO

      real*8 :: bbb,bbq,pp,berr,bmin
      integer :: nbd,j,k

      bbb=0.d0
      bbq=0.d0
      nbd=0
       do i=1,mo_num
        if (numgrp(i).eq.igrp) then
        do j=i+1,mo_num
         if (numgrp(j).eq.igrp) then
         nbd=nbd+1
         pp=0.d0
         do k=1,3
          pp=pp+(baryz_orb(i,k)-baryz_orb(j,k))*  &
               (baryz_orb(i,k)-baryz_orb(j,k))
        end do
        bbb=bbb+sqrt(pp)
        bbq=bbq+pp
        end if
       end do
        end if
      end do
      if (nbd.gt.1) then
       bbb=bbb/nbd
       bbq=bbq/nbd
       berr=sqrt(bbq-bbb*bbb)
       
       write(6,9906) bbb,berr
 9906 format(/,' Mean intercentroid distance ',f12.5,' +- ',f12.5,/)
      else
       write(6,*) ' nothing to measure for less than 2 orbitals '
       bbb=0.D0
      endif

      boys_measure(igrp)=bbb
!
! the average of the nearest neighbour distance and its variance
!
      bbb=0.d0
      bbq=0.d0
       do i=1,mo_num
        if (numgrp(i).eq.igrp) then
       bmin=1.d6
        do j=1,mo_num
         if (numgrp(j).eq.igrp) then
        if (i.ne.j) then
         pp=0.d0
         do k=1,3
          pp = pp + (baryz_orb(i,k)-baryz_orb(j,k)) &
               *(baryz_orb(i,k)-baryz_orb(j,k))
         end do
         if (pp.le.bmin) then
          bmin=pp
         end if
        end if
        end if
       end do
       bbb=bbb+sqrt(bmin)
       bbq=bbq+bmin
        end if
      end do
      if (nbd.gt.1) then
      bbb=bbb/nbd  
      bbq=bbq/nbd  
      berr=sqrt(bbq-bbb*bbb)

      write(6,9907) bbb,berr
 9907 format(/,' Mean minimum intercentroid distance ',f12.5,' +- ',f12.5,/)
      else
       write(6,*) ' nothing to measure for less than 2 orbitals '
       bbb=0.D0
      endif
      
      write(6,*)
      end do
END_PROVIDER

      subroutine sweepb(igrp,lsweep)
        implicit none
        logical :: lsweep
        integer :: it,i,j,igrp,k
        integer :: jm1,jp1,l,ip1,im1,ialph,ibeta
        real*8 :: a0,a1,a2,a10,aqu,thsw,ax,b,aux1,aux2
        real*8 :: s,s2,c,c2,x0,cs,c2m2,csa,cta,c2ms2,pi
        pi=acos(0.D0)*2.D0
!
        a0=0.d0
        a1=1.d0
        a2=2.d0
        a10=10.d0
        aqu=0.25d0
        thsw=a0
        do i=1,mo_num
         if (numgrp(i).eq.igrp) then
          im1=i-1
          ip1=i+1
          do j=1,im1
           if (numgrp(j).eq.igrp) then
!
!     loop over i,j rotations
!     maximize sum(k) <i|dipol(k)|i>**2 + <j|dipol(k)|j>**2
!     like in jacobi
!
!     get ax , b determining the tan of the rotation angle
!
            ax=a0
            b=a0
            do k=1,3
             aux1=dipol(j,j,k)-dipol(i,i,k)
             aux2=dipol(i,j,k)
             ax=ax+aux1*aux2
             b=b+aqu*aux1**2-aux2**2
            end do
!
!     rotation angle x0
!
            if (dabs(ax)+dabs(b).gt.1.d-12) then
             x0=aqu*datan2(ax,b)
             thsw=dmax1(thsw,dabs(x0))
             if (dabs(x0).gt.1.d-13) then
!
!     rotation
!
!             write(6,*) ' rotating orbitals ',i,' and ',j,' with angle ' &
!                  ,x0/pi*180.D0,' deg'
              s=dsin(x0)
              s2=s*s
              c2=a1-s2
              c=dsqrt(c2)
!
! update of the vector
!
              do ialph=1,ao_num
               csa=mo_coef(ialph,i)
               cta=mo_coef(ialph,j)
               mo_coef(ialph,i)=c*csa-s*cta
               mo_coef(ialph,j)=c*cta+s*csa
              end do
             
              jm1=j-1
!     c2=c*c
!     s2=s*s
              cs=c*s
              c2ms2=c2-s2
! update of the dipole matrix, only the row/columns involving i and j
              
              jp1=j+1
              do k=1,3
               aux1=dipol(i,i,k)
               aux2=dipol(j,j,k)
               dipol(i,i,k)=aux1*c2+aux2*s2-a2*dipol(i,j,k)*cs
               dipol(j,j,k)=aux1*s2+aux2*c2+a2*dipol(i,j,k)*cs
               dipol(i,j,k)=cs*(aux1-aux2)+dipol(i,j,k)*c2ms2
               dipol(j,i,k)=dipol(i,j,k)
               do l=1,jm1
                aux1=c*dipol(i,l,k)-s*dipol(j,l,k)
                dipol(j,l,k)=s*dipol(i,l,k)+c*dipol(j,l,k)
                dipol(i,l,k)=aux1
                dipol(l,i,k)=dipol(i,l,k)
                dipol(l,j,k)=dipol(j,l,k)
               end do
               do l=jp1,im1
                aux1=c*dipol(i,l,k)-s*dipol(j,l,k)
                dipol(j,l,k)=c*dipol(j,l,k)+s*dipol(i,l,k)
                dipol(i,l,k)=aux1
                dipol(l,i,k)=dipol(i,l,k)
                dipol(l,j,k)=dipol(j,l,k)
               end do
               do l=ip1,mo_num
                aux1=c*dipol(j,l,k)+s*dipol(i,l,k)
                dipol(i,l,k)=c*dipol(i,l,k)-s*dipol(j,l,k)
                dipol(j,l,k)=aux1
                dipol(l,i,k)=dipol(i,l,k)
                dipol(l,j,k)=dipol(j,l,k)
               end do
              end do
             end if
            end if
           end if
          end do
         end if
        end do
         
!     check convergence
!
! if we need another sweep, we leave lsweep as true
        lsweep=.true.
        write(6,*) ' sweep: gammax = ',thsw
        if (thsw.le.1.d-9) lsweep=.false.
!
      end subroutine sweepb

