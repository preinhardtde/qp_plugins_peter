! -*- F90 -*- 
 BEGIN_PROVIDER [real*8, mo_purified_lz, (ao_num, mo_num) ]
&BEGIN_PROVIDER [integer, mo_lz, (mo_num) ]
BEGIN_DOC
! we have the L2-separated MOs for atoms, and we have to extract 
! L_z eigenfunctions
! for linear molecules the L separation is not necessary
!
! spherical (real solid harmonics) are ordered 
! in the quantum package as Cl0, Cl1, Sl1, Cl2, Sl2 etc
! which we write as (l,0), (l,1), (l,-1), (l,2), (l,-2)
!
END_DOC
         implicit none
         integer :: alpha,i,j,beta,k
         real*8 :: ovl,pop_lz(-ao_l_max:ao_l_max,mo_num),pp,pt
         real*8 :: pop_lz_proj(-ao_l_max:ao_l_max,mo_num)
         real*8 :: pop_lz_tot(-ao_l_max:ao_l_max)
         real*8 :: pop_lz_proj_tot(-ao_l_max:ao_l_max)
         real*8 :: overlap_2MOs

         call frobenius_product(ortho_spher,mo_num,mo_purified,mo_num)

         pop_lz=0.D0
         pop_lz_tot=0.D0
         pop_lz=0.D0
         pop_lz_proj_tot=0.D0

! first we analyze, and then we modify

         do i=1,mo_num
!
! project on the different possible l_z
!
integer :: indxmx
real*8 :: popmx
          do j=1,mo_num
           ovl=overlap_2MOs(mo_purified(1,i),ortho_spher(1,j))
           pop_lz(ortho_spher_lz(j),i)+=ovl*ovl
          end do
          write(6,*)
          popmx=0.D0
real*8 :: popsum 
          popsum=0.D0
          do j=-ao_l_max,ao_l_max
           pop_lz_tot(j)+=pop_lz(j,i)
           popsum+=pop_lz(j,i)
           if (pop_lz(j,i).gt.1.D-4) write(6,*) i,j,pop_lz(j,i),pop_lz_tot(j)
           if (pop_lz(j,i).gt.popmx) then
            popmx=pop_lz(j,i)
            indxmx=j
           end if
          end do
          write(6,*) ' sum of all projections = ',popsum
          pop_lz_proj_tot(indxmx)+=1.D0

          write(6,*)
          popmx=0.D0
         end do

         write(6,*) ' L_z    population proj pop'
         pt=0.D0
         pp=0.D0
         do j=-ao_l_max,ao_l_max
          write(6,'(I4,2F12.5)') j,pop_lz_tot(j),pop_lz_proj_tot(j)
          pt+=pop_lz_tot(j)
          pp+=pop_lz_proj_tot(j)
         end do
         write(6,*) '  ------------------------ '
         write(6,'(10Hsum of all,2F12.5)') pt,pp

! now we start to work
         pop_lz=0.D0
         pop_lz_tot=0.D0
         pop_lz=0.D0
         pop_lz_proj_tot=0.D0
         mo_purified_lz=0.D0

         do i=1,mo_num
          do j=1,mo_num
           ovl=overlap_2MOs(mo_purified(1,i),ortho_spher(1,j))
           pop_lz(ortho_spher_lz(j),i)+=ovl*ovl
          end do
          popmx=0.D0
          do j=-ao_l_max,ao_l_max
           if (pop_lz(j,i).gt.popmx) then
            popmx=pop_lz(j,i)
            indxmx=j
           end if
          end do
          write(6,*) ' assigning Lz = ',indxmx,' to orbital No ',i
          do j=1,mo_num
           if (ortho_spher_lz(j).eq.indxmx) then
            ovl=overlap_2MOs(mo_purified(1,i),ortho_spher(1,j))
            do alpha=1,ao_num
             mo_purified_lz(alpha,i)+=ovl*ortho_spher(alpha,j)
            end do
           end if
          end do
! normalize
          ovl=overlap_2MOs(mo_purified_lz(1,i),mo_purified_lz(1,i))
          ovl=1.D0/sqrt(ovl)
          do alpha=1,ao_num
           mo_purified_lz(alpha,i)*=ovl
          end do
!
! orthogonalize to all previous ones
!
          do j=1,i-1
           ovl=overlap_2MOs(mo_purified_lz(1,i),mo_purified_lz(1,j))
           do alpha=1,ao_num
            mo_purified_lz(alpha,i)-=ovl*mo_purified_lz(alpha,j)
           end do
          end do
! normalize
          ovl=overlap_2MOs(mo_purified_lz(1,i),mo_purified_lz(1,i))
          ovl=1.D0/sqrt(ovl)
          do alpha=1,ao_num
           mo_purified_lz(alpha,i)*=ovl
          end do
! verify that we have indeed only one Lz present
          do j=-ao_l_max,ao_l_max
           pop_lz(j,i)=0.D0
           do k=1,mo_num
            if (ortho_spher_lz(k).eq.j) then
             ovl=overlap_2MOs(mo_purified_lz(1,i),ortho_spher(1,k))
             pop_lz(j,i)+=ovl*ovl
            end if
           end do
!          if (abs(pop_lz(j,i)).gt.1.D-4) write(6,*) ' new ',i,j,pop_lz(j,i)
          end do
!
! orthogonalize the remaining mo_purified to the generated mo_purified_lz
! 
          do j=i+1,mo_num
           do k=1,i
            ovl=overlap_2MOs(mo_purified(1,j),mo_purified_lz(1,k))
            do alpha=1,ao_num
             mo_purified(alpha,j)-=ovl*mo_purified_lz(alpha,k)
            end do
           end do
! normalize
           ovl=overlap_2MOs(mo_purified(1,j),mo_purified(1,j))
           ovl=1.D0/sqrt(ovl)
           do alpha=1,ao_num
            mo_purified_lz(alpha,j)*=ovl
           end do
          end do
         end do

         write(6,*) ' L_z    population '
         do i=1,mo_num
          do j=-ao_l_max,ao_l_max
           pop_lz_tot(j)+=pop_lz(j,i)
          end do
         end do
         pt=0.D0
         pp=0.D0
         do j=-ao_l_max,ao_l_max
          write(6,'(I4,F12.5)') j,pop_lz_tot(j)
          pt+=pop_lz_tot(j)
         end do
         write(6,*) '  ------------------------ '
         write(6,'(10Hsum of all,2F12.5)') pt


END_PROVIDER
