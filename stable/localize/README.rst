========
localize
========

An ensemble of plugins to localize orbitals, and analyze them.
Derived from the lpmb tool of the Ortho programs.

boys_loc - Boys localization:
  occupied or active orbitals are localized
  virtual orbitals are projected atomic orbitals
  orbital groups are assembled together

pm_loc - Pipek-Mezey localization

analyze_orbs - Orbital analysis

correct_lz - Lz purification for atomic orbitals

merge_atoms - Assembly of atomic orbitals to form a guess for a molecule
     calculate first a WF for each type of atom
     extract from the corresponding ezfio vie extract_basiis_and_mos
     name the files Basis_and_MOs.dat_$ELEM   ($ELEM = element symbol)
     specify how many core and active orbitals for each atom type
