! -*- F90 -*-
 BEGIN_PROVIDER [real*8, l_presence,  (0:ao_l_max) ]
&BEGIN_PROVIDER [real*8, lz_presence, (-ao_l_max:ao_l_max) ]
&BEGIN_PROVIDER [real*8, ortho_spher, (ao_num,mo_num)]
&BEGIN_PROVIDER [real*8, ortho_spher_lz, (mo_num)]
&BEGIN_PROVIDER [real*8, ortho_spher_l, (mo_num)]
        implicit none
        integer :: i,alpha,j
        real*8 :: l_mult_cart(0:ao_l_max)

        l_mult_cart(0)=1
        if (ao_l_max.ge.1) then
         l_mult_cart(1)=3
         if (ao_l_max.ge.2) then
          l_mult_cart(2)=6
          if (ao_l_max.ge.3) then
           l_mult_cart(3)=10
           if (ao_l_max.ge.4) then
            l_mult_cart(4)=15
            if (ao_l_max.ge.5) then
             l_mult_cart(5)=21
            end if
           end if
          end if
         end if
        end if

       l_presence=0
       lz_presence=0
       do i=1,ao_num
!       write(6,*) ' ao power in x, y, z = ',(ao_power(i,j),j=1,3)
        l_presence(ao_l(i))+=1.D0/l_mult_cart(ao_l(i))
        if (ao_l(i).eq.0) then
         lz_presence(0)+=1.D0/l_mult_cart(ao_l(i))
        else
         lz_presence(0)+=1.D0/l_mult_cart(ao_l(i))
         do j=1,ao_l(i)
          lz_presence(-j)+=1.D0/l_mult_cart(ao_l(i))
          lz_presence( j)+=1.D0/l_mult_cart(ao_l(i))
         end do
        end if
       end do

       write(6,*) '   l  lz+, lz- values of the AOs'
       write(6,*) '          l       l-shells      lz=l        lz=-l '
       i=0
       write(6,*) i,nint(l_presence(0)),nint(lz_presence(0))
       do i=1,ao_l_max
        write(6,*) i,nint(l_presence(i)),nint(lz_presence(i)),nint(lz_presence(-i))
       end do
        
       ortho_spher_lz=0
logical :: new_multiplet
       new_multiplet=.true.
integer :: actual_lz,actual_lz_indx
integer :: actual_l 
       do i=1,mo_num
        if (new_multiplet) then
!        write(6,*) ' new multiplet '
         actual_lz=0
         actual_lz_indx=1
        end if
        do alpha=1,ao_num
         if (abs(ao_cart_to_sphe_coef(alpha,i)).gt.1.D-6) then
          actual_l=ao_l(alpha)
         end if
        end do
!       write(6,*) i,actual_l,actual_lz,actual_lz_indx,new_multiplet
        ortho_spher_lz(i)=actual_lz
        ortho_spher_l(i) =actual_l
        if (actual_lz_indx.eq.(2*actual_l+1)) then
         new_multiplet=.true.
        else
         new_multiplet=.false.
! next lz
         if (actual_lz.le.0) then
          actual_lz=-actual_lz+1
         else
          actual_lz=-actual_lz
         end if
         actual_lz_indx+=1
        end if
        do alpha=1,ao_num
         ortho_spher(alpha,i)=ao_cart_to_sphe_coef(alpha,i)
        end do
       end do

       call shalf(1,mo_num,ortho_spher)

       write(6,*) ' constructed ortho_spher '

END_PROVIDER

 BEGIN_PROVIDER [real*8, mo_purified, (ao_num,mo_num)]
&BEGIN_PROVIDER [integer, l_purified, (mo_num)]
         implicit none
         integer :: i,l,indx,alpha,beta,j,indxmx
         integer :: num_lfound(0:ao_l_max)
         integer :: ipop_corr(0:ao_l_max)
         real*8 :: pop_corr(0:ao_l_max)
         real*8 :: pop(0:ao_l_max),ss,poptot(0:ao_l_max)
         real*8 :: orb_tmp(ao_num,mo_num)

         if (nucl_num.gt.1) then
          mo_purified=mo_coef
          l_purified=0
         else
          mo_purified=0.D0
          poptot=0.D0
          ipop_corr=0
          pop_corr=0.D0
          num_lfound=0
          do i=1,mo_num
           pop=0.D0
           do alpha=1,ao_num
            l=ao_l(alpha)
            do beta=1,ao_num
             ss=mo_coef(alpha,i)*mo_coef(beta,i)*ao_overlap(alpha,beta)
             pop(l)+=ss
            end do
           end do
! find out which l is representative for the MO
           ss=0.D0
           do l=0,ao_l_max
            poptot(l)+=pop(l)
            if (abs(pop(l)).gt.ss) then
             ss=abs(pop(l))
             l_purified(i)=l
            end if
! is there one which is not 1, but not 0 neither ? i.e. between 0.2 and 0.8 ?
            if (pop(l).lt.0.8.and.pop(l).gt.pop_corr(l)) then
             pop_corr(l)=pop(l)
             ipop_corr(l)=i
            end if
           end do
           num_lfound(l_purified(i))+=1
           write(6,9711) i,l_to_character(l_purified(i)),(pop(l),l=0,ao_l_max)
   9711 format(' orbital No ',i4,' -> ',a1,'; Pop = ',6f10.4)
          end do
 integer :: norb_recalc
          norb_recalc=0
          do l=0,ao_l_max
           norb_recalc+=nint(poptot(l))
           if (nint(poptot(l)).ne.num_lfound(l)) then
            write(6,*) ' we have to correct for l= ',l,': '   &
               ,nint(poptot(l))-num_lfound(l)
            if (nint(poptot(l))-num_lfound(l).eq.1) then
             write(6,*) ' we may use orbital No ',ipop_corr(l)
             num_lfound(l)+=1
             num_lfound(l_purified(ipop_corr(l)))-=1
             l_purified(ipop_corr(l))=l
            else
             write(6,*) ' we can only correct for one orbital '
             stop
            end if
           end if
          end do
          write(6,*) '                                             '  &
          ,'       s     ' &
          ,'      p     ' &
          ,'      d     ' &
          ,'      f     ' &
          ,'      g     '
          write(6,*) ' expected number of orbitals for each l: ' &
            ,(nint(poptot(l)),l=0,ao_l_max),' -> sum = ',norb_recalc
          write(6,*) '    found number of orbitals for each l: ' &
            ,(num_lfound(l),l=0,ao_l_max)

          do i=1,mo_num
! set all other coefficients to zero
           do alpha=1,ao_num
            if (ao_l(alpha).eq.l_purified(i)) then
             mo_purified(alpha,i)=mo_coef(alpha,i)
            end if
           end do
          end do
! orthogonalize the orbitals with common l among each other
! S-1/2 for all should do this already as different l should have overlap zero. 
          do l=0,ao_l_max
           write(6,*) ' treating orbitals for l= ',l
           indx=0
           do i=1,mo_num
            if (l.eq.l_purified(i)) then
             indx+=1
             do alpha=1,ao_num
              if (abs(mo_coef(alpha,i)).gt.1.D-9) then
               orb_tmp(alpha,indx)=mo_coef(alpha,i)
              else
               orb_tmp(alpha,indx)=0.D0
              end if
             end do
            end if
           end do
! we might use Gram-Schmidt preserving occ/virt separation
           call GramSchmidt(1,indx,orb_tmp)
!         call shalf(1,indx,orb_tmp)
! put orbitals back
           indx=0
           do i=1,mo_num
            if (l.eq.l_purified(i)) then
             indx+=1
             do alpha=1,ao_num
              mo_coef(alpha,i)=orb_tmp(alpha,indx)
             end do
            end if
           end do
          end do
         end if
END_PROVIDER

      subroutine simplify_lz
         implicit none
         integer :: i,l,indx,alpha,beta,j,indxmx
         real*8 :: ss
         logical :: lused(mo_num)
!
! for atoms we may turn the orbitals to standard orientations as 
! given by the cart_to_spherical transformation
!
! we should program the L_z operator for atoms and diagonalize it
! we have already the eigenfunctions as the solid harmonics !
! project on the OAOs and do another MCSCF ?
!
! a set of coefficients of a shell is a1*l1 + a2*l2 + a3*l3 + ... 
! is this the matrix spher_to_cart_inv ? 

! overlap of the MOs with the ao_cart_to_spher

       lused=.false.
       do i=1,mo_num
! look for the largest overlap with the spherical harmonics
real*8 :: ovmx,one_orb(ao_num)
        ovmx=0.D0
        one_orb=0.D0
        indxmx=0
        do j=1,mo_num
real*8 :: overlap_2MOs
         ss=overlap_2MOs(mo_coef(1,i),ao_cart_to_sphe_coef(1,j))
!        ss=0.D0
!        do alpha=1,ao_num
!         do beta=1,ao_num
!          ss+=mo_coef(alpha,i)*ao_cart_to_sphe_coef(beta,j)*ao_overlap(alpha,beta)
!         end do
!        end do
         if (abs(ss).gt.ovmx.and..not.lused(j)) then
          ovmx=abs(ss)
          indxmx=j
          if (ss.lt.0.D0) indxmx=-indxmx
         end if
        end do
        lused(abs(indxmx))=.true.
        if (indxmx.ne.0) then
         write(6,*) ' found j = ',indxmx,' for MO No ',i,' with overlap ',ovmx
        else
         write(6,*) ' found no orbital for MO No ',i
         stop
        end if
! calculate the norm of ao_cart_to_sphe_coef(:,indxmx)
        do alpha=1,ao_num
         if (indxmx.gt.0) then
          mo_coef(alpha,i)=ovmx*ao_cart_to_sphe_coef(alpha,indxmx)
         else
          mo_coef(alpha,i)=-ovmx*ao_cart_to_sphe_coef(alpha,-indxmx)
         end if
        end do
       end do
       call shalf(1,mo_num,mo_coef)
      end subroutine simplify_lz
