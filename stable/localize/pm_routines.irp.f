! -*- F90 -*-
BEGIN_PROVIDER [real*8, PM_measure]
        implicit none
        integer :: istart,iend,i,nvect,ialph,ibeta,iat
        real*8 :: d,xdum,di,qia,cia,pab

        D=0.D0
        XDUM=0.D0
        DO I=1,mo_num
         DI=0.D0
! construct the density matrix for orbital I
         DO IAT=1,Nucl_num
! QIA is the Mulliken population of of orbital I on atom iat
! DI is the sum of the squared populations        
          QIA=0.D0
          DO IALPH=1,ao_num
           if (ao_nucl(ialph).eq.iat) then
            CIA=mo_coef(IALPH,I)
            DO IBETA=1,ao_num
             PAB=CIA*mo_coef(IBETA,I)
             QIA=QIA+PAB*ao_overlap(IBETA,IALPH)
            END DO
           end if
          END DO
          DI=DI+QIA*QIA
          XDUM=XDUM+QIA
         END DO
         D=D+DI
        END DO
        XDUM=2.D0*XDUM
!
! we should have XDUM = number of electrons
        
        IF (ABS(2*mo_num-XDUM).GT.1.D-3) THEN
         WRITE(6,*) &
              ' MEASUR: COUNTING pseudo-ELECTRONS: SHOULD ',2*mo_num  &
              ,' HAVE  ',XDUM
         WRITE(6,*)
        END IF

        D=0.5D0*DBLE(mo_num)/D

        write(6,*) ' Pipek-Mezey measure for the complete orbital set is ',D
        PM_measure=D
        
END_PROVIDER

      SUBROUTINE PipekMezey(igrp)
        implicit none
        integer :: igrp,iter,norb,i
        
        real*8 :: d
        LOGICAL LSWEEP
BEGIN_DOC
!
! Pipek-Mezey localization, i.e. the occupied orbitals are localized,
! and as virtual orbitals the atomic orbitals are taken, and
! orthogonalized to the occupied and among themselves
!
! J Pipek, PC Mezey, J.Chem.Phys. 90 (1989) 4916.
!
END_DOC
      WRITE(6,*)
      WRITE(6,*) ' Pipek-Mezey LOCALIZATION, group No ',igrp,' : '
      WRITE(6,*)

      norb=0
      do i=1,mo_num
       if (numgrp(i).eq.igrp) then
        norb+=1
       end if
      end do
!
      IF (norb.le.1) THEN
       WRITE(6,*) ' FOR zero or one orbital THERE IS NOTHING TO OPTIMIZE '
       WRITE(6,*) ' in group ',igrp,' there are ',norb,' orbitals '
       WRITE(6,*) 
      else   
       
       ITER=0
       lsweep=.true.
       do while (lsweep)
        ITER=ITER+1
!
        CALL SWEEP(igrp,LSWEEP)
        if (iter.gt.100) then
         write(6,*) ' no convergence '
         exit
        end if
       end do
!
       if (iter.gt.100) then
        WRITE(6,*) ' Pipek-Mezey PROCEDURE did not converge within '  &
             ,ITER-1,' SWEEPS '
       else
        WRITE(6,*) ' Pipek-Mezey PROCEDURE TOOK ',ITER,' SWEEPS TO CONVERGE'
       end if
       WRITE(6,*)
       
      end if
      
    END SUBROUTINE pipekmezey

    subroutine sweep(igrp,lsweep)
      implicit none
      logical :: lsweep
      integer :: it,i,j,igrp,k,is,nvect
      integer :: ialph,ibeta,iat
      real*8 :: pi,sumal,sss,sst,stt
      real*8 :: cias,ciat,cibs,cibt,ast,bst,alpha,gamm
      real*8 :: s4a,c4a,foura,alpi
      real*8 :: cssa, ctta, saoab
      real*8 :: s,c2,s2,c,sina,cosa,csa,cta
      real*8 :: sps(nucl_num),spt(nucl_num),tpt(nucl_num)

      nvect=0
      do is=1,mo_num
       if (numgrp(is).eq.igrp) then
        nvect+=1
       end if
      end do

      pi=acos(0.d0)*2.d0
      sumal=0.d0
      do is=1,mo_num
       if (numgrp(is).eq.igrp) then
        do it=is+1,mo_num
         if (numgrp(it).eq.igrp) then
!
! with every calculated angle orbital s will be changed as well
! we have to recalculate sps again and again
!
          do iat=1,nucl_num
           sss=0.d0
           sst=0.d0
           stt=0.d0
           do ialph=1,ao_num
            if (ao_nucl(ialph).eq.iat) then
             cias=mo_coef(ialph,is)
             ciat=mo_coef(ialph,it)
             do ibeta=1,ao_num
              cibs=mo_coef(ibeta,is)
              cibt=mo_coef(ibeta,it)
              saoab=ao_overlap(ialph,ibeta)
              sss=sss +  cias*cibs           *saoab
              stt=stt +  ciat*cibt           *saoab
              sst=sst + (cias*cibt+ciat*cibs)*saoab
             end do
            end if
           end do
           sps(iat)=sss
           spt(iat)=sst*0.5d0
           tpt(iat)=stt
          end do

          ast=0.d0
          bst=0.d0
          do iat=1,nucl_num
           ast=ast+spt(iat)*spt(iat)-0.25d0*(sps(iat) &
                -tpt(iat))*(sps(iat)-tpt(iat))
           bst=bst+spt(iat)*(sps(iat)-tpt(iat))
          end do
          s4a= bst/sqrt(ast*ast+bst*bst)
          c4a=-ast/sqrt(ast*ast+bst*bst)
          foura=asin(abs(s4a))
! which quadrant?
          if (s4a.gt.0.D0) then
! between 0 and pi/2: do nothing
           if (c4a.le.0.D0) then
! between pi/2 and pi
            foura=pi-foura
           end if
          else
           if (c4a.lt.0.D0) then
! between pi and 3/2 pi
            foura=foura+pi
           else
! between 3/2 pi and 2 pi
            foura=2.d0*pi-foura
           end if
          end if
          alpha=0.25d0*foura
          alpi=4.d0*alpha/pi
          sumal=sumal+(nint(alpi)-alpi)*(nint(alpi)-alpi)
!
! there is this story about gamma and alpha in the paper of Pipek and
! Mezey; for bst=0 we obtain alpha=pi/4, so it is gamma=alpha for
! localizing, thus minimizing d or maximizing p=d^-1
!
          gamm=alpha
!         write(6,*) ' mixing orbitals ',is,it,' with angle ',gamm*180/pi
!
! now we have to mix the two orbitals
          s=dsin(gamm)
          s2=s*s
          c2=1.d0-s2
          c=dsqrt(c2)
          
          sina=s
          cosa=c
          
          do ialph=1,ao_num
           csa=mo_coef(ialph,is)
           cta=mo_coef(ialph,it)
           cssa=cosa*csa+sina*cta
           ctta=cosa*cta-sina*csa
           mo_coef(ialph,is)=cssa
           mo_coef(ialph,it)=ctta
          end do
! that is all
         end if
        end do
       end if
      end do
      sumal=sqrt(sumal)/dble(nvect*(nvect-1)/2)
! if we need another sweep, we leave lsweep as true
      lsweep=.true.
!     if (sumal.le.1.d-7) lsweep=.false.
      write(6,*) ' sweep: sumal = ',sumal
      if (sumal.le.1.d-9) lsweep=.false.
!
      
    end subroutine sweep

