! -*- F90 -*-
program correct_lz
  implicit none
  BEGIN_DOC
! construct proper eigenfunctions of L^2 and L_z
  END_DOC
real*8 :: cpu0,wall0
 read_wf = .True.
 touch read_wf

          call cpu_time(cpu0)
          call wall_time(wall0)
         
          call header
          call driver
          call cpustamp(cpu0,wall0,cpu0,wall0,'C LZ')
end program correct_lz

     subroutine header
       write(6,*)
       write(6,*) ' for atoms we wouuld like to have proper l states'
       write(6,*) ' (and for diatomics aligned one the z axis) ' 
       write(6,*) ' with l_z eigenfunctions '
       write(6,*) ' set spurious terms to zero                      '
       write(6,*) '  P Reinhardt, Paris, June 2020 '
       write(6,*) ' in confinement we trust '
       write(6,*)
     end subroutine header

     subroutine driver
       implicit none
       integer :: i,j,k,iat,istate,alpha,beta
       real*8 :: d,ovl,r1,r2,r3,rik,rjk

       if (nucl_num.gt.1) then
        write(6,*) ' L^2 and L_z are reasonable quantites only for'    &
             ,' spherical systems' 
        write(6,*) ' we may try if you have a linear molecule aligned along z '
! for a linear molecule interatomic distances must be additive
! take any 3 atoms in the system, and r1 + r2 = r3
logical :: is_linear
        is_linear=.true.
        if (nucl_num.gt.2) then
        do i=1,nucl_num-2
         do j=i+1,nucl_num-1
          r1=nucl_dist(i,j)
          do k=j+1,nucl_num
           rjk=nucl_dist(j,k)
           rik=nucl_dist(i,k)
           r2=max(rjk,r1)
           r1=min(rjk,r1)
           r3=max(rik,r2)
           r2=min(rik,r2)
           if (abs(r3-r1-r2).gt.1.D-5) then
            is_linear=.false.
            write(6,*) ' Atoms No ',i,j,k,' are not on a common axis '
           end if
          end do
         end do
        end do
        else
         write(6,*) ' a diatomic molecule is necessarily linear '
        end if
        if (is_linear) then
logical :: is_on_z
         is_on_z=.true.
         do i=1,nucl_num
          if (nucl_coord(i,1)*nucl_coord(i,1)+nucl_coord(i,2)*nucl_coord(i,2).gt.1.D-5) then
           is_on_z=.false.
          end if
         end do
         if (.not.is_on_z) then
          write(6,*) 
          write(6,*) ' your molecule is not aligned along z '
          write(6,*) 
          write(6,*) ' Proposition of a new xyz file '
          write(6,*) nucl_num
          write(6,*) ' Aligned molecule '
          write(6,*)  element_name(nint(nucl_charge(1))),' 0. 0. 0. '
          write(6,*)  element_name(nint(nucl_charge(2))),' 0. 0. ',nucl_dist(1,2)
          do i=3,nucl_num
! outside the interval 1-2
           if ((nucl_dist(i,1).gt.nucl_dist(1,2)).or.(nucl_dist(i,2).gt.nucl_dist(1,2))) then
            if (nucl_dist(i,1).gt.nucl_dist(i,2)) then
             write(6,*)  element_name(nint(nucl_charge(i))),' 0. 0. ',nucl_dist(1,i)
            else
             write(6,*)  element_name(nint(nucl_charge(i))),' 0. 0. ',-nucl_dist(1,i)
            end if
           else
! inside
            write(6,*)  element_name(nint(nucl_charge(i))),' 0. 0. ',nucl_dist(1,i)
           end if
          end do
          return
         else
          write(6,*) ' your molecule is correctly aligned '
         end if
        else
         write(6,*) 
         write(6,*) ' your molecule is not linear '
         write(6,*) ' fixing L_z seems not useful '
         write(6,*) 
         return
        end if
       end if
!      write(6,*) 
!      write(6,*) ' the final Lz separated MOs '
!      write(6,*) 
!      do i=1,mo_num
!       write(6,*) ' Orbital No ',i
!       do j=1,mo_num
!eal*8 :: overlap_2MOs
!        ovl=overlap_2MOs(mo_purified_lz(1,i),ortho_spher(1,j))
!        ovl=0.D0
!        do alpha=1,ao_num
!         do beta=1,ao_num
!          ovl+=mo_purified_lz(alpha,i)*ortho_spher(beta,j)*ao_overlap(alpha,beta)
!         end do
!        end do
!        if (abs(ovl).gt.1.D-12) then
!         if (nucl_num.eq.1) then
!          write(6,*) ' component, l, lz = ',j,ovl,ortho_spher_l(j),ortho_spher_lz(j)
!         else
!          write(6,*) ' component, lz = ',j,ovl,ortho_spher_lz(j)
!         end if
!        end if
!       end do
!      end do

       mo_coef=mo_purified_lz
          
       WRITE(6,*)
       mo_label='Localized'
       call save_mos
        

     end subroutine driver

