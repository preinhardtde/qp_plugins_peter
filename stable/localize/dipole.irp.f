! -*- F90 -*-
BEGIN_PROVIDER [real*8, quad_rr, (mo_num,mo_num)]
     implicit none
     quad_rr=mo_spread_x+mo_spread_y+mo_spread_z
END_PROVIDER

BEGIN_PROVIDER [real*8, dipol, (mo_num,mo_num,3)]
     implicit none
     integer :: i,j
     do i=1,mo_num
      do j=1,mo_num
       dipol(i,j,1)=mo_dipole_x(i,j)
       dipol(i,j,2)=mo_dipole_y(i,j)
       dipol(i,j,3)=mo_dipole_z(i,j)
      end do
     end do
     write(6,*) ' filled dipole'
END_PROVIDER

