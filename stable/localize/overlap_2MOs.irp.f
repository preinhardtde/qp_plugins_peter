
real*8 function overlap_2MOs(mo1,mo2)
    implicit none
    real*8 :: mo1(ao_num),mo2(ao_num),ddot
    real*8, allocatable :: vec_tmp(:)

    allocate (vec_tmp(ao_num))

    call dgemv ('N', ao_num, ao_num, 1.D0, ao_overlap, ao_num, mo2, 1, 0.D0, vec_tmp, 1)
    overlap_2MOs=ddot(ao_num,mo1,1,vec_tmp,1)
    deallocate (vec_tmp)
end function overlap_2MOs
