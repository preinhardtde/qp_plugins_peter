! -*- F90 -*-
program correct_l
  implicit none
  BEGIN_DOC
! determination of Kato s condition around each atom
! we read a wavefunction, create density and grad density
! on a grid of points
  END_DOC
real*8 :: cpu0,wall0
 read_wf = .True.
 touch read_wf

          call cpu_time(cpu0)
          call wall_time(wall0)
         
          call header
          call driver
          call cpustamp(cpu0,wall0,cpu0,wall0,'CORL')
end program correct_l 

     subroutine header
       write(6,*)
       write(6,*) ' for atoms we wouuld like to have proper l states'
       write(6,*) ' set spurious terms to zero                      '
       write(6,*) '  P Reinhardt, Paris, June 2020 '
       write(6,*) ' in confinement we trust '
       write(6,*)
     end subroutine header

     subroutine driver
       implicit none
       integer :: i,alpha

       if (nucl_num.eq.1) then
        do i=1,mo_num
         write(6,*) ' orbital No ',i
         write(6,'(5(i5,F10.5))') (alpha,mo_purified(alpha,i),alpha=1,mo_num)
        end do
       else
        write(6,*) ' tool only working for atoms '
       end if

!      touch mo_coef
       WRITE(6,*)
       mo_label='Localized'
       mo_coef=mo_purified
       call save_mos
       write(6,*) ' saved MOs as ',mo_label

     end subroutine driver

