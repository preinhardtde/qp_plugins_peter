! -*- F90 -*-
BEGIN_PROVIDER [logical, lfragm]
    implicit none
    character*1 :: resp
    write(6,*) ' would you like to define fragments ? (y/n)'
    read(5,*) resp
    if (resp.eq.'y'.or.resp.eq.'Y') then
     lfragm=.true.
    else
     lfragm=.false.
    end if
END_PROVIDER

BEGIN_PROVIDER [integer, ifragm, (nucl_num)]
      implicit none
      integer :: i
      write(6,*) ' please indicate the fragment number of' &
         ,' each of the ',nucl_num,' atoms '
      read(5,*) (ifragm(i),i=1,nucl_num)
      WRITE(6,*)
      WRITE(6,*) ' attribution of fragments: '
      WRITE(6,'(6(I4,4H -> ,I3))') (I,IFRAGM(I),I=1,nucl_num)
      WRITE(6,*)
END_PROVIDER
