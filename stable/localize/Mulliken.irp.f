! -*- F90 -*-
 BEGIN_PROVIDER [real*8, Mull_tot_el]
&BEGIN_PROVIDER [real*8, Mull_tot, (nucl_num)]
&BEGIN_PROVIDER [real*8, Mull_net, (nucl_num)]
&BEGIN_PROVIDER [real*8, Mull_s, (nucl_num)]
&BEGIN_PROVIDER [real*8, Mull_p, (nucl_num)]
&BEGIN_PROVIDER [real*8, Mull_d, (nucl_num)]
&BEGIN_PROVIDER [real*8, Mull_f, (nucl_num)]
&BEGIN_PROVIDER [real*8, Mull_s_tot, (nucl_num)]
&BEGIN_PROVIDER [real*8, Mull_p_tot, (nucl_num)]
&BEGIN_PROVIDER [real*8, Mull_d_tot, (nucl_num)]
&BEGIN_PROVIDER [real*8, Mull_f_tot, (nucl_num)]
&BEGIN_PROVIDER [real*8, Mull_totot, (nucl_num)]

END_PROVIDER 

       subroutine print_Mulliken
       implicit none
       integer :: istate,iat,iorb,ii

       WRITE(6,*)
       WRITE(6,9919)
9919   FORMAT(/,'  Mulliken analysis')
       do istate=1,n_states
        Mull_s_tot=0.D0
        Mull_p_tot=0.D0
        Mull_d_tot=0.D0
        Mull_f_tot=0.D0
        Mull_totot=0.D0
        call Mulliken(istate)
       write(6,9914) istate
 9914 format(' Mulliken analysis, state No ',i6,/,'Atom',8x,'total' &
                 ,11x,'net',15x,'s',15x,'p',15x,'d',15x,'f')
       do iat=1,nucl_num
        WRITE(6,9912) nucl_label(iat),mull_tot(iat),mull_net(iat),mull_s(iat)    &
            ,mull_p(iat),mull_d(iat),mull_f(iat)
        Mull_s_tot(iat)+=mull_s(iat)
        Mull_p_tot(iat)+=mull_p(iat)
        Mull_d_tot(iat)+=mull_d(iat)
        Mull_f_tot(iat)+=mull_f(iat)
 9912   FORMAT(A6,6F16.8)
       end do
       write(6,*)
       end do
       WRITE(6,9929)
9929   FORMAT(/,'  Mulliken analysis broken down to orbitals and angular momenta')
       if (n_core_orb+n_inact_orb.ne.0) then
        istate=1
        write(6,*) ' Common orbitals '
        write(6,*) '              ' ,('            Atom No ',iat,'          ',iat=1,nucl_num)
        write(6,*) '# n        pop ',('       tot      s       p       d      f   ',iat=1,nucl_num)
write(6,*) ' Core '
        do ii=1,n_core_orb
         iorb=list_core(ii)
         call Mulliken_orb(istate,iorb)
         write(6,9931) iorb,Mull_tot_el,(Mull_tot(iat),Mull_s(iat),Mull_p(iat),Mull_d(iat),Mull_f(iat),iat=1,nucl_num)
         do iat=1,nucl_num
          Mull_totot(iat)+=mull_s(iat)
          Mull_totot(iat)+=mull_p(iat)
          Mull_totot(iat)+=mull_d(iat)
          Mull_totot(iat)+=mull_f(iat)
         end do
        end do
write(6,*) ' Inactive '
        do ii=1,n_inact_orb
         iorb=list_inact(ii)
         call Mulliken_orb(istate,iorb)
         write(6,9931) iorb,Mull_tot_el,(Mull_tot(iat),Mull_s(iat),Mull_p(iat),Mull_d(iat),Mull_f(iat),iat=1,nucl_num)
         do iat=1,nucl_num
          Mull_totot(iat)+=mull_s(iat)
          Mull_totot(iat)+=mull_p(iat)
          Mull_totot(iat)+=mull_d(iat)
          Mull_totot(iat)+=mull_f(iat)
         end do
        end do
       end if
       write(6,*)
       write(6,*)

       do istate=1,N_states
       write(6,*) ' state No ',istate
       write(6,*) '              ' ,('            Atom No ',iat,'          ',iat=1,nucl_num)
       write(6,*) '# n        pop ',('       tot      s       p       d      f   ',iat=1,nucl_num)
        Mull_s_tot=0.D0
        Mull_p_tot=0.D0
        Mull_d_tot=0.D0
        Mull_f_tot=0.D0
       Mull_totot=0.D0
real*8 :: ss
       ss=0.D0
       do ii=1,n_act_orb
        iorb=list_act(ii)
        call Mulliken_orb(istate,iorb)
        do iat=1,nucl_num
         Mull_s_tot(iat)+=mull_s(iat)
         Mull_p_tot(iat)+=mull_p(iat)
         Mull_d_tot(iat)+=mull_d(iat)
         Mull_f_tot(iat)+=mull_f(iat)
         Mull_totot(iat)+=mull_s(iat)
         Mull_totot(iat)+=mull_p(iat)
         Mull_totot(iat)+=mull_d(iat)
         Mull_totot(iat)+=mull_f(iat)
        end do
        if (abs(mull_tot_el).gt.1.D-5) write(6,9931) iorb,Mull_tot_el,(Mull_tot(iat),Mull_s(iat),Mull_p(iat),Mull_d(iat),Mull_f(iat),iat=1,nucl_num)
        ss+=mull_tot_el
 9931 format(i5,f10.3,30(' | ',5f8.3))
       end do
       write(6,*)
integer :: zero
       zero=0
       write(6,9931) zero,ss     &
           ,(mull_totot(iat),mull_s_tot(iat),mull_p_tot(iat)   &
           ,mull_d_tot(iat),mull_f_tot(iat),iat=1,nucl_num)
       write(6,*)
       write(6,9932) ss+2.D0*n_core_orb+2.D0*n_inact_orb
 9932 Format(' Found in total',f9.3,' electrons',/)
       end do

write(6,*) ' Virtual orbitals '
       do ii=1,n_virt_orb
        iorb=list_virt(ii)
        call Mulliken_orb_simple(iorb)
        do iat=1,nucl_num
         Mull_s_tot(iat)+=mull_s(iat)
         Mull_p_tot(iat)+=mull_p(iat)
         Mull_d_tot(iat)+=mull_d(iat)
         Mull_f_tot(iat)+=mull_f(iat)
        end do
        write(6,9931) iorb,Mull_tot_el,(Mull_tot(iat),Mull_s(iat),Mull_p(iat),Mull_d(iat),Mull_f(iat),iat=1,nucl_num)
       end do
       write(6,*)

write(6,*) ' Deleted orbitals '
       do ii=1,n_del_orb
        iorb=list_del(ii)
        call Mulliken_orb_simple(iorb)
        do iat=1,nucl_num
         Mull_s_tot(iat)+=mull_s(iat)
         Mull_p_tot(iat)+=mull_p(iat)
         Mull_d_tot(iat)+=mull_d(iat)
         Mull_f_tot(iat)+=mull_f(iat)
        end do
        if (abs(mull_tot_el).gt.1.D-5) write(6,9931) iorb,Mull_tot_el,(Mull_tot(iat),Mull_s(iat),Mull_p(iat),Mull_d(iat),Mull_f(iat),iat=1,nucl_num)
       end do
       write(6,*)

       end subroutine print_Mulliken
 
       subroutine Mulliken(istate)
        implicit none
        integer :: i,alpha,beta,iat,istate,il
        real*8 :: ss,w
        
        Mull_tot_el=0.D0
        Mull_tot=0.D0
        Mull_net=0.D0
        Mull_s  =0.D0
        Mull_p  =0.D0
        Mull_d  =0.D0
        Mull_f  =0.D0

        call fill_dens(istate)

        do alpha=1,ao_num
         do beta=1,ao_num
          iat=ao_nucl(beta)
          il=ao_l(beta)
          ss=dens(alpha,beta)*ao_overlap(alpha,beta)
          Mull_tot_el+=ss
          Mull_tot(iat)+=ss
          if (il.eq.0) then
           Mull_s(iat)+=ss
          else if (il.eq.1) then
           Mull_p(iat)+=ss
          else if (il.eq.2) then
           Mull_d(iat)+=ss
          else if (il.eq.3) then
           Mull_f(iat)+=ss
          end if
          if (iat.eq.ao_nucl(alpha)) then
           Mull_net(iat)+=ss
          end if
         end do
        end do

       end subroutine Mulliken

       subroutine Mulliken_orb(istate,iorb)
        implicit none
        integer :: i,alpha,beta,iat,istate,il,iorb
        real*8 :: ss,w
        
        Mull_tot_el=0.D0
        Mull_tot=0.D0
        Mull_net=0.D0
        Mull_s  =0.D0
        Mull_p  =0.D0
        Mull_d  =0.D0
        Mull_f  =0.D0

        call fill_dens_orb(istate,iorb)

        do alpha=1,ao_num
         do beta=1,ao_num
          iat=ao_nucl(beta)
          il=ao_l(beta)
          ss=dens(alpha,beta)*ao_overlap(alpha,beta)
          Mull_tot_el+=ss
          Mull_tot(iat)+=ss
          if (il.eq.0) then
           Mull_s(iat)+=ss
          else if (il.eq.1) then
           Mull_p(iat)+=ss
          else if (il.eq.2) then
           Mull_d(iat)+=ss
          else if (il.eq.3) then
           Mull_f(iat)+=ss
          end if
          if (iat.eq.ao_nucl(alpha)) then
           Mull_net(iat)+=ss
          end if
         end do
        end do

       end subroutine Mulliken_orb

       subroutine Mulliken_orb_simple(iorb)
        implicit none
        integer :: i,alpha,beta,iat,istate,il,iorb
        real*8 :: ss,w
        
        Mull_tot_el=0.D0
        Mull_tot=0.D0
        Mull_net=0.D0
        Mull_s  =0.D0
        Mull_p  =0.D0
        Mull_d  =0.D0
        Mull_f  =0.D0

        call fill_dens_orb_simple(iorb)

        do alpha=1,ao_num
         do beta=1,ao_num
          iat=ao_nucl(beta)
          il=ao_l(beta)
          ss=dens(alpha,beta)*ao_overlap(alpha,beta)
          Mull_tot_el+=ss
          Mull_tot(iat)+=ss
          if (il.eq.0) then
           Mull_s(iat)+=ss
          else if (il.eq.1) then
           Mull_p(iat)+=ss
          else if (il.eq.2) then
           Mull_d(iat)+=ss
          else if (il.eq.3) then
           Mull_f(iat)+=ss
          end if
          if (iat.eq.ao_nucl(alpha)) then
           Mull_net(iat)+=ss
          end if
         end do
        end do

       end subroutine Mulliken_orb_simple

