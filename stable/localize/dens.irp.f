! -*- F90 -*-
BEGIN_PROVIDER [real*8, dens, (ao_num,ao_num)]
END_PROVIDER

       subroutine fill_dens(istate)
BEGIN_DOC
! for a multi-state, multi-reference wavefunction we generate 
! a density matrix state by state. 
! A MR density matrix is the weighted superposition of density
! matrices of the determinants, as 1 = <Psi|Psi> = sum_I c_I^2 <I|I>
END_DOC
          use bitmasks
          implicit none
          integer :: alpha,beta,istate,ii,IDET
          integer(bit_kind), allocatable :: det_mu(:,:)
          integer(bit_kind)              :: imask

          integer :: i_nibble,ipos,iorb
           real*8 :: w
 
          dens=0.D0
          if (n_core_orb.ne.0) then
           do iorb=1,n_core_orb
            ii=list_core(iorb)
            do alpha=1,ao_num
             do beta=1,ao_num
              dens(alpha,beta)+=mo_coef(alpha,ii)*mo_coef(beta,ii)
             end do
            end do
           end do
           dens*=2.D0
          end if

          if (n_act_orb.ne.0) then
           allocate(det_mu(N_int,2))
           do idet=1,N_det
            w=psi_coef(idet,istate)
            if (abs(w).gt.1.D-7) write(6,*) ' istate, idet, psi_coef = ',istate,idet,w
            w*=w
            call det_extract(det_mu,idet,N_int)
            do iorb=1,n_act_orb
             ii=list_act(iorb)
             i_nibble = shiftr(ii-1,bit_kind_shift)+1
             ipos = ii-shiftl(i_nibble-1,bit_kind_shift)-1
             imask = ibset(0_bit_kind,ipos)
! is there an alpha spin ?
             if (iand(det_mu(i_nibble,1),imask) /= 0_bit_kind) then
              do alpha=1,ao_num
               do beta=1,ao_num
                dens(alpha,beta)+=w*mo_coef(alpha,ii)*mo_coef(beta,ii)
               end do
              end do
             end if
! is there a beta spin ?
             if (iand(det_mu(i_nibble,2),imask) /= 0_bit_kind) then
              do alpha=1,ao_num
               do beta=1,ao_num
                dens(alpha,beta)+=w*mo_coef(alpha,ii)*mo_coef(beta,ii)
               end do
              end do
             end if
            end do
           end do
           deallocate(det_mu)
          end if

       end subroutine fill_dens

       subroutine fill_dens_orb(istate,inorb)
BEGIN_DOC
! for a multi-state, multi-reference wavefunction we generate 
! a density matrix state by state. 
! A MR density matrix is the weighted superposition of density
! matrices of the determinants, as 1 = <Psi|Psi> = sum_I c_I^2 <I|I>
END_DOC
          use bitmasks
          implicit none
          integer :: i,alpha,beta,istate,ii,IDET
          integer(bit_kind), allocatable :: det_mu(:,:)
          integer(bit_kind)              :: imask

          integer :: i_nibble,ipos,iorb,inorb
          real*8 :: w
          logical :: is_core,is_act
 
          dens=0.D0
! what kind of orbital ?
          is_core=.false.
          is_act=.false.

          do i=1,n_core_orb
           ii=list_core(i)
           if (ii.eq.inorb) then
            is_core=.true.
            do alpha=1,ao_num
             do beta=1,ao_num
              dens(alpha,beta)+=mo_coef(alpha,inorb)*mo_coef(beta,inorb)
             end do
            end do
            dens*=2.D0
           end if
          end do

          do i=1,n_inact_orb
           ii=list_inact(i)
           if (ii.eq.inorb) then
            is_core=.true.
            do alpha=1,ao_num
             do beta=1,ao_num
              dens(alpha,beta)+=mo_coef(alpha,inorb)*mo_coef(beta,inorb)
             end do
            end do
            dens*=2.D0
           end if
          end do

          if (.not.is_core) then
           do i=1,n_act_orb
            ii=list_act(i)
            if (inorb.eq.ii) then
             is_act=.true.
             allocate(det_mu(N_int,2))
             do idet=1,N_det
              w=psi_coef(idet,istate)
              w*=w
              call det_extract(det_mu,idet,N_int)
              i_nibble = shiftr(ii-1,bit_kind_shift)+1
              ipos = ii-shiftl(i_nibble-1,bit_kind_shift)-1
              imask = ibset(0_bit_kind,ipos)
! is there an alpha spin ?
              if (iand(det_mu(i_nibble,1),imask) /= 0_bit_kind) then
               do alpha=1,ao_num
                do beta=1,ao_num
                 dens(alpha,beta)+=w*mo_coef(alpha,ii)*mo_coef(beta,ii)
                end do
               end do
              end if
! is there a beta spin ?
              if (iand(det_mu(i_nibble,2),imask) /= 0_bit_kind) then
               do alpha=1,ao_num
                do beta=1,ao_num
                 dens(alpha,beta)+=w*mo_coef(alpha,ii)*mo_coef(beta,ii)
                end do
               end do
              end if
             end do
             deallocate(det_mu)
            end if
           end do
          end if

        end subroutine fill_dens_orb

       subroutine fill_dens_orb_simple(inorb)
BEGIN_DOC
! no reference to wavefunctions, just 1 electron assumed, as for
! an analysis in % 
END_DOC
          implicit none
          integer :: alpha,beta,inorb

          dens=0.D0
          do alpha=1,ao_num
           do beta=1,ao_num
            dens(alpha,beta)+=mo_coef(alpha,inorb)*mo_coef(beta,inorb)
           end do
          end do
        end subroutine fill_dens_orb_simple
