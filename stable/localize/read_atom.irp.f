! -*- F90 -*-
 BEGIN_PROVIDER [integer, ao_num_at, (nucl_num)]
&BEGIN_PROVIDER [integer, mo_num_at, (nucl_num)]
          implicit none
          integer :: iat,idum,ao_tot,mo_tot
          
          ao_tot=0
          mo_tot=0
          do iat=1,nucl_num
           open(unit=12                                                 &
             ,file=filename_root//trim(element_name(nint(nucl_charge(iat))))   &
             ,status='old',form='formatted')
           read(12,*)
           read(12,*)
           read(12,*) ao_num_at(iat),idum,mo_num_at(iat)
           ao_tot+=ao_num_at(iat)
           mo_tot+=mo_num_at(iat)
           close(12)
          end do

          if (ao_tot.ne.ao_num) then
           write(6,*) ' number of AOs does not match ao_num = ',ao_num,ao_tot
           write(6,*) ao_num_at
           stop
          end if
          if (mo_tot.ne.mo_num) then
           write(6,*) ' number of mOs does not match mo_num = ',mo_num,mo_tot
           write(6,*) mo_num_at
           stop
          end if
END_PROVIDER

BEGIN_PROVIDER [character*18, filename_root]
          implicit none
    filename_root='Basis_and_MOs.dat_'
END_PROVIDER

 BEGIN_PROVIDER [logical, atom_typ_present, (0:127)]
&BEGIN_PROVIDER [integer, atom_typ_num]
           implicit none
           integer :: i

           atom_typ_present=.false.
           atom_typ_num=0
           do i=1,nucl_num
            if (.not.atom_typ_present(nint(nucl_charge(i)))) then
             atom_typ_present(nint(nucl_charge(i)))=.true.
             atom_typ_num+=1
            end if
           end do
END_PROVIDER

 BEGIN_PROVIDER [integer,num_core_at,(atom_typ_num)]
&BEGIN_PROVIDER [integer,num_act_at,(atom_typ_num)]
&BEGIN_PROVIDER [integer,num_virt_at,(atom_typ_num)]
&BEGIN_PROVIDER [integer,nz_at,(atom_typ_num)]
&BEGIN_PROVIDER [integer,atom_typ,(nucl_num)]
              implicit none
              integer :: iat,indx,inuc
              logical :: encountered

              indx=0
              do iat=0,127
               if (atom_typ_present(iat)) then
                indx+=1
                nz_at(indx)=iat
                write(6,*) ' please give the number of core orbitals for atom type ' & 
                    ,element_name(iat)
                read(5,*) num_core_at(indx)
                write(6,*) ' please give the number of active orbitals for atom type ' & 
                    ,element_name(iat)
                read(5,*) num_act_at(indx)
                encountered=.false.
                do inuc=1,nucl_num
                 if (nint(nucl_charge(inuc)).eq.nz_at(indx)) then
                  atom_typ(inuc)=indx
                  if (.not.encountered) then
                   num_virt_at(indx)=mo_num_at(inuc)-num_core_at(indx)-num_act_at(indx)
                   write(6,*) ' num_virt_at indx, inuc, core, act virt, tot ',indx,inuc,num_virt_at(indx),num_core_at(indx),num_act_at(indx),ao_num_at(inuc)
                   encountered=.true.
                  end if
                 end if
                end do
               end if
              end do
END_PROVIDER
              
BEGIN_PROVIDER [real*8, mo_merged, (ao_num,mo_num) ]
           implicit none
           integer :: iat,i,alpha,i_offset,alpha_offset,idum,indx
           real*8 :: xdum
           real*8, allocatable :: orb_tmp(:,:)

           mo_merged=0.D0
           i_offset=0
           alpha_offset=0
           do iat=1,nucl_num
            open(unit=12 &
             ,file=filename_root//trim(element_name(nz_at(atom_typ(iat))))   &
             ,status='old',form='formatted')
            read(12,*)
            read(12,*)
            read(12,*)
            read(12,*)
integer :: ii,icon,ncon
            do ii=1,ao_num_at(iat)
             read(12,*) idum,idum,idum,idum,ncon
             do icon=1,ncon
              read(12,*) xdum
             end do
            end do
            read(12,*)
            do ii=1,mo_num_at(iat)
            read(12,*)
            read(12,*) (mo_merged(alpha+alpha_offset,ii+i_offset)   &
                 ,alpha=1,ao_num_at(iat))
            read(12,*)
            end do
            close(12)
            alpha_offset+=ao_num_at(iat)
            i_offset+=mo_num_at(iat)
           end do
           
           allocate(orb_tmp(ao_num,mo_num))
           orb_tmp=0.D0
           i_offset=0
           indx=0
! core orbitals
           do iat=1,nucl_num
            do i=1,num_core_at(atom_typ(iat))
             indx+=1
             write(6,*) ' Core iat i i+i_offset indx', iat, i, i+i_offset, indx
             do alpha=1,ao_num
              orb_tmp(alpha,indx)=mo_merged(alpha,i+i_offset)
             end do
            end do
            i_offset+=mo_num_at(iat)
           end do
! active orbitals
           i_offset=0
           do iat=1,nucl_num
            do i=1,num_act_at(atom_typ(iat))
             indx+=1
             write(6,*) ' Act  iat i i+i_offset indx', iat, i, i+i_offset+num_core_at(atom_typ(iat)), indx
             do alpha=1,ao_num
              orb_tmp(alpha,indx)=mo_merged(alpha,i+i_offset+num_core_at(atom_typ(iat)))
             end do
            end do
            i_offset+=mo_num_at(iat)
           end do
! virtual orbitals
           i_offset=0
           do iat=1,nucl_num
            do i=1,num_virt_at(atom_typ(iat))
             indx+=1
             write(6,*) ' Virt  iat i i+i_offset indx', iat, i, i+i_offset+num_act_at(atom_typ(iat))+num_core_at(atom_typ(iat)), indx
             do alpha=1,ao_num
              orb_tmp(alpha,indx)=mo_merged(alpha,i+i_offset+num_act_at(atom_typ(iat)) &
                  +num_core_at(atom_typ(iat)))
             end do
            end do
            i_offset+=mo_num_at(iat)
           end do
           
! copy all orbitals back

           mo_merged=orb_tmp

           deallocate(orb_tmp)

           write(6,*) ' the merged orbitals '
           do i=1,mo_num
            write(6,*) ' Orbital No ',i
            write(6,'(5(i5,f15.5))') (alpha,mo_merged(alpha,i),alpha=1,ao_num)
           end do
END_PROVIDER


 BEGIN_PROVIDER [integer,ncore_tot]
&BEGIN_PROVIDER [integer,nact_tot]
&BEGIN_PROVIDER [integer,nvirt_tot]
            implicit none
            integer :: iat,itype
            ncore_tot=0
            nact_tot=0
            nvirt_tot=0
            do iat=1,nucl_num
             do itype=1,atom_typ_num
              if (nz_at(itype).eq.nint(nucl_charge(iat))) then
               ncore_tot+=num_core_at(itype)
               nact_tot+=num_act_at(itype)
               nvirt_tot+=num_virt_at(itype)
              end if
             end do
            end do
END_PROVIDER
