! -*- F90 -*-
 BEGIN_PROVIDER[real*8, baryz_orb, (mo_num,3)]
&BEGIN_PROVIDER[real*8, radii_orb, (mo_num)]
       implicit none
       integer :: i
       real*8 :: b2
       DO I=1,mo_num
        baryz_orb(I,1)=mo_DIPOLe_x(I,I)
        baryz_orb(I,2)=mo_DIPOLe_y(I,I)
        baryz_orb(I,3)=mo_DIPOLe_z(I,I)
        b2=baryz_orb(I,1)*baryz_orb(I,1)
        b2=b2+baryz_orb(I,2)*baryz_orb(I,2)
        b2=b2+baryz_orb(I,3)*baryz_orb(I,3)
        radii_orb(I)=QUAD_RR(I,I)-B2
       END DO
END_PROVIDER

BEGIN_PROVIDER [real*8, baryz_nuc, (3)]
BEGIN_DOC
! the trivial, non-mass-weighted barycentre of the molecule
END_DOC
      implicit none
      integer :: i,iat
      baryz_nuc=0.D0

      do iat=1,nucl_num
       do i=1,3
        baryz_nuc(i)+=+nucl_coord(iat,i)
       end do
      end do
      do i=1,3
       baryz_nuc(i)*=1.D0/dble(nucl_num)
      end do

END_PROVIDER

       subroutine print_baryz
         implicit none
         integer :: i,k,j
       WRITE(6,*)
       WRITE(6,9909)
9909   FORMAT(/,'  Barycenters of the orbitals and spread' &
            ,//,5x,'No',10x,'x',15x,'y',15x,'z',10x,'SQRT(<rr>)   type   atoms ',/)
       DO I=1,mo_num
        IF (IOTYP(I).EQ.5) THEN
        WRITE(6,9902) I,(baryz_orb(I,K),K=1,3),sqrt(radii_orb(i)),COTYP(IOTYP(I))
        ELSE
        WRITE(6,9902) I,(baryz_orb(I,K),K=1,3),sqrt(radii_orb(i)),COTYP(IOTYP(I)),(IATYP(J,I),J=1,IOTYP(I))
        END IF
9902    FORMAT(I6,4F16.8,5X,A4,5I3)
       END DO
       WRITE(6,*)

       end subroutine print_baryz

 BEGIN_PROVIDER [integer, IOTYP, (mo_num)]
&BEGIN_PROVIDER [integer, IATYP, (4,mo_num)]
BEGIN_DOC
! spread of the orbitals over the atoms, as determined via a
! Mulliken analysis orbital per orbital
END_DOC
       implicit none 
       integer :: i,iat
       integer :: iatom,iorb
       real*8 :: ocdum,cia
        real*8 :: omx,oms
       integer :: ialph,ibeta,nocc
       
       real*8, allocatable :: occp(:),occt(:)
       allocate (occp(nucl_num),occt(nucl_num))
       
       nocc=elec_alpha_num+elec_beta_num
       occt=0.d0

       write(6,9900)(i,i=1,nucl_num)
       do iorb=1,mo_num
! density matrix for each orbital
        do iatom=1,nucl_num
         ocdum=0.d0
         do ialph=1,ao_num
          if (ao_nucl(ialph).eq.iatom) then
           cia=mo_coef(ialph,iorb)
           do ibeta=1,ao_num
            ocdum=ocdum+cia*mo_coef(ibeta,iorb)*ao_overlap(ialph,ibeta)
           end do
          end if
         end do
         occp(iatom)=ocdum
        end do
        write(6,9901) iorb,(2.d0*occp(i),i=1,nucl_num)
        if (iorb.le.nocc) then
         do iat=1,nucl_num
          occt(iat)+=2.d0*occp(iat)
         end do
        end if
!
! well, what type of orbital do we have?
        omx=0.d0
        oms=0.d0
        do iatom=1,nucl_num
         oms=oms+occp(iatom)
         if (occp(iatom).gt.omx) then
          omx=occp(iatom)
          iatyp(1,iorb)=iatom
         end if
        end do
! how many other occupations are at least 50% of omx?
integer :: iomx
        iomx=1
        do iatom=1,nucl_num
         if (occp(iatom).gt.0.5d0*omx.and.iatom.ne.iatyp(1,iorb)) then
          iomx=iomx+1
          if (iomx.le.4) iatyp(iomx,iorb)=iatom
         end if
        end do
        iotyp(iorb)=min(5,iomx)
        if (iotyp(iorb).eq.5) then
         do i=1,4
          iatyp(i,iorb)=0
         end do
        end if
       end do

       write(6,*)
       write(6,9900) (iat,iat=1,nucl_num)
       write(6,9902) (occt(iat),iat=1,nucl_num)
9900   format('  Atom',i4,19(i8))
9901   format(i4,20(f8.4))
9902   format(' Total ',20(f8.4))
       deallocate (occp,occt)

END_PROVIDER

