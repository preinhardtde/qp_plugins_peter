program extract_all
  implicit none
  BEGIN_DOC
! TODO : Put the documentation of the program here
  END_DOC
  real*8 :: cpu0, wall0
  call cpu_time(cpu0)
  call wall_time(wall0)
  read_wf=.true.
  touch read_wf

  call header
  call driver

  call cpustamp(cpu0,wall0,cpu0,wall0,'All ')
end program extract_all

subroutine header
        implicit none
        write(6,*)
        write(6,*) ' dump information on basis sets and MOs'
        write(6,*) ' P Reinhardt (Auxerre, june 2020) '
        write(6,*)
end subroutine header

subroutine driver
      implicit none

      integer :: i,j

      write(6,*) 
      write(6,*) ' Dumping basis set and MOs to a file <Basis_and_MOs.dat>'
      open(unit=12,file='Basis_and_MOs.dat',status='unknown',form='formatted')
      write(12,*) nucl_num,' # nucl_num'
      do i=1,nucl_num
       write(12,*) nucl_charge(i),(nucl_coord(i,j),j=1,3),' # nucl_charge, nucl_coord'
      end do
      write(12,*) ao_num,ao_prim_num_max,mo_num,' # ao_num,ao_prim_num_max,mo_num'
      write(12,*) elec_alpha_num,elec_beta_num,' # elec_alpha_num,elec_beta_num '
      do i=1,ao_num
       write(12,*) (ao_power(i,j),j=1,3),ao_nucl(i),ao_prim_num(i),ao_l(i),' # ao_power,ao_nucl,ao_prim_num, ao_l '
       do j=1,ao_prim_num(i)
         write(12,*) ao_expo_ordered_transp(j,i),ao_coef_normalized_ordered_transp(j,i),' # exponent, coefficient '
!        write(12,*) ao_expo(i,j),ao_coef(i,j),' # exponent, coefficient '
       end do
      end do
      write(12,*) '# the molecular orbitals '
      do i=1,mo_num
       do j=1,ao_num
        if (abs(mo_coef(j,i)).lt.1.D-10) mo_coef(j,i)=0.D0
       end do
       write(12,*) '# orbital No ',i
       write(12,'(5E20.12)') (mo_coef(j,i),j=1,ao_num)
       write(12,*) 
      end do
      close(12)
      write(6,*)  ' ... done '
      write(6,*) 
end subroutine driver
