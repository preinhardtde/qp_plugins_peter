! -*- F90 -*-
 BEGIN_PROVIDER [integer, ao_num_old]
&BEGIN_PROVIDER [integer, ao_prim_num_max_old]
&BEGIN_PROVIDER [integer, mo_num_old]
&BEGIN_PROVIDER [integer, nucl_num_old]
        implicit none
        integer :: i,j
        write(6,*)
        write(6,*)  ' reading dimensions from file <Basis_and_MOs.dat>'
        open(unit=12,file='Basis_and_MOs.dat',status='old',form='formatted')
        read(12,*) nucl_num_old
! number of electrons, as well as spin states may change
! but not the number and the order of the atoms
        if (nucl_num_old.ne.nucl_num) then
         write(6,*) ' from file, from ezfio ',nucl_num_old,nucl_num
         stop ' number of nuclei is not conserved !'
        end if
        do i=1,nucl_num
         read(12,*)
        end do
        read(12,*) ao_num_old,ao_prim_num_max_old,mo_num_old
        close(12)
        write(6,*) ' ... done '
        write(6,*)

!       if (ao_num.lt.ao_num_old) then 
!        write(6,*) ' not yet ready for reducing basis sets '
!        stop ' no reduction yet '
!       end if

        write(6,*) 
        write(6,*) ' dimensions '
        write(6,*) '                      from file    from ezfio '
        write(6,*) ' ao_num          ',ao_num_old,ao_num
        write(6,*) ' mo_num          ',mo_num_old,mo_num
        write(6,*) ' ao_prim_num_max ',ao_prim_num_max_old,ao_prim_num_max
        write(6,*) 
   
END_PROVIDER

 BEGIN_PROVIDER [real*8, nucl_coord_old, (nucl_num,3)]
&BEGIN_PROVIDER [real*8, nucl_charge_old, (nucl_num)]
&BEGIN_PROVIDER [integer, ao_nucl_old, (ao_num_old)]
&BEGIN_PROVIDER [integer, ao_power_old, (ao_num_old,3)]
&BEGIN_PROVIDER [integer, ao_prim_num_old, (ao_num_old)]
&BEGIN_PROVIDER [real*8, ao_expo_ordered_transp_old, (ao_prim_num_max_old,ao_num_old)]
&BEGIN_PROVIDER [real*8, ao_coef_normalized_ordered_transp_old, (ao_prim_num_max_old,ao_num_old)]
&BEGIN_PROVIDER [real*8, mo_coef_old, (ao_num_old,mo_num_old)]
&BEGIN_PROVIDER [integer, elec_alpha_num_old]
&BEGIN_PROVIDER [integer, elec_beta_num_old ]
        integer :: i,j,idum,k
        write(6,*)
        write(6,*)  ' reading data from file <Basis_and_MOs.dat>'
        open(unit=12,file='Basis_and_MOs.dat',status='old',form='formatted')
        read(12,*) idum
        do i=1,nucl_num
         read(12,*) nucl_charge_old(i),(nucl_coord_old(i,j),j=1,3)
         if (nucl_charge_old(i).ne.nucl_charge(i)) then
          write(6,*) ' nuclear charge for atom ',i,' is not consistent '
          stop ' nuclear charges ? '
! positions may be different, it may be same basis but different geometry
         end if
        end do
        read(12,*) idum
        read(12,*) elec_alpha_num_old,elec_beta_num_old
        do i=1,ao_num_old
         read(12,*) (ao_power_old(i,j),j=1,3),ao_nucl_old(i),ao_prim_num_old(i)
         do j=1,ao_prim_num_old(i)
          read(12,*) ao_expo_ordered_transp_old(j,i)  & 
            ,ao_coef_normalized_ordered_transp_old(j,i)
 !        write(6,*) j,i,ao_expo_ordered_transp_old(j,i)  & 
 !          ,ao_coef_normalized_ordered_transp_old(j,i),ao_expo_ordered_transp(j,i),ao_coef_normalized_ordered_transp(j,i)
         end do
        end do
        write(6,*) ' number of electrons from file  ',elec_alpha_num_old,elec_beta_num_old
        write(6,*) ' number of electrons from ezfio ',elec_alpha_num,elec_beta_num
        read(12,*)
        do i=1,mo_num_old
         read(12,*)
         read(12,*) (mo_coef_old(j,i),j=1,ao_num_old)
         read(12,*)
        end do
        close(12)
        write(6,*) ' ... done '
        write(6,*)
 END_PROVIDER
