======
blowup
======

2 programs

- We extract from an existing ezfio the basis set and the orbitals, and save them externally to a file
- projection of the orbitals to another ezfio, i.e. basis set. Orbitals are stored as Guess

The routine for calculating overlaps is rewritten for overlaps between the old and the new basis set.

The MOs are already orthogonal. We orthogonalize the basis in the ezfio, and write

|  |phi_k> <phi_k|Phi_i> = \sum_a |Xi_B,a> C_ak \sum_bc C_bk <Xi_B,b|Xi_A,c> c_ci  
| c_ci = expansion of Phi_i in old basis  
| C_ak = expansion of OAO k in new basis  
| Xi_B,a = new basis functions  
| Xi_A,c = old basis functions  
