program change_ezfio
  implicit none
  BEGIN_DOC
! TODO : Put the documentation of the program here
  END_DOC
  real*8 :: cpu0,wall0
  read_wf=.true.
  touch read_wf

  call cpu_time(cpu0)
  call wall_time(wall0)

  call header
  call setup
  call driver

  call cpustamp(cpu0,wall0,cpu0,wall0,'All ')
end program change_ezfio

subroutine header
    implicit none 
    write(6,*) 
    write(6,*)  ' Tool for expanding orbitals from one qp2 calculation '
    write(6,*)  ' to another qp2 calculation '
    write(6,*)  ' basis set may change, geometry may change '
    write(6,*)  ' number of electrons may change, spin state may change '
    write(6,*)  ' but not number, kind and order of atoms '
    write(6,*) 
    write(6,*)  ' P Reinhardt (train between Laroche-Migennes and Paris, june 2020) '
    write(6,*) 

    write(6,*) 
end subroutine header

subroutine setup
    implicit none
    integer :: i,j
    write(6,*) ' creating OAO basis'
    oao_coef=ao_cart_to_sphe_coef
    call shalf(1,mo_num,oao_coef)
    write(6,*)
    write(6,*) ' the OAO orbitals '
    write(6,*)
    do i=1,mo_num
     write(6,9901) i,mo_occ(i)
 9901 format(' Orbital No ',i5,' occupation ',f4.2)
     write(6,'(6(i4,F12.5))') (j,mo_coef(j,i),j=1,ao_num)
     write(6,*)
    end do

     write(6,*)
    write(6,*) ' ... done '

end subroutine setup

subroutine driver
      implicit none
      integer :: i,j
      write(6,*)
      write(6,*) ' xyz file of the ezfio '
      write(6,9911)
      write(6,*) nucl_num
      do i=1,nucl_num
       write(6,'(a4,3F20.12)') element_name(nint(nucl_charge(i))),(nucl_coord(i,j),j=1,3)
      end do
      write(6,9911)
      write(6,*) ' xyz dat found on file '
      write(6,*) nucl_num_old
      do i=1,nucl_num
       write(6,'(a4,3F20.12)') element_name(nint(nucl_charge_old(i))),(nucl_coord_old(i,j),j=1,3)
      end do
      write(6,9911)
 9911 format(65('='))
      write(6,*)

      mo_label='Guess'
      mo_coef=orbs_in_new_basis

      write(6,*)
      write(6,*) ' the orbitals to be saved '
      write(6,*)
      do i=1,mo_num
       write(6,9901) i,mo_occ(i)
 9901 format(' Orbital No ',i5,' occupation ',f4.2)
       write(6,'(6(i4,F12.5))') (j,mo_coef(j,i),j=1,ao_num)
       write(6,*)
      end do
      call save_mos
      write(6,*) ' saved new orbitals '

end subroutine driver
