! -*- F90 -*-
BEGIN_PROVIDER [ real*8,ao_overlap_2basis, (ao_num_old,ao_num) ]
  implicit none
  BEGIN_DOC
! Overlap between atomic basis functions:
!
! :math:`\int \chi_i(r) \chi_j(r) dr`
!
! 2 basis sets: the one from the ezfio, and another one from file called 'old'
  END_DOC
  integer :: i,j,iprim,jprim
  double precision :: f
  integer :: dim1
  double precision :: overlap,overlap_x,overlap_y,overlap_z
  double precision :: alpha, beta, c
  double precision :: A_center(3), B_center(3)
  integer :: power_A(3), power_B(3)

  ao_overlap_2basis = 0.d0
  dim1=100

  write(6,*) ' providing ao_overlap_2basis '

!$XXYXXY PARALLEL DO SCHEDULE(GUIDED) &
!$XXYXXY DEFAULT(NONE) &
!$XXYXXY PRIVATE(A_center,B_center,power_A,power_B,&
!$XXYXXY  overlap_x,overlap_y, overlap_z, overlap, &
!$XXYXXY  alpha, beta,i,j,c) &
!$XXYXXY SHARED(nucl_coord,ao_power,ao_prim_num,ao_overlap_2basis, &
!$XXYXXY        nucl_coord_old,ao_power_old,ao_prim_num_old, &
!$XXYXXY  ao_num,ao_coef_normalized_ordered_transp,ao_nucl,  &
!$XXYXXY  ao_num_old,ao_coef_normalized_ordered_transp_old,ao_nucl_old,  &
!$XXYXXY  ao_expo_ordered_transp_old,     &
!$XXYXXY  ao_expo_ordered_transp,dim1)
  do j=1,ao_num_old
   A_center(1) = nucl_coord_old( ao_nucl_old(j), 1 )
   A_center(2) = nucl_coord_old( ao_nucl_old(j), 2 )
   A_center(3) = nucl_coord_old( ao_nucl_old(j), 3 )
   power_A(1)  = ao_power_old( j, 1 )
   power_A(2)  = ao_power_old( j, 2 )
   power_A(3)  = ao_power_old( j, 3 )
   do i= 1,ao_num
    B_center(1) = nucl_coord( ao_nucl(i), 1 )
    B_center(2) = nucl_coord( ao_nucl(i), 2 )
    B_center(3) = nucl_coord( ao_nucl(i), 3 )
    power_B(1)  = ao_power( i, 1 )
    power_B(2)  = ao_power( i, 2 )
    power_B(3)  = ao_power( i, 3 )
    do jprim = 1,ao_prim_num_old(j)
     alpha = ao_expo_ordered_transp_old(jprim,j)
     do iprim = 1, ao_prim_num(i)
      beta = ao_expo_ordered_transp(iprim,i)
      call overlap_gaussian_xyz(A_center,B_center,alpha,beta,power_A &
         ,power_B,overlap_x,overlap_y,overlap_z,overlap,dim1)
      c = ao_coef_normalized_ordered_transp_old(jprim,j) * ao_coef_normalized_ordered_transp(iprim,i)
      ao_overlap_2basis(j,i) += c * overlap
     end do
    end do
   end do
  end do
!$XXYXXY END PARALLEL DO

!   open(unit=12,file='AO_OVERLAP_2BASIS',form='formatted',status='unknown')
!   do i=1,ao_num_old
!    do j=1,ao_num
!     if (abs(ao_overlap_2basis(i,j)).gt.1.D-12) write(12,*) i,j,ao_overlap_2basis(i,j),ao_overlap(i,j)
!    end do
!   end do
!   close(12)
!   write(6,*) ' dumped overlap matrix to file <AO_OVERLAP_2BASIS> '
!   write(6,*)

    write(6,*) ' ... done '

END_PROVIDER

