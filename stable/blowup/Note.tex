\documentstyle[aps,epsf,rotating]{revtex}
\input epsf
\input colordvi
\epsfverbosetrue
%
\def\bild#1#2#3{$$\vbox{\hfil\hskip -1 true cm\epsfysize=#2 true cm
\epsffile{#1.eps}\bildunterschrift{#3}\hfill}$$}
\def\vierbild#1#2#3#4#5#6{$$\vbox{\hfil\hskip -1 true cm\epsfysize=#5 true cm
\epsffile{#1.ps}\epsffile{#2.ps}\epsffile{#3.ps}\epsffile{#4.ps}\bildunterschrift{#6}\hfill}$$}

\def\Det#1#2{\Phi_{\rm #1}^{\rm #2}}
\def\DDet#1#2#3#4{\Phi_{\rm #1#2}^{\rm #3#4}}
\def\bint#1#2#3#4{(\,\rm #1\, #2\,|\,#3\,#4\,)}


\def\dbtilde#1{\overset{\approx}{#1}}

\def\E#1#2{\hat{E}_{#1#2}}

\def\bild#1#2#3{
\begin{figure}
$$\vbox{\hfil\hskip -1 true cm\epsfysize=#2 true cm \epsffile{#1.eps}} $$
\caption{\vtop{\hsize = 10 true cm #3}}
\label{#1}
\end{figure}
}


\def\obaseskip{\baselineskip = 10 pt}
\def\bb#1{\setbox1=\vtop{\hsize = 15 true cm \obaseskip\noindent #1}\box1}
\def\reference#1#2#3#4#5{\sl #1\rm , #2, \bf #3 \rm (#4) #5}
\newcount\footnoteindex
\footnoteindex=1
\def\footnotenumber{$^{\number\footnoteindex}$}
\def\footnotenew#1{\footnote{\bb{\obaseskip #1}}}

\def\footref#1#2#3#4#5{\footnotenew{\reference{\obaseskip #1}{\obaseskip #2}{\obaseskip #3}{\obaseskip #4}{\obaseskip #5}}}

\def\va{\vec{\rm a}}
\def\vr{\vec{\rm r}}
\def\vR{\vec{\rm R}}
\def\vb{\vec{\rm b}}
\def\vc{\vec{\rm c}}
\def\vd{\vec{\rm d}}
\def\vg{\vec{\rm g}}
\def\vgp{\vec{\rm g'}}
\def\vm{\vec{\rm m}}
\def\vn{\vec{\rm n}}
\def\vh{\vec{\rm h}}
\def\vs{\vec{\rm s}}
\def\vi{\vec{\rm i}}
\def\vj{\vec{\rm j}}
\def\vk{\vec{\rm k}}
\def\vl{\vec{\rm l}}
\def\vmu{\vec{\mu}}
\def\vo{\vec{0}}

\def\ket#1{|#1\rangle}
\def\bra#1{\langle #1}

\def\matel#1#2#3{\left\langle\, #1\, \left| \, 
#2\,\right|\,#3\,\right\rangle}
\def\reference#1#2#3#4#5{\sl #1\rm , #2, \bf #3 \rm (#4) #5}
%
\begin{document}
\title{Changing the ezfio for subsequent calculations with the Quantum Package} 
\author{P.\ Reinhardt}
\address{Laboratoire de Chimie Th\'eorique, Facult\'e des Sciences et de 
l'Ing\'enierie, Sorbonne Universit\'e,
  \\ 4, place Jussieu, F -- 75252 Paris CEDEX 05, France}
\maketitle

\begin{abstract}
Changing the ezfio may be useful when conducting calculations in 
different basis sets, different numbers of electrons, different geometries, 
or just different spin states, but having orbitals generated from the previous 
calculation.
  \bigskip
  \noindent(TeXed on \today)
\end{abstract}

\section{Introduction}
For executing a series of calculation it may be useful to transfer data from 
oone ezfio to another. For instance we may want to 
\begin{itemize}
 \item{change basis set}
 \item{change number of electrons}
 \item{change the spin state}
 \item{change geometry of a molecule}
\end{itemize}
and see how the orbitals or the wavefunction evolves. The number of atoms, 
their nuclei, and their order have to be preserved. 

A simple rotation of the coordinate system, useful in the context of 
intermolecular interaction, is another story, to be treated exactly, not 
furnishing a guess for a subsequent SCF.

\section{The formula}
The mathematics are quite simple: we develop orthogonal vectors in one basis in 
another orthogonal basis. To do so we write the unit matrix as a sum of 
projectors on orthogonal orbitals (or insert an identity within this 
orbital space) as
\begin{equation}
\hat{P}=\hat{1}_P = \sum_i 
|\phi_i\rangle\,\langle\phi_i| = \sum_\i \sum_{\alpha,\beta} 
c_{\alpha\,i}c_{\beta\,i}\ |\chi_{\alpha}\rangle\,\langle\chi_{\beta}| 
\end{equation}
Indeed, we have the necessary identity $\hat{P}^2=\hat{P}$:
\begin{eqnarray}
 \hat{P}^2 & = & \sum_i\sum_j 
|\phi_i\rangle\,\langle\phi_i|\phi_j\rangle\,\langle\phi_j| \nonumber\\ 
& = & \sum_i\sum_j\sum_{\alpha\beta\gamma\delta} 
c_{\alpha\,i}c_{\beta\,i}c_{\gamma\,j}c_{\delta\,j}
|\chi_\alpha \rangle\,\underbrace{\langle\chi_\beta| \chi_\gamma 
\rangle}_{=S_{\beta\gamma}}\,\langle\chi_\delta| \nonumber \\
& = & \sum_i\sum_j\sum_{\alpha\delta} 
c_{\alpha\,i}c_{\delta\,j} |\chi_\alpha 
\rangle\,\underbrace{\sum_{\beta\gamma}c_{\beta\,i}c_{\gamma\,j}
\langle\chi_\beta| \chi_\gamma 
\rangle}_{=\delta_{ij}}\,\langle\chi_\delta| \nonumber \\
& = & \sum_i\sum_{\alpha\delta} 
c_{\alpha\,i}c_{\delta\,i} |\chi_\alpha 
\rangle\,\langle\chi_\delta| = \sum_i |\phi_i\rangle\,\langle\phi_i| = \hat{P}
\end{eqnarray}

An orbital in another basis is expressed as $|\tilde{\varphi}_k\rangle = 
\sum_{\gamma} 
\tilde{c}_{\gamma\,k}\, |\tilde{\chi}_{\gamma}\rangle$. 
Expressing $\tilde{\varphi}$ in the other basis comes then down to writing 
\begin{eqnarray}
 |\varphi_k\rangle & = & \sum_i 
|\phi_i\rangle\,\langle\phi_i|\tilde{\varphi}_k\rangle \nonumber \\ &  = & 
\sum_i 
\sum_{\alpha\beta\gamma}
c_{\alpha\,i}\,c_{\beta\,i}\,\tilde{c}_{\gamma\,k}
\ |\chi_{\alpha}\rangle\,\langle\chi_{\beta}|\tilde{\chi}_{\gamma}\rangle 
\nonumber \\
& = & \sum_{\alpha}
\underbrace{\left(
\sum_i\,c_{\alpha\,i} \sum_{\beta\gamma} \,c_{\beta\,i}\,\tilde{c}_{\gamma\,k} 
\,\langle\chi_{\beta}|\tilde{\chi}_{\gamma}\rangle
\right)}_{=\, d_{\alpha k}} |\chi_{\alpha}\rangle\nonumber  \\
& = & \sum_{\alpha} \, d_{\alpha k}\, |\chi_{\alpha}\rangle
\end{eqnarray}
The overlap $\langle \tilde{\varphi}_k|\varphi_k\rangle$ is 
\begin{equation}
 \langle \tilde{\varphi}_k|\varphi_k\rangle = \sum_i | \langle \phi_i 
|\tilde{\varphi}_k\rangle|^2 
\end{equation}
which becomes one if the $\chi_{\alpha}$ and the $\tilde{\chi}_{\gamma}$ span 
the same space of basis functions.



\section{Details of the implementation}

\section{}

\end{document}



