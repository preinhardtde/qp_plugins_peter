! -*- F90 -*-
BEGIN_PROVIDER [real*8, oao_coef, (ao_num,mo_num)] 
END_PROVIDER 

BEGIN_PROVIDER [real*8, orbs_in_new_basis, (ao_num,mo_num)] 
BEGIN_DOC
!
! new expansion coefficients of the old MOs in a new basis
! at the end we have a new set MO coefficients for the new ezfio
! First we expand the old MOs in the new basis, then we add 
! remaining OAOs from the actual ezfio, and orthonormalize all.
! Via hierarchial order S^-1/2, projection, S^-1/2
!
! if we need a reduction instead of an expansion we may take 
! the projected MOs with the largest components in the new basis
END_DOC
         implicit none
         integer :: i,j,alph,beta,gamm,k
         real*8 :: cpu0,wall0,ss
         integer :: pick
         real*8 :: ov_min,ov_max
         real*8, allocatable :: orb_tmp(:,:)
         real*8, allocatable :: smo(:,:)
         call cpu_time(cpu0)
         call wall_time(wall0)

!        do i=1,ao_num_old
!         do j=1,ao_num
!          if (abs(ao_overlap_2basis(i,j)).gt.1.D-7) then
!           write(6,*) i,j,ao_overlap(i,j),ao_overlap_2basis(i,j)
!          end if 
!         end do
!        end do

         orbs_in_new_basis=0.D0
allocate (orb_tmp(ao_num,mo_num))
allocate (smo(mo_num,mo_num))

        if (mo_num_old.le.mo_num) then
! expansion
         do k=1,mo_num_old
          do alph=1,ao_num
real*8 :: dak,cai,cabi
           dak=0.D0
           do i=1,mo_num
            cai=oao_coef(alph,i)
            do beta=1,ao_num
             cabi=oao_coef(beta,i)*cai
             do gamm=1,ao_num_old
              dak+=cabi*mo_coef_old(gamm,k)*ao_overlap_2basis(gamm,beta)
             end do
            end do
           end do 
           orbs_in_new_basis(alph,k)=dak
          end do

          write(6,*) ' new projected orbital No ',k
          write(6,'(5(i4,F12.5))') (gamm,orbs_in_new_basis(gamm,k),gamm=1,ao_num)
 
         end do 
! orthogonalize the obtained projections

! calculate the overlaps of the projections
real*8 :: sss
         write(6,*)
         write(6,*) ' Overlaps of the projections '
         write(6,*) ' Difference to unit matrix '
         do i=1,mo_num_old
          sss=0.D0
          do alph=1,ao_num
           do beta=1,ao_num
            sss+=orbs_in_new_basis(alph,i)*orbs_in_new_basis(beta,i)*ao_overlap(alph,beta)
           end do
          end do
          if (abs(sss-1.D0).gt.1.D-10) write(6,*) ' new diagonal overlap ',i,sss
          do j=i+1,mo_num_old
           sss=0.D0
           do alph=1,ao_num
            do beta=1,ao_num
             sss+=orbs_in_new_basis(alph,i)*orbs_in_new_basis(beta,j)*ao_overlap(alph,beta)
            end do
           end do
           if (abs(sss).gt.1.D-10) write(6,*) ' new overlap ',i,j,sss
          end do
         end do 
         write(6,*) ' ... done '
         write(6,*)

         call shalf(1,mo_num_old,orbs_in_new_basis)

         do k=1,mo_num_old
          write(6,*) ' new projected orbital No ',k
          write(6,'(5(i4,F12.5))') (gamm,orbs_in_new_basis(gamm,k),gamm=1,ao_num)
         end do 
 

        if (mo_num_old.lt.mo_num) then

         write(6,*) ' subtract the projection from the OMOs'
! subtract the space from the OMOs
         orb_tmp=oao_coef
         do i=1,mo_num
          do j=1,mo_num_old
           sss=0.D0
           do alph=1,ao_num
            do beta=1,ao_num
             sss+=orb_tmp(alph,i)*orbs_in_new_basis(beta,j)     &
                   *ao_overlap(alph,beta)
            end do
           end do
           do alph=1,ao_num
            orb_tmp(alph,i)-=sss*orbs_in_new_basis(alph,j)
           end do
          end do
         end do
!
         write(6,*) 'Diagonalizing S in the resulting OMOs'

!   the easiest procedure is a SVD of the overlap matrix.
real*8, allocatable :: eves(:,:)
real*8, allocatable :: evas(:)
allocate (eves(mo_num,mo_num))
allocate (evas(mo_num))

         call transf2sq(ao_overlap,smo,mo_num,orb_tmp)
         call lapack_diag(evas,eves,smo,mo_num,mo_num)
         write(6,*) ' Eigenvalues : '
         write(6,'(5(i5,F12.5))') (i,evas(i),i=1,mo_num)
! take the eigenvectors with the largest eigenvals
         write(6,*)

         do i=mo_num_old+1,mo_num
          pick=0
          ov_max=0.D0
          do j=1,mo_num
           if (abs(evas(j)).gt.ov_max) then
            pick=j
            ov_max=evas(j)
           end if
          end do
          do alph=1,ao_num
           sss=0.D0
           do j=1,mo_num
            sss+=eves(j,pick)*orb_tmp(alph,j)
           end do
           orbs_in_new_basis(alph,i)=sss
          end do
          evas(pick)=0.D0
!         write(6,*) ' i= ',i,' added OMO No ',pick,' with eigenvalue ',ov_max
         end do
! 
         call shalf(mo_num_old+1,mo_num,orbs_in_new_basis)

deallocate (eves)
deallocate (evas)

         end if

       else
! reduction
logical, allocatable :: used(:)
allocate(used(mo_num_old))
        used=.false.
        write(6,*)
        write(6,*) ' reducing the basis ',mo_num_old,' -> ',mo_num
        write(6,*)

        do i=1,mo_num
! which one to take for this one ?
         ov_max=0.D0
         do j=1,mo_num_old
          ss=0.D0
          do alph=1,ao_num
           do beta=1,ao_num_old
            ss+=oao_coef(alph,i)*mo_coef_old(beta,j)*ao_overlap_2basis(beta,alph)
           end do
          end do
          if (abs(ss).gt.ov_max.and..not.used(j)) then
           pick=j
           if (ss.lt.0.D0) pick=-pick
           ov_max=abs(ss)
          end if
         end do
! we have to take the projection of 'pick' onto the smaller basis
         write(6,9917) i,pick,ov_max
 9917    format(' for OMO No ',i4,' we have as best MO ',i4 &
              ,' with overlap ',F12.6)
         used(abs(pick))=.true.
         do alph=1,ao_num
          ss=0.D0
          do j=1,mo_num
           do beta=1,ao_num_old
            ss+=dble(sign(1,pick))     &
             *oao_coef(alph,j)*mo_coef_old(beta,abs(pick))*ao_overlap_2basis(beta,alph)
           end do
          end do
          orb_tmp(alph,i)=ss
         end do
! close loop over i
        end do
        deallocate (used)
        write(6,*)
! orthogonalize
         call transf2sq(ao_overlap,smo,mo_num,orb_tmp)
integer :: nclosed
integer :: nopen
         nclosed=min(elec_alpha_num,elec_beta_num)
         nopen=max(elec_alpha_num,elec_beta_num)-nclosed
  
! we guess the open and active orbitals
          
         call shalf(1,nclosed,orb_tmp)
         call sfull(orb_tmp,nclosed)
         if (nopen.ne.0) then
          call shalf(1+nclosed,nclosed+nopen,orb_tmp)
          call sfull(orb_tmp,nclosed+nopen)
         end if
         call shalf(nclosed+nopen+1,mo_num,orb_tmp)
        write(6,*)

        orbs_in_new_basis=orb_tmp
       end if

deallocate (orb_tmp)
deallocate (smo)

!       write(6,*) ' Overlap of the MOs in the old and the new basis:'
!       do i=1,mo_num_old
!        do j=1,mo_num
!         sss=0.D0
!         do alph=1,ao_num
!          do beta=1,ao_num
!           sss+=mo_coef_old(alph,i)*orbs_in_new_basis(beta,j)* &
!              ao_overlap(alph,beta)
!          end do
!         end do
!         if (abs(sss).gt.1.D-1) write(6,*) '<',i,' old | ',j,' new> = ',sss
!        end do
!       end do
! here we have to introduce the SVD ?

        call cpustamp(cpu0,wall0,cpu0,wall0,'expd')

END_PROVIDER
