#!/usr/bin/env bats

source $QP_ROOT/tests/bats/common.bats.sh
source $QP_ROOT/quantum_package.rc


function run() {
  thresh=1.e-8
  test_exe change_ezfio || skip
  test_exe extract_basis_and_mos || skip
  qp set_file $1.ezfio
  qp edit --check
  qp run extract_basis_and_mos
  qp_create_ezfio -b $2 -o $1.2nd_ezfio $4
  qp set_file $1.2nd_ezfio
  qp_run change_ezfio $1.2nd_ezfio
  qp_run scf  $1.2nd_ezfio
  energy="$(ezfio get hartree_fock energy)"
  eq $energy $3 $thresh
  rm -rf $1.2nd_ezfio Basis_and_MOs.dat
}


@test "B-B" { # 3s
  run b2_stretched cc-pvdz -49.0127990112 b2_stretched.zmt
}

@test "N2" { # 8.648100 13.754s
  run n2 6-31g -108.8677689260 n2.xyz
}

