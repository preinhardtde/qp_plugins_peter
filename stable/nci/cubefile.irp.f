! -*- F90 -*-
BEGIN_PROVIDER [character*1, cnumstr, (0:9)]
      CNUMSTR(0)='0'
      CNUMSTR(1)='1'
      CNUMSTR(2)='2'
      CNUMSTR(3)='3'
      CNUMSTR(4)='4'
      CNUMSTR(5)='5'
      CNUMSTR(6)='6'
      CNUMSTR(7)='7'
      CNUMSTR(8)='8'
      CNUMSTR(9)='9'
END_PROVIDER

BEGIN_PROVIDER [integer, NPMX]
      NPMX=1000
END_PROVIDER

 BEGIN_PROVIDER [real*8, PPMDATA, (NPMX,mo_num)]
&BEGIN_PROVIDER [real*8, orbmax_ppm, (mo_num)]
&BEGIN_PROVIDER [real*8, orbmin_ppm, (mo_num)]
&BEGIN_PROVIDER [real*8, cubuf_ppm, (6,mo_num)
&BEGIN_PROVIDER [integer, IRGB_ppm, (15,mo_num)]
&BEGIN_PROVIDER [integer, indx_ppm, (mo_num)]
&BEGIN_PROVIDER [integer, idtot_ppm, (mo_num)]

 orbmax_ppm=0.D0
 orbmin_ppm=0.D0

END_PROVIDER

      subroutine cubeopen
        implicit none
        real*8 :: zero
        integer :: iiorb,iorb,iunitm,i100,i10,i1,j,iat,i

        zero=0.d0
        iiorb=0
        do iorb=1,mo_num
         if (lorbplt(iorb)) then
          iiorb=iiorb+1
          iunitm=73+iiorb
          i100=iorb/100
          i10=(iorb-100*i100)/10
          i1=iorb-100*i100-10*i10
          open(unit=iunitm  &
               ,file='orb_'//cnumstr(i100)//cnumstr(i10)//cnumstr(i1)//'.cube' &
               ,status='unknown',form='formatted')
          write(iunitm,*) 'orb_'//cnumstr(i100)//cnumstr(i10)//cnumstr(i1)//'.cube'
          write(iunitm,*) 'created by gradrho/ortho (p. reinhardt) '
          write(iunitm,'(i5,3f12.6)') nucl_num,(xstart(i),i=1,3)
          write(iunitm,'(i5,3f12.6)') npoint,(xend(1)-xstart(1))   &
               /dble(npoint),zero,zero
          write(iunitm,'(i5,3f12.6)') npoint,zero,(xthird(2)-xend(2))/dble(npoint),zero
          write(iunitm,'(i5,3f12.6)') npoint,zero,zero,(xfourth(3)-xthird(3))/dble(npoint)
          do iat=1,nucl_num
           write(iunitm,'(i5,4f12.5)') nint(nucl_charge(iat)),nucl_charge(iat),(nucl_coord(iat,j),j=1,3)
          end do
          indx_ppm(iiorb)=1
          idtot_ppm(iiorb)=0
         end if
        end do
        
      end subroutine cubeopen
      
      subroutine cubeclose
        implicit none
        integer :: iiorb,iorb,i
        
        iiorb=0
        do iorb=1,mo_num
         if (lorbplt(iorb)) then
          iiorb=iiorb+1
          if (indx_ppm(iiorb).ne.1) then
           idtot_ppm(iiorb)=idtot_ppm(iiorb)+indx_ppm(iiorb)-1
           write(73+iiorb,9904) (cubuf_ppm(i,iiorb),i=1,indx_ppm(iiorb)-1)
          end if
          close(73+iiorb)
          write(6,*) ' orbital no ',iorb,': wrote ',idtot_ppm(iiorb)   &
               ,' points on the file '
         end if
        end do
9904    format(6(1pe13.5))
        write(6,*)
        write(6,*) ' orbitals dumped as cube files orb_??.cube'
        write(6,*)
      end subroutine cubeclose
 
      subroutine cubeadd(iiorb,f)
        implicit none
        integer :: iiorb,ii
        real*8 :: f
        
        cubuf_ppm(indx_ppm(iiorb),iiorb)=f
        if (indx_ppm(iiorb).eq.6) then
         write(73+iiorb,9904) (cubuf_ppm(ii,iiorb),ii=1,6)
         idtot_ppm(iiorb)=idtot_ppm(iiorb)+6
         indx_ppm(iiorb)=1
        else
         indx_ppm(iiorb)=indx_ppm(iiorb)+1
        end if
9904    format(6(1pe13.5))
        
      end subroutine cubeadd
      
      subroutine cubewrite
        implicit none
        real*8 :: val
        integer :: iiorb,i
        
! we write only one line of the data
        
        do iiorb=1,norplt
         do i=1,npoint
          val=ppmdata(i,iiorb)
          call cubeadd(iiorb,val)
         end do
        end do
        
      end subroutine cubewrite


