! -*- F90 -*-
program nci
  implicit none
  BEGIN_DOC
! TODO : Put the documentation of the program here
  END_DOC

real*8 :: cpu0,wall0
          call cpu_time(cpu0)
          call wall_time(wall0)
          call header
          call driver
          call cpustamp(cpu0,wall0,cpu0,wall0,'All ')
end program nci

     subroutine header
       write(6,*)
       WRITE(6,*) ' tracing the Non-covalent interaction index '
       WRITE(6,*) '  of J Contreras and E Johnson '
       write(6,*) '  P Reinhardt, Paris, june 2020 '
       write(6,*) ' in confinement we trust '
       write(6,*)
     end subroutine header
     
     subroutine driver
       implicit none
       integer :: iat,ipoint,i,jpoint,kpoint,k,l
       real*8 :: xnono,u_dot_v,rho,gradrho(3),xpos(3)
       real*8 :: xlambda,ylambda,zlambda,pi,snci,xl2
       integer :: jend,kend
       
       pi=2.D0*acos(0.D0)
       
       OPEN(UNIT=16,FILE='nci.data',FORM='FORMATTED',STATUS='UNKNOWN')
       WRITE(16,9917)
9915   FORMAT(3I7,3F9.5,2E22.12)
9917   FORMAT('#      I        X        Y         Z         RHO  ' &
            ,'    NCI    ')
       
       IF (lplane.or.lcube) then
        jend=npoint-1
       else
        jend=0
       end if
       if (lcube) then
        kend=npoint-1
       else
        kend=0
       end if
       
       DO IPOINT=0,NPOINT-1
        IF (IPOINT.EQ.0) THEN
         XLAMBDA=0.D0
        ELSE
         XLAMBDA=DBLE(IPOINT)/DBLE(NPOINT-1)
        END IF
        
        DO JPOINT=0,JEND
         IF (JPOINT.EQ.0) THEN
          YLAMBDA=0.D0
         ELSE
          YLAMBDA=DBLE(JPOINT)/DBLE(NPOINT-1)
         END IF
         
         DO KPOINT=0,KEND
          IF (KPOINT.EQ.0) THEN
           ZLAMBDA=0.D0
          ELSE
           ZLAMBDA=DBLE(KPOINT)/DBLE(NPOINT-1)
          END IF
          
          DO I=1,3
           XPOS(I)=XLAMBDA*(XEND(I)-xstart(i))+YLAMBDA*(XTHIRD(I) &
                -xend(i))+ZLAMBDA*(Xfourth(I)-xthird(i))+XSTART(I)
          END DO
          if (bavard) WRITE(6,9902) IPOINT,JPOINT,KPOINT,(XPOS(I),I=1,3 &
               ),xlambda,ylambda,zlambda
          
          CALL EVALRHOhess(XPOS,RHO,GRADRHO,xl2)
          SNCI=0.D0
          DO I=1,3
           SNCI=SNCI+GRADRHO(I)*GRADRHO(I)
          END DO
          SNCI = SQRT(SNCI) / RHO**(4.D0/3.D0)
          SNCI=SNCI/(2.D0*(3.D0*PI*PI)**(1.D0/3.D0))
          WRITE(16,9915) IPOINT,JPOINT,KPOINT,(XPOS(I),I=1,3) &
               ,sign(rho,xl2),SNCI
         END DO
         if (bavard) write(6,*)
        END DO
        write(16,*)
       END DO
       write(6,*) ' we should have ',NPOINT,' X ',JEND+1,' X '     &
            ,KEND+1, ' points'
       CLOSE(16)
9975   FORMAT(3I7,3F9.5,3E22.12)
9902   FORMAT('  point No ',3I5,'  pos = (',3F15.5,'), ',3F6.3)
9905   FORMAT(3I7,3F9.5,2F20.5,2F15.5,F20.5,2F15.5,F15.5)
       
     end subroutine driver


