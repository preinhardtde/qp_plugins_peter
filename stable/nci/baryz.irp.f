! -*- F90 -*-
BEGIN_PROVIDER [real*8, baryz, (3)]
BEGIN_DOC
! the trivial, non-mass-weighted barycentre of the molecule
END_DOC
      implicit none
      integer :: i,iat
      baryz=0.D0

      do iat=1,nucl_num
       do i=1,3
        baryz(i)+=+nucl_coord(iat,i)
       end do
      end do
      do i=1,3
       baryz(i)*=1.D0/dble(nucl_num)
      end do
 
END_PROVIDER
