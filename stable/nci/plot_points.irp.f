! -*- F90 -*-
 BEGIN_PROVIDER [logical, lcube]
&BEGIN_PROVIDER [logical, lplane]
    implicit none
    integer :: dim_grid

provide nucl_coord
       
    lcube=.false.
    lplane=.false.
 100 continue
    write(6,*) ' please give the dimension of the grid  (1, 2, 3)' 
    read(5,*,err=100) dim_grid
    if (dim_grid.eq.3) then
     lcube=.true.
    else 
     if (dim_grid.eq.2) then
      lplane=.true.
     else
      if (dim_grid.ne.1) then 
       write(6,*) ' neither 1, 2  or 3, what did you mean ? '
       stop ' impossible grid dimension '
      end if
     end if
    end if
END_PROVIDER

BEGIN_PROVIDER  [integer, npoint]
    implicit none
    write(6,*) ' please give the number of points per segment '
    read(5,*) npoint
END_PROVIDER

 BEGIN_PROVIDER [real*8, xstart, (3)]
&BEGIN_PROVIDER [real*8, xend, (3)]
&BEGIN_PROVIDER [real*8, xthird, (3)]
&BEGIN_PROVIDER [real*8, xfourth, (3)]
BEGIN_DOC
! points defining the borders of the grid
END_DOC
    implicit none
    integer :: i
    real*8 :: dx,dy,dz
double precision, parameter    :: a0= 0.529177249d0
       write(6,*) ' units are Angstrom '
       if (lcube) then
        write(6,*) ' Defining a 3D lattice of grid points '
       else 
        if (lplane) then
         write(6,*) ' Defining a 2D lattice of grid points '
        else 
         write(6,*) ' Defining a linear segment of points '
        end if
       end if
        
! the starting point
       write(6,*) ' please give the starting point of the grid (x0,y0,z0) '
       READ(5,*) (XSTART(I),I=1,3)
       if (lcube) then
        write(6,*) ' please give the opposite corder of the 3D grid (x1,y1,z1)'
        READ(5,*) (XEND(I),I=1,3)
       else 
        if (lplane) then
         write(6,*) ' please give the two other points defining the 2D sheet' 
         write(6,*) ' inside (x0,y0,z0) - (x1,y1,z1) - (x2,y2,z2)' 
         write(6,*) ' (x1,y1,z1) and (x2,y2,z2)' 
         READ(5,*) (XEND(I),I=1,3)
         READ(5,*) (Xthird(I),I=1,3)
        else 
         write(6,*) ' please give the end point of the segment (x1,y1,z1)'
         READ(5,*) (XEND(I),I=1,3)
        end if
       end if

      if (lcube) then
       WRITE(6,*)
       WRITE(6,*) ' from the starting point and the dimensions we generated '
       WRITE(6,*) ' the four points defining the cube : '
       WRITE(6,*) ' (x0,y0,z0), (x1,y0,z0), (x0,y1,z0), (x0,y0,z1) '
       DX=XEND(1)
       DY=XEND(2)
       DZ=XEND(3)
       XEND(1)=xstart(1)+dx
       XEND(2)=xstart(2)
       XEND(3)=xstart(3)

       Xthird(1)=xend(1)
       Xthird(2)=xend(2)+dy
       Xthird(3)=xend(3)

       Xfourth(1)=xthird(1)
       Xfourth(2)=xthird(2)
       Xfourth(3)=xthird(3)+dz

       WRITE(6,'(3F20.12)') (XSTART(I),I=1,3)
       WRITE(6,'(3F20.12)') (XEND(I),I=1,3)
       WRITE(6,'(3F20.12)') (XTHIRD(I),I=1,3)
       WRITE(6,'(3F20.12)') (Xfourth(I),I=1,3)
       WRITE(6,*) ' the number of points per segment ',NPOINT
       WRITE(6,*) ' we will write a .cube file as well for each orbital ',NPOINT
       WRITE(6,*)
      else
       if (lplane) then
        WRITE(6,*)
        WRITE(6,*) ' the three points defining the plane : '
        WRITE(6,'(3F20.12)') (XSTART(I),I=1,3)
        WRITE(6,'(3F20.12)') (XEND(I),I=1,3)
        WRITE(6,'(3F20.12)') (XTHIRD(I),I=1,3)
        WRITE(6,*) ' the number of points per segment ',NPOINT
        WRITE(6,*)
       else
        WRITE(6,*)
        WRITE(6,*) ' the starting point of the calculation (a.u.) '
        WRITE(6,'(3F20.12)') (XSTART(I),I=1,3)
        WRITE(6,*) ' the end point of the calculation (a.u.) '
        WRITE(6,'(3F20.12)') (XEND(I),I=1,3)
        WRITE(6,*) ' the number of points ',NPOINT
        WRITE(6,*)
       end if
      end if
      xstart *=1.D0/a0
      xend   *=1.D0/a0
      xthird *=1.D0/a0
      xfourth*=1.D0/a0

END_PROVIDER

BEGIN_PROVIDER [integer, norplt]
        write(6,*) ' how many orbitals to plot ?'
        read(5,*) norplt
END_PROVIDER

BEGIN_PROVIDER [integer, indxplt, (norplt)]
       implicit none
       integer :: i
       write(6,*) ' please give he number of the ',norplt,' orbiatls to plot '
       read(5,*) (indxplt(i),i=1,norplt)
END_PROVIDER

BEGIN_PROVIDER [logical, lorbplt, (mo_num)]
       implicit none
       integer :: i
       lorbplt=.false.

       do i=1,norplt
        if (.not.lorbplt(indxplt(i))) then
         lorbplt(indxplt(i))=.true.
        else
         stop ' orbital to be plotted indicated twice '
        end if
       end do
END_PROVIDER
      
 BEGIN_PROVIDER [real*8, orbdata, (norplt)]

END_PROVIDER
