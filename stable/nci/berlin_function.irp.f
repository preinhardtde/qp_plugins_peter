! -*- F90 -*-
      real*8 function fberlin(r)
BEGIN_DOC
! generalized Berlin function for n atoms
END_DOC
      implicit none
      real*8 :: vec1(3),vec2(3),vec3(3),r(3),u_dot_v

      fBERlin=0.D0
! the virial of the forces, leading to the generalized Berlin function
integer :: i,iat
real*8 :: rra,rb,rr
      do iat=1,nucl_num
       DO I=1,3
        VEC1(I)=r(I) - baryz(i)
        VEC2(I)=nucl_coord(iat,3) - baryz(i)
        VEC3(I)=vec1(i)-vec2(i)
       END DO

       RRA=u_dot_v(vec1,vec2,3)
       RB=SQRT(vec2(1)*vec2(1)+vec2(2)*vec2(2)+vec2(3)*vec2(3))
       RR=SQRT(vec3(1)*vec3(1)+vec3(2)*vec3(2)+vec3(3)*vec3(3))
       if (rr.le.1.d-8) then
        write(6,*) ' position exactly on atomic coordinate,' &
             ,' setting contribution to zero '
       else
        fBERlin-=DBLE(nucl_charge(iat))*(RRA-RB*RB)/RR/RR/RR
       end if
      end do

      end function fberlin
