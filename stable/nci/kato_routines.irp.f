! -*- F90 -*-
BEGIN_PROVIDER [real*8, kato, (nucl_num)]
END_PROVIDER

BEGIN_PROVIDER [integer, nbuck]
    nbuck=6
END_PROVIDER

BEGIN_PROVIDER [integer, nbuckp ]
BEGIN_DOC
! 
! number of summits of a Buckminster-Fuller refined icosahedron, n=6
!
END_DOC
    nbuckp=40962
END_PROVIDER

BEGIN_PROVIDER [real*8, kato_pos, (3,nbuckp)]
BEGIN_DOC
! positions to evaluate rho and grad rho around a center
END_DOC
       implicit none
       integer :: i,j,npbuck2,npbuck,jj,ibuck
       real*8 :: dist1i,distm,distp,distij,xnormv


       write(6,*) ' Buckminster-Fuller refinement of the icosahedron '
       npbuck=12

      do i=1,12
       do j=1,3
        kato_pos(j,i)=xikos(j,i)
       end do
      end do
!
! now the iterations on the table buckp
!
      do ibuck=1,nbuck

       distm=100000.d0
       do i=2,npbuck
        dist1i=distp(kato_pos(1,1),kato_pos(1,i))
        distm=min(distm,dist1i)
       end do

       npbuck2=npbuck

       do i=1,npbuck-1
        do j=i+1,npbuck
         distij=distp(kato_pos(1,i),kato_pos(1,j))
         if (abs(distij-distm).lt.1.d-6) then
! we have a new point
          npbuck2=npbuck2+1
          if (npbuck2.gt.nbuckp) then
           write(6,*) ' too many points '
           stop ' too many points '
          end if
          do jj=1,3
           kato_pos(jj,npbuck2)=0.5d0*(kato_pos(jj,i)+kato_pos(jj,j))
          end do
         end if
        end do
       end do
       npbuck=npbuck2
      end do
!
! get the points onto the unit sphere
!
   real*8 :: xdist
      do i=1,npbuck2
       xdist=xnormv(kato_pos(1,i),kato_pos(2,i),kato_pos(3,i))
       do jj=1,3
        kato_pos(jj,i)=kato_pos(jj,i)/xdist
       end do
      end do
 
      write(6,*) ' iteration ',ibuck,': we have now ',npbuck2,' points'

 9901 format(' point no ',i5,': ',3f20.12)

END_PROVIDER

BEGIN_PROVIDER [ real*8, radbuc]
          implicit none
          radbuc=0.01D0
END_PROVIDER

BEGIN_PROVIDER [real*8, xikos, (3,12)]
      implicit none
      real*8 :: s5,golinv
      integer :: i,j,ii

      xikos=0.D0
      s5=sqrt(5.D0)
      GOLINV=0.5D0*(-1.D0+S5)
!
! icosahedron
!
!  pA := [ 0,-s, 1 ];
!  pB := [ 0, s, 1 ];
!  pC := [ 1, 0, s ];
!  pD := [ 1, 0,-s ];
!  pE := [ s, 1, 0 ];
!  pF := [-s, 1, 0 ];
!  pG := [ 0, s,-1 ];
!  pH := [ 0,-s,-1 ];
!  pI := [-1, 0, s ];
!  pJ := [-1, 0,-s ];
!  pK := [ s,-1, 0 ];
!  pL := [-s,-1, 0 ];
!
      xikos(2,1)=-golinv
      xikos(3,1)= 1.d0

      xikos(2,2)= golinv
      xikos(3,2)= 1.d0

      xikos(2,3)=-golinv
      xikos(3,3)=-1.d0

      xikos(2,4)= golinv
      xikos(3,4)=-1.d0

      xikos(1,5)= 1.d0
      xikos(3,5)=-golinv

      xikos(1,6)= 1.d0
      xikos(3,6)= golinv

      xikos(1,7)=-1.d0
      xikos(3,7)=-golinv

      xikos(1,8)=-1.d0
      xikos(3,8)= golinv

      xikos(1,9)= golinv
      xikos(2,9)= 1.d0

      xikos(1,10)=-golinv
      xikos(2,10)= 1.d0

      xikos(1,11)= golinv
      xikos(2,11)=-1.d0

      xikos(1,12)=-golinv
      xikos(2,12)=-1.d0

!
! normalisation
!
 real*8 :: xiknrm,xnormv
      xiknrm=1.d0/xnormv(1.d0,golinv,0.d0)

      do i=1,3
       do j=1,12
        xikos(i,j)=xikos(i,j)*xiknrm
       end do
      end do

      write(6,*) ' platonic objects created: '

      write(6,*) ' icosahedron: '
      do j=1,12
       write(6,9901) j,(xikos(ii,j),ii=1,3)             &
            ,xnormv(xikos(1,j),xikos(2,j),xikos(3,j))
      end do
 9901 format(i3,': (',3f15.8,') norm = ',f12.8)

END_PROVIDER

     real*8 FUNCTION XNORMV(X,Y,Z)
      implicit none
      real*8 :: x,y,z
      XNORMV=SQRT(X*X+Y*Y+Z*Z)
     end FUNCTION XNORMV

     real*8 FUNCTION distp(a,b)
       implicit none
       real*8 :: a(3),b(3)

       distp=sqrt((a(1)-b(1))*(a(1)-b(1))  &
            +(a(2)-b(2))*(a(2)-b(2))+(a(3)-b(3))*(a(3)-b(3)))
     end FUNCTION distp



