! -*- F90 -*-
program plot_gradrho_over_rho
  implicit none
  BEGIN_DOC
! The Berlin function, calculated on a grid
! 
! 
  END_DOC
 read_wf = .True.
 touch read_wf
         
real*8 :: cpu0,wall0
          call cpu_time(cpu0)
          call wall_time(wall0)
          call header
          call driver
          call cpustamp(cpu0,wall0,cpu0,wall0,'All ')
end program plot_gradrho_over_rho

     subroutine header
       write(6,*)
       write(6,*) ' plotting grad rho / rho on a grid'
       write(6,*) '  P Reinhardt, Paris, june 2020 '
       write(6,*) ' in confinement we trust '
       write(6,*)
     end subroutine header

     subroutine driver
       implicit none
       integer :: iat,ipoint,i,jpoint,kpoint,k,l
       real*8 :: xnono,u_dot_v,rho,gradrho(3),xpos(3)
       real*8 :: xlambda,ylambda,zlambda
integer :: jend,kend

       write(6,*)
       OPEN(UNIT=16,FILE='grad_rho.data',FORM='FORMATTED',STATUS='UNKNOWN')
       WRITE(16,9906)
 9906  FORMAT('# I   X   Y    Z    RHO  ','    GRAD        GRAD/RHO     NORM')

       write(6,*) ' Writing file <gradrho.dat> '
       write(6,*) ' Starting point ',(xstart(i),i=1,3)

       IF (lplane.or.lcube) then
        jend=npoint-1
       else
        jend=0
       end if
       if (lcube) then
        kend=npoint-1
       else
        kend=0
       end if

     if (lcube) then
       WRITE(6,*)
       WRITE(6,*) ' the four points defining the cube : '
       WRITE(6,'(3F20.12)') (XSTART(I),I=1,3)
       WRITE(6,'(3F20.12)') (XEND(I),I=1,3)
       WRITE(6,'(3F20.12)') (XTHIRD(I),I=1,3)
       WRITE(6,'(3F20.12)') (Xfourth(I),I=1,3)
       WRITE(6,*) ' the number of points per segment ',NPOINT
       WRITE(6,*)
      else
       if (lplane) then
        WRITE(6,*)
        WRITE(6,*) ' the three points defining the plane : '
        WRITE(6,'(3F20.12)') (XSTART(I),I=1,3)
        WRITE(6,'(3F20.12)') (XEND(I),I=1,3)
        WRITE(6,'(3F20.12)') (XTHIRD(I),I=1,3)
        WRITE(6,*) ' the number of points per segment ',NPOINT
        WRITE(6,*)
       else
        WRITE(6,*)
        WRITE(6,*) ' the starting point of the calculation (a.u.) '
        WRITE(6,'(3F20.12)') (XSTART(I),I=1,3)
        WRITE(6,*) ' the end point of the calculation (a.u.) '
        WRITE(6,'(3F20.12)') (XEND(I),I=1,3)
        WRITE(6,*) ' the number of points ',NPOINT
        WRITE(6,*)
       end if
      end if


       DO IPOINT=0,NPOINT-1
        IF (IPOINT.EQ.0) THEN
         XLAMBDA=0.D0
        ELSE
         XLAMBDA=DBLE(IPOINT)/DBLE(NPOINT-1)
        END IF

        DO JPOINT=0,JEND
         IF (JPOINT.EQ.0) THEN
          YLAMBDA=0.D0
         ELSE
          YLAMBDA=DBLE(JPOINT)/DBLE(NPOINT-1)
         END IF

         DO KPOINT=0,KEND
          IF (KPOINT.EQ.0) THEN
           ZLAMBDA=0.D0
          ELSE
           ZLAMBDA=DBLE(KPOINT)/DBLE(NPOINT-1)
          END IF

          DO I=1,3
           XPOS(I)=XLAMBDA*(XEND(I)-xstart(i))+YLAMBDA*(XTHIRD(I) &
                -xend(i))+ZLAMBDA*(Xfourth(I)-xthird(i))+XSTART(I)
          END DO
          if (bavard) WRITE(6,9902) IPOINT,JPOINT,KPOINT,(XPOS(I),I=1,3 &
               ),xlambda,ylambda,zlambda
          CALL density_and_grad_at_r(xpos,rho, gradrho)
          xnono=u_dot_v(gradrho,gradrho,3)/rho
          WRITE(16,9905) IPOINT,JPOINT,KPOINT,(XPOS(I),I=1,3),RHO  &
                   ,(GRADRHO(K),K=1,3),(-0.5D0*GRADRHO(L)/rho,L=1,3)  &
                   ,XNONO
        END DO
        if (bavard) write(6,*)
        END DO
        write(16,*)
       END DO
       write(6,*) ' we should have ',NPOINT,' X ',JEND+1,' X '     &
            ,KEND+1, ' points'
       CLOSE(16)
 9975  FORMAT(3I7,3F9.5,3E22.12)
 9902  FORMAT('  point No ',3I5,'  pos = (',3F15.5,'), ',3F6.3)
 9905  FORMAT(3I7,3F9.5,2F20.5,2F15.5,F20.5,2F15.5,F15.5)

     end subroutine driver
     
