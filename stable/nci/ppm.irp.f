! -*- F90 -*-
     SUBROUTINE PPMOPEN
       implicit none
       integer :: iiorb,iorb,i100,i10,i1,izero,i255,ifile,i,iunitm
       real*8 :: xmarg,x,xextr
       character*3 :: orbstrg
! the matrix header
9901   FORMAT('P3',/,'# CREATOR: qp2 (P.Reinhardt)')
9902   FORMAT('255')
       
       iiorb=0
       xextr=0.D0
       do iorb=1,mo_num
        if (lorbplt(iorb)) then
         iiorb=iiorb+1
         IUNITM=73+iiorb
         I100=iorb/100
         I10=(iorb-100*i100)/10
         I1=iorb-100*i100-10*i10
         orbstrg=cnumstr(i100)//cnumstr(i10)//cnumstr(i1)
         OPEN(UNIT=IUNITM &
              ,FILE='orb_'//orbstrg//'.ppm',STATUS='UNKNOWN',FORM='FORMATTED')
         WRITE(IUNITM,9901)
         WRITE(IUNITM,'(2I8)') Npoint,Npoint+3
         WRITE(IUNITM,9902)
         WRITE(6,9903) iorb,-orbmax_ppm(iiorb),orbmax_ppm(iiorb)
         xextr=max(xextr,2.D0*orbmax_ppm(iiorb))
        end if
        end do
9903    FORMAT(' extremes for orbital No ',i3,' are ',2F20.12)
        write(6,*) ' colors go from red (negative values)'      &
             ,' to black (positive values)'
!
        IZERO=0
        I255=255
integer :: ii,jj,kk
        XMARG=XEXTR/256.D0
!
! the first 3 lines give the colour table
! we map -255 .. 1 0 1 .. 255 onto 1 .. N
        DO iorb=1,norplt
         indx_ppm(iorb)=1
         ifile=73+iorb
         DO I=0,Npoint-1
          X=2.D0*DBLE(I)/DBLE(Npoint-1)-1.D0
          CALL RGBMAP(X,II,JJ,KK)
          CALL RGBADD(iorb,II,JJ,KK)
         END DO
!
! we have to care for 1 line of blanks, and 1 black line
!
         I255=255
         DO I=1,Npoint
          CALL RGBADD(iorb,I255,I255,I255)
         END DO
         DO I=1,Npoint
          CALL RGBADD(iorb,IZERO,IZERO,IZERO)
         END DO
        END DO
        
      END SUBROUTINE PPMOPEN

      SUBROUTINE PPMWRITE
        implicit none
        integer :: iiorb,i,ii,jj,kk
        real*8 :: val
        
! we write only one line of the data
        
        DO IiORB=1,NOrplt
         DO I=1,Npoint
          VAL=ppmdata(i,iiorb)/orbmax_ppm(iiorb)
          CALL RGBMAP(VAL,II,JJ,KK)
          CALL RGBADD(iIORB,II,JJ,KK)
         END DO
        END DO
        
      END SUBROUTINE PPMWRITE
!
      SUBROUTINE RGBADD(iiorb,I,J,K)
        implicit none
        integer :: iiorb,ii,i,j,k
        
        IRGB_ppm(INDX_ppm(iiorb),iiorb)=I
        IRGB_ppm(INDX_ppm(iiorb)+1,iiorb)=J
        IRGB_ppm(INDX_ppm(iiorb)+2,iiorb)=K
        IF (INDX_ppm(iiorb).EQ.13) THEN
         WRITE(73+iiorb,9904) (IRGB_ppm(II,iiorb),II=1,15)
         INDX_ppm(iiorb)=1
        ELSE
         INDX_ppm(iiorb)=INDX_ppm(iiorb)+3
        END IF
9904    FORMAT(15I4)
        
      END SUBROUTINE RGBADD

      SUBROUTINE PPMCLOSE
        implicit none
        CHARACTER*3 :: ORBNUM
        integer :: iiorb,iorb,i100,i10,i1,i
        
        open(unit=72,file='gradrho.cmd',status='unknown',form='formatted')
        iiorb=0
        do iorb=1,mo_num
         if (lorbplt(iorb)) then
          iiorb=iiorb+1
          I100=iorb/100
          I10=(iorb-100*i100)/10
          I1=iorb-100*i100-10*i10
          orbnum=cnumstr(i100)//cnumstr(i10)//cnumstr(i1)
          IF (INDX_ppm(iiorb).NE.1) WRITE(73+iiorb,9904) (IRGB_ppm(I,iiorb) &
               ,I=1,INDX_ppm(iiorb)-1)
          close(73+iiorb)
          write(72,9901) orbnum,orbnum,orbnum
         end if
        end do
9904    FORMAT(15I4)
9901    FORMAT('convert orb_',a3,'.ppm orb_',a3,'.jpg ',/,' rm orb_',a3,'.ppm')
        WRITE(6,*)
        WRITE(6,*) ' Orbitals dumped as picture .ppm files '
        WRITE(6,*)
        close(72)
        call system('sh ./gradrho.cmd')
        open(unit=72,file='gradrho.cmd',status='old',form='formatted')
        close(72,status='delete')
        WRITE(6,*)
        WRITE(6,*)
        WRITE(6,*) ' Files converted from PPM to JPG '
        WRITE(6,*) ' deleted the .ppm files '
        WRITE(6,*)
        RETURN
      END SUBROUTINE ppmclose
      
      SUBROUTINE RGBMAP(X,I,J,K)
BEGIN_DOC
!
! mapping a real between -1 and 1 onto
! a RGB colour
!
! negative: red
! zero: white
! positive: black
!
END_DOC
       implicit none   
       real*8 :: x,xmarg
       integer :: i,j,k,iy
       IF (ABS(X).GT.1.D0) THEN
        WRITE(6,*) ' DO NOT KNOW HOW TO MAP A REAL OF ',X
        STOP ' RANGE -1 .. 1 EXCEEDED '
       END IF
!
       XMARG=1.D0/255.D0
       IF (ABS(X).LT.XMARG) THEN
        I=255
        J=255
        K=255
       ELSE IF (X.GE.XMARG) THEN
        IY=255.D0*X
        IY=255-IY
        I=IY
        J=IY
        K=IY
       ELSE
        IY=255.D0*(1.D0+X)
        I=255
        J=IY
        K=IY
       END IF
     END SUBROUTINE RGBmap

