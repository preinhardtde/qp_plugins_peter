! -*- F90 -*-
 subroutine density_and_grad_at_r(r,dm_ab, grad_dm_ab)
 implicit none
 BEGIN_DOC
! input:
!
! * r(1) ==> r(1) = x, r(2) = y, r(3) = z
!
! output:
!
! * dm_a = alpha density evaluated at r
! * dm_b = beta  density evaluated at r
! * grad_dm_a(1) = X gradient of the alpha density evaluated in r
! * grad_dm_a(1) = X gradient of the beta  density evaluated in r
!
 END_DOC
       double precision, intent(in)  :: r(3)
       double precision :: dm_a,dm_b
       double precision :: grad_dm_a(3),grad_dm_b(3)
       integer :: i,j,istate
       double precision  :: aos_array(ao_num) &
            ,aos_array_bis(ao_num),u_dot_v
       double precision  :: aos_grad_array(ao_num,3) &
            , aos_grad_array_bis(ao_num,3)
       real*8 :: dm_ab,grad_dm_ab(3)
       real*8 :: grad_aos_array(3,ao_num)

       call give_all_aos_and_grad_at_r(r,aos_array,grad_aos_array)
       do i = 1, ao_num
        do j = 1, 3
         aos_grad_array(i,j) =  grad_aos_array(j,i)
        end do
       end do

        istate = 1
         ! alpha density
         ! aos_array_bis = \rho_ao * aos_array
        call dsymv('U',ao_num,1.d0,one_e_dm_alpha_ao_for_dft(1,1,istate) &
             ,size(one_e_dm_alpha_ao_for_dft,1),aos_array,1 &
             ,0.d0,aos_array_bis,1)
        dm_a = u_dot_v(aos_array,aos_array_bis,ao_num)
        
! grad_dm(1) = \sum_i aos_grad_array(i,1) * aos_array_bis(i)
        grad_dm_a(1) = u_dot_v(aos_grad_array(1,1),aos_array_bis,ao_num)
        grad_dm_a(2) = u_dot_v(aos_grad_array(1,2),aos_array_bis,ao_num)
        grad_dm_a(3) = u_dot_v(aos_grad_array(1,3),aos_array_bis,ao_num)
! aos_grad_array_bis = \rho_ao * aos_grad_array
        
! beta density
        call dsymv('U',ao_num,1.d0,one_e_dm_beta_ao_for_dft(1,1,istate) &
             ,size(one_e_dm_beta_ao_for_dft,1) &
             ,aos_array,1,0.d0,aos_array_bis,1)
        dm_b = u_dot_v(aos_array,aos_array_bis,ao_num)
        
! grad_dm(1) = \sum_i aos_grad_array(i,1) * aos_array_bis(i)
        grad_dm_b(1) = u_dot_v(aos_grad_array(1,1),aos_array_bis,ao_num)
        grad_dm_b(2) = u_dot_v(aos_grad_array(1,2),aos_array_bis,ao_num)
        grad_dm_b(3) = u_dot_v(aos_grad_array(1,3),aos_array_bis,ao_num)
! aos_grad_array_bis = \rho_ao * aos_grad_array
        
        grad_dm_a *= 2.d0
        grad_dm_b *= 2.d0
        
        dm_ab=dm_a+dm_b
        grad_dm_ab=grad_dm_a+grad_dm_b
      end subroutine density_and_grad_at_r
      
      SUBROUTINE EVALRHOHess(XPOS,RHO,GRADRHO,xlambda2)
        implicit none    
        integer :: i
        
        real*8 ::  XPOS(3),GRADRHO(3),rho,xlambda2
        real*8, allocatable :: eigvechess(:,:),hess(:,:),xptmp(:),gradtmp(:)
        real*8 :: delta,rho2
        allocate (eigvechess(3,3))
        allocate (hess(3,3))
        allocate (xptmp(3))
        allocate (gradtmp(3))
        
        delta=1.0D-4
        
! first the central point
        CALL density_and_grad_at_r(xpos,rho,gradrho) 
        
! displace in x
        do i=1,3
         xptmp(i)=xpos(i)
        end do
        xptmp(1)=xptmp(1)+delta
        CALL density_and_grad_at_r(XPtmp,rho2,gradtmp)
! we can calculate d2/dx2, d2/dydx, d2/dzdz
        hess(1,1)=(gradtmp(1)-gradrho(1))/delta
        hess(1,2)=(gradtmp(2)-gradrho(2))/delta
        hess(1,3)=(gradtmp(3)-gradrho(3))/delta
        
! displace in y
        do i=1,3
         xptmp(i)=xpos(i)
        end do
        xptmp(2)=xptmp(2)+delta
        CALL density_and_grad_at_r(XPtmp,rho2,gradtmp)
! we calculate d2/dy2, d2/dydz
! d2/dydx as average of d2/dxdy and d2/dydx
        hess(2,2)=(gradtmp(2)-gradrho(2))/delta
        hess(2,3)=(gradtmp(3)-gradrho(3))/delta
        hess(2,1)=0.5D0*(hess(1,2)+(gradtmp(1)-gradrho(1))/delta)
        hess(1,2)=hess(2,1)

! displace in z
        do i=1,3
         xptmp(i)=xpos(i)
        end do
        xptmp(3)=xptmp(3)+delta
        CALL density_and_grad_at_r(XPtmp,rho2,gradtmp)
! we calculate d2/dz2
        hess(3,3)=(gradtmp(3)-gradrho(3))/delta
        hess(3,1)=0.5D0*(hess(1,3)+(gradtmp(1)-gradrho(1))/delta)
        hess(1,3)=hess(3,1)
        hess(3,2)=0.5D0*(hess(2,3)+(gradtmp(2)-gradrho(2))/delta)
        hess(2,3)=hess(3,2)
        
! get 2nd eigenvalue
        call lapack_diag(gradtmp,eigvechess,hess,3,3)
        xlambda2=gradtmp(2)
        
        deallocate (eigvechess)
        deallocate (hess)
        deallocate (xptmp)
        deallocate (gradtmp)

      END SUBROUTINE EVALRHOHess

