#!/usr/bin/env bats

source $QP_ROOT/tests/bats/common.bats.sh
source $QP_ROOT/quantum_package.rc


function run() {
  thresh=1.e-6
  test_exe nci || skip
  test_exe berlin || skip
  test_exe kato || skip
  test_exe plot_orb || skip
  test_exe plot_gradrho_over_rho || skip
  echo 200 > input.tmp
  echo 2 >> input.tmp
  echo '0. 0. 0. ' >> input.tmp
  echo '0. 0. 1. ' >> input.tmp
  echo '0. 1. 1. ' >> input.tmp
  qp set_file $1.ezfio
  qp edit --check
  qp run nci < input.tmp
  data="$(awk 'NR == 3 {print $7 }' nci.data)"
  echo $data $2 > $1.nci.log
  echo ok >> $1.nci.log
  eq $data $2 $thresh
  data="$(awk 'NR == 3 {print $8 }' nci.data)"
  echo $data $3 >> $1.nci.log
  echo ok >> $1.nci.log
  eq $data $3 $thresh

  qp run berlin < input.tmp
  data="$(awk 'NR == 3 {print $8 }' Berlin.data)"
  echo $data $4 >> $1.nci.log
  echo ok >> $1.nci.log
  eq $data $4 $thresh

  qp_run kato $1.ezfio > $1.kato.log
  data="$(grep spherical $1.kato.log | tail -n 1 | awk '{print $8}')"
  echo $data $5 >> $1.nci.log
  echo ok >> $1.nci.log
  eq $data $5 0.0001

  rm nci.data input.tmp Berlin.data 

  
}


@test "N2" { # 8.648100 13.754s
  run n2 0.480037056704E+00    0.295814958195E-01  0.779389297729E+01  0.05903
}

@test "B-B" { # 3s
  run b2_stretched -0.303952496313E+01    0.758021791909E+00   0.424760560320E+00  0.19724
}

