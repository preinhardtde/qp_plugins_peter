! -*- F90 -*-
program plot_orb
  implicit none
  BEGIN_DOC
! plotting orbitals, calculated on a grid
! 
! 
  END_DOC
 read_wf = .True.
 touch read_wf
         
real*8 :: cpu0,wall0
          call cpu_time(cpu0)
          call wall_time(wall0)
          call header
          call driver
          call cpustamp(cpu0,wall0,cpu0,wall0,'All ')
end program plot_orb 

     subroutine header
       write(6,*)
       write(6,*) ' a tool for plotting molecular orbitals '
       write(6,*) '  P Reinhardt, Paris, june 2020 '
       write(6,*)
     end subroutine header

     subroutine driver
       implicit none
       integer :: iat,i,j,k,jdum,iorb,idum,kdum,iiorb
       integer :: ipoint,jpoint,kpoint,jend,kend
       real*8 :: xlambda,ylambda,zlambda,xpos(3),mos_array(mo_num),xdum

       write(6,*)
        OPEN(UNIT=16,FILE='orb.data',FORM='FORMATTED',STATUS='UNKNOWN')
        WRITE(16,9926)
 9926  FORMAT('# I   X   Y    Z    orbitals  ')

       IF (lplane.or.lcube) then
        jend=npoint-1
       else
        jend=0
       end if

       if (lcube) then
        kend=npoint-1
       else
        kend=0
       end if

       DO IPOINT=0,NPOINT-1
        IF (IPOINT.EQ.0) THEN
         XLAMBDA=0.D0
        ELSE
         XLAMBDA=DBLE(IPOINT)/DBLE(NPOINT-1)
        END IF

        DO JPOINT=0,JEND
         IF (JPOINT.EQ.0) THEN
          YLAMBDA=0.D0
         ELSE
          YLAMBDA=DBLE(JPOINT)/DBLE(NPOINT-1)
         END IF

         DO KPOINT=0,KEND
          IF (KPOINT.EQ.0) THEN
           ZLAMBDA=0.D0
          ELSE
           ZLAMBDA=DBLE(KPOINT)/DBLE(NPOINT-1)
          END IF

          DO I=1,3
           XPOS(I)=XLAMBDA*(XEND(I)-xstart(i))+YLAMBDA*(XTHIRD(I) &
                -xend(i))+ZLAMBDA*(Xfourth(I)-xthird(i))+XSTART(I)
          END DO
          if (bavard) WRITE(6,9902) IPOINT,JPOINT,KPOINT,(XPOS(I),I=1,3 &
               ),xlambda,ylambda,zlambda
          call give_all_mos_at_r(xpos,mos_array)
          iiorb=0
          do iorb=1,mo_num
real*8 :: modata
           modata=mos_array(iorb)
           if (lorbplt(iorb)) then
            iiorb+=1
            orbdata(iiorb)=modata
            orbmin_ppm(iiorb)=min(modata,orbmin_ppm(iiorb))
            orbmax_ppm(iiorb)=max(modata,orbmax_ppm(iiorb))
           end if
          end do
 
           WRITE(16,9925) IPOINT,JPOINT,KPOINT,(XPOS(I),I=1,3)  &
                ,(ORBDATA(K),K=1,NORPLT)
 9902  FORMAT('  point No ',3I5,'  pos = (',3F15.5,'), ',3F6.3)
 9925      FORMAT(3I4,50F12.5)
! loop over the orbitals to plot
        END DO
        if (bavard) write(6,*)
        END DO
        write(16,*)
       END DO
       write(6,*) ' we should have ',NPOINT,' X ',JEND+1,' X '     &
            ,KEND+1, ' points'
       CLOSE(16)
 9975  FORMAT(3I7,3F9.5,3E22.12)
!         iiorb=0
!         do iorb=1,mo_num
!          if (lorbplt(iorb)) then
!           iiorb+=1
!           write(6,*) ' values for orbital No ',iorb,' ranging from ' &
!             ,orbmin_ppm(iiorb),' to ',orbmax_ppm(iiorb)
!          end if
!         end do
 

! for the ppm files we reread the orb.data file and dump the data
! for each orbital, if we have the data for a plane

       if (lplane) then
         iiorb=0
         do iorb=1,mo_num
          if (lorbplt(iorb)) then
! take care of rounding errors ...
           iiorb=iiorb+1
           orbmax_ppm(iiorb)=max(orbmax_ppm(iiorb),-orbmin_ppm(iiorb))+1.D-5
          end if
         end do
         call ppmopen
! encode the PPM files
         OPEN(UNIT=16,FILE='orb.data',FORM='FORMATTED',STATUS='OLD')
         read(16,*)
         DO I=1,NPOINT
! read one line
          DO J=1,NPOINT
           READ(16,*) idum,jdum,kdum,xdum,xdum,xdum,(orbdata(iiorb) &
                ,iiorb=1,norplt)
           do iiorb=1,norplt
            PPMDATA(J,iIORB)=ORBDATA(iIORB)
            if (orbdata(iiorb).gt.orbmax_ppm(iiorb)) then
             write(6,*) ' how did this happen ? '
             write(6,*) iiorb,i,j,idum,jdum,orbdata(iiorb),orbmax_ppm(iiorb)
            end if
           end do
          end do
! dump the line
          CALL PPMWRITE
         end do
         call ppmclose
         close(16)
       end if

!   we do the same for the cube, if the data has been generated for a
! cube
       if (lcube) then
        call cubeopen
         OPEN(UNIT=16,FILE='orb.data',FORM='FORMATTED',STATUS='OLD')
         read(16,*)
         DO I=1,NPOINT
! read one line
          DO J=1,NPOINT
           DO K=1,NPOINT
            READ(16,*) idum,jdum,kdum,xdum,xdum,xdum,(orbdata(iiorb) &
                 ,iiorb=1,norplt)
            do iiorb=1,norplt
             PPMDATA(K,iIORB)=ORBDATA(iIORB)
            end do
           end do
! dump the line
           CALL cubeWRITE
          end do
         end do
         call cubeclose
         close(16)
       end if

     end subroutine driver
     
