! -*- F90 -*-
program katoprog
  implicit none
  BEGIN_DOC
! determination of Kato s condition around each atom
! we read a wavefunction, create density and grad density
! on a grid of points
  END_DOC
real*8 :: cpu0,wall0
 read_wf = .True.
 touch read_wf

          call cpu_time(cpu0)
          call wall_time(wall0)
         
          call header
          call driver
          call cpustamp(cpu0,wall0,cpu0,wall0,'KATO')
end program katoprog 

     subroutine header
       write(6,*)
       write(6,*) ' Kato''s condition, a first exercise toward a DFT program'
       write(6,*) '  P Reinhardt, Paris, may 2020 '
       write(6,*) ' in confinement we trust '
       write(6,*)
     end subroutine header

     subroutine driver
       implicit none
       integer :: iat,ipoint,i
       real*8 :: sphav,xnono,xnormv,rho,gradrho(3),xpos(3)
!
! we draw the Buckminster-Fuller construction around each atom
!
 integer :: ir
      do ir=0,6
       radbuc=10**(-dble(ir))
       write(6,*) ' Calculation for radbuc = ',radbuc,' bohr '
       DO IAT=1,nucl_num
        WRITE(6,*) ' Kato''s condition around atom No ',IAT
        WRITE(6,*) ' number of points around the atom = ',nbuckp
        SPHAV=0.D0
        DO IPOINT=1,nbuckp
         DO I=1,3
          XPOS(I)=nucl_coord(iat,I)+RADBUC*kato_pos(I,IPOINT)
         END DO
         CALL density_and_grad_at_r(XPOS,RHO,GRADRHO)
         XNONO=0.5D0*XNORMV(GRADRHO(1),GRADRHO(2),GRADRHO(3))/RHO
         SPHAV=SPHAV+XNONO
        END DO
        SPHAV=SPHAV/DBLE(NBUCKp)
        WRITE(6,9101) SPHAV
 9101   FORMAT('  we find for the Kato spherical average ',F15.5)
        WRITE(6,*)
       end do
       write(6,*) ' ------------------------------------- '
       write(6,*) ' ------------------------------------- '
       write(6,*) 
      end do

     end subroutine driver
     
