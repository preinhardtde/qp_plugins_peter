! -*- F90 -*-
program katoprog
  implicit none
  BEGIN_DOC
! The Berlin function, calculated on a grid
! 
! 
  END_DOC
 read_wf = .True.
 touch read_wf
         
real*8 :: cpu0,wall0
          call cpu_time(cpu0)
          call wall_time(wall0)
          call header
          call driver
          call cpustamp(cpu0,wall0,cpu0,wall0,'All ')
end program katoprog 

     subroutine header
       write(6,*)
       write(6,*) ' calculating the Berlin index (T. Berlin, JCP, 19 (1951) 208)'
       write(6,*) '  P Reinhardt, Paris, june 2020 '
       write(6,*) ' in confinement we trust '
       write(6,*)
     end subroutine header

     subroutine driver
       implicit none
       integer :: iat,i
       integer :: ipoint,jpoint,kpoint,jend,kend
       real*8 :: sphav,xnono,xnormv,rho,gradrho(3),xpos(3)
       real*8 :: xlambda,ylambda,zlambda
       real*8 :: dm_a,dm_b,ber,fberlin

       write(6,*)
       write(6,*) ' calculating the generalized Berlin function '
       write(6,*) ' corresponding to the virial of the atomic forces '
       write(6,*) ' divide by the internuclear distance R in '
       write(6,*) '   the diatomic case for the original Berlin function'
       write(6,*)
       OPEN(UNIT=16,FILE='Berlin.data',FORM='FORMATTED',STATUS='UNKNOWN')
       WRITE(16,9918)
 9918  FORMAT('#      I        X        Y         Z         RHO  ' &
            ,'    F      F*RHO  ')
       IF (lplane.or.lcube) then
        jend=npoint-1
       else
        jend=0
       end if
       if (lcube) then
        kend=npoint-1
       else
        kend=0
       end if

       DO IPOINT=0,NPOINT-1
        IF (IPOINT.EQ.0) THEN
         XLAMBDA=0.D0
        ELSE
         XLAMBDA=DBLE(IPOINT)/DBLE(NPOINT-1)
        END IF

        DO JPOINT=0,JEND
         IF (JPOINT.EQ.0) THEN
          YLAMBDA=0.D0
         ELSE
          YLAMBDA=DBLE(JPOINT)/DBLE(NPOINT-1)
         END IF

         DO KPOINT=0,KEND
          IF (KPOINT.EQ.0) THEN
           ZLAMBDA=0.D0
          ELSE
           ZLAMBDA=DBLE(KPOINT)/DBLE(NPOINT-1)
          END IF

          DO I=1,3
           XPOS(I)=XLAMBDA*(XEND(I)-xstart(i))+YLAMBDA*(XTHIRD(I) &
                -xend(i))+ZLAMBDA*(Xfourth(I)-xthird(i))+XSTART(I)
          END DO
          if (bavard) WRITE(6,9902) IPOINT,JPOINT,KPOINT,(XPOS(I),I=1,3 &
               ),xlambda,ylambda,zlambda
          CALL dm_dft_alpha_beta_at_r(xpos,dm_a,dm_b)
          rho=dm_a+dm_b
          BER=fberlin(xpos)
          WRITE(16,9975) IPOINT,JPOINT,KPOINT,(XPOS(I),I=1,3)     &
                 ,rho,ber,ber*rho
        END DO
        if (bavard) write(6,*)
        END DO
        write(16,*)
       END DO
       write(6,*) ' we should have ',NPOINT,' X ',JEND+1,' X '     &
            ,KEND+1, ' points'
       CLOSE(16)
 9902  FORMAT('  point No ',3I5,'  pos = (',3F15.5,'), ',3F6.3)
 9975  FORMAT(3I7,3F9.5,3E22.12)

     end subroutine driver
     
