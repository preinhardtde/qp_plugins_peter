===
nci
===

| The NCI in the quantum package  

We need a box, and increments in the three directions. We calculate all 
on the gridpoints via the subroutine 
density_and_grad_alpha_beta_and_all_aos_and_grad_aos_at_r.

- berlin: plot the geometrical factor of the integrand of the Hellmann-Feynman forces, called the Berlin function  
- kato: evaluate Kato's condition
- nci: evaluate the NCI (non-covalent index) function 
- plot_gradrho_over_rho: ingredient for Kato's function
- plot_orb: tool for plotting molecullar orbitals as JPG images

